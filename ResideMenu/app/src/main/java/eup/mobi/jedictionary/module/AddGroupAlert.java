package eup.mobi.jedictionary.module;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.MyWordDatabase;

import java.util.List;

/**
 * Created by LinhNT on 1/12/2016.
 */
public class AddGroupAlert {
    static Button bt_plus, btn_tao, btn_huy;
    static Dialog main, sub;
    static EditText et_add;
    static MyWordDatabase dbWord;
    private static Entry entry;
    private static List<Category> category;

    public static void settingClick(Dialog dialog) {
        main = dialog;
        bt_plus = (Button) dialog.findViewById(R.id.bt_plus);
        bt_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert();
            }
        });
    }

    public static void setEntry(Entry entry) {
        AddGroupAlert.entry = entry;
    }

    public static void settingClick(Dialog dialog, MyWordDatabase db) {
        main = dialog;
        dbWord = db;
        bt_plus = (Button) dialog.findViewById(R.id.bt_plus);
        bt_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert();
            }
        });
    }


    private static void showAlert() {
        // custom dialog
        sub = new Dialog(main.getOwnerActivity());
        sub.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sub.setContentView(R.layout.add_group_dialog);
        // map edit text, button
        et_add = (EditText) sub.findViewById(R.id.et_add);
        btn_tao = (Button) sub.findViewById(R.id.btn_tao);
        btn_huy = (Button) sub.findViewById(R.id.btn_huy);

        btn_tao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedTao();
            }
        });
        btn_huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedHuy();
            }
        });

        sub.show();
        Window window = sub.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        sub.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    private static void clickedTao() {
        if (et_add.getText() == null) {
            return;
        }
        final String text = et_add.getText().toString().trim();
        if (TextUtils.isEmpty(text) || text.length() == 0) {
            return;
        }
        new AsyncTask<Void, Void, Boolean>() {
            Category category;

            @Override
            protected Boolean doInBackground(Void... params) {
                category = new Category();
                category.setName(text);
                long idCategory = dbWord.insertCategory(category);
                if (idCategory != -1) {
                    entry.setIdCategory((int) idCategory);
                    dbWord.insertEntry(entry);
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                sub.dismiss();
                if (aBoolean) {
                    main.dismiss();
                    Toast.makeText(sub.getContext(), sub.getContext().getString(R.string.add_word_success, category.getName()), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(sub.getContext(), R.string.add_group_fail, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();

    }

    private static void clickedHuy() {
        sub.dismiss();
    }

    public static void setCategory(List<Category> category) {
        AddGroupAlert.category = category;
    }
}
