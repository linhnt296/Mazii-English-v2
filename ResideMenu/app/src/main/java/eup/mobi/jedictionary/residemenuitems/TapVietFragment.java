package eup.mobi.jedictionary.residemenuitems;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.Kanji;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.google.Track;
import eup.mobi.jedictionary.horizontallistview.HorizontalListView;
import eup.mobi.jedictionary.myadapter.MyAdapterHanTu;
import eup.mobi.jedictionary.myadapter.MyAdapterHanTu2;
import eup.mobi.jedictionary.myadapter.MyAdapterHorizontal;
import eup.mobi.jedictionary.myadapter.MyAdapterTuJa;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.module.MyGridView;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.svgwriter.SVGFragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * User: special
 * Date: 13-12-22
 * Time: 下午3:28
 * Mail: specialcyci@gmail.com
 */
public class TapVietFragment extends Fragment {
    private View parentView;
    //    private SVGCanvasView svgView;
    private SearchView searchView;
    private ProgressBar progressView;
    private ArrayList<Kanji> kanji, sKanji, rKanji;
    private ArrayList<Jaen> sJavi;
    private MenuActivity menuActivity;
    private HorizontalListView horizontalListView;
    MyAdapterHorizontal adapterHanTu;
    MyAdapterHanTu2 adapterHanTu_2;
    public Dialog dialog;
    FragmentManager fragmentManager;
    ListView lv_suggest;
    MyAdapterHanTu adapter_suggest_kanji;
    MyAdapterTuJa adapter_suggest_ja;
    Boolean isSearch = false, isSubK = false;
    Handler handler;
    String query_WKG, oldText, defaltQuery = "";
    FrameLayout frameLayout;
    RelativeLayout layout_tapviet, layout_tapviet_1, layout_tapviet_2;
    CardView cardView_horizontal;
    MyGridView gridViewRandom;
    TextView tv_khac;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.tapviet, container, false);
        ImageButton bt_menu = (ImageButton) parentView.findViewById(R.id.title_bar_left_menu);
        FloatingActionButton fab = (FloatingActionButton) parentView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Track.sendTrackerScreen("Search");
                TraCuuFragment traCuuFragment = new TraCuuFragment();
                traCuuFragment.setMenuActivity(menuActivity);
                changeFragment(traCuuFragment);
            }
        });
        searchView = (SearchView) parentView.findViewById(R.id.hand_write_search);
        SearchView.SearchAutoComplete theTextArea = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        theTextArea.setTextColor(Color.WHITE);//or any color that you want
        // set color text hint
        ((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text))
                .setHintTextColor(getResources().getColor(R.color.search_hint));
        horizontalListView = (HorizontalListView) parentView.findViewById(R.id.horizontal_listview);
        progressView = (ProgressBar) parentView.findViewById(R.id.pbHeaderProgress);
        progressView.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.holo_blue), PorterDuff.Mode.SRC_IN);
        lv_suggest = (ListView) parentView.findViewById(R.id.lv_suggest);
        layout_tapviet = (RelativeLayout) parentView.findViewById(R.id.layout_tapviet);
        layout_tapviet_1 = (RelativeLayout) parentView.findViewById(R.id.rl_tapviet_1);
        layout_tapviet_2 = (RelativeLayout) parentView.findViewById(R.id.rl_tapviet_2);
        gridViewRandom = (MyGridView) parentView.findViewById(R.id.gridView_k);
        cardView_horizontal = (CardView) parentView.findViewById(R.id.cardview_horizontal);
        frameLayout = (FrameLayout) parentView.findViewById(R.id.frag_svg);
        tv_khac = (TextView) parentView.findViewById(R.id.tv_khac);
        handler = new Handler();
        //  resideMenu = ((MenuActivity) getActivity()).getResideMenu();
        //fragment
        fragmentManager = getChildFragmentManager();

        //adsmod

        boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
        float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
//        Adsmod.fragmentBanner(parentView, banner, probBanner, isPremium);

        //buton menu
        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                        MenuActivity.drawer.openDrawer(GravityCompat.START);
                } catch (Exception e) {
                }
            }
        });

        // random
        tv_khac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random RAND = new Random();
                int position = RAND.nextInt(14) + 1;
                int colors[] = getResources().getIntArray(R.array.arr_color_progress);
                int color = colors[position];
                tv_khac.setTextColor(color);

                new asyntask_randomKanji().execute();
            }
        });
        new asyntask_randomKanji().execute();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isSubK = true;
                if (adapter_suggest_ja != null) {
                    adapter_suggest_ja.clear();
                    adapter_suggest_ja.notifyDataSetChanged();
                    lv_suggest.setVisibility(View.GONE);
                } else if (adapter_suggest_kanji != null) {
                    adapter_suggest_kanji.clear();
                    adapter_suggest_kanji.notifyDataSetChanged();
                    lv_suggest.setVisibility(View.GONE);
                }
                if (!query.equals("") && !query.equals(" ")) {
                    new myAsyntask_tracuu2().execute(query);
                } else {
                    new asyntask_randomKanji().execute();
                    layout_tapviet_1.setVisibility(View.GONE);
                    layout_tapviet_2.setVisibility(View.VISIBLE);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                if (newText.equals("") || newText.equals(" ")) {
                    if (adapter_suggest_ja != null) {
                        adapter_suggest_ja.clear();
                        adapter_suggest_ja.notifyDataSetChanged();
                        lv_suggest.setVisibility(View.GONE);
                    } else if (adapter_suggest_kanji != null) {
                        adapter_suggest_kanji.clear();
                        adapter_suggest_kanji.notifyDataSetChanged();
                        lv_suggest.setVisibility(View.GONE);
                    }
//                    horizontalListView.setVisibility(View.GONE);
                    if (adapterHanTu != null) {
                        layout_tapviet.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        cardView_horizontal.setVisibility(View.GONE);
                        adapterHanTu.clear();
                        adapterHanTu.notifyDataSetChanged();
                    }
                    frameLayout.setVisibility(View.GONE);

                    new asyntask_randomKanji().execute();
                    layout_tapviet_1.setVisibility(View.GONE);
                    layout_tapviet_2.setVisibility(View.VISIBLE);
                } else {
                    layout_tapviet_1.setVisibility(View.VISIBLE);
                    layout_tapviet_2.setVisibility(View.GONE);
                }
                if (!isSubK) {
                    if (!newText.equals("") && !newText.equals(" ")) {
                        if (isSearch) {
                            handler.removeCallbacksAndMessages(null);
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    suggestKanji(newText);
                                    isSearch = true;

                                }
                            }, 300);
                        } else {
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    suggestKanji(newText);
                                }
                            }, 300);
                        }

                        isSearch = true;
                    }
                }
                return false;
            }

        });

        return parentView;
    }


    private class myAsyntask_tracuu2 extends AsyncTask<String, Void, ArrayList<Kanji>> {
        @Override
        protected ArrayList<Kanji> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            kanji = db.getKanji(params[0]);
            return kanji;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressView.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(ArrayList<Kanji> kanjis) {
            super.onPostExecute(kanjis);
            progressView.setVisibility(View.GONE);
            if (kanji.size() == 0) {
//                horizontalListView.setVisibility(View.GONE);
                if (adapterHanTu != null) {
                    cardView_horizontal.setVisibility(View.GONE);
                    adapterHanTu.clear();
                    adapterHanTu.notifyDataSetChanged();
                }
                frameLayout.setVisibility(View.GONE);
                if (defaltQuery != "") {
                    isSubK = true;
                    searchView.setQuery(defaltQuery, true);
                }
//            }else if (kanji.size() == 1) {
//                setFragment(kanji.get(0).getmImg());
////                horizontalListView.setVisibility(View.GONE);
//                if (adapterHanTu != null) {
//                    cardView_horizontal.setVisibility(View.GONE);
//                    adapterHanTu.clear();
//                    adapterHanTu.notifyDataSetChanged();
//                }
            } else if (kanji.size() >= 1) {
                getListKanji();
                if (kanji.get(0).getmKanji() != null && !kanji.get(0).getmKanji().equals("")) {
                    setFragment(readSvg(kanji.get(0).getmKanji().charAt(0)));
                    layout_tapviet_1.setVisibility(View.VISIBLE);
                    layout_tapviet_2.setVisibility(View.GONE);
                }
            }
            isSubK = false;
        }
    }

    private void getListKanji() {
        if (kanji == null) {
            return;
        }
        adapterHanTu = new MyAdapterHorizontal(getActivity(),
                R.layout.tapviet_horizontal_item,
                kanji);
//        horizontalListView.setVisibility(View.VISIBLE);
        cardView_horizontal.setVisibility(View.VISIBLE);
        horizontalListView.setAdapter(adapterHanTu);
        adapterHanTu.notifyDataSetChanged();
//        setListViewHeightBasedOnChildren(gridview);
        horizontalListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getContext(), position + " " + kanji.get(position).getmKanji(), Toast.LENGTH_SHORT).show();
                if (kanji.get(position).getmKanji() != null && !kanji.get(position).getmKanji().equals("")) {
                    setFragment(readSvg(kanji.get(position).getmKanji().charAt(0)));
                    layout_tapviet_1.setVisibility(View.VISIBLE);
                    layout_tapviet_2.setVisibility(View.GONE);
                }
            }
        });
    }


    private void setFragment(String svg) {
        layout_tapviet.setBackgroundColor(Color.parseColor("#E8E8E8"));
        SVGFragment svgFragment = new SVGFragment();
        Bundle bundle = new Bundle();
        bundle.putString("svg", svg);
        svgFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frag_svg, svgFragment);
        fragmentTransaction.commit();
        frameLayout.setVisibility(View.VISIBLE);
    }

    private void suggestKanji(String newText) {
        if (!newText.equals("") && !newText.equals(" ") && !newText.equals(oldText)) {
            new suggestKanjiSynctask().execute(newText);
        }
    }

    private class suggestKanjiSynctask extends AsyncTask<String, Void, ArrayList<Kanji>> {
        long time_1, time_2;
        String k_query;

        @Override
        protected ArrayList<Kanji> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            k_query = params[0];
            sKanji = db.getKanji(k_query);
            return sKanji;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            time_1 = System.currentTimeMillis();
        }

        @Override
        protected void onPostExecute(ArrayList<Kanji> kanjis) {
            super.onPostExecute(kanjis);
            if (sKanji.size() != 0) {
                getListSuggestKanji();
            } else {
                new sJaSynctask().execute(k_query);
            }
            time_2 = System.currentTimeMillis() - time_1;

        }
    }

    private void getListSuggestKanji() {
        if (sKanji != null && sKanji.size() > 0) {
            adapter_suggest_kanji = new MyAdapterHanTu(getActivity(), R.layout.tracuu_tab2_items, sKanji);

            if (adapter_suggest_kanji != null) {
                lv_suggest.setAdapter(adapter_suggest_kanji);
                lv_suggest.setVisibility(View.VISIBLE);
                layout_tapviet.setBackgroundColor(Color.parseColor("#FFFFFF"));
                layout_tapviet_1.setVisibility(View.GONE);
                layout_tapviet_2.setVisibility(View.GONE);
                lv_suggest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        query_WKG = sKanji.get(position).getmKanji().toString();
//                                query_WKG = query_WKG.replace("\"", "");
                        String[] arrquery = query_WKG.split(" ");
                        query_WKG = arrquery[0];

                        //todo Suggest selected 2
                        searchView.setQuery(query_WKG, true);

                        oldText = query_WKG;
                        adapter_suggest_kanji.clear();
                        adapter_suggest_kanji.notifyDataSetChanged();
                        lv_suggest.setVisibility(View.GONE);
                    }
                });
            }
        }
    }

    private class sJaSynctask extends AsyncTask<String, Void, List<Jaen>> {

        @Override
        protected List<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            if (!params[0].equals("") && !params[0].equals(" ")) {
                sJavi = db.getJaenSuggest(params[0]);
            }
            return sJavi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<Jaen> vijas) {
            super.onPostExecute(vijas);
            getListSuggestJa();

        }
    }

    private void getListSuggestJa() {
        if (sJavi != null && sJavi.size() != 0) {
            adapter_suggest_ja = new MyAdapterTuJa(getActivity(), R.layout.tracuu_tab1_ja_items, sJavi);

            if (adapter_suggest_ja != null) {
                lv_suggest.setAdapter(adapter_suggest_ja);
                lv_suggest.setVisibility(View.VISIBLE);
                layout_tapviet_1.setVisibility(View.GONE);
                layout_tapviet_2.setVisibility(View.GONE);
                layout_tapviet.setBackgroundColor(Color.parseColor("#FFFFFF"));
                lv_suggest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        query_WKG = sJavi.get(position).getmWord().toString();
                        //todo Suggest selected 1-ja
                        searchView.setQuery(query_WKG, true);
                        oldText = query_WKG;
                        adapter_suggest_ja.clear();
                        adapter_suggest_ja.notifyDataSetChanged();
                        lv_suggest.setVisibility(View.GONE);
                    }
                });
                defaltQuery = sJavi.get(0).getmWord().toString();
            }
        } else {
            lv_suggest.setVisibility(View.GONE);
        }
    }

    public MenuActivity getMenuActivity() {
        return menuActivity;
    }

    public void setMenuActivity(MenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }

    private void changeFragment(Fragment targetFragment) {
        // resideMenu.clearIgnoredViewList();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private class asyntask_randomKanji extends AsyncTask<String, Void, ArrayList<Kanji>> {
        @Override
        protected ArrayList<Kanji> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            rKanji = db.getRandomKanji();
            return rKanji;
        }

        @Override
        protected void onPostExecute(ArrayList<Kanji> kanjis) {
            super.onPostExecute(kanjis);
            if (rKanji != null) {
                adapterHanTu_2 = new MyAdapterHanTu2(getActivity(),
                        R.layout.gridview_ht,
                        rKanji);
            }
            gridViewRandom.setAdapter(adapterHanTu_2);
            gridViewRandom.setVisibility(View.VISIBLE);
            adapterHanTu_2.notifyDataSetChanged();
            // lv_2.setVisibility(View.VISIBLE);
            gridViewRandom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    hideSoftKeyboard(getActivity());
                    //get svg view
                    if (rKanji.get(position).getmKanji() != null && !rKanji.get(position).getmKanji().equals("")) {
                        setFragment(readSvg(rKanji.get(position).getmKanji().charAt(0)));
                        layout_tapviet_1.setVisibility(View.VISIBLE);
                        layout_tapviet_2.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.tapviet_img_null), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    layout_tapviet_2.setVisibility(View.VISIBLE);
                }
            }, 200);
        }
    }

    public void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    private String readSvg(char file_name) {
        String hex = Integer.toHexString(file_name);
        if (hex.length() < 5) {
            for (int i = 0; i < (5 - hex.length()); i++)
                hex = "0" + hex;
        }
        StringBuilder result = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(menuActivity.getAssets().open("stroke/" + hex + ".svg"), "UTF-8"));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                result.append(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return result.toString();
    }
}
