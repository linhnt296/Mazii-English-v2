package eup.mobi.jedictionary.residemenuactivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.database.MyWordDatabase;
import eup.mobi.jedictionary.fast.search.FastSearchService;
import eup.mobi.jedictionary.module.Category;
import eup.mobi.jedictionary.module.Entry;
import eup.mobi.jedictionary.myadapter.MyGroupWordAdapter;

import java.util.ArrayList;
import java.util.List;

public class PlusActitity extends AppCompatActivity {
    SwipeMenuListView swipeListView;
    MyWordDatabase db;
    TextView tv_title;
    TextView tv_add_word_null;
    Button bt_plus;
    int mFavorite, edit_position;
    Entry entry = new Entry();
    private List<Category> categories;
    Dialog sub, dialogEdit;
    EditText et_add, et_edit;
    Button btn_tao, btn_huy, btn_edit, btn_huy_edit;
    String edit_name;
    private static ArrayList<String> arrFavorite = new ArrayList<>();
    private static ArrayList<String> arrRemove = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_word_dialog);

        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_add_word_null = (TextView) findViewById(R.id.tv_add_word_null);
        swipeListView = (SwipeMenuListView) findViewById(R.id.swipe_listview);
        bt_plus = (Button) findViewById(R.id.bt_plus);
        bt_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert();
            }
        });

        Intent callerIntent = getIntent();
        //có intent rồi thì lấy Bundle dựa vào MyPackage
        Bundle bundle = callerIntent.getBundleExtra("PlusActivity");
        String word = bundle.getString("word");
        String mean = bundle.getString("mean");
        String phonetic = bundle.getString("phonetic");
        String type = bundle.getString("type");
        int idEntry = bundle.getInt("id");
        mFavorite = bundle.getInt("favorite");
        entry.setWord(word);
        entry.setMean(mean);
        entry.setPhonetic(phonetic);
        entry.setIdEntry(idEntry);
        entry.setType(type);
        setData();
        if (entry.getWord() == null) {
            tv_title.setText(getString(R.string.sotay_dialog_themtu) + entry.getPhonetic() + getString(R.string.sotay_dialog_vao));
        } else {
            tv_title.setText(getString(R.string.sotay_dialog_themtu) + entry.getWord() + getString(R.string.sotay_dialog_vao));
        }
        //check curent favorite
        for (String favorite : arrFavorite) {
            if (word != null && !word.equals("")) {
                if (favorite.equals(entry.getWord())) {
                    mFavorite = 1;
                    break;
                }
            } else {
                if (favorite.equals(entry.getPhonetic())) {
                    mFavorite = 1;
                    break;
                }
            }
        }
        for (String remove : arrRemove) {
            if (word != null && !word.equals("")) {
                if (remove.equals(entry.getWord())) {
                    mFavorite = 0;
                    break;
                }
            } else {
                if (remove.equals(entry.getPhonetic())) {
                    mFavorite = 0;
                    break;
                }
            }
        }
        // setUpFavorite
        setUpFavorite();
        // setUp swipe
        swipeListView();
        if (MenuActivity.sharedPreferences != null) {
            boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
            String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
            float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
//            Adsmod.banner(this, banner, probBanner, isPremium);
        }
    }

    private void showAlert() {
        // custom dialog
        sub = new Dialog(this);
        sub.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sub.setContentView(R.layout.add_group_dialog);
        // map edit text, button
        et_add = (EditText) sub.findViewById(R.id.et_add);
        btn_tao = (Button) sub.findViewById(R.id.btn_tao);
        btn_huy = (Button) sub.findViewById(R.id.btn_huy);

        btn_tao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedTao();
            }
        });
        btn_huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedHuy();
            }
        });

        sub.show();
        Window window = sub.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        sub.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    private void clickedTao() {
        if (et_add.getText() == null) {
            return;
        }
        final String text = et_add.getText().toString().trim();
        if (TextUtils.isEmpty(text) || text.length() == 0) {
            return;
        }
        new AsyncTask<Void, Void, Boolean>() {
            Category category;

            @Override
            protected Boolean doInBackground(Void... params) {
                category = new Category();
                category.setName(text);
                long idCategory = db.insertCategory(category);
                if (idCategory != -1) {
                    entry.setIdCategory((int) idCategory);
                    return db.insertEntry(entry);
                } else {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                sub.dismiss();
                if (aBoolean) {
                    onBackPressed();
                    Toast.makeText(sub.getContext(), sub.getContext().getString(R.string.add_group_success, category.getName()), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(sub.getContext(), R.string.add_group_fail, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();

    }

    private void clickedHuy() {
        sub.dismiss();
    }

    private void setUpFavorite() {
        //check on/off favorite
        final ImageView mNotification_favorite_off = (ImageView) findViewById(R.id.favorite_off);
        final ImageView mNotification_favorite_on = (ImageView) findViewById(R.id.favorite_on);
        //set image on if isFavorite
        if (mFavorite == 1) {
            mNotification_favorite_off.setVisibility(View.GONE);
            mNotification_favorite_on.setVisibility(View.VISIBLE);
        } else {
            mNotification_favorite_on.setVisibility(View.GONE);
            mNotification_favorite_off.setVisibility(View.VISIBLE);
        }

        mNotification_favorite_off.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNotification_favorite_off.setVisibility(View.GONE);
                mNotification_favorite_on.setVisibility(View.VISIBLE);
                // do query
                new UPDATESyntask().execute(String.valueOf(entry.getIdEntry()), entry.getType());
                if (entry.getWord() == null) {
                    arrFavorite.add(entry.getPhonetic());
                    arrRemove.remove(entry.getPhonetic());
                    Toast.makeText(getApplicationContext(), getString(R.string.add_to_favorite_dathem) + entry.getPhonetic() + getString(R.string.add_to_favorite_vaoyeuthich), Toast.LENGTH_SHORT).show();
                } else {
                    arrFavorite.add(entry.getWord());
                    arrRemove.remove(entry.getWord());
                    Toast.makeText(getApplicationContext(), getString(R.string.add_to_favorite_dathem) + entry.getWord() + getString(R.string.add_to_favorite_vaoyeuthich), Toast.LENGTH_SHORT).show();
                }

            }
        });
        mNotification_favorite_on.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNotification_favorite_on.setVisibility(View.GONE);
                mNotification_favorite_off.setVisibility(View.VISIBLE);
                // do query
                new REMOVESyntask().execute(String.valueOf(entry.getIdEntry()));
                if (entry.getWord() == null) {
                    arrFavorite.remove(entry.getPhonetic());
                    arrRemove.add(entry.getPhonetic());
                    Toast.makeText(getApplicationContext(), getString(R.string.add_to_favorite_daxoa) + entry.getPhonetic() + getString(R.string.add_to_favorite_khoiyeuthich), Toast.LENGTH_SHORT).show();
                } else {
                    arrFavorite.remove(entry.getWord());
                    arrRemove.add(entry.getWord());
                    Toast.makeText(getApplicationContext(), getString(R.string.add_to_favorite_daxoa) + entry.getWord() + getString(R.string.add_to_favorite_khoiyeuthich), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void swipeListView() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.parseColor("#EFE4B0")));
                // set item width
                openItem.setWidth(90);
//                // set item title
//                openItem.setTitle("Open");
//                // set item title fontsize
//                openItem.setTitleSize(18);
//                // set item title font color
//                openItem.setTitleColor(Color.WHITE);
                //set icon
                openItem.setIcon(android.R.drawable.ic_menu_edit);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.parseColor("#F33F3F")));
                // set item width
                deleteItem.setWidth(90);
                // set a icon
                deleteItem.setIcon(android.R.drawable.ic_menu_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        swipeListView.setMenuCreator(creator);
        swipeListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
    }

    public void setData() {
        new AsyncTask<Void, Void, List<Category>>() {
            @Override
            protected List<Category> doInBackground(Void... params) {
                db = new MyWordDatabase(PlusActitity.this);
                return db.qCategory();
            }

            @Override
            protected void onPostExecute(List<Category> categoryList) {
                super.onPostExecute(categoryList);
                categories = categoryList;
                if (!categoryList.isEmpty()) {
                    tv_add_word_null.setVisibility(View.INVISIBLE);
                    swipeListView.setVisibility(View.VISIBLE);
                    getListCategories();
                }
            }
        }.execute();

    }

    MyGroupWordAdapter adapter;

    private void getListCategories() {
        adapter = new MyGroupWordAdapter(this, R.layout.sotay_item_recyclerview, categories);
        if (adapter != null) {
            swipeListView.setAdapter(adapter);
        }

        //Swipe items click listener
        swipeListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        showDialogEdit(position);
                        break;
                    case 1:
                        // delete
                        AlertDialog.Builder builder = new AlertDialog.Builder(PlusActitity.this);
                        builder.setTitle(getString(R.string.sotay_alert_title))
                                .setMessage(getString(R.string.sotay_alert_message))
                                .setCancelable(true)
                                .setNegativeButton(getString(R.string.history_delete), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        Toast.makeText(PlusActitity.this, getString(R.string.sotay_deleted), Toast.LENGTH_SHORT).show();
                                        onDelRow(position);
                                    }
                                })
                                .setPositiveButton(getString(R.string.history_cancel), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
        //item click listener
        swipeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                // onlick
                entry.setIdCategory(categories.get(position).getId());
                new AsyncTask<Void, Void, Boolean>() {
                    @Override
                    protected Boolean doInBackground(Void... params) {
                        return db.insertEntry(entry);
                    }

                    @Override
                    protected void onPostExecute(Boolean aBoolean) {
                        super.onPostExecute(aBoolean);
                        if (aBoolean) {
                            onBackPressed();
                            Toast.makeText(getApplicationContext(), getString(R.string.add_word_success, categories.get(position).getName()), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.add_word_fail, Toast.LENGTH_SHORT).show();
                        }

                    }
                }.execute();
            }
        });

    }

    private void showDialogEdit(final int pos) {
        edit_position = pos;
        dialogEdit = new Dialog(this);
        dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEdit.setContentView(R.layout.edit_group_dialog);
        // map edit text, button
        et_edit = (EditText) dialogEdit.findViewById(R.id.et_add);
        btn_edit = (Button) dialogEdit.findViewById(R.id.btn_tao);
        btn_huy_edit = (Button) dialogEdit.findViewById(R.id.btn_huy);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedEdit(pos);
            }
        });
        btn_huy_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedHuyEdit();
            }
        });

        dialogEdit.show();
        Window window = dialogEdit.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        dialogEdit.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    private void clickedHuyEdit() {
        dialogEdit.dismiss();
    }

    private void clickedEdit(int pos) {
        if (et_edit.getText() == null) {
            return;
        }
        final String text = et_edit.getText().toString().trim();
        if (TextUtils.isEmpty(text) || text.length() == 0) {
            return;
        }
        new editCategories().execute(text, String.valueOf(categories.get(pos).getId()));
    }


    public void onDelRow(int pos) {
        db.deleleCategory(categories.get(pos).getId());
        categories.remove(pos);
        adapter = new MyGroupWordAdapter(this, R.layout.sotay_item_recyclerview, categories);
        swipeListView.setAdapter(adapter);

    }

    //update favorite tu
    private class UPDATESyntask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(PlusActitity.this);

            // Reading all contacts
            switch (entry.getType()) {
                case "jaen":
                    db.setFavoriteOn_tu(params[0]);
                    break;
                case "kanji":
                    db.setFavoriteOn_hantu(params[0]);
                    break;
            }
            return null;
        }
    }

    //remove favorite tu
    private class REMOVESyntask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(PlusActitity.this);

            // Reading all contacts
            switch (entry.getType()) {
                case "jaen":
                    db.setFavoriteOff_tu(params[0]);
                    break;
                case "kanji":
                    db.setFavoriteOff_hantu(params[0]);
                    break;
            }

            return null;
        }
    }


    private class editCategories extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            MyWordDatabase db = new MyWordDatabase(PlusActitity.this);
            edit_name = params[0];
            return db.renameCategory(params[0], Integer.parseInt(params[1]));
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialogEdit.dismiss();
            if (aBoolean) {
                //todo change category name
                categories.get(edit_position).setName(edit_name);
                adapter = new MyGroupWordAdapter(PlusActitity.this, R.layout.sotay_item_recyclerview, categories);
                swipeListView.setAdapter(adapter);
                Toast.makeText(dialogEdit.getContext(), getString(R.string.edit_group_success), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(dialogEdit.getContext(), getString(R.string.edit_group_fail), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean back;

    @Override
    public void onBackPressed() {
        back = true;
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        stopService(new Intent(this, FastSearchService.class));
    }

    @Override
    protected void onStop() {
        if (!back && MenuActivity.sharedPreferences.getBoolean("fast_search", false)) {
            if (Build.VERSION.SDK_INT < 23) {
                startService(new Intent(this, FastSearchService.class));
            } else if (Build.VERSION.SDK_INT >= 23 && Settings.canDrawOverlays(this)) {
                startService(new Intent(this, FastSearchService.class));
            }
//            Toast.makeText(this, "start service", Toast.LENGTH_SHORT).show();
        }
        super.onStop();
    }

}
