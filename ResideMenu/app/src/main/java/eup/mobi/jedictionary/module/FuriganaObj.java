package eup.mobi.jedictionary.module;

/**
 * Created by nguye on 12/1/2015.
 */
public class FuriganaObj {
    private String mText;
    private String mFurigana;

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }

    public String getmFurigana() {
        return mFurigana;
    }

    public void setmFurigana(String mFurigana) {
        this.mFurigana = mFurigana;
    }

    public FuriganaObj(String mText, String mFurigana) {
        this.mText = mText;
        this.mFurigana = mFurigana;
    }
}
