package eup.mobi.jedictionary.flashcard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.module.Entry;
import eup.mobi.jedictionary.residemenuactivity.FlashCardMyWordActivity;

import me.grantland.widget.AutofitTextView;

/**
 * Created by chenupt@gmail.com on 2015/1/31.
 * Description TODO
 */
public class FlashCardFragmentMyWord2 extends Fragment {
    TextView tvMean, tvPhonetic;
    AutofitTextView tvWord;
    int mPostion;
    String mWord = "", mMean = "", mPhonetic = "";
    Entry entry;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup parentView = (ViewGroup) inflater.inflate(
                R.layout.flashcard_fragment_w2, container, false);
        tvWord = (AutofitTextView) parentView.findViewById(R.id.fcw_word);
        tvMean = (TextView) parentView.findViewById(R.id.fcw_mean);
        tvPhonetic = (TextView) parentView.findViewById(R.id.fcw_phonetic);
        if (FlashCardMyWordActivity.STATUS_FC == 2) {
            Bundle bundle2 = getArguments();
            mPostion = bundle2.getInt("position");
            entry = FlashCardMyWordActivity.arrayList.get(mPostion - 1);
        } else if (FlashCardMyWordActivity.STATUS_FC == 0) {
            if (FlashCardMyWordActivity.arrayList_1.size() > 0) {
                Bundle bundle2 = getArguments();
                mPostion = bundle2.getInt("position");
                entry = FlashCardMyWordActivity.arrayList_1.get(mPostion - 1);
            }
        } else if (FlashCardMyWordActivity.STATUS_FC == 1) {
            if (FlashCardMyWordActivity.arrayList_2.size() > 0) {
                Bundle bundle2 = getArguments();
                mPostion = bundle2.getInt("position");
                entry = FlashCardMyWordActivity.arrayList_2.get(mPostion - 1);
            }
        }
        if (entry != null) {
            mWord = entry.getWord();
            mMean = entry.getMean();
            mPhonetic = entry.getPhonetic();
        }
        tvWord.setText(mWord);
        tvMean.setText(mMean);
        tvPhonetic.setText(mPhonetic);

        return parentView;
    }
}