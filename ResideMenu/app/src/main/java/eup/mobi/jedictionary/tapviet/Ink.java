package eup.mobi.jedictionary.tapviet;

import java.util.ArrayList;

/**
 * Created by thanh on 10/19/2015.
 */
public class Ink {
    private ArrayList<Long> currentLineSig1;
    private ArrayList<Long> currentLineSig2;
    private ArrayList<Long> currentLineSig3;

    public Ink(ArrayList<Long> currentLineSig1, ArrayList<Long> currentLineSig2, ArrayList<Long> currentLineSig3) {
        this.currentLineSig1 = currentLineSig1;
        this.currentLineSig2 = currentLineSig2;
        this.currentLineSig3 = currentLineSig3;
    }

    public Ink() {
    }

    public ArrayList<Long> getCurrentLineSig1() {
        return currentLineSig1;
    }

    public void setCurrentLineSig1(ArrayList<Long> currentLineSig1) {
        this.currentLineSig1 = currentLineSig1;
    }

    public ArrayList<Long> getCurrentLineSig2() {
        return currentLineSig2;
    }

    public void setCurrentLineSig2(ArrayList<Long> currentLineSig2) {
        this.currentLineSig2 = currentLineSig2;
    }

    public ArrayList<Long> getCurrentLineSig3() {
        return currentLineSig3;
    }

    public void setCurrentLineSig3(ArrayList<Long> currentLineSig3) {
        this.currentLineSig3 = currentLineSig3;
    }
}
