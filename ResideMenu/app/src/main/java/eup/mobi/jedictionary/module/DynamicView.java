package eup.mobi.jedictionary.module;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import eup.mobi.jedictionary.R;

import eup.mobi.jedictionary.database.Example;
import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.MergeObj;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.database.VerbObj;
import eup.mobi.jedictionary.furiganaview.FuriganaView;
import eup.mobi.jedictionary.google.NetWork;
import eup.mobi.jedictionary.myadapter.MyAdapterExKanji;
import eup.mobi.jedictionary.myadapter.MyAdapterVerbTable;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.residemenuactivity.TraCuuTuActivity;
import eup.mobi.jedictionary.residemenuitems.FragTab1;

import com.rey.material.app.BottomSheetDialog;
import com.rey.material.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by nguye on 12/1/2015.
 */
public class DynamicView {
    private static TextView tvMeanTrans, tvDich;
    private static LinearLayout layout1;
    private static String finalTrans = "";

    public static LinearLayout mLayoutJMean(final View view, ArrayList<JMeanObj> arrJMean) {
        final LinearLayout layout = (LinearLayout) view
                .findViewById(R.id.frag1_mean);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        finalTrans = "";
        if (arrJMean.size() > 0) {
            String oldKind = "";
            for (JMeanObj meanObj : arrJMean) {
                layout1 = new LinearLayout(view.getContext());
                layout1.setOrientation(LinearLayout.VERTICAL);
                if (!meanObj.getKind().equals("") && !meanObj.getKind().equals(oldKind)) {
                    TextView tvKind = new TextView(view.getContext());
                    tvKind.setText("☆ " + meanObj.getKind());
                    tvKind.setTextColor(Color.parseColor("#A52A2A"));
                    tvKind.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv4));
                    tvKind.setPadding(0, 0, 0, 20);
                    tvKind.setTextIsSelectable(true);
                    layout1.addView(tvKind);
                    oldKind = meanObj.getKind();
                }
                if (!meanObj.getMean().equals("")) {
                    TextView tvMean = new TextView(view.getContext());
                    String mean = meanObj.getMean();
                    if (!mean.equals(""))
                        mean = mean.substring(0, 1).toUpperCase() + mean.substring(1);
                    tvMean.setText("• " + mean);
                    tvMean.setTextColor(Color.parseColor("#0000FF"));
                    tvMean.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv2));
                    tvMean.setPadding(0, 8, 0, 20);
                    tvMean.setTextIsSelectable(true);
                    layout1.addView(tvMean);

                    if (NetWork.isNetWork(view.getContext()) && MenuActivity.sharedPreferences != null && !MenuActivity.sharedPreferences.getString("to_language", "en").equals("en")) {
                        tvMeanTrans = new TextView(view.getContext());
                        tvMeanTrans.setTextColor(Color.parseColor("#808080"));
                        tvMeanTrans.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv2));
                        tvMeanTrans.setPadding(0, 8, 0, 20);
                        tvMeanTrans.setLineSpacing(8f, 1f);

                        tvDich = new TextView(view.getContext());
                        tvDich.setTextColor(Color.parseColor("#808080"));
                        tvDich.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv4));
                        tvDich.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
                        tvDich.setPadding(0, 8, 0, 20);
                        tvDich.setGravity(Gravity.RIGHT);

                        String google_translate_url = "https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&dt=bd&dj=1&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=at";
                        String from = "en";
                        String to = MenuActivity.sharedPreferences.getString("to_language", Locale.getDefault().getLanguage());
                        String t_query = null, url;
                        try {
                            t_query = URLEncoder.encode(meanObj.getMean(), "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        url = google_translate_url + "&sl=" + from + "&tl=" + to + "&q=" + t_query;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new transMeanSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
                        } else {
                            new transMeanSyncTask().execute(url);
                        }
                    }
                }
                if (meanObj.getExamples() != null && meanObj.getExamples().size() > 0) {
                    Context context = view.getContext();
                    if (context != null) {
                        LinearLayout layout2 = new LinearLayout(context);
                        layout2.setOrientation(LinearLayout.VERTICAL);
                        layout2.setPadding(40, 0, 0, 0);
                        for (Example example : meanObj.getExamples()) {
                            if (example != null) {
                                String text = "", mean = "";
                                FuriganaView furiganaView = new FuriganaView(view.getContext());
                                TextView textView = new TextView(view.getContext());
                                if (example.getmContent() != null)
                                    mean = example.getmContent();
                                if (!mean.equals(""))
                                    mean = mean.substring(0, 1).toUpperCase() + mean.substring(1);
                                textView.setText(Html.fromHtml(mean).toString());
                                textView.setTextColor(Color.parseColor("#808080"));
                                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                                // line spacing
                                textView.setLineSpacing(8f, 1f);
                                textView.setTextIsSelectable(true);
                                TextPaint tp = new TextPaint();
                                tp.setColor(Color.parseColor("#C31313"));
                                tp.setTextSize(view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                                tp.setAntiAlias(true);
                                tp.setDither(true);
                                tp.setStyle(Paint.Style.FILL);
                                tp.setStrokeJoin(Paint.Join.ROUND);
//                        tp.setStrokeWidth(1f);
//                        tp.setTextSize(24f);

                                if (MenuActivity.sharedPreferences.getBoolean("cb1", true)) {
                                    text = mergeKH(example.getmMean(), example.getmTrans());
                                } else {
                                    text = example.getmMean();
                                }
                                if (!text.equals("")) {
                                    furiganaView.text_set(tp, text, -1, -1);
                                    furiganaView.setPadding(0, 28, 0, 16);
                                    final String finalContent = example.getmMean();
                                    furiganaView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showAlert((Activity) view.getContext(), finalContent);
                                        }
                                    });
                                    layout2.addView(furiganaView);
                                }
                                if (!mean.equals("")) {
                                    textView.setPadding(0, 16, 0, 28);
                                    layout2.addView(textView);
                                }
                                layout2.setLayoutParams(params);
                            }
                        }
                        layout1.addView(layout2);
                    }
                    layout1.setLayoutParams(params);
                }
                layout.addView(layout1);
            }
        }
        return layout;
    }

    //VerbTable frag1
    public static ListView verbTable(Activity view, ArrayList<VerbObj> arrVerb) {
        ListView listView = (ListView) view.findViewById(R.id.lv_verb);
        MyAdapterVerbTable adapterExKanji = new MyAdapterVerbTable(view, R.layout.list_row_verb, arrVerb);
        listView.setAdapter(adapterExKanji);
        setListViewHeightBasedOnChildrenVerb(listView);
        return listView;
    }

    public static LinearLayout synsetLayout(View view, String word, String pos, ArrayList<String[]> entry) {
        final LinearLayout layout = (LinearLayout) view
                .findViewById(R.id.frag1_sync);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout layout_title = new LinearLayout(view.getContext());
        layout_title.setOrientation(LinearLayout.HORIZONTAL);

        TextView tv_title = new TextView(view.getContext());
        tv_title.setText("Synonyms of: ");
        tv_title.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
        tv_title.setPadding(0, 0, 8, 16);
        tv_title.setTextColor(Color.parseColor("#9E9E9E"));

        TextView tv_word = new TextView(view.getContext());
        tv_word.setText(word);
        tv_word.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
        tv_word.setTextColor(Color.RED);
        tv_word.setTextIsSelectable(true);
        layout_title.addView(tv_title);
        layout_title.addView(tv_word);
        layout_title.setLayoutParams(params);
        layout.addView(layout_title);
        for (final String[] obj : entry) {
            LinearLayout layout_item = new LinearLayout(view.getContext());
            LinearLayout layout_item2 = new LinearLayout(view.getContext());
            LinearLayout layout_item3 = new LinearLayout(view.getContext());
            LinearLayout layout_item4 = new LinearLayout(view.getContext());
            layout_item.setOrientation(LinearLayout.HORIZONTAL);
            layout_item2.setOrientation(LinearLayout.HORIZONTAL);
            layout_item3.setOrientation(LinearLayout.HORIZONTAL);
            layout_item4.setOrientation(LinearLayout.HORIZONTAL);
            layout_item.setPadding(4, 8, 0, 4);
            layout_item2.setPadding(8, 4, 0, 4);
            layout_item3.setPadding(8, 4, 0, 4);
            layout_item4.setPadding(8, 4, 0, 4);
            TextView tv_start = new TextView(view.getContext());
            tv_start.setText("• ");
            layout_item.addView(tv_start);
            for (int i = 0; i < obj.length; i++) {
                final TextView tv_item = new TextView(view.getContext());
                tv_item.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                tv_item.setTextColor(Color.parseColor("#191919"));
                tv_item.setSingleLine(true);
                tv_item.setTextIsSelectable(true);
                final String synonym = obj[i];
                if (i == obj.length - 1) {
                    tv_item.setText(synonym + ".");
                } else {
                    tv_item.setText(synonym + ",  ");
                }

                tv_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragTab1.onSynonymClick(synonym);
                        tv_item.setPaintFlags(tv_item.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    }
                });

                if (obj[0].length() > 4) {
                    if (i < 3) {
                        layout_item.addView(tv_item);
                        layout_item.setLayoutParams(params);
                    } else if (i >= 3 && i < 6) {
                        layout_item2.addView(tv_item);
                        layout_item2.setLayoutParams(params);
                    } else if (i >= 6 && i < 9) {
                        layout_item3.addView(tv_item);
                        layout_item3.setLayoutParams(params);
                    } else {
                        layout_item4.addView(tv_item);
                        layout_item4.setLayoutParams(params);
                    }
                } else if (obj[0].length() == 4) {

                    if (i < 4) {
                        layout_item.addView(tv_item);
                        layout_item.setLayoutParams(params);
                    } else if (i >= 4 && i < 8) {
                        layout_item2.addView(tv_item);
                        layout_item2.setLayoutParams(params);
                    } else if (i >= 8 && i < 12) {
                        layout_item3.addView(tv_item);
                        layout_item3.setLayoutParams(params);
                    } else {
                        layout_item4.addView(tv_item);
                        layout_item4.setLayoutParams(params);
                    }
                } else if (obj[0].length() == 3) {

                    if (i < 5) {
                        layout_item.addView(tv_item);
                        layout_item.setLayoutParams(params);
                    } else if (i >= 5 && i < 10) {
                        layout_item2.addView(tv_item);
                        layout_item2.setLayoutParams(params);
                    } else if (i >= 10 && i < 15) {
                        layout_item3.addView(tv_item);
                        layout_item3.setLayoutParams(params);
                    } else {
                        layout_item4.addView(tv_item);
                        layout_item4.setLayoutParams(params);
                    }
                } else if (obj[0].length() <= 2) {

                    if (i < 6) {
                        layout_item.addView(tv_item);
                        layout_item.setLayoutParams(params);
                    } else if (i >= 6 && i < 12) {
                        layout_item2.addView(tv_item);
                        layout_item2.setLayoutParams(params);
                    } else if (i >= 12 && i < 18) {
                        layout_item3.addView(tv_item);
                        layout_item3.setLayoutParams(params);
                    } else {
                        layout_item4.addView(tv_item);
                        layout_item4.setLayoutParams(params);
                    }
                }
            }
            layout.addView(layout_item);
            layout.addView(layout_item2);
            layout.addView(layout_item3);
            layout.addView(layout_item4);
        }
        return layout;
    }

    public static LinearLayout mLayoutJMean_Activity(final Activity view, ArrayList<JMeanObj> arrJMean) {
        final LinearLayout layout = (LinearLayout) view
                .findViewById(R.id.frag1_mean);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        finalTrans = "";
        if (arrJMean.size() > 0) {
            String oldKind = "";
            for (JMeanObj meanObj : arrJMean) {
                layout1 = new LinearLayout(view);
                layout1.setOrientation(LinearLayout.VERTICAL);
                if (!meanObj.getKind().equals("") && !meanObj.getKind().equals(oldKind)) {
                    TextView tvKind = new TextView(view);
                    tvKind.setText("☆ " + meanObj.getKind());
                    tvKind.setTextColor(Color.parseColor("#A52A2A"));
                    tvKind.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv4));
                    tvKind.setPadding(0, 0, 0, 20);
                    layout1.addView(tvKind);
                    oldKind = meanObj.getKind();
                }
                if (!meanObj.getMean().equals("")) {
                    TextView tvMean = new TextView(view);
                    String mean = meanObj.getMean();
                    if (!mean.equals(""))
                        mean = mean.substring(0, 1).toUpperCase() + mean.substring(1);
                    tvMean.setText("• " + mean);
                    tvMean.setTextColor(Color.parseColor("#0000FF"));
                    tvMean.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv2));
                    tvMean.setPadding(0, 8, 0, 20);
                    tvMean.setTextIsSelectable(true);
                    layout1.addView(tvMean);
                    if (NetWork.isNetWork(view) && MenuActivity.sharedPreferences != null && !MenuActivity.sharedPreferences.getString("to_language", "en").equals("en")) {
                        tvMeanTrans = new TextView(view);
                        tvMeanTrans.setTextColor(Color.parseColor("#808080"));
                        tvMeanTrans.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv2));
                        tvMeanTrans.setPadding(0, 8, 0, 20);
                        tvMeanTrans.setLineSpacing(8f, 1f);

                        tvDich = new TextView(view);
                        tvDich.setTextColor(Color.parseColor("#808080"));
                        tvDich.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv4));
                        tvDich.setTypeface(Typeface.defaultFromStyle(Typeface.ITALIC));
                        tvDich.setPadding(0, 8, 0, 20);
                        tvDich.setGravity(Gravity.RIGHT);

                        String google_translate_url = "https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&dt=bd&dj=1&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=at";
                        String from = "en";
                        String to = MenuActivity.sharedPreferences.getString("to_language", Locale.getDefault().getLanguage());
                        String t_query = null, url;
                        try {
                            t_query = URLEncoder.encode(meanObj.getMean(), "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        url = google_translate_url + "&sl=" + from + "&tl=" + to + "&q=" + t_query;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new transMeanSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
                        } else {
                            new transMeanSyncTask().execute(url);
                        }
                    }
                }
                if (meanObj.getExamples() != null && meanObj.getExamples().size() > 0) {
                    LinearLayout layout2 = new LinearLayout(view);
                    layout2.setOrientation(LinearLayout.VERTICAL);
                    layout2.setPadding(40, 0, 0, 0);
                    for (Example example : meanObj.getExamples()) {
                        if (example != null) {
                            String text = "", mean = "";
                            FuriganaView furiganaView = new FuriganaView(view);
                            TextView textView = new TextView(view);
                            if (example.getmContent() != null)
                                mean = example.getmContent();
                            if (!mean.equals(""))
                                mean = mean.substring(0, 1).toUpperCase() + mean.substring(1);
                            textView.setText(Html.fromHtml(mean).toString());
                            textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                            textView.setTextColor(Color.parseColor("#808080"));
                            textView.setTextIsSelectable(true);
                            // line spacing
                            textView.setLineSpacing(8f, 1f);
                            TextPaint tp = new TextPaint();
                            tp.setColor(Color.parseColor("#C31313"));
                            tp.setTextSize(view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                            tp.setStyle(Paint.Style.FILL);
                            tp.setAntiAlias(true);
                            tp.setDither(true);
                            tp.setStyle(Paint.Style.FILL);
                            tp.setStrokeJoin(Paint.Join.ROUND);

                            if (MenuActivity.sharedPreferences != null && MenuActivity.sharedPreferences.getBoolean("cb1", true)) {
                                text = mergeKH(example.getmMean(), example.getmTrans());
                            } else {
                                text = example.getmMean();
                            }
                            if (!text.equals("")) {
                                furiganaView.text_set(tp, text, -1, -1);
                                furiganaView.setPadding(0, 28, 0, 16);
                                final String finalContent = example.getmMean();
                                furiganaView.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        showAlert(view, finalContent);
                                    }
                                });
                                layout2.addView(furiganaView);
                            }
                            if (!mean.equals("")) {
                                textView.setPadding(0, 16, 0, 28);
                                layout2.addView(textView);
                            }
                            layout2.setLayoutParams(params);
                        }
                    }
                    layout1.addView(layout2);
                    layout1.setLayoutParams(params);
                }
                layout.addView(layout1);
            }
        }
        return layout;
    }

    public static LinearLayout synsetLayout_Activity(Activity view, String word, String pos, ArrayList<String[]> entry) {
        final LinearLayout layout = (LinearLayout) view
                .findViewById(R.id.frag1_sync);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout layout_title = new LinearLayout(view);
        layout_title.setOrientation(LinearLayout.HORIZONTAL);

        TextView tv_title = new TextView(view);
        tv_title.setText("Synonyms of: ");
        tv_title.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
        tv_title.setPadding(0, 0, 8, 16);
        tv_title.setTextColor(Color.parseColor("#9E9E9E"));
        TextView tv_word = new TextView(view);
        tv_word.setText(word);
        tv_word.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
        tv_word.setTextColor(Color.RED);
        tv_word.setTextIsSelectable(true);
        layout_title.addView(tv_title);
        layout_title.addView(tv_word);
        layout_title.setLayoutParams(params);
        layout.addView(layout_title);
        for (final String[] obj : entry) {
            LinearLayout layout_item = new LinearLayout(view);
            LinearLayout layout_item2 = new LinearLayout(view);
            LinearLayout layout_item3 = new LinearLayout(view);
            LinearLayout layout_item4 = new LinearLayout(view);
            layout_item.setOrientation(LinearLayout.HORIZONTAL);
            layout_item4.setOrientation(LinearLayout.HORIZONTAL);
            layout_item2.setOrientation(LinearLayout.HORIZONTAL);
            layout_item3.setOrientation(LinearLayout.HORIZONTAL);
            layout_item.setPadding(4, 8, 0, 4);
            layout_item2.setPadding(8, 4, 0, 4);
            layout_item4.setPadding(8, 4, 0, 4);
            layout_item3.setPadding(8, 4, 0, 4);
            TextView tv_start = new TextView(view);
            tv_start.setText("• ");
            layout_item.addView(tv_start);
            for (int i = 0; i < obj.length; i++) {
                final TextView tv_item = new TextView(view);
                tv_item.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                tv_item.setTextColor(Color.parseColor("#191919"));
                tv_item.setSingleLine(true);
                tv_item.setTextIsSelectable(true);
                final String synonym = obj[i];
                if (i == obj.length - 1) {
                    tv_item.setText(synonym + ".");
                } else {
                    tv_item.setText(synonym + ",  ");
                }

                tv_item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TraCuuTuActivity.onSynonymClick(synonym);
                        tv_item.setPaintFlags(tv_item.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    }
                });
                if (obj[0].length() > 4) {
                    if (i < 3) {
                        layout_item.addView(tv_item);
                        layout_item.setLayoutParams(params);
                    } else if (i >= 3 && i < 6) {
                        layout_item2.addView(tv_item);
                        layout_item2.setLayoutParams(params);
                    } else if (i >= 6 && i < 9) {
                        layout_item3.addView(tv_item);
                        layout_item3.setLayoutParams(params);
                    } else {
                        layout_item4.addView(tv_item);
                        layout_item4.setLayoutParams(params);
                    }
                } else if (obj[0].length() == 4) {

                    if (i < 4) {
                        layout_item.addView(tv_item);
                        layout_item.setLayoutParams(params);
                    } else if (i >= 4 && i < 8) {
                        layout_item2.addView(tv_item);
                        layout_item2.setLayoutParams(params);
                    } else if (i >= 8 && i < 12) {
                        layout_item3.addView(tv_item);
                        layout_item3.setLayoutParams(params);
                    } else {
                        layout_item4.addView(tv_item);
                        layout_item4.setLayoutParams(params);
                    }
                } else if (obj[0].length() == 3) {

                    if (i < 5) {
                        layout_item.addView(tv_item);
                        layout_item.setLayoutParams(params);
                    } else if (i >= 5 && i < 10) {
                        layout_item2.addView(tv_item);
                        layout_item2.setLayoutParams(params);
                    } else if (i >= 10 && i < 15) {
                        layout_item3.addView(tv_item);
                        layout_item3.setLayoutParams(params);
                    } else {
                        layout_item4.addView(tv_item);
                        layout_item4.setLayoutParams(params);
                    }
                } else if (obj[0].length() <= 2) {

                    if (i < 6) {
                        layout_item.addView(tv_item);
                        layout_item.setLayoutParams(params);
                    } else if (i >= 6 && i < 12) {
                        layout_item2.addView(tv_item);
                        layout_item2.setLayoutParams(params);
                    } else if (i >= 12 && i < 18) {
                        layout_item3.addView(tv_item);
                        layout_item3.setLayoutParams(params);
                    } else {
                        layout_item4.addView(tv_item);
                        layout_item4.setLayoutParams(params);
                    }
                }
            }
            layout.addView(layout_item);
            layout.addView(layout_item2);
            layout.addView(layout_item3);
            layout.addView(layout_item4);
        }
        return layout;
    }

    //merge Kanji and hiragana
    private static String mergeKH(String kanji, String hiragana) {
        String myruby = null;
        if (hiragana == null) {
            myruby = kanji;
        } else if (hiragana.equals("xxx。") || hiragana.equals("xxx") || Language.isVietnamese(hiragana)) {
            myruby = kanji;
        } else {
            ArrayList<MergeObj> mergeObjs = MergeKanjiAndHiragana.mergeKanjiAndHiragana(kanji, hiragana);
            if (mergeObjs != null) {

                for (int i = 0; i < mergeObjs.size(); i++) {
                    MergeObj myMg = mergeObjs.get(i);
                    String ruby = "{" + myMg.getKanji() + ";" + myMg.getHiragana() + "}";
                    if (myruby == null) {
                        myruby = ruby;
                    } else {
                        myruby += ruby;
                    }
                }
            } else {
                myruby = "";
            }
        }
        return myruby;
    }

    public static ListView viduRecycler(Activity view, ArrayList<KanjiExampleObj> arrayList) {
        ListView listView = (ListView) view.findViewById(R.id.lv_exKanji);
        if (arrayList != null) {
            MyAdapterExKanji adapterExKanji = new MyAdapterExKanji(view, R.layout.list_row_kanji, arrayList);
            listView.setAdapter(adapterExKanji);
            setListViewHeightBasedOnChildrenKanji(listView);
        }
        return listView;
    }

    //set listview full items
    private static void setListViewHeightBasedOnChildrenVerb(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 110;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    //set listview full items
    private static void setListViewHeightBasedOnChildrenKanji(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 120;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private static class transMeanSyncTask extends AsyncTask<String, JSONObject, String> {

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = null;
            String trans = null;
            String data = getJSON(params[0], 1000);
            try {
                jsonObject = getMyJSON(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jsonObject == null) {
                return null;
            }
            if (jsonObject.has("sentences")) {
                try {
                    JSONArray sentences = jsonObject.getJSONArray("sentences");
                    JSONObject jTrans = sentences.getJSONObject(0);
                    // read trans
                    if (jTrans.has("trans")) {
                        trans = jTrans.getString("trans");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return trans;
        }

        @Override
        protected void onPostExecute(String strings) {
            super.onPostExecute(strings);
            if (strings != null) {
                String trans = strings.substring(0, 1).toUpperCase() + strings.substring(1);
                if (finalTrans.equals("")) {
                    finalTrans = trans;
                } else {
                    finalTrans = finalTrans + "\n  " + trans;
                }
                tvMeanTrans.setText("  " + finalTrans);
                tvDich.setText("Auto translation");
                if (tvMeanTrans.getParent() != null)
                    ((ViewGroup) tvMeanTrans.getParent()).removeView(tvMeanTrans); // <- fix
                layout1.addView(tvMeanTrans);
                if (tvDich.getParent() != null)
                    ((ViewGroup) tvDich.getParent()).removeView(tvDich); // <- fix
                layout1.addView(tvDich);
            }
        }
    }

    public static String getJSON(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
//            c.setRequestProperty("Content-length", "0");
            c.setRequestProperty("charset", "UTF-8");
            c.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36");

            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
//                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    private static JSONObject getMyJSON(String data) throws JSONException {
        if (data == null) {
            return null;
        }
        JSONObject object = new JSONObject(data);
        return object;
    }

    private static void showAlert(Activity activity, String content) {
        BottomSheetDialog mDialog = new BottomSheetDialog(activity);
        mDialog.applyStyle(R.styleable.BottomSheetDialog_android_layout_height)
                .contentView(R.layout.dialog_sheet)
                .heightParam(ViewGroup.LayoutParams.WRAP_CONTENT)
                .inDuration(300)
                .cancelable(true)
                .show();
        init(activity, mDialog, content);
    }

    private static void init(final Activity activity, final BottomSheetDialog dialog, final String content) {
        Button btn_speech, btn_copy;
        btn_speech = (Button) dialog.findViewById(R.id.btn_speech);
        btn_copy = (Button) dialog.findViewById(R.id.btn_copy);

        btn_speech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textToSpeech(activity, content);
                dialog.dismiss();
            }
        });
        btn_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyToClipBoard(activity, content);
                dialog.dismiss();
            }
        });
    }

    private static void copyToClipBoard(Activity activity, String content) {
        String label = activity.getString(R.string.copy);
        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(activity.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(label, content);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(activity, activity.getString(R.string.copy_done), Toast.LENGTH_SHORT).show();
    }

    private static TextToSpeech t1;

    private static void textToSpeech(final Activity activity, final String content) {
        try {
            t1 = new TextToSpeech(activity, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.ERROR) {
                        t1.setLanguage(Locale.JAPAN);
                        t1.speak(content, TextToSpeech.QUEUE_FLUSH, null);
                    } else {
                        Toast.makeText(activity, activity.getString(R.string.text_to_speech_notsuport), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            Toast.makeText(activity, activity.getString(R.string.text_to_speech_notsuport), Toast.LENGTH_SHORT).show();
        }
    }

    private static class fvExamKanjiSynctask extends AsyncTask<String, Void, ArrayList<Jaen>> {
        Activity activity;

        public fvExamKanjiSynctask(Activity activity) {
            this.activity = activity;
        }

        @Override
        protected ArrayList<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(activity);
            return db.getJaen(params[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<Jaen> javis) {
            super.onPostExecute(javis);
            if (javis != null && javis.size() > 0) {
                String vMean, vTitle, mPhonetic;
                int wID, mFavorite, mSeq;
                Intent myIntent = new Intent(activity, TraCuuTuActivity.class);
                //Khai báo Bundle
                Bundle bundle = new Bundle();
                wID = javis.get(0).getmId();
                vMean = javis.get(0).getmMean();
                vTitle = javis.get(0).getmWord();
                mFavorite = javis.get(0).getmFavorite();
                mPhonetic = javis.get(0).getmPhonetic();

                //đưa dữ liệu riêng lẻ vào Bundle
                bundle.putString("vMean", vMean);
                bundle.putString("vTitle", vTitle);
                bundle.putInt("mFavorite", mFavorite);
                bundle.putString("verbPhonetic", mPhonetic);
                if (mPhonetic != null) {
                    bundle.putString("vPhonetic", "「" + mPhonetic + "」");
                }
                bundle.putInt("wID", wID);

                //Đưa Bundle vào Intent
                myIntent.putExtra("TraCuuFragment", bundle);
                //Mở Activity ResultActivity
                activity.startActivity(myIntent);
            }
        }
    }
}
