package eup.mobi.jedictionary.svgwriter;

/**
 * Created by nguye on 11/9/2015.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import eup.mobi.jedictionary.R;


public class SVGFragment extends Fragment {
    private View parentView;
    private TapvietCanvasView canvasView;
    private Button btnReplay, btnHide, btnClear;
    private boolean isHide = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.svg_frag, container, false);
        canvasView = (TapvietCanvasView) parentView.findViewById(R.id.svg_activity);

        Bundle bundle = getArguments();
        String svg = bundle.getString("svg");
        canvasView.init(svg);
        btnReplay = (Button) parentView.findViewById(R.id.button_replay_fragment);
        btnHide = (Button) parentView.findViewById(R.id.button_hide_fragment);
        btnClear = (Button) parentView.findViewById(R.id.button_clear_fragment);
        btnReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvasView.rePaint();
            }
        });
        btnHide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isHide) {
                    canvasView.hideSample();
                    isHide = true;
                    btnHide.setText(R.string.show_sample);
                } else {
                    canvasView.hideSample();
                    isHide = false;
                    btnHide.setText(R.string.hide_sample) ;
                }
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvasView.clearCanvas();
            }
        });
        return parentView;
    }

}