package eup.mobi.jedictionary.residemenuitems;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;

import eup.mobi.jedictionary.R;

import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.JlptDatabase;
import eup.mobi.jedictionary.database.JlptWord;
import eup.mobi.jedictionary.database.Kanji;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.module.MyGridView;
import eup.mobi.jedictionary.myadapter.MyAdapterHanTu;
import eup.mobi.jedictionary.myadapter.MyAdapterJlptWord;
import eup.mobi.jedictionary.residemenuactivity.FlashCardActivity;
import eup.mobi.jedictionary.residemenuactivity.FlashCardWordActivity;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.residemenuactivity.TraCuuHanTuActivity;
import eup.mobi.jedictionary.residemenuactivity.TraCuuTuActivity;

import com.rey.material.widget.CheckBox;
import com.rey.material.widget.Spinner;

import java.util.ArrayList;

/**
 * User: special
 * Date: 13-12-22
 * Time: 下午1:31
 * Mail: specialcyci@gmail.com
 */
public class JLPTFragment extends Fragment {
    private View parentView;
    private Spinner sp2;
    private ListView listview_w;
    private MyGridView gridview;
    private Button btn_prev2, btn_prev2_w;
    private Button btn_next2, btn_next2_w;
    private ImageButton bt_tuychon, bt_flashcard;
    private Dialog dialog;
    private CheckBox cb_jlpt;
    static Boolean isCbJLPT;

    //    private ArrayList<String> data;
    public static ArrayList<Kanji> kanji = new ArrayList<>();
    public static ArrayList<JlptWord> word = new ArrayList<>();
    //    ArrayAdapter<String> sd;
    MyAdapterHanTu adapterHanTu;
    MyAdapterJlptWord myAdapterJlptWord;

    //    Boolean isKanji = true;
    String nLevel = "";

    private int pageCount_HT;
    private int pageCount_W;

    private int increment_HT = 0;
    private int increment_W = 0;

    public int TOTAL_LIST_ITEMS_HT = 0;
    public int TOTAL_LIST_ITEMS_W = 0;
    public int NUM_ITEMS_PAGE_HT = 50;
    public int NUM_ITEMS_PAGE_W = 50;
    private int noOfBtns_HT;
    private int noOfBtns_W;
    private Button[] btns_HT;
    private Button[] btns_W;
    LinearLayout ll, ll_w;
    LinearLayout.LayoutParams lp;

    ProgressBar progressBar;
    TabHost tab;
    boolean isLoad1, isLoad2, isLoad3;
    String mKanji, mMean, svg_kanji;
    int mFavorite, wID, wFavorite, w_id, mSeq;
    private String mKun, mOn, mStroke_count, mLevel, mCompDetail, mDetail, mExamples = null;
    private String wWord, wPhonetic, wMean;
    private MenuActivity menuActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.jlpt, container, false);
        ImageButton bt_menu = (ImageButton) parentView.findViewById(R.id.title_bar_left_menu);
//        sp1 = (Spinner) parentView.findViewById(R.id.sp_1);
        sp2 = (Spinner) parentView.findViewById(R.id.sp_2);
        gridview = (MyGridView) parentView.findViewById(R.id.gridView_jlpt);
        listview_w = (ListView) parentView.findViewById(R.id.lv_jlpt_w);
        btn_prev2 = (Button) parentView.findViewById(R.id.bt_prev2);
        btn_prev2_w = (Button) parentView.findViewById(R.id.bt_prev2_w);
        btn_next2 = (Button) parentView.findViewById(R.id.bt_next2);
        btn_next2_w = (Button) parentView.findViewById(R.id.bt_next2_w);
        bt_tuychon = (ImageButton) parentView.findViewById(R.id.bt_tuychon);
        bt_flashcard = (ImageButton) parentView.findViewById(R.id.bt_flashcard);
        ll = (LinearLayout) parentView.findViewById(R.id.btnLay);
        ll_w = (LinearLayout) parentView.findViewById(R.id.btnLay_w);
        lp = new LinearLayout.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT);
        progressBar = (ProgressBar) parentView.findViewById(R.id.pbHeaderProgress);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.holo_blue), PorterDuff.Mode.SRC_IN);
        progressBar.setVisibility(View.VISIBLE);
        tab = (TabHost) parentView.findViewById(android.R.id.tabhost);
        btn_prev2.setEnabled(false);
        btn_prev2_w.setEnabled(false);
        nLevel = getString(R.string.jlpt_n5);
        //  resideMenu = ((MenuActivity) getActivity()).getResideMenu();
        // add gesture operation's ignored views
        LinearLayout ignored_view = (LinearLayout) parentView.findViewById(R.id.fl_ignore);
        LinearLayout ignored_view_w = (LinearLayout) parentView.findViewById(R.id.fl_ignore_w);
        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                        MenuActivity.drawer.openDrawer(GravityCompat.START);
                } catch (Exception e) {
                }
            }
        });
        loadTabs();
        setSpiner();
        clickButtonTuyChon();
        clickButtonFlashCard();

        boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
        float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);

//        Adsmod.fragmentBannerJLPT(parentView.findViewById(R.id.jlpt_1), banner, probBanner, isPremium);
//        Adsmod.fragmentBannerJLPT(parentView.findViewById(R.id.jlpt_2), banner, probBanner, isPremium);
//        Adsmod.fragmentBannerJLPT(parentView.findViewById(R.id.jlpt_3), banner, probBanner, isPremium);

        // load han tu defaul
        new myAsyntask_jlpt_word().execute(getString(R.string.jlpt_n5));


        return parentView;
    }

    private void setSpiner() {
//        String[] items_1 = new String[]{"Hán tự", "Ngữ pháp"};
        String[] items_2 = new String[]{getString(R.string.jlpt_n5), getString(R.string.jlpt_n4), getString(R.string.jlpt_n3), getString(R.string.jlpt_n2), getString(R.string.jlpt_n1)};
//        ArrayAdapter<String> adapter_1 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items_1);
        ArrayAdapter<String> adapter_2 = new ArrayAdapter<String>(getActivity(), R.layout.my_spinner, items_2) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextSize(18);
                return v;
            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setGravity(Gravity.CENTER);
                return v;
            }

        };

        sp2.setAdapter(adapter_2);
        sp2.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {
                switch (tab.getCurrentTab()) {
                    case 0:
                        switch (position) {
                            case 0:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad2 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n5);
                                new myAsyntask_jlpt_word().execute(nLevel);

                                break;
                            case 1:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad2 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n4);
                                new myAsyntask_jlpt_word().execute(nLevel);

                                break;
                            case 2:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad2 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n3);
                                new myAsyntask_jlpt_word().execute(nLevel);

                                break;
                            case 3:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad2 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n2);
                                new myAsyntask_jlpt_word().execute(nLevel);

                                break;
                            case 4:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad2 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n1);
                                new myAsyntask_jlpt_word().execute(nLevel);

                        }
                        break;

                    case 1:
                        switch (position) {
                            case 0:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad1 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n5);
                                new myAsyntask_tracuu2().execute(nLevel);


                                break;
                            case 1:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad1 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n4);
                                new myAsyntask_tracuu2().execute(nLevel);

                                break;
                            case 2:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad1 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n3);
                                new myAsyntask_tracuu2().execute(nLevel);

                                break;
                            case 3:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad1 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n2);
                                new myAsyntask_tracuu2().execute(nLevel);

                                break;
                            case 4:
                                progressBar.setVisibility(View.VISIBLE);
                                isLoad1 = false;
                                isLoad3 = false;
                                nLevel = getString(R.string.jlpt_n1);
                                new myAsyntask_tracuu2().execute(nLevel);

                                break;
                        }
                        break;
                }
            }
        });
    }

    private void pagingListViewHT() {
//        data = new ArrayList<String>();
//        if (kanji == null) {
//            return;
//        }
        TOTAL_LIST_ITEMS_HT = kanji.size();
        /**
         * this block is for checking the number of pages
         * ====================================================
         */

        int val = TOTAL_LIST_ITEMS_HT % NUM_ITEMS_PAGE_HT;
        val = val == 0 ? 0 : 1;
        pageCount_HT = TOTAL_LIST_ITEMS_HT / NUM_ITEMS_PAGE_HT + val;
        /**
         * =====================================================
         */

        /**
         * The ArrayList data contains all the list items
         */
//        for (int i = 0; i < TOTAL_LIST_ITEMS; i++) {
//            data.add("This is Item " + (i + 1));
//        }

        loadListHT(0);
//        CheckEnable_HT();
//        sp1.setVisibility(View.VISIBLE);
//        sp2.setVisibility(View.VISIBLE);
        gridview.setVisibility(View.VISIBLE);
        btn_prev2.setVisibility(View.VISIBLE);
        btn_next2.setVisibility(View.VISIBLE);
        ll.setVisibility(View.VISIBLE);
        BtnfooterHT();
        CheckBtnBackGroud_HT(0);

        btn_next2.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                try {
                    increment_HT++;
                    loadListHT(increment_HT);
                    CheckEnable_HT();
                    CheckBtnBackGroud_HT(increment_HT);
                } catch (Exception e) {
                }
            }
        });

        btn_prev2.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                try {
                    increment_HT--;
                    loadListHT(increment_HT);
                    CheckEnable_HT();
                    CheckBtnBackGroud_HT(increment_HT);
                } catch (Exception e) {
                }
            }
        });

        CheckEnable_HT();
    }

    private void pagingListViewW() {
        TOTAL_LIST_ITEMS_W = word.size();
        /**
         * this block is for checking the number of pages
         * ====================================================
         */
        int val = TOTAL_LIST_ITEMS_W % NUM_ITEMS_PAGE_W;
        val = val == 0 ? 0 : 1;
        pageCount_W = TOTAL_LIST_ITEMS_W / NUM_ITEMS_PAGE_W + val;
        /**
         * =====================================================
         */

        /**
         * The ArrayList data contains all the list items
         */
//        for (int i = 0; i < TOTAL_LIST_ITEMS; i++) {
//            data.add("This is Item " + (i + 1));
//        }

        loadListW(0);
//        CheckEnable_NP();
//        sp1.setVisibility(View.VISIBLE);
//        sp2.setVisibility(View.VISIBLE);
        listview_w.setVisibility(View.VISIBLE);
        btn_prev2_w.setVisibility(View.VISIBLE);
        btn_next2_w.setVisibility(View.VISIBLE);
        ll_w.setVisibility(View.VISIBLE);
        BtnfooterW();
        CheckBtnBackGroud_W(0);

        btn_next2_w.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                increment_W++;
                loadListW(increment_W);
                CheckEnable_W();
                CheckBtnBackGroud_W(increment_W);
            }
        });

        btn_prev2_w.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                increment_W--;
                loadListW(increment_W);
                CheckEnable_W();
                CheckBtnBackGroud_W(increment_W);
            }
        });
        CheckEnable_W();
    }

    /**
     * Method for enabling and disabling Buttons
     */
    private void CheckEnable_HT() {
        if (increment_HT + 1 == pageCount_HT && increment_HT != 0) {
            btn_next2.setEnabled(false);
            btn_prev2.setEnabled(true);
        } else if (increment_HT == 0 && pageCount_HT != 1) {
            btn_prev2.setEnabled(false);
            btn_next2.setEnabled(true);
        } else if (increment_HT == 0 && pageCount_HT == 1) {
            btn_prev2.setEnabled(false);
            btn_next2.setEnabled(false);
        } else {
            btn_prev2.setEnabled(true);
            btn_next2.setEnabled(true);
        }
    }

    private void CheckEnable_W() {
        if (increment_W + 1 == pageCount_W && increment_W != 0) {
            btn_next2_w.setEnabled(false);
            btn_prev2_w.setEnabled(true);
        } else if (increment_W == 0 && pageCount_W != 1) {
            btn_prev2_w.setEnabled(false);
            btn_next2_w.setEnabled(true);

        } else if (increment_W == 0 && pageCount_W == 1) {
            btn_prev2_w.setEnabled(false);
            btn_next2_w.setEnabled(false);
        } else {
            btn_prev2_w.setEnabled(true);
            btn_next2_w.setEnabled(true);
        }
    }

    private void BtnfooterHT() {
        int val = TOTAL_LIST_ITEMS_HT % NUM_ITEMS_PAGE_HT;
        val = val == 0 ? 0 : 1;
        noOfBtns_HT = TOTAL_LIST_ITEMS_HT / NUM_ITEMS_PAGE_HT + val;
        btns_HT = new Button[noOfBtns_HT];
        ll.removeAllViews();
        for (int i = 0; i < noOfBtns_HT; i++) {
            btns_HT[i] = new Button(getContext());
            btns_HT[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
            btns_HT[i].setText("" + (i + 1));

//            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT);
            ll.addView(btns_HT[i], lp);

            final int j = i;
            btns_HT[j].setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    loadListHT(j);
                    CheckBtnBackGroud_HT(j);
                }
            });
        }

    }

    private void BtnfooterW() {
        int val = TOTAL_LIST_ITEMS_W % NUM_ITEMS_PAGE_W;
        val = val == 0 ? 0 : 1;
        noOfBtns_W = TOTAL_LIST_ITEMS_W / NUM_ITEMS_PAGE_W + val;
        btns_W = new Button[noOfBtns_W];
        ll_w.removeAllViews();
        for (int i = 0; i < noOfBtns_W; i++) {
            btns_W[i] = new Button(getContext());
            btns_W[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
            btns_W[i].setText("" + (i + 1));

//            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT);
            ll_w.addView(btns_W[i], lp);

            final int j = i;
            btns_W[j].setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    loadListW(j);
                    CheckBtnBackGroud_W(j);
                }
            });
        }

    }

    /**
     * Method for Checking Button Backgrounds
     */
    private void CheckBtnBackGroud_HT(int index) {
        increment_HT = index;
        for (int i = 0; i < noOfBtns_HT; i++) {
            if (i == index) {

                btns_HT[index].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns_HT[i].setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
                btns_HT[i].setTextSize(24);
            } else {

                btns_HT[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns_HT[i].setTextColor(getResources().getColor(android.R.color.black));
                btns_HT[i].setTextSize(12);
            }
        }

    }

    private void CheckBtnBackGroud_W(int index) {
        increment_W = index;
        for (int i = 0; i < noOfBtns_W; i++) {
            if (i == index) {

                btns_W[index].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns_W[i].setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
                btns_W[i].setTextSize(24);
            } else {

                btns_W[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns_W[i].setTextColor(getResources().getColor(android.R.color.black));
                btns_W[i].setTextSize(12);
            }
        }

    }

    /**
     * Method for loading data in listview
     *
     * @param number
     */
    private void loadListHT(int number) {

        ArrayList<Kanji> sort = new ArrayList<Kanji>();
        int start = number * NUM_ITEMS_PAGE_HT;
        for (int i = start; i < (start) + NUM_ITEMS_PAGE_HT; i++) {
            if (i < kanji.size()) {
                sort.add(kanji.get(i));
            } else {
                break;
            }
        }
        if (sort != null && sort.size() > 0) {
            adapterHanTu = new MyAdapterHanTu(getActivity(),
                    R.layout.jlpt_ht_items,
                    sort);
            gridview.setVisibility(View.VISIBLE);
            gridview.setAdapter(adapterHanTu);
            setGridViewHeightBasedOnChildren(gridview, 4);
            adapterHanTu.notifyDataSetChanged();
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    int mPosition = position + increment_HT * NUM_ITEMS_PAGE_HT;
                    Intent myIntent = new Intent(getActivity(), TraCuuHanTuActivity.class);
                    //Khai báo Bundle
                    Bundle bundle = new Bundle();
                    mKanji = kanji.get(mPosition).getmKanji();
                    if (kanji.get(mPosition).getmMean() != null) {
                        mMean = kanji.get(mPosition).getmMean();
                    } else mMean = "";
                    if (kanji.get(mPosition).getmKun() != null) {
                        mKun = kanji.get(mPosition).getmKun();
                    } else mKun = "";
                    if (kanji.get(mPosition).getmImg() != null) {
                        svg_kanji = kanji.get(mPosition).getmImg();
                    } else {
                        svg_kanji = "";
                    }
                    if (kanji.get(mPosition).getmOn() != null) {
                        mOn = kanji.get(mPosition).getmOn();
                    } else mOn = "";
                    if (String.valueOf(kanji.get(mPosition).getmStroke_count()) != null) {
                        mStroke_count = String.valueOf(kanji.get(mPosition).getmStroke_count());
                    } else mStroke_count = "";
                    if (String.valueOf(kanji.get(mPosition).getmLevel()) != null) {
                        mLevel = String.valueOf(kanji.get(mPosition).getmLevel());
                    } else mLevel = "";
                    if (kanji.get(mPosition).getmCompdetail() != null) {
                        mCompDetail = kanji.get(mPosition).getmCompdetail();
                    } else mCompDetail = "";
                    if (kanji.get(mPosition).getmDetail() != null) {
                        mDetail = kanji.get(mPosition).getmDetail();
                    } else mDetail = "";
                    if (kanji.get(mPosition).getmExamples() != null) {
                        mExamples = kanji.get(mPosition).getmExamples();
                    } else mExamples = "";
                    mFavorite = kanji.get(mPosition).getmFavorite();
                    wID = kanji.get(mPosition).getmId();

                    //đưa dữ liệu riêng lẻ vào Bundle
                    bundle.putString("mKanji", mKanji);
                    bundle.putString("mMean", mMean);
                    bundle.putString("mKun", mKun);
                    bundle.putString("svg_kanji", svg_kanji);
                    bundle.putString("mOn", mOn);
                    bundle.putString("mStroke_count", mStroke_count);
                    bundle.putString("mLevel", mLevel);
                    bundle.putString("mCompDetail", mCompDetail);
                    bundle.putString("mDetail", mDetail);
                    bundle.putString("mExamples", mExamples);
                    bundle.putInt("mFavorite", mFavorite);
                    bundle.putInt("wID", wID);

                    //Đưa Bundle vào Intent
                    myIntent.putExtra("TraCuuFragment_2", bundle);
                    //Mở Activity ResultActivity
                    MenuActivity.startMyActivity(myIntent, getMenuActivity());
                }
            });
        }
//        gridview.setExpanded(true);
//        setListViewHeightBasedOnChildren(gridview);
    }

    private void loadListW(int number) {

        ArrayList<JlptWord> sort = new ArrayList<JlptWord>();
        int start = number * NUM_ITEMS_PAGE_W;
        for (int i = start; i < (start) + NUM_ITEMS_PAGE_W; i++) {
            if (i < word.size()) {
                sort.add(word.get(i));
            } else {
                break;
            }
        }

        if (getActivity() != null && sort != null) {
            myAdapterJlptWord = new MyAdapterJlptWord(getActivity(),
                    R.layout.jlpt_w_items,
                    sort);
            listview_w.setAdapter(myAdapterJlptWord);
            setListViewHeightBasedOnChildren(listview_w);
        }
        listview_w.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int mPosition = position + increment_W * NUM_ITEMS_PAGE_W;
                new myAsyntask_tracuu1().execute(word.get(mPosition).getWord());
            }
        });
    }

    //set listview full items
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 30;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private class myAsyntask_tracuu1 extends AsyncTask<String, Void, Jaen> {

        @Override
        protected Jaen doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            Jaen javi = db.getWordJLPT(params[0]);
            return javi;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(Jaen javi) {
            super.onPostExecute(javi);
            // intent tracutu activity
            if (javi != null) {
                Intent myIntent = new Intent(getActivity(), TraCuuTuActivity.class);
                //Khai báo Bundle
                Bundle bundle = new Bundle();
                w_id = javi.getmId();
                wMean = javi.getmMean();
                wWord = javi.getmWord();
                wFavorite = javi.getmFavorite();
                wPhonetic = javi.getmPhonetic();

                //đưa dữ liệu riêng lẻ vào Bundle
                bundle.putString("vMean", wMean);
                bundle.putString("vTitle", wWord);
                bundle.putInt("mFavorite", wFavorite);
                bundle.putString("verbPhonetic", wPhonetic);
                if (wPhonetic != null) {
                    bundle.putString("vPhonetic", "「" + wPhonetic + "」");
                }
                bundle.putInt("wID", w_id);

                //Đưa Bundle vào Intent
                myIntent.putExtra("TraCuuFragment", bundle);
                //Mở Activity ResultActivity
                MenuActivity.startMyActivity(myIntent, getMenuActivity());
            }
        }
    }

    private class myAsyntask_tracuu2 extends AsyncTask<String, Void, ArrayList<Kanji>> {

        @Override
        protected ArrayList<Kanji> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            kanji = db.getKanjiJLPT(params[0]);
            return kanji;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(ArrayList<Kanji> kanjis) {
            super.onPostExecute(kanjis);
            pagingListViewHT();
            progressBar.setVisibility(View.GONE);
            isLoad2 = true;
        }
    }

    // My Asynctask_tracuu tab3
    private class myAsyntask_jlpt_word extends AsyncTask<String, Void, ArrayList<JlptWord>> {

        @Override
        protected ArrayList<JlptWord> doInBackground(String... params) {
            try {
                JlptDatabase db = new JlptDatabase(getContext());
                word = db.getJlptWord(params[0]);
            } catch (Exception e) {
            }
            return word;
        }

        @Override
        protected void onPostExecute(ArrayList<JlptWord> words) {
            super.onPostExecute(words);
            if (words != null && words.size() > 0) {
                pagingListViewW();
                progressBar.setVisibility(View.GONE);
                isLoad1 = true;
            }
        }

    }

    // Cấu hình tab
    private void loadTabs() {
        // Lấy Tabhost _id ra trước (cái này của built - in android

        // gọi lệnh setup
        tab.setup();
        TabHost.TabSpec spec;
        // Tạo tab3
        spec = tab.newTabSpec("jlpt_tab3");
        spec.setContent(R.id.jlpt_tab3);
        spec.setIndicator("Word");
        tab.addTab(spec);
        // Tạo tab1
        spec = tab.newTabSpec("jlpt_tab1");
        spec.setContent(R.id.jlpt_tab1);
        spec.setIndicator("Kanji");
        tab.addTab(spec);
        // Thiết lập tab mặc định được chọn ban đầu là tab 0
        tab.setCurrentTab(0);


        tab.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                switch (tabId) {
                    case "jlpt_tab3":
                        bt_flashcard.setVisibility(View.VISIBLE);
                        bt_flashcard.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent myIntent = new Intent(getActivity(), FlashCardWordActivity.class);
                                //Khai báo Bundle
                                Bundle bundle = new Bundle();

                                //đưa dữ liệu riêng lẻ vào Bundle
                                bundle.putInt("increment_w", increment_W);
                                bundle.putInt("page_count", pageCount_W);
                                bundle.putInt("total_items", TOTAL_LIST_ITEMS_W);
                                bundle.putString("level", nLevel);

                                //Đưa Bundle vào Intent
                                myIntent.putExtra("JLPT_Fragment_w", bundle);
                                //Mở Activity ResultActivity
                                MenuActivity.startMyActivity(myIntent, getMenuActivity());
                            }
                        });
                        if (!isLoad1) {
                            new myAsyntask_jlpt_word().execute(nLevel);
                            progressBar.setVisibility(View.VISIBLE);
                        }
                        break;
                    case "jlpt_tab1":
                        bt_flashcard.setVisibility(View.VISIBLE);
                        bt_flashcard.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent myIntent = new Intent(getActivity(), FlashCardActivity.class);
                                //Khai báo Bundle
                                Bundle bundle = new Bundle();

                                //đưa dữ liệu riêng lẻ vào Bundle
                                bundle.putInt("increment_HT", increment_HT);
                                bundle.putInt("page_count", pageCount_HT);
                                bundle.putInt("total_items", TOTAL_LIST_ITEMS_HT);
                                bundle.putString("level", nLevel);

                                //Đưa Bundle vào Intent
                                myIntent.putExtra("JLPT_Fragment", bundle);
                                //Mở Activity ResultActivity
                                MenuActivity.startMyActivity(myIntent, getMenuActivity());
                            }
                        });
                        if (!isLoad2) {
                            new myAsyntask_tracuu2().execute(nLevel);
                            progressBar.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        });
    }

    private void clickButtonTuyChon() {
        bt_tuychon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogSetting();
            }
        });
    }

    private void clickButtonFlashCard() {
        bt_flashcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), FlashCardWordActivity.class);
                //Khai báo Bundle
                Bundle bundle = new Bundle();

                //đưa dữ liệu riêng lẻ vào Bundle
                bundle.putInt("increment_w", increment_W);
                bundle.putInt("page_count", pageCount_W);
                bundle.putInt("total_items", TOTAL_LIST_ITEMS_W);
                bundle.putString("level", nLevel);

                //Đưa Bundle vào Intent
                myIntent.putExtra("JLPT_Fragment_w", bundle);
                //Mở Activity ResultActivity
                MenuActivity.startMyActivity(myIntent, getMenuActivity());
            }
        });
    }

    private void showdialogSetting() {
        // custom dialog
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.setting_dialog);
        isCbJLPT = MenuActivity.sharedPreferences.getBoolean("jlpt", true);
        cb_jlpt = (CheckBox) dialog.findViewById(R.id.cb_jlpt);
        cb_jlpt.setChecked(isCbJLPT);

        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                MenuActivity.sharedPreferences.edit().putBoolean("jlpt", cb_jlpt.isChecked()).commit();
                if (adapterHanTu != null) {
                    adapterHanTu.notifyDataSetChanged();
                }
            }
        });

    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if (items > columns) {
            x = items / columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight + 400;
        gridView.setLayoutParams(params);

    }

    public MenuActivity getMenuActivity() {
        return menuActivity;
    }

    public void setMenuActivity(MenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }
}