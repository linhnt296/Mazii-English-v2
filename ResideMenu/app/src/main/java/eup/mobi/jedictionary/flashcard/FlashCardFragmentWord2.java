package eup.mobi.jedictionary.flashcard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Grammar;
import eup.mobi.jedictionary.database.JlptWord;
import eup.mobi.jedictionary.residemenuactivity.FlashCardWordActivity;

/**
 * Created by chenupt@gmail.com on 2015/1/31.
 * Description TODO
 */
public class FlashCardFragmentWord2 extends Fragment {
    TextView tvWord, tvMean, tvPhonetic;
    int mPostion;
    String mWord = "", mMean = "", mPhonetic = "";
    JlptWord jlptWord;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup parentView = (ViewGroup) inflater.inflate(
                R.layout.flashcard_fragment_w2, container, false);
        tvWord = (TextView) parentView.findViewById(R.id.fcw_word);
        tvMean = (TextView) parentView.findViewById(R.id.fcw_mean);
        tvPhonetic = (TextView) parentView.findViewById(R.id.fcw_phonetic);
        if (FlashCardWordActivity.type == 0) {//hien thij flashcard Word
            if (FlashCardWordActivity.STATUS_FC == 2) {
                Bundle bundle2 = getArguments();
                mPostion = bundle2.getInt("mposition");
                jlptWord = FlashCardWordActivity.arrayList.get(mPostion);
            } else if (FlashCardWordActivity.STATUS_FC == 0) {
                if (FlashCardWordActivity.arrayList_1.size() > 0) {
                    Bundle bundle2 = getArguments();
                    mPostion = bundle2.getInt("position");
                    jlptWord = FlashCardWordActivity.arrayList_1.get(mPostion - 1);
                }
            } else if (FlashCardWordActivity.STATUS_FC == 1) {
                if (FlashCardWordActivity.arrayList_2.size() > 0) {
                    Bundle bundle2 = getArguments();
                    mPostion = bundle2.getInt("position");
                    jlptWord = FlashCardWordActivity.arrayList_2.get(mPostion - 1);
                }
            }
            mWord = jlptWord.getWord();
            mMean = jlptWord.getMean();
            mPhonetic = jlptWord.getPhonetic();

            tvWord.setText(mWord);
            tvMean.setText(mMean);
            tvPhonetic.setText("「" + mPhonetic + "」");
        }
        return parentView;
    }
}