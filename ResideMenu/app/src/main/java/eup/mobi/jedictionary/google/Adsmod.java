package eup.mobi.jedictionary.google;

import java.util.Random;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import android.app.Activity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class Adsmod {

    public static AdView banner(Activity activity, String bannerAd, float prob, boolean isPremium) {
        Random ran = new Random();
        final RelativeLayout layout = (RelativeLayout) activity
                .findViewById(R.id.adView);
        if (prob < ran.nextFloat()) {
            if (isHideBanner())
                layout.setVisibility(View.GONE);
            return null;
        }

        LayoutParams params = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

//		layout.setVisibility(View.VISIBLE);

        AdView mAdView = new AdView(activity);

        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(bannerAd);
        mAdView.setLayoutParams(params);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        adRequestBuilder.addTestDevice(activity.getString(R.string.test_device));
        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());
        mAdView.resume();
        if (isHideBanner() || isPremium) {
            layout.setVisibility(View.GONE);
        }

        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                MenuActivity.sharedPreferences.edit().putLong("time", System.currentTimeMillis()).commit();
                layout.setVisibility(View.GONE);
                Track.sendTrackerAction("Click Ads", "true");
            }
        });
        return mAdView;
    }

    public static AdView fragmentBanner(View fragment, String bannerAd, float prob, boolean isPremium) {
        Random ran = new Random();
        final RelativeLayout layout = (RelativeLayout) fragment
                .findViewById(R.id.adView);
        if (prob < ran.nextFloat()) {
            if (isHideBanner())
                layout.setVisibility(View.GONE);
            return null;
        }

        LayoutParams params = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
//        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

//		layout.setVisibility(View.VISIBLE);

        AdView mAdView = new AdView(fragment.getContext());

        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(bannerAd);
        mAdView.setLayoutParams(params);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

        // Optionally populate the ad request builder.
        // adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        adRequestBuilder.addTestDevice(fragment.getResources().getString(R.string.test_device));

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());
        mAdView.resume();
        if (isHideBanner() || isPremium) {
            layout.setVisibility(View.GONE);
        }

        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                MenuActivity.sharedPreferences.edit().putLong("time", System.currentTimeMillis()).commit();
                layout.setVisibility(View.GONE);
                Track.sendTrackerAction("Click Ads", "true");
            }
        });
        return mAdView;
    }

    public static AdView fragmentBannerJLPT(View fragment, String bannerAd, float prob, boolean isPremium) {
        Random ran = new Random();
        final RelativeLayout layout = (RelativeLayout) fragment
                .findViewById(R.id.adView);
        if (prob < ran.nextFloat()) {
            if (isHideBanner())
                layout.setVisibility(View.GONE);
            return null;
        }
        LayoutParams params = new LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

//		layout.setVisibility(View.VISIBLE);

        AdView mAdView = new AdView(fragment.getContext());

        mAdView.setAdSize(AdSize.SMART_BANNER);
        mAdView.setAdUnitId(bannerAd);
        mAdView.setLayoutParams(params);

        // Create an ad request.
        AdRequest.Builder adRequestBuilder = new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        adRequestBuilder.addTestDevice(fragment.getResources().getString(R.string.test_device));

        // Add the AdView to the view hierarchy.
        layout.addView(mAdView);

        // Start loading the ad.
        mAdView.loadAd(adRequestBuilder.build());
        mAdView.resume();
        if (isHideBanner() || isPremium) {
            layout.setVisibility(View.GONE);
        }

        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                MenuActivity.sharedPreferences.edit().putLong("time", System.currentTimeMillis()).commit();
                layout.setVisibility(View.GONE);
                Track.sendTrackerAction("Click Ads", "true");
            }
        });
        return mAdView;
    }

    private static boolean isHideBanner() {
        // check ads clicked
        long lastTimeClickedAds = MenuActivity.sharedPreferences.getLong("time", 0);
        long probAds = MenuActivity.sharedPreferences.getLong("probAds", 0);
        long currentTime = System.currentTimeMillis();
        if (currentTime < (lastTimeClickedAds + probAds)) {
            return true;
        }
        return false;
    }
}
