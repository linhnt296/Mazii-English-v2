package eup.mobi.jedictionary.tapviet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.util.JsonWriter;
import android.view.MotionEvent;
import android.view.View;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by thanh on 10/16/2015.
 */
public class CanvasViewDialog extends View {
    public int width, height;
    private Bitmap bitmap;
    private Canvas mCanvas;
    private Path path;
    Context context;
    private Paint paint;
    private float mX, mY;
    private static final float TOLERANCE = 1;
    private ArrayList<Long> currentLineSig1;
    private ArrayList<Long> currentLineSig2;
    private ArrayList<Long> currentLineSig3;
    private ArrayList<Path> pathArrayList;
    private ArrayList<Path> undoPath;
    private ArrayList<Integer> undoInk;
    private int startLength = 0, currentLength = 0;
    private long startDrawTime = 0, currentDrawTime = 0;
    private HandWriteDialog handWriteDialog;
    private ArrayList<Ink> inks;
    private Ink ink;

    public HandWriteDialog getHandWriteDialog() {
        return handWriteDialog;
    }

    public void setHandWriteDialog(HandWriteDialog handWriteDialog) {
        this.handWriteDialog = handWriteDialog;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        long x = (long) event.getX();
        long y = (long) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                invalidate();
                break;
        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (Path p : pathArrayList) {
            canvas.drawPath(p, paint);
        }
        canvas.drawPath(path, paint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(bitmap);
    }

    public CanvasViewDialog(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.context = context;
        path = new Path();
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(10f);
        currentLineSig1 = new ArrayList<Long>();
        currentLineSig2 = new ArrayList<Long>();
        currentLineSig3 = new ArrayList<Long>();
        pathArrayList = new ArrayList<Path>();
        undoPath = new ArrayList<Path>();
        undoInk = new ArrayList<Integer>();
        inks = new ArrayList<Ink>();
    }

    private void startTouch(long x, long y) {
        ink = new Ink();
        currentLineSig1 = new ArrayList<Long>();
        currentLineSig2 = new ArrayList<Long>();
        currentLineSig3 = new ArrayList<Long>();
        handWriteDialog.resultList.clear();
        handWriteDialog.resultAdapter.notifyDataSetChanged();
        path.moveTo(x, y);
        mX = x;
        mY = y;
        currentLineSig1.add(x);
        currentLineSig2.add(y);
        currentLength = 1;
        if (startDrawTime == 0) {
            startDrawTime = new Date().getTime();
            currentLineSig3.add(0l);
        } else {
            currentDrawTime = new Date().getTime();
            currentLineSig3.add(currentDrawTime - startDrawTime);
        }
        undoPath.clear();
    }

    private void moveTouch(long x, long y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOLERANCE || dy >= TOLERANCE) {
            path.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
        currentDrawTime = new Date().getTime();
        currentLineSig1.add(x);
        currentLineSig2.add(y);
        currentLength++;
        currentLineSig3.add(currentDrawTime - startDrawTime);
    }

    public void clearCanvas() {
        handWriteDialog.resultList.clear();
        handWriteDialog.resultAdapter.notifyDataSetChanged();
        path.reset();
        pathArrayList.clear();
        undoInk.clear();
        invalidate();
        currentLineSig1.clear();
        currentLineSig2.clear();
        currentLineSig3.clear();
        inks.clear();

    }

    private void upTouch() {
        ink.setCurrentLineSig1(currentLineSig1);
        ink.setCurrentLineSig2(currentLineSig2);
        ink.setCurrentLineSig3(currentLineSig3);
        inks.add(ink);
        undoInk.add(currentLength);
        path.lineTo(mX, mY);
        pathArrayList.add(path);
        path = new Path();
        RequestDialog requestTask = new RequestDialog();
        requestTask.setHandWriteDialog(handWriteDialog);
        String json = jsonCreat();
        if (json != null) {
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB){
                 requestTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
            }else {
                requestTask.execute(json);
            }
        }

    }

    public String jsonCreat() {
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonWriter(stringWriter);

        try {
            jsonWriter.beginObject(); // {
            jsonWriter.name("api_level").value("537.36");
            jsonWriter.name("app_version").value(0.4);
            jsonWriter.name("input_type").value(0);
            jsonWriter.name("device").value("5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36");
            jsonWriter.name("options").value("enable_pre_space");
            jsonWriter.name("requests");
            jsonWriter.beginArray(); // [
            jsonWriter.beginObject(); // {
            jsonWriter.name("writing_guide");
            jsonWriter.beginObject(); // {
            jsonWriter.name("writing_area_width").value(this.width);
            jsonWriter.name("writing_area_height").value(this.height);
            jsonWriter.endObject(); // }
            jsonWriter.name("pre_context").value("");
            jsonWriter.name("max_num_results").value(10);
            jsonWriter.name("max_completions").value(0);
            jsonWriter.name("ink");
            jsonWriter.beginArray(); // [
            for (int a = 0; a < inks.size(); a++) {
                jsonWriter.beginArray(); // [
                jsonWriter.beginArray(); // [
                ArrayList<Long> tempCurrentLine = inks.get(a).getCurrentLineSig1();
                for (int i = 0; i < tempCurrentLine.size(); i++) {
                    jsonWriter.value(tempCurrentLine.get(i));
                }
                jsonWriter.endArray(); // ]
                jsonWriter.beginArray(); // [
                tempCurrentLine = inks.get(a).getCurrentLineSig2();
                for (int i = 0; i < tempCurrentLine.size(); i++) {
                    jsonWriter.value(tempCurrentLine.get(i));
                }
                jsonWriter.endArray(); // ]
                jsonWriter.beginArray(); // [
                tempCurrentLine = inks.get(a).getCurrentLineSig3();
                for (int i = 0; i < tempCurrentLine.size(); i++) {
                    jsonWriter.value(tempCurrentLine.get(i));
                }
                jsonWriter.endArray(); // ]
                jsonWriter.endArray(); // ]
            }
            jsonWriter.endArray(); // ]
            jsonWriter.endObject(); // }
            jsonWriter.endArray(); // ]
            jsonWriter.endObject(); // }

            jsonWriter.flush();
            jsonWriter.close();
            String json = stringWriter.toString();
//            Log.d("Json", json);
            return json;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void undo() {
        if (inks.size() > 0) {
            handWriteDialog.resultList.clear();
            handWriteDialog.resultAdapter.notifyDataSetChanged();
            inks.remove(inks.get(inks.size() - 1));
            String json = jsonCreat();
            RequestDialog task = new RequestDialog();
            task.setHandWriteDialog(handWriteDialog);
            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.HONEYCOMB){
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
            }else {
                task.execute(json);
            }
            if (pathArrayList.size() > 0) {
                undoPath.add(pathArrayList.remove(pathArrayList.size() - 1));
                invalidate();
            }
        }

    }

    public void removeInk(int number) {
        for (int i = 0; i < number; i++) {
            currentLineSig1.remove(currentLineSig1.size() - 1);
            currentLineSig2.remove(currentLineSig2.size() - 1);
            currentLineSig3.remove(currentLineSig3.size() - 1);

        }
    }

    @Override
    public void addOnLayoutChangeListener(OnLayoutChangeListener listener) {
        super.addOnLayoutChangeListener(listener);
    }
}

