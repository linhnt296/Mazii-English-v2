package eup.mobi.jedictionary.myadapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.module.Entry;

import java.util.List;

/**
 * Created by dovantiep on 13/01/2016.
 */
public class MyWordAdapter extends RecyclerView.Adapter<MyWordAdapter.ViewHolder> {

    private List<Entry> entryList;

    public interface OnItemClick {
        void onItemClick(int pos);

        void onDelClick(int pos);
    }

    private OnItemClick onItemClick;

    public MyWordAdapter(List<Entry> entryList, OnItemClick onItemClick) {
        this.entryList = entryList;
        this.onItemClick = onItemClick;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sotay_item_word, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Entry entry = entryList.get(position);
        holder.tvTitle.setText(entry.getWord());
        holder.tvDate.setText(entry.getDate());
        holder.tvPhonetic.setText(entry.getPhonetic());
        holder.tvMean.setText(entry.getMean());
    }

    @Override
    public int getItemCount() {
        return entryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView tvTitle, tvDate, tvPhonetic, tvMean;
        ImageButton ibDelWord;

        public ViewHolder(View v) {
            super(v);
            view = v;
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            tvDate = (TextView) v.findViewById(R.id.tvDate);
            tvPhonetic = (TextView) v.findViewById(R.id.tvPhonetic);
            tvMean = (TextView) v.findViewById(R.id.tvMean);
            ibDelWord = (ImageButton) v.findViewById(R.id.ibDelWord);
            ibDelWord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onDelClick(getLayoutPosition());
                }
            });
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClick.onItemClick(getLayoutPosition());
                }
            });
        }
    }
}
