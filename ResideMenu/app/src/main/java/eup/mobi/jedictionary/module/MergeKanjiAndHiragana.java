package eup.mobi.jedictionary.module;

import eup.mobi.jedictionary.database.MergeObj;

import java.util.ArrayList;

/**
 * Created by nguye on 10/15/2015.
 */
public class MergeKanjiAndHiragana {

    public static int getLengthHiragana(String kanji) {
        if (kanji == null || kanji.length() == 0) {
            return 0;
        }

        int result = 0;
        for (int i = 0; i < kanji.length(); i++) {
            char c = kanji.charAt(i);
            if (c == 'ん' ||
                    c == 'ぁ' ||
                    c == 'ぃ' ||
                    c == 'ぇ' ||
                    c == 'ぅ' ||
                    c == 'ぉ' ||
                    c == 'ゅ' ||
                    c == 'ょ') {
                continue;
            }

            result++;
        }

        return result;
    }

    public static ArrayList<MergeObj> mergeKanjiAndHiragana(String kanji, String hiragana) {
        if (kanji == "" || hiragana == "")
            return null;

        if (Language.isJapanese(kanji) == false) {
            return null;
        }


        if (kanji.indexOf(' ') != -1) {
            kanji = kanji.replace(" ", "");
        }

        if (hiragana.indexOf(' ') != -1) {
            hiragana = hiragana.replace(" ", "");
        }

//        re = new RegExp('　', 'g');
        if (kanji.indexOf('　') != -1) {
            kanji = kanji.replace("　", "");
        }

        if (hiragana.indexOf('　') != -1) {
            hiragana = hiragana.replace("　", "");
        }

        ArrayList<MergeObj> result = new ArrayList<MergeObj>();
        String currentKanji = "";
        String currentHiragana = "";
        String nextHiraganaChar = "";

        int j = 0;
        for (int i = 0; i < kanji.length(); i++) {
            char c = kanji.charAt(i);
            if (Language.isKanji(String.valueOf(c)) || Language.isKatakan(String.valueOf(c))) {
                if (currentKanji == "" && currentHiragana != "") {
                    MergeObj mergedObj = new MergeObj(currentHiragana, "");
                    result.add(mergedObj);
                    currentHiragana = "";
                    j += currentHiragana.length();
                }
                currentKanji += c;
            } else {
                if (currentKanji == "") {
                    currentHiragana += c;
                    j++;
                } else {

                    nextHiraganaChar = String.valueOf(c);
                    while (j < hiragana.length()) {
                        if (getLengthHiragana(currentHiragana) < currentKanji.length() ||
                                !String.valueOf(hiragana.charAt(j)).equals(nextHiraganaChar)) {
                            currentHiragana += hiragana.charAt(j);
                        } else if ( String.valueOf(hiragana.charAt(j)).equals(nextHiraganaChar)) {
                            MergeObj mergedObj = new MergeObj(currentKanji, currentHiragana);
                            result.add(mergedObj);

                            currentKanji = "";
                            currentHiragana = String.valueOf(c);
                            j++;
                            break;
                        }

                        j++;
                    }

                    if (j == hiragana.length() && currentKanji != "") {
                        MergeObj mergedObj = new MergeObj(currentKanji, currentHiragana);
                        result.add(mergedObj);
                    }

                    if (j == hiragana.length() && i < kanji.length() - 1) {
                        // this case is parse error
                        return null;
                    }
                }
            }
        }

        if (currentKanji != "") {
            while (j < hiragana.length()) {
                currentHiragana += hiragana.charAt(j);
                j++;
            }

            MergeObj mergedObj = new MergeObj(currentKanji, currentHiragana);

            result.add(mergedObj);
        } else if (currentHiragana != "") {

            MergeObj mergedObj = new MergeObj(currentHiragana, "");
            result.add(mergedObj);
        }

        return result;
    }
}
