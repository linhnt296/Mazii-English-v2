package eup.mobi.jedictionary.module;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by nguye on 10/7/2015.
 */
public class Language {
    public static boolean isVietnamese(String word) {
        char[] vietnameseChars = new char[]{'à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ', 'è', 'é', 'ẹ', 'ẻ', 'ẽ', 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ', 'ì', 'í', 'ị', 'ỉ', 'ĩ', 'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ', 'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ', 'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ', 'đ', 'À', 'Á', 'Ạ', 'Ả', 'Ã', 'Â', 'Ầ', 'Ấ', 'Ậ', 'Ẩ', 'Ẫ', 'Ă', 'Ằ', 'Ắ', 'Ặ', 'Ẳ', 'Ẵ', 'È', 'É', 'Ẹ', 'Ẻ', 'Ẽ', 'Ê', 'Ề', 'Ế', 'Ệ', 'Ể', 'Ễ', 'Ì', 'Í', 'Ị', 'Ỉ', 'Ĩ', 'Ò', 'Ó', 'Ọ', 'Ỏ', 'Õ', 'Ô', 'Ồ', 'Ố', 'Ộ', 'Ổ', 'Ỗ', 'Ơ', 'Ờ', 'Ớ', 'Ợ', 'Ở', 'Ỡ', 'Ù', 'Ú', 'Ụ', 'Ủ', 'Ũ', 'Ư', 'Ừ', 'Ứ', 'Ự', 'Ử', 'Ữ', 'Ỳ', 'Ý', 'Ỵ', 'Ỷ', 'Ỹ', 'Đ'};
        int len = word.length();
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < vietnameseChars.length; j++)
                if (vietnameseChars[j] == (word.charAt(i))) {
                    return true;
                }
        }

        return false;

    }

    public static boolean isJapanese(String keyword) {
        if (keyword != null) {
            int len = keyword.length();

            for (int i = 0; i < len; i++) {
                if (isKanji(String.valueOf(keyword.charAt(i))) ||
                        isHiragana(String.valueOf(keyword.charAt(i))) ||
                        isKatakan(String.valueOf(keyword.charAt(i)))) {
                    return true;
                }
            }

        }

        return false;
    }

    private boolean isRomanji(String c) {
        int charcode = Character.codePointAt(c, 0);
        if (charcode >= 0x0020 && charcode <= 0x007e) {
            return true;
        }

        return false;
    }

    public static boolean isKanji(String c) {
        if (c.equals("々")) {
            return true;
        }
        int charcode = Character.codePointAt(c, 0);
        if (charcode >= 0x4E00 && charcode <= 0x9FBF) {
            return true;
        }
        return false;
    }

    private static boolean isHiragana(String c) {
        int charcode = Character.codePointAt(c, 0);
        if (charcode >= 0x3040 && charcode <= 0x309F) {
            return true;
        }
        return false;
    }

    public static boolean isKatakan(String c) {
        int charcode = Character.codePointAt(c, 0);
        if (charcode >= 0x30A0 && charcode <= 0x30FF) {
            return true;
        }

        return false;
    }

    private static HashMap<String, String> map = new HashMap<String, String>();

    private static void initSearch() {
        map.put("à", "à");
        map.put("á", "á");
        map.put("ạ", "ạ");
        map.put("ả", "ả");
        map.put("ã", "ã");
//â
        map.put("ậ̀", "ầ");
        map.put("ấ", "ấ");
        map.put("ậ", "ậ");
        map.put("ẩ", "ẩ");
        map.put("ẫ", "ẫ");
//ă
        map.put("ằ", "ằ");
        map.put("ắ", "ắ");
        map.put("ặ", "ặ");
        map.put("ẳ", "ẳ");
        map.put("ẵ", "ẵ");
        map.put("è", "è");
        map.put("é", "é");
        map.put("ẹ", "ẹ");
        map.put("ẻ", "ẻ");
        map.put("ẽ", "ẽ");
//ê
        map.put("ề", "ề");
        map.put("ế", "ế");
        map.put("ệ", "ệ");
        map.put("ể", "ể");
        map.put("ễ", "ễ");
        map.put("ì", "ì");
        map.put("í", "í");
        map.put("ị", "ị");
        map.put("ỉ", "ỉ");
        map.put("ĩ", "ĩ");
        map.put("ò", "ò");
        map.put("ó", "ó");
        map.put("ọ", "ọ");
        map.put("ỏ", "ỏ");
        map.put("õ", "õ");
//ô
        map.put("ồ", "ồ");
        map.put("ố", "ố");
        map.put("ộ", "ộ");
        map.put("ổ", "ổ");
        map.put("ỗ", "ỗ");
//ơ
        map.put("ờ", "ờ");
        map.put("ớ", "ớ");
        map.put("ợ", "ợ");
        map.put("ở", "ở");
        map.put("ỡ", "ỡ");
        map.put("ù", "ù");
        map.put("ú", "ú");
        map.put("ụ", "ụ");
        map.put("ủ", "ủ");
        map.put("ũ", "ũ");
        map.put("ừ", "ừ");
        map.put("ứ", "ứ");
        map.put("ự", "ự");
        map.put("ử", "ử");
        map.put("ữ", "ữ");
        map.put("ỳ", "ỳ");
        map.put("ý", "ý");
        map.put("ỵ", "ỵ");
        map.put("ỷ", "ỷ");
        map.put("ỹ", "ỹ");
    }

    public static String convertToVN(String q) {
        if (TextUtils.isEmpty(q))
            return q;
        String newQ = null;
        if (map.isEmpty()) {
            initSearch();
        }
        Set<String> set = map.keySet();
        Iterator<String> iterator = set.iterator();
        String t;
        newQ = q;
        while (iterator.hasNext()) {
            t = iterator.next();
//            boolean c = q.contains(t);
            newQ = newQ.replace(t, map.get(t));
        }

        return newQ;
    }
}
