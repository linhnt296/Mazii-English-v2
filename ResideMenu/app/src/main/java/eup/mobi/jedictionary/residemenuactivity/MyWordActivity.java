package eup.mobi.jedictionary.residemenuactivity;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.Kanji;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.database.MyWordDatabase;
import eup.mobi.jedictionary.fast.search.FastSearchService;
import eup.mobi.jedictionary.module.Entry;
import eup.mobi.jedictionary.myadapter.MyWordAdapter;
import eup.mobi.jedictionary.service.WidgetProvider;

import java.util.List;

public class MyWordActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private int id;
    private TextView tvNoResult;
    MyWordAdapter adapter;
    MyWordDatabase db;
    private List<Entry> entryList;
    String name;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_word);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        tvNoResult = (TextView) findViewById(R.id.tvNoResult);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        id = getIntent().getIntExtra("id", 0);
        name = getIntent().getStringExtra("name");
        getSupportActionBar().setTitle(name);

        new AsyncTask<Void, Void, List<Entry>>() {
            @Override
            protected List<Entry> doInBackground(Void... params) {
                db = new MyWordDatabase(MyWordActivity.this);
                return db.qEntry(id);
            }

            @Override
            protected void onPostExecute(final List<Entry> entryList) {
                super.onPostExecute(entryList);
                MyWordActivity.this.entryList = entryList;
                if (entryList.isEmpty()) {
                    recyclerView.setVisibility(View.INVISIBLE);
                    tvNoResult.setVisibility(View.VISIBLE);
                    return;
                }
                adapter = new MyWordAdapter(entryList, new MyWordAdapter.OnItemClick() {
                    @Override
                    public void onItemClick(int pos) {
//                        Toast.makeText(MyWordActivity.this, "idEntry: " + entryList.get(pos).getIdEntry(), Toast.LENGTH_SHORT).show();
                        switch (entryList.get(pos).getType()) {
                            case "jaen":
                                new myAsyntask_tracuu1_ja().execute(String.valueOf(entryList.get(pos).getIdEntry()));
                                break;
                            case "kanji":
                                new myAsyntask_tracuu2().execute(String.valueOf(entryList.get(pos).getIdEntry()));
                                break;
                        }
                    }

                    @Override
                    public void onDelClick(int pos) {
                        db.deleleEntry(entryList.get(pos).getId());
                        entryList.remove(pos);
                        adapter.notifyItemRemoved(pos);
                        // notify widget
                        notifyWidget();

                        if (entryList.isEmpty()) {
                            recyclerView.setVisibility(View.INVISIBLE);
                            tvNoResult.setVisibility(View.VISIBLE);
                        }
                    }
                });
                recyclerView.setAdapter(adapter);
            }
        }.execute();
        if (MenuActivity.sharedPreferences != null) {
            boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
            String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
            float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
//            Adsmod.banner(this, banner, probBanner, isPremium);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else if (item.getItemId() == R.id.action_flashcard) {
            if (entryList.size() > 0) {
                Intent intent = new Intent(this, FlashCardMyWordActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putInt("id_category", entryList.get(0).getIdCategory());

                intent.putExtra("FlashCardMyWord", bundle1);
                //Mở Activity ResultActivity
                back = true;
                MenuActivity.startMyActivity(intent, activity);
            } else {
                Toast.makeText(activity, getString(R.string.flashcard_entry_null), Toast.LENGTH_SHORT).show();
            }
            return true;
        } else if (item.getItemId() == R.id.action_delALl) {
            if (entryList.isEmpty()) {
                Toast.makeText(MyWordActivity.this, R.string.empty_list, Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MyWordActivity.this);

                builder.setMessage(getString(R.string.del_my_word, entryList.size(), name));
                builder.setNegativeButton(R.string.contine, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        db.deleleEntryCategory(entryList.get(0).getIdCategory());
                        entryList.clear();
                        recyclerView.setAdapter(null);
                        recyclerView.setVisibility(View.INVISIBLE);
                        tvNoResult.setVisibility(View.VISIBLE);
                    }
                });
                builder.setPositiveButton(R.string.ingore, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_word, menu);
        return true;
    }

    private class myAsyntask_tracuu1_ja extends AsyncTask<String, Void, Jaen> {

        @Override
        protected Jaen doInBackground(String... params) {
            MyDatabase db = new MyDatabase(activity);
            Jaen javi = db.getJaenMyWord(params[0]);
            return javi;
        }

        @Override
        protected void onPostExecute(Jaen javi) {
            super.onPostExecute(javi);
            // intent tracutu activity
            if (javi != null) {
                Intent myIntent = new Intent(activity, TraCuuTuActivity.class);
                //Khai báo Bundle
                Bundle bundle = new Bundle();
                int wID = javi.getmId();
                String vMean = javi.getmMean();
                String vTitle = javi.getmWord();
                int mFavorite = javi.getmFavorite();
                String mPhonetic = javi.getmPhonetic();

                //đưa dữ liệu riêng lẻ vào Bundle
                bundle.putString("vMean", vMean);
                bundle.putString("vTitle", vTitle);
                bundle.putInt("mFavorite", mFavorite);
                bundle.putString("verbPhonetic", mPhonetic);
                if (mPhonetic != null) {
                    bundle.putString("vPhonetic", "「" + mPhonetic + "」");
                }
                bundle.putInt("wID", wID);

                //Đưa Bundle vào Intent
                myIntent.putExtra("TraCuuFragment", bundle);
                //Mở Activity ResultActivity
                back = true;
                MenuActivity.startMyActivity(myIntent, MyWordActivity.this);
            }
        }
    }

    private class myAsyntask_tracuu2 extends AsyncTask<String, Void, Kanji> {

        @Override
        protected Kanji doInBackground(String... params) {
            MyDatabase db = new MyDatabase(activity);
            Kanji kanji = db.getKanjiMyWord(params[0]);
            return kanji;
        }

        @Override
        protected void onPostExecute(Kanji kanji) {
            super.onPostExecute(kanji);
            String mKanji, mMean, mKun, mOn, svg_kanji, mStroke_count, mLevel, mCompDetail, mDetail, mExamples;
            int mFavorite, wID;
            Intent myIntent = new Intent(activity, TraCuuHanTuActivity.class);
            //Khai báo Bundle
            Bundle bundle = new Bundle();
            mKanji = kanji.getmKanji();
            if (kanji.getmMean() != null)
                mMean = kanji.getmMean();
            else mMean = "";
            if (kanji.getmKun() != null) {
                mKun = kanji.getmKun();
            } else mKun = "";
            if (kanji.getmImg() != null) {
                svg_kanji = kanji.getmImg();
            } else {
                svg_kanji = "";
            }
            if (kanji.getmOn() != null) {
                mOn = kanji.getmOn();
            } else mOn = "";
            if (String.valueOf(kanji.getmStroke_count()) != null) {
                mStroke_count = String.valueOf(kanji.getmStroke_count());
            } else mStroke_count = "";
            if (String.valueOf(kanji.getmLevel()) != null) {
                mLevel = String.valueOf(kanji.getmLevel());
            } else mLevel = "";
            if (kanji.getmCompdetail() != null) {
                mCompDetail = kanji.getmCompdetail();
            } else mCompDetail = "";
            if (kanji.getmDetail() != null) {
                mDetail = kanji.getmDetail();
            } else mDetail = "";
            if (kanji.getmExamples() != null) {
                mExamples = kanji.getmExamples();
            } else mExamples = "";
            mFavorite = kanji.getmFavorite();
            wID = kanji.getmId();

            //đưa dữ liệu riêng lẻ vào Bundle
            bundle.putString("mKanji", mKanji);
            bundle.putString("mMean", mMean);
            bundle.putString("mKun", mKun);
            bundle.putString("svg_kanji", svg_kanji);
            bundle.putString("mOn", mOn);
            bundle.putString("mStroke_count", mStroke_count);
            bundle.putString("mLevel", mLevel);
            bundle.putString("mCompDetail", mCompDetail);
            bundle.putString("mDetail", mDetail);
            bundle.putString("mExamples", mExamples);
            bundle.putInt("mFavorite", mFavorite);
            bundle.putInt("wID", wID);

            //Đưa Bundle vào Intent
            myIntent.putExtra("TraCuuFragment_2", bundle);
            //Mở Activity ResultActivity
            back = true;
            MenuActivity.startMyActivity(myIntent, MyWordActivity.this);
        }
    }

    private boolean back;

    @Override
    public void onBackPressed() {
        back = true;
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        back = false;
        stopService(new Intent(this, FastSearchService.class));
    }

    @Override
    protected void onStop() {
        if (!back && MenuActivity.sharedPreferences != null && MenuActivity.sharedPreferences.getBoolean("fast_search", false)) {
            if (Build.VERSION.SDK_INT < 23) {
                startService(new Intent(this, FastSearchService.class));
            } else if (Build.VERSION.SDK_INT >= 23 && Settings.canDrawOverlays(this)) {
                startService(new Intent(this, FastSearchService.class));
            }
//            Toast.makeText(this, "start service", Toast.LENGTH_SHORT).show();
        }
        super.onStop();
    }

    private void notifyWidget() {
        // notify data set changed widget
        final AppWidgetManager mgr = AppWidgetManager.getInstance(this);
        final ComponentName cn = new ComponentName(this,
                WidgetProvider.class);
        mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn),
                R.id.page_flipper);
    }
}
