package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import eup.mobi.jedictionary.R;

import java.util.ArrayList;

/**
 * Created by nguye on 8/21/2015.
 */
public class MyAdapterChuyenNganh extends
        ArrayAdapter<String> {
    Activity context = null;
    ArrayList<String> myArray = null;
    int layoutId;
    LayoutInflater inflater;

    public MyAdapterChuyenNganh(Activity context,
                                int layoutId,
                                ArrayList<String> arr) {
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_display = (TextView) convertView.findViewById(R.id.tv_tracuutab1);
            holder.iv_icon = (ImageView) convertView.findViewById(R.id.ivIcon);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final String vj = myArray.get(position);
        holder.tv_display.setText(vj);
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getRandomColor();
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(String.valueOf(vj.charAt(0)), color);
        holder.iv_icon.setImageDrawable(drawable);

        return convertView;
    }

    class ViewHolder {
        TextView tv_display;
        ImageView iv_icon;
    }
}
