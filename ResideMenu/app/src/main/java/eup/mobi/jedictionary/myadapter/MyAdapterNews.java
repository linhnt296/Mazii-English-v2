package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.docbao.JResult;
import eup.mobi.jedictionary.docbao.JValue;
import eup.mobi.jedictionary.R;

import java.util.ArrayList;

public class MyAdapterNews extends ArrayAdapter<JResult> {
    Activity context = null;
    ArrayList<JResult> myArray = null;
    int layouID;
    LayoutInflater inflater;

    public MyAdapterNews(Activity context, int layouID, ArrayList<JResult> arrayList) {
        super(context, 0, arrayList);
        this.context = context;
        this.myArray = arrayList;
        this.layouID = layouID;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layouID, parent, false);
            holder = new ViewHolder();
            holder.tv_title = (TextView) convertView.findViewById(R.id.title_items);
            holder.tv_pubdate = (TextView) convertView.findViewById(R.id.pubdate_items);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final JResult jResult = myArray.get(position);
        if (jResult.getKey() != null) {
            holder.tv_pubdate.setText(jResult.getKey());

        }
        if (jResult.getValue() != null) {
            JValue jValue = jResult.getValue();
            if (jValue.getTitle() != null) {
                String convert = Html.fromHtml(jValue.getTitle()).toString();
                holder.tv_title.setText(convert);
            }
            MyDatabase db = new MyDatabase(getContext());
            if (db.checkNewsReaded(jValue.getId())) {
                jValue.setIsRead(true);
            }
            if (jValue.isRead()) {
                holder.tv_title.setPaintFlags(holder.tv_title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {

            }

        }
        return convertView;
    }

    class ViewHolder {
        TextView tv_title, tv_pubdate;
    }
}