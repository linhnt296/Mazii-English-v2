package eup.mobi.jedictionary.history;

/**
 * Created by nguye on 10/30/2015.
 */
public class HistoryKanji {
    String hKanji;
    long hDate;

    public String gethWords() {
        return hKanji;
    }

    public void sethWords(String hKanji) {
        this.hKanji = hKanji;
    }

    public long gethDate() {
        return hDate;
    }

    public void sethDate(long hDate) {
        this.hDate = hDate;
    }

    public HistoryKanji(String hKanji, long hDate) {
        this.hKanji = hKanji;
        this.hDate = hDate;
    }
}
