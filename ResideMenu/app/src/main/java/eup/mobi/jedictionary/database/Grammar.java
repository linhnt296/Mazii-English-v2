package eup.mobi.jedictionary.database;

/**
 * Created by nguye on 8/21/2015.
 */
public class Grammar {
    private int mId;
    private String mStruct;
    private int mFavorite;
    private int remmember;

    public Grammar(String mStruct, int mFavorite, String mStruct_vi, String mDetail, String mLevel) {
        this.mStruct = mStruct;
        this.mFavorite = mFavorite;
        this.mStruct_vi = mStruct_vi;
        this.mDetail = mDetail;
        this.mLevel = mLevel;
    }

    public int getRemmember() {
        return remmember;
    }

    public void setRemmember(int remmember) {
        this.remmember = remmember;
    }

    public int getmFavorite() {

        return mFavorite;
    }

    public void setmFavorite(int mFavorite) {
        this.mFavorite = mFavorite;
    }

    public String getmStruct_vi() {
        return mStruct_vi;
    }

    public void setmStruct_vi(String mStruct_vi) {
        this.mStruct_vi = mStruct_vi;
    }

    private String mStruct_vi;
    private String mDetail;
    private String mLevel;


    public Grammar(int mId, String mStruct, String mDetail, String mLevel) {
        this.mId = mId;
        this.mStruct = mStruct;
        this.mDetail = mDetail;
        this.mLevel = mLevel;
    }

    public Grammar(String mStruct, String mDetail, String mLevel, String mStruct_vi) {
        this.mStruct = mStruct;
        this.mDetail = mDetail;
        this.mLevel = mLevel;
        this.mStruct_vi = mStruct_vi;
    }

    public Grammar(int mID,String mStruct, String mDetail, String mLevel, String mStruct_vi, int mFavorite) {
        this.mStruct = mStruct;
        this.mDetail = mDetail;
        this.mLevel = mLevel;
        this.mStruct_vi = mStruct_vi;
        this.mFavorite = mFavorite;
        this.mId = mID;
    }
    public Grammar(int mID,String mStruct, String mDetail, String mLevel, String mStruct_vi, int mFavorite, int mRemember) {
        this.mStruct = mStruct;
        this.mDetail = mDetail;
        this.mLevel = mLevel;
        this.mStruct_vi = mStruct_vi;
        this.mFavorite = mFavorite;
        this.mId = mID;
        this.remmember = mRemember;
    }




    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmStruct() {
        return mStruct;
    }

    public void setmStruct(String mStruct) {
        this.mStruct = mStruct;
    }

    public String getmDetail() {
        return mDetail;
    }

    public void setmDetail(String mDetail) {
        this.mDetail = mDetail;
    }

    public String getmLevel() {
        return mLevel;
    }

    public void setmLevel(String mLevel) {
        this.mLevel = mLevel;
    }
}
