package eup.mobi.jedictionary.kanjirecognize;

public interface AddingStrokeListener {
        void onStrokeAddListener();
    }