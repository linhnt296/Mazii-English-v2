package eup.mobi.jedictionary.database;

import android.text.TextUtils;

/**
 * Created by nguye on 8/21/2015.
 */
public class Kanji {

    private int mId;
    private String mKanji;
    private String mMean;
    private int mLevel;
    private String mOn;
    private String mKun;
    private int mFreq;
    private int mStroke_count;
    private int mFavorite;
    private int mRemember;
    private String mExamples = "", mImg, mCompdetail, mDetail;

    public String getmExamples() {
        return mExamples;
    }

    public String getmImg() {
        return mImg;
    }

    public String getmCompdetail() {
        return mCompdetail;
    }

    public String getmDetail() {
        return mDetail;
    }

    public Kanji(int mFavorite, int mStroke_count, int mFreq, String mKun, String mOn, int mLevel, String mMean, String mKanji) {
        this.mFavorite = mFavorite;
        this.mStroke_count = mStroke_count;
        this.mFreq = mFreq;
        this.mKun = mKun;
        this.mOn = mOn;
        this.mLevel = mLevel;
        setmMean(mMean);
        this.mKanji = mKanji;
    }

    public int getmFavorite() {

        return mFavorite;
    }

    public void setmFavorite(int mFavorite) {
        this.mFavorite = mFavorite;
    }

    public int getmFreq() {
        return mFreq;
    }

    public void setmFreq(int mFreq) {
        this.mFreq = mFreq;
    }


    public int getmStroke_count() {
        return mStroke_count;
    }

    public void setmStroke_count(int mStroke_count) {
        this.mStroke_count = mStroke_count;
    }


    public Kanji(int mId, String mKanji, String mMean, int mLevel, String mOn, String mKun, String mImg, String mDetail, String mShort) {
        this.mId = mId;
        this.mKanji = mKanji;
        setmMean(mMean);
        this.mLevel = mLevel;
        this.mOn = mOn;
        this.mKun = mKun;
    }

    public Kanji(int mId, String mKanji, String mMean, int mLevel, String mOn, String mKun, int mFreq, int mStroke_count, int mFavorite, int mRemember) {
        this.mId = mId;
        this.mKanji = mKanji;
        setmMean(mMean);
        this.mLevel = mLevel;
        this.mOn = mOn;
        this.mKun = mKun;
        this.mFreq = mFreq;
        this.mStroke_count = mStroke_count;
        this.mFavorite = mFavorite;
        this.mRemember = mRemember;
    }

    public int getmRemember() {
        return mRemember;
    }

    public void setmRemember(int mRemember) {
        this.mRemember = mRemember;
    }

    public Kanji(int mId, String mKanji, String mMean, int mLevel, String mOn, String mKun, String mImg, String mDetail, String mShort, int mFreq, String mComp, int mStroke_count, String mCompdetail, String mExamples, int mFavorite, int mRemember) {
        this.mId = mId;
        this.mKanji = mKanji;
        setmMean(mMean);
        this.mLevel = mLevel;
        this.mOn = mOn;
        this.mKun = mKun;
        this.mFreq = mFreq;
        this.mStroke_count = mStroke_count;
        this.mFavorite = mFavorite;
        this.mRemember = mRemember;

    }


    public String getmMean() {
        return mMean;
    }

    public void setmMean(String mMean) {
        if (!TextUtils.isEmpty(mMean)) {
            this.mMean = mMean.replaceAll(",", ", ");
        } else {
            this.mMean = mMean;
        }
    }

    public int getmLevel() {
        return mLevel;
    }

    public void setmLevel(int mLevel) {
        this.mLevel = mLevel;
    }

    public String getmOn() {
        return mOn;
    }

    public void setmOn(String mOn) {
        this.mOn = mOn;
    }

    public String getmKun() {
        return mKun;
    }

    public void setmKun(String mKun) {
        this.mKun = mKun;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmKanji() {
        return mKanji;
    }

    public void setmKanji(String mKanji) {
        this.mKanji = mKanji;
    }

}
