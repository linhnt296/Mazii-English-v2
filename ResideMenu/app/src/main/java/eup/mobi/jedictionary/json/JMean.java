package eup.mobi.jedictionary.json;

import eup.mobi.jedictionary.database.Example;

import java.util.List;

/**
 * Created by nguye on 8/27/2015.
 */
public class JMean {
     private String kind;
     private String mean;
     private List<Example> examples;

    public JMean(String kind ,String mean){
        this.kind = kind;
        this.mean = mean;
    }
    public JMean(String kind ,String mean , List<Example> examples){
        this.kind = kind;
        this.mean = mean;
        this.examples = examples;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getMean() {
        return mean;
    }

    public void setMean(String mean) {
        this.mean = mean;
    }

    public List<Example> getExamples() {
        return examples;
    }

    public void setExamples(List<Example> examples) {
        this.examples = examples;
    }
}
