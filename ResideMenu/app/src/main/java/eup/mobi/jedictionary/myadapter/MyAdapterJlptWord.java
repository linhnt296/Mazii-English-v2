package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.JlptWord;

import java.util.ArrayList;

/**
 * Created by nguye on 8/21/2015.
 */
public class MyAdapterJlptWord extends
        ArrayAdapter<JlptWord> {
    Activity context = null;
    ArrayList<JlptWord> myArray = null;
    int layoutId;
    LayoutInflater inflater;


    public MyAdapterJlptWord(Activity context,
                             int layoutId,
                             ArrayList<JlptWord> arr) {
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_word = (TextView) convertView.findViewById(R.id.tv_jlpt_w_word);
            holder.tv_phonetic = (TextView) convertView.findViewById(R.id.tv_jlpt_w_phonetic);
            holder.tv_mean = (TextView) convertView.findViewById(R.id.tv_jlpt_w_mean);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final JlptWord word = myArray.get(position);
        if (word.getWord()!=null){
            holder.tv_word.setText(word.getWord());
        }
        if (word.getPhonetic()!=null){
            holder.tv_phonetic.setText("「" + word.getPhonetic() + "」");
        }
        if (word.getMean()!=null){
            holder.tv_mean.setText(word.getMean());
        }
        return convertView;
    }

    class ViewHolder {
        TextView tv_word, tv_phonetic, tv_mean;
    }
}