package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import eup.mobi.jedictionary.R;

/**
 * Created by LinhNT on 6/3/2016.
 */
public class MyAdapterLanguage extends ArrayAdapter<String[][]> {
    Activity context = null;
    String[][] myArray = null;
    int layoutId;
    LayoutInflater inflater;

    public MyAdapterLanguage(Activity context,
                              int layoutId,
                              String[][] arr) {
        super(context, layoutId);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
       return myArray.length;
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_name = (TextView) convertView.findViewById(R.id.tvTitle);
            holder.iv_icon = (ImageView) convertView.findViewById(R.id.ivIcon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final String[] category = myArray[position];
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getRandomColor();
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(String.valueOf(category[0].charAt(0)), color);

        holder.tv_name.setText(category[1]);
        holder.iv_icon.setImageDrawable(drawable);

        return convertView;
    }

    class ViewHolder {
        TextView tv_name;
        ImageView iv_icon;
    }
}

