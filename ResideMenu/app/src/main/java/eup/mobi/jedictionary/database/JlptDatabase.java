package eup.mobi.jedictionary.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import eup.mobi.jedictionary.database.sqliteAsset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by LinhNT on 1/12/2016.
 */
public class JlptDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "jlpt.db";
    private static final int DATABASE_VERSION = 1;

    public JlptDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public ArrayList<JlptWord> getJlptWord(String level) {

        ArrayList<JlptWord> listJlptWord = new ArrayList<JlptWord>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
        }
        if (db != null) {
            JlptWord jlptWord = null;
            int mId;
            String mWord, mPhonetic, mMean;
            int mLevel, mRemember;

            level = level.replace("N", "");
            String query = "SELECT * FROM word where level =" + level;
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                mId = cursor.getInt(cursor.getColumnIndex("id"));
                mWord = cursor.getString(cursor.getColumnIndex("word"));
                mPhonetic = cursor.getString(cursor.getColumnIndex("phonetic"));
                mMean = cursor.getString(cursor.getColumnIndex("mean"));
                mLevel = cursor.getInt(cursor.getColumnIndex("level"));
                mRemember = cursor.getInt(cursor.getColumnIndex("remember"));
                jlptWord = new JlptWord(mId, mWord, mPhonetic, mMean, mLevel, mRemember);
                listJlptWord.add(jlptWord);
                cursor.moveToNext();
            }

            cursor.close();
        }
        return listJlptWord;
    }

    public void setRememberOn(int id, int level) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
        }
        if (db != null) {
            String sql_Query1 = "UPDATE word SET remember= 1 WHERE id=" + id + " AND level = " + level;
            db.execSQL(sql_Query1);
        }
    }

    public void setRememberOff(int id, int level) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
        }
        if (db != null) {
            String sql_Query1 = "UPDATE word SET remember= 0 WHERE id= " + id + " AND level = " + level;
            db.execSQL(sql_Query1);
        }
    }

}