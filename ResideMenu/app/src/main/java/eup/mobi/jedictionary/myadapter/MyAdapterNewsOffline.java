package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.graphics.Paint;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.database.NewsOffline;

import java.util.ArrayList;


/**
 * Created by nguye on 9/15/2015.
 */
public class MyAdapterNewsOffline extends
        ArrayAdapter<NewsOffline> {
    Activity context = null;
    ArrayList<NewsOffline> myArray = null;
    int layoutId;
    LayoutInflater inflater;

    public MyAdapterNewsOffline(Activity context,
                                int layoutId,
                                ArrayList<NewsOffline> arr) {

        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_title = (TextView) convertView.findViewById(R.id.title_items);
            holder.tv_pubdate = (TextView) convertView.findViewById(R.id.pubdate_items);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final NewsOffline offline = myArray.get(position);
        if (offline.getPubdate() != null) {
            holder.tv_pubdate.setText(offline.getPubdate());
        }
        if (offline.getTitle() != null) {
            String convert = Html.fromHtml(offline.getTitle()).toString();
            holder.tv_title.setText(convert);
        }
        MyDatabase db = new MyDatabase(getContext());
        if (db.checkNewsReaded(offline.getTitle())) {
            offline.setIsRead(true);
        }
        if (offline.isRead()) {
            holder.tv_title.setPaintFlags(holder.tv_title.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {

        }
        return convertView;

    }

    class ViewHolder {
        TextView tv_title, tv_pubdate;
    }
}