package eup.mobi.jedictionary.fast.search;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.view.View;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;

/**
 * Created by dovantiep on 14/01/2016.
 */
public class FastSearchService extends Service implements IconCallback {

    private Magnet mMagnet;

    public float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

//    @Override
//    public void onCreate() {
//        super.onCreate();
//        Log.d("tag", "service create");
////        ImageView iconView = new ImageView(FastSearchService.this);
////        int size = (int) convertDpToPixel(48, this);
////        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(size, size);
////        iconView.setLayoutParams(params);
////        iconView.setImageResource(R.drawable.icon_tracuu);
////        iconView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//
//        mMagnet = new Magnet.Builder(FastSearchService.this)
//                .setIconView(R.layout.icon_fast_search)
//                .setIconCallback(FastSearchService.this)
//                .setRemoveIconResId(R.drawable.ic_close_circle_outline_grey600_48dp)
//                .setRemoveIconShadow(R.drawable.bottom_shadow)
//                .setShouldFlingAway(true)
//                .setShouldStickToWall(true)
//                .setRemoveIconShouldBeResponsive(true)
//                .build();
//        mMagnet.show();
//    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        runAsForeground();
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void runAsForeground() {
        Intent notificationIntent = new Intent();
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ComponentName cn = new ComponentName(this, MenuActivity.class);
        notificationIntent.setComponent(cn);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.touch_to_open))
                .setContentIntent(pendingIntent).build();

        startForeground(111, notification);

    }

    @Override
    public void onFlingAway() {

    }

    @Override
    public void onMove(float x, float y) {

    }

    boolean isStop;

    @Override
    public void onIconClick(View icon, float iconXPose, float iconYPose) {
        if (mMagnet != null) {
            mMagnet.destroy();
            mMagnet = null;
        }
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ComponentName cn = new ComponentName(this, MenuActivity.class);
        intent.setComponent(cn);
        intent.putExtra("showIcon", true);
        startActivity(intent);
    }

    @Override
    public void onIconDestroyed() {
        if (!isStop) {
            isStop = true;
            stopSelf();
        }
    }

    @Override
    public void onDestroy() {

        if (!isStop && mMagnet != null) {
            isStop = true;
            mMagnet.destroy();
            mMagnet = null;
        }
        super.onDestroy();

    }
}
