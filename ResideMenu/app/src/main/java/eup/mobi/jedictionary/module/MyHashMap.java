package eup.mobi.jedictionary.module;

import java.util.HashMap;

/**
 * Created by nguye on 9/1/2015.
 */
public class MyHashMap {
    static String result = "";    //add key-value pair to hashmap
    static HashMap<String, String> hashMap;

    public static HashMap<String, String> init() {
        hashMap = new HashMap<String, String>();
        hashMap.put("abbr", "abbreviation");
        hashMap.put("adj", "adjective (keiyoushi)");
        hashMap.put("adj-i", "adjective (keiyoushi)");
        hashMap.put("adj-na", "adjectival nouns or quasi-adjectives (keiyodoshi)");
        hashMap.put("adj-no", "nouns which may take the genitive case particle 'no'");
        hashMap.put("adj-pn", "pre-noun adjectival (rentaishi)");
        hashMap.put("adj-s", "special adjective (e.g. ookii)");
        hashMap.put("adj-t", "'taru' adjective");
        hashMap.put("adv", "adverb (fukushi)");
        hashMap.put("adv-n", "adverbial noun");
        hashMap.put("adv-to", "adverb taking the 'to' particle");
        hashMap.put("arch", "archaism");
        hashMap.put("ateji", "ateji (phonetic) reading");
        hashMap.put("aux", "auxiliary");
        hashMap.put("aux-v", "auxiliary verb");
        hashMap.put("aux-adj", "auxiliary adjective");
        hashMap.put("Buddh", "Buddhist term");
        hashMap.put("chn", "children's language");
        hashMap.put("col", "colloquialism");
        hashMap.put("comp", "computer terminology");
        hashMap.put("conj", "conjunction");
        hashMap.put("derog", "derogatory");
        hashMap.put("ek", "exclusively kana");
        hashMap.put("exp", "expressions (phrases, clauses, etc.)");
        hashMap.put("fam", "familiar language");
        hashMap.put("fem", "female term or language");
        hashMap.put("food", "food term");
        hashMap.put("geom", "geometry term");
        hashMap.put("gikun", "gikun (meaning as reading) or jukujikun (special kanji reading)");
        hashMap.put("gram", "grammatical term");
        hashMap.put("hon", "honorific or respectful (sonkeigo) language");
        hashMap.put("hum", "humble (kenjougo) language");
        hashMap.put("id", "idiomatic expression");
        hashMap.put("int", "interjection (kandoushi)");
        hashMap.put("iK", "word containing irregular kanji usage");
        hashMap.put("ik", "word containing irregular kana usage");
        hashMap.put("io", "irregular okurigana usage");
        hashMap.put("iv", "irregular verb");
        hashMap.put("kyb", "Kyoto-ben");
        hashMap.put("ksb", "Kansai-ben");
        hashMap.put("ktb", "Kantou-ben");
        hashMap.put("ling", "linguistics terminology");
        hashMap.put("MA", "martial arts term");
        hashMap.put("male", "male term or language");
        hashMap.put("math", "mathematics");
        hashMap.put("mil", "military");
        hashMap.put("m-sl", "manga slang");
        hashMap.put("n", "noun (common) (futsuumeishi)");
        hashMap.put("n-adv", "adverbial noun (fukushitekimeishi)");
        hashMap.put("n-pref", "noun, used as a prefix");
        hashMap.put("n-suf", "noun, used as a suffix");
        hashMap.put("n-t", "noun (temporal) (jisoumeishi)");
        hashMap.put("neg", "negative (in a negative sentence, or with negative verb)");
        hashMap.put("neg-v", "negative verb (when used with)");
        hashMap.put("obs", "obsolete term");
        hashMap.put("obsc", "obscure term");
        hashMap.put("oK", "word containing out-dated kanji");
        hashMap.put("ok", "out-dated or obsolete kana usage");
        hashMap.put("osk", "Osaka-ben");
        hashMap.put("physics", "physics terminology");
        hashMap.put("pol", "polite (teineigo) language");
        hashMap.put("pref", "prefix");
        hashMap.put("prt", "particle");
        hashMap.put("qv", "quod vide (see another entry)");
        hashMap.put("rare", "rare");
        hashMap.put("sl", "slang");
        hashMap.put("suf", "suffix");
        hashMap.put("tsb", "Tosa-ben");
        hashMap.put("uK", "word usually written using kanji alone");
        hashMap.put("uk", "word usually written using kana alone");
        hashMap.put("v", "verb");
        hashMap.put("v1", "Ichidan verb");
        hashMap.put("v5aru", "Godan verb - -aru special class");
        hashMap.put("v5b", "Godan verb with 'bu' ending");
        hashMap.put("v5g", "Godan verb with 'gu' ending");
        hashMap.put("v5k", "Godan verb with 'ku' ending");
        hashMap.put("v5k-s", "Godan verb - Iku/Yuku special class");
        hashMap.put("v5m", "Godan verb with 'mu' ending");
        hashMap.put("v5n", "Godan verb with 'nu' ending");
        hashMap.put("v5r", "Godan verb with 'ru' ending");
        hashMap.put("v5r-i", "Godan verb with 'ru' ending (irregular verb)");
        hashMap.put("v5s", "Godan verb with 'su' ending");
        hashMap.put("v5t", "Godan verb with 'tsu' ending");
        hashMap.put("v5u", "Godan verb with 'u' ending");
        hashMap.put("v5u-s", "Godan verb with 'u' ending (special class)");
        hashMap.put("v5uru", "Godan verb - Uru old class verb (old form of Eru)");
        hashMap.put("vi", "intransitive verb");
        hashMap.put("vk", "Kuru verb - special class");
        hashMap.put("vs", "noun or participle which takes the aux. verb suru");
        hashMap.put("vs-i", "suru verb - irregular");
        hashMap.put("vt", "Transitive verb");
        hashMap.put("vulg", "vulgar expression or word");
        hashMap.put("vz", "Transitive verb");
        hashMap.put("X", "rude or X-rated term (not displayed in educational software)");

        return hashMap;
    }

    public static String getResult(String key) {
        if (hashMap == null) {
            init();
        }
        if (hashMap.get(key) != null) {
            result = hashMap.get(key);
        }
        return result;
    }

}
