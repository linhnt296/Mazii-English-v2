package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.module.Category;

import java.util.List;

/**
 * Created by dovantiep on 13/01/2016.
 */
public class MyGroupWordAdapter extends ArrayAdapter<Category> {
    Activity context = null;
    List<Category> myArray = null;
    int layoutId;
    LayoutInflater inflater;

    public MyGroupWordAdapter(Activity context,
                              int layoutId,
                              List<Category> arr) {
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_name = (TextView) convertView.findViewById(R.id.tvTitle);
            holder.tv_date = (TextView) convertView.findViewById(R.id.tvDate);
            holder.iv_icon = (ImageView) convertView.findViewById(R.id.ivIcon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Category category = myArray.get(position);
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getRandomColor();
        TextDrawable drawable = TextDrawable.builder()
                .buildRound(String.valueOf(category.getName().charAt(0)), color);

        holder.tv_name.setText(category.getName());
        holder.tv_date.setText(category.getDate());
        holder.iv_icon.setImageDrawable(drawable);

        return convertView;
    }

    class ViewHolder {
        TextView tv_name, tv_date;
        ImageView iv_icon;
    }
}
