package eup.mobi.jedictionary.service;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.MyWordDatabase;
import eup.mobi.jedictionary.module.Entry;

import java.util.List;


public class WidgetService extends RemoteViewsService {
    final static int[] mLayoutIds = {R.layout.layout_widget_main};
    private SharedPreferences sharedPreferences;
    private int id;
    private final String LOCK_SCREEN = "LOCK_MINDER";
    private List<Entry> entries;

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ViewFactory(intent);
    }

    private class ViewFactory implements RemoteViewsFactory {

        private int mInstanceId = AppWidgetManager.INVALID_APPWIDGET_ID;
        private String word = "", phonetic = "", mean = "";

        public ViewFactory(Intent intent) {
            mInstanceId = intent.getIntExtra(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        @Override
        public void onCreate() {
            sharedPreferences = getSharedPreferences(LOCK_SCREEN, Context.MODE_PRIVATE);
        }

        @Override
        public void onDataSetChanged() {
            // load arraylist
            id = sharedPreferences.getInt("id_widget", -1);
            MyWordDatabase db = new MyWordDatabase(getApplicationContext());
            entries = db.qEntry(id);
        }

        @Override
        public void onDestroy() {

        }

        @Override
        public int getCount() {
            if (entries != null && entries.size() > 0)
                return entries.size();
            else return 1;
        }

        @Override
        public RemoteViews getViewAt(int position) {
            RemoteViews page = new RemoteViews(getPackageName(), mLayoutIds[0]);
            if (entries != null && entries.size() > 0) {
                word = entries.get(position).getWord();
                phonetic = entries.get(position).getPhonetic();
                mean = entries.get(position).getMean();
            } else {
                word = getString(R.string.no_data_widget);
                mean = getString(R.string.chose_subject);
                page.setViewVisibility(R.id.tvLock_P, View.GONE);
            }

            if (word != null && !word.equals(""))
                page.setTextViewText(R.id.tvLock_W, word);
            else page.setViewVisibility(R.id.tvLock_W, View.GONE);
            if (phonetic != null && !phonetic.equals(""))
                page.setTextViewText(R.id.tvLock_P, phonetic);
            else page.setViewVisibility(R.id.tvLock_P, View.GONE);
            if (mean != null && !mean.equals(""))
                page.setTextViewText(R.id.tvLock_M, mean);
            else page.setViewVisibility(R.id.tvLock_M, View.GONE);
            return page;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            if (entries != null && entries.size() > 0)
                return entries.size();
            else return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
}