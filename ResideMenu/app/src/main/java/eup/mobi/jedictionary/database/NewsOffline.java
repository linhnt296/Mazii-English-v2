package eup.mobi.jedictionary.database;

/**
 * Created by nguye on 11/26/2015.
 */
public class NewsOffline {
    String title;
    String des;
    String html;
    String pubdate;
    boolean isRead;

    public boolean isRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getPubdate() {
        return pubdate;
    }

    public void setPubdate(String pubdate) {
        this.pubdate = pubdate;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public NewsOffline(String title, String des, String pubdate) {
        this.title = title;
        this.des = des;
        this.pubdate = pubdate;
    }

    public NewsOffline(String title, String des, String pubdate, String html) {
        this.title = title;
        this.des = des;
        this.pubdate = pubdate;
        this.html = html;
    }
}
