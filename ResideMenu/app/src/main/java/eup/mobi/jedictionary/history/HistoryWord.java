package eup.mobi.jedictionary.history;

/**
 * Created by nguye on 10/30/2015.
 */
public class HistoryWord {
    String hWords;
    long hDate;

    public String gethWords() {
        return hWords;
    }

    public void sethWords(String hWords) {
        this.hWords = hWords;
    }

    public long gethDate() {
        return hDate;
    }

    public void sethDate(long hDate) {
        this.hDate = hDate;
    }

    public HistoryWord(String hWords, long hDate) {
        this.hWords = hWords;
        this.hDate = hDate;
    }
}
