package eup.mobi.jedictionary.module;

import android.app.Activity;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.VerbObj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nguye on 11/27/2015.
 */
public class VerbTable {
    public static HashMap<String, String[]> verbHashMap;
    public static String[] arrString;

    public static void init() {
        if (verbHashMap == null) {
            verbHashMap = new HashMap<String, String[]>();
            //  0           1       2       3       4       5           6           7       8                           9           10
            //  end with, past, negative, i form, te form, potential, passive, causative, Provisional Conditional, Imperative, Volitional

        /*"aux-i": [
            "だ", "だった", "じゃない", "であり", "で", "", "", "", "であれば", "であれ", "だろう"
        ],

        "aux-p": [
            "です", "ですた", "じゃありません", "であり", "", "", "", "", "", "", "でしょう"
        ],

        "masu": [
            "ます", "ました", "", "", "まして", "", "", "", "", "", "ましょう"
        ],*/

            verbHashMap.put("vs",
                    new String[]{"する", "した", "しない", "し", "して", "できる", "される", "させる", "すれば", "しろ", "しよう"});
            verbHashMap.put("vk",
                    new String[]{"来る", "きた", "こない", "き", "きて", "来られる", "来られる", "来させる", "くれば", "こい", "こよう"});
            verbHashMap.put("v5u",
                    new String[]{"う", "った", "わない", "い", "って", "える", "われる", "わせる", "えば", "え", "おう"});
            verbHashMap.put("v5u-s",
                    new String[]{"う", "うた", "わない", "い", "うて", "える", "われる", "わせる", "えば", "え", "おう"});
            verbHashMap.put("v5k", new String[]{"く", "いた", "かない", "き", "いて", "ける", "かれる", "かせる", "けば", "け", "こう"}
            );
            verbHashMap.put("v5k-s", new String[]{"く", "った", "かない", "き", "って", "ける", "かれる", "かせる", "けば", "け", "こう"});
            verbHashMap.put("v5g",
                    new String[]{"ぐ", "いだ", "がない", "ぎ", "いで", "げる", "がれる", "がせる", "げば", "げ", "ごう"});
            verbHashMap.put("v5s",
                    new String[]{"す", "した", "さない", "し", "して", "せる", "される", "させる", "せば", "せ", "そう"});
            verbHashMap.put("v5t",
                    new String[]{"つ", "った", "たない", "ち", "って", "てる", "たれる", "たせる", "てば", "て", "とう"
                    });
            verbHashMap.put("v5n",
                    new String[]{"ぬ", "んだ", "なない", "に", "んで", "ねる", "なれる", "なせる", "ねば", "ね", "のう"});
            verbHashMap.put("v5b",
                    new String[]{"ぶ", "んだ", "ばない", "び", "んで", "べる", "ばれる", "ばせる", "べば", "べ", "ぼう"});
            verbHashMap.put("v5m",
                    new String[]{"む", "んだ", "まない", "み", "んで", "める", "まれる", "ませる", "めば", "め", "もう"});
            verbHashMap.put("v5r",
                    new String[]{"る", "った", "らない", "り", "って", "れる", "られる", "らせる", "れば", "れ", "ろう"});
            verbHashMap.put("v5r-i",
                    new String[]{"る", "った", "", "り", "って", "ありえる", "", "らせる", "れば", "れる", "ろう"});
            verbHashMap.put("v5aru",
                    new String[]{"る", "った", "らない", "い", "って", "りえる", "", "", "", "い", ""});
            verbHashMap.put("v1",
                    new String[]{"る", "た", "ない", "-", "て", "られる", "られる", "させる", "れば", "ろ", "よう"});
        }
    }

    public static String[] tableConjugationConvert(String type) {

        if (verbHashMap.get(type) != null) {
            arrString = verbHashMap.get(type);
        } else {
            arrString = null;
        }

        return arrString;
        /*"adj-i": [
            "い", "かった", "くない", "くて", "", "", "", "", "ければ", "", "かろう"
        ],
        "adj-na": [
            "な", "だった", "ではない", "で", "", "", "", "", "であれば", "", "だろう"
        ]*/
    }

    public static ArrayList<VerbObj> getConjugationTableOfVerb(String dictVerb, String phonetic, String type, Activity activity) {
        // normalize phonetic
        if (phonetic == null || phonetic.equals(""))
            return null;

        // check type is in table
        String[] rule = tableConjugationConvert(type);
        if (rule == null)
            return null;

//        phonetic = phonetic.split(" ")[0];

        // check base form
        if (dictVerb.indexOf(rule[0]) == -1) {
            dictVerb = dictVerb + rule[0];
            phonetic = phonetic + rule[0];
        }

        ArrayList<VerbObj> verbConjug = new ArrayList<VerbObj>();
        String word = dictVerb + "/" + phonetic;
        String name = activity.getString(R.string.verbTable_1);
        if (dictVerb == phonetic) {
            word = dictVerb;
        }
        VerbObj verbObj = new VerbObj(name, word);
        verbConjug.add(verbObj);


//        var regex = new RegExp(rule[0] + "$");
        String regex = rule[0] + "$";
        Pattern pattern = Pattern.compile(regex);
        word = pattern.matcher(dictVerb).replaceAll(rule[1]);
        name = activity.getString(R.string.verbTable_2);
        verbObj = new VerbObj(name, word);
        verbConjug.add(verbObj);

        // get nagative form
        if (rule[2] != "") {
            word = pattern.matcher(dictVerb).replaceAll(rule[2]);
            name = activity.getString(R.string.verbTable_3);

        } else {
            if (dictVerb.indexOf("する") != -1) {
                word = dictVerb.replace("する", "しない");
            } else if (dictVerb.indexOf("くる") != -1) {
                word = dictVerb.replace("くる", "こない");
            }

            name = activity.getString(R.string.verbTable_3);
        }
        verbObj = new VerbObj(name, word);
        verbConjug.add(verbObj);

        // get polite form
        if (rule[3] != "") {
            if (rule[3] == "-") {
                word = pattern.matcher(dictVerb).replaceAll("")
                        + "ます";
            } else {
                word = pattern.matcher(dictVerb).replaceAll(rule[3])
                        + "ます";
            }
            name = activity.getString(R.string.verbTable_4);
        }
        verbObj = new VerbObj(name, word);
        verbConjug.add(verbObj);

        // get te form
        if (rule[4] != "") {
            word = pattern.matcher(dictVerb).replaceAll(rule[4]);
            name = activity.getString(R.string.verbTable_5);
            verbObj = new VerbObj(name, word);
            verbConjug.add(verbObj);
        }


        // get potential form
        if (rule[5] != "") {
            word = pattern.matcher(dictVerb).replaceAll(rule[5]);
            name = activity.getString(R.string.verbTable_6);
            verbObj = new VerbObj(name, word);
            verbConjug.add(verbObj);
        }

        // get passive form
        if (rule[6] != "") {
            word = pattern.matcher(dictVerb).replaceAll(rule[6]);
            name = activity.getString(R.string.verbTable_7);
            verbObj = new VerbObj(name, word);
            verbConjug.add(verbObj);
        }


        // get causative form
        if (rule[7] != "") {
            word = pattern.matcher(dictVerb).replaceAll(rule[7]);
            name = activity.getString(R.string.verbTable_8);
            verbObj = new VerbObj(name, word);
            verbConjug.add(verbObj);
        }

        if (rule[6] != "" && rule[7] != "") {
            String[] caupassRule = tableConjugationConvert("v5r");
            String regex2 = caupassRule[0] + "$";
            Pattern pattern2 = Pattern.compile(regex2);
            Matcher matcher2 = pattern2.matcher(dictVerb);
//            var caupassRegex = new RegExp(caupassRule[0] + "$");
            word = pattern.matcher(word).replaceAll(caupassRule[6]);

            name = activity.getString(R.string.verbTable_9);
            verbObj = new VerbObj(name, word);
            verbConjug.add(verbObj);
        }

        // get conditional form
        if (rule[8] != "") {
            word = pattern.matcher(dictVerb).replaceAll(rule[8]);
            name = activity.getString(R.string.verbTable_10);
            verbObj = new VerbObj(name, word);
            verbConjug.add(verbObj);
        }


        // get imperative form
        if (rule[9] != "") {
            word = pattern.matcher(dictVerb).replaceAll(rule[9]);
            name = activity.getString(R.string.verbTable_11);
            verbObj = new VerbObj(name, word);
            verbConjug.add(verbObj);
        }


        // get volitional form
        if (rule[10] != "") {
            word = pattern.matcher(dictVerb).replaceAll(rule[10]);
            name = activity.getString(R.string.verbTable_12);
            verbObj = new VerbObj(name, word);
            verbConjug.add(verbObj);
        }

        word = dictVerb + "な";
        name = activity.getString(R.string.verbTable_13);
        verbObj = new VerbObj(name, word);
        verbConjug.add(verbObj);

        return verbConjug;
    }
}
