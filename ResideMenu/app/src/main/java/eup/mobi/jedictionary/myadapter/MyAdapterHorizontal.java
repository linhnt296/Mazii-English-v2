package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Kanji;

import java.util.ArrayList;

/**
 * Created by nguye on 8/21/2015.
 */
public class MyAdapterHorizontal extends
        ArrayAdapter<Kanji> {
    Activity context = null;
    ArrayList<Kanji> myArray = null;
    int layoutId;
    LayoutInflater inflater;

    public MyAdapterHorizontal(Activity context,
                               int layoutId,
                               ArrayList<Kanji> arr) {
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_display = (TextView) convertView.findViewById(R.id.tv_horizontal_kanji);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Kanji kanji = myArray.get(position);
        if (kanji.getmKanji() != null) {
            holder.tv_display.setText(kanji.getmKanji());
        }
        return convertView;
    }

    class ViewHolder {
        TextView tv_display;
    }
}
