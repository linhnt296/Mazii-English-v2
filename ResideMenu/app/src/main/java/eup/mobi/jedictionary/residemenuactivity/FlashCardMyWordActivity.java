package eup.mobi.jedictionary.residemenuactivity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.MyWordDatabase;
import eup.mobi.jedictionary.fast.search.FastSearchService;
import eup.mobi.jedictionary.flashcard.FlashCardFragmentMyWord;
import eup.mobi.jedictionary.google.Adsmod;
import eup.mobi.jedictionary.module.Entry;
import com.rey.material.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;

public class FlashCardMyWordActivity extends FragmentActivity {

    private int ID_CATEGORY;
    private ViewPager mPager, mPager1, mPager2;
    Button bt_back;
    private PagerAdapter mPagerAdapter, mPagerAdapter1, mPagerAdapter2;
    public static ArrayList<Entry> arrayList = new ArrayList<>();
    public static ArrayList<Entry> arrayList_1 = new ArrayList<>();
    public static ArrayList<Entry> arrayList_2 = new ArrayList<>();
    Spinner spinner;
    Activity activity;
    public static int STATUS_FC = 0;
    TextView tv_flashcard_null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        //set fullscreen
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.flash_card_myword_activity);
        activity = this;
        bt_back = (Button) findViewById(R.id.button_back);
        spinner = (Spinner) findViewById(R.id.sp_flashcard);
        tv_flashcard_null = (TextView) findViewById(R.id.tv_flashcard_null);
        btnBackSelected();
        setSpiner();

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager1 = (ViewPager) findViewById(R.id.pager1);
        mPager2 = (ViewPager) findViewById(R.id.pager2);

        //lấy intent gọi Activity này
        Intent callerIntent = getIntent();
        //có intent rồi thì lấy Bundle dựa vào MyPackage
        Bundle packageFromCaller =
                callerIntent.getBundleExtra("FlashCardMyWord");
        //Có Bundle rồi thì lấy các thông số dựa vào soa, sob
        ID_CATEGORY = packageFromCaller.getInt("id_category");
        STATUS_FC = 0;
        new myAsyntask_tracuu2().execute(ID_CATEGORY);

        if (MenuActivity.sharedPreferences != null) {
            boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
            String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
            float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
            Adsmod.banner(this, banner, probBanner, isPremium);
        }
    }


    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            FlashCardFragmentMyWord flashcard_fragment = new FlashCardFragmentMyWord();
            Bundle bundle = new Bundle();
//            bundle.putStringArrayList("arrlist", getWord());
            bundle.putInt("position", position);
            bundle.putInt("numpage", arrayList.size());
            flashcard_fragment.setArguments(bundle);
//            Collections.shuffle(arrayList);
            return flashcard_fragment;
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }
    }

    private class ScreenSlidePagerAdapter1 extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter1(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            FlashCardFragmentMyWord flashcard_fragment = new FlashCardFragmentMyWord();

            Bundle bundle = new Bundle();
//            bundle.putStringArrayList("arrlist", getWord1());
            bundle.putInt("position", position);
            bundle.putInt("numpage", arrayList_1.size());
            flashcard_fragment.setArguments(bundle);
//            Collections.shuffle(arrayList);
            return flashcard_fragment;
        }

        @Override
        public int getCount() {
            return arrayList_1.size();
        }
    }

    private class ScreenSlidePagerAdapter2 extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter2(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            FlashCardFragmentMyWord flashcard_fragment = new FlashCardFragmentMyWord();

            Bundle bundle = new Bundle();
//            bundle.putStringArrayList("arrlist", getWord2());
            bundle.putInt("position", position);
            bundle.putInt("numpage", arrayList_2.size());
            flashcard_fragment.setArguments(bundle);
//            Collections.shuffle(arrayList);
            return flashcard_fragment;
        }

        @Override
        public int getCount() {
            return arrayList_2.size();
        }
    }

    private void btnBackSelected() {
        bt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setSpiner() {
        String[] items_2 = new String[]{getString(R.string.flashcard_chuathuoc), getString(R.string.flashcard_dathuoc), getString(R.string.flashcard_tatca)};
        ArrayAdapter<String> adapter_2 = new ArrayAdapter<String>(this, R.layout.my_spinner, items_2) {
            public View getView(int position, View convertView, ViewGroup parent) {

                View v = super.getView(position, convertView, parent);

                ((TextView) v).setTextSize(18);

                return v;

            }

            public View getDropDownView(int position, View convertView, ViewGroup parent) {

                View v = super.getDropDownView(position, convertView, parent);

                ((TextView) v).setGravity(Gravity.CENTER);

                return v;

            }

        };

        spinner.setAdapter(adapter_2);
        spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {
                switch (position) {
                    case 0:
//                        Toast.makeText(activity, "Chưa thuộc", Toast.LENGTH_SHORT).show();
//                        mPager.setAdapter(mPagerAdapter1);
//                        mPagerAdapter1.notifyDataSetChanged();
                        mPager1.setVisibility(View.VISIBLE);
                        mPager2.setVisibility(View.GONE);
                        mPager.setVisibility(View.GONE);
                        if (arrayList_1.size() > 0) {
                            tv_flashcard_null.setVisibility(View.GONE);
                        } else {
                            tv_flashcard_null.setVisibility(View.VISIBLE);
                        }
                        STATUS_FC = 0;
                        break;
                    case 1:
//                        Toast.makeText(activity, "Đã thuộc", Toast.LENGTH_SHORT).show();
//                        mPager.setAdapter(mPagerAdapter2);
//                        mPagerAdapter2.notifyDataSetChanged();
                        mPager1.setVisibility(View.GONE);
                        mPager2.setVisibility(View.VISIBLE);
                        mPager.setVisibility(View.GONE);
                        STATUS_FC = 1;
                        if (arrayList_2.size() > 0) {
                            tv_flashcard_null.setVisibility(View.GONE);
                        } else {
                            tv_flashcard_null.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
//                        Toast.makeText(activity, "Tất cả", Toast.LENGTH_SHORT).show();
//                        mPager.setAdapter(mPagerAdapter);
//                        mPagerAdapter.notifyDataSetChanged();
                        mPager1.setVisibility(View.GONE);
                        mPager2.setVisibility(View.GONE);
                        mPager.setVisibility(View.VISIBLE);
                        STATUS_FC = 2;
                        if (arrayList.size() > 0) {
                            tv_flashcard_null.setVisibility(View.GONE);
                        } else {
                            tv_flashcard_null.setVisibility(View.VISIBLE);
                        }
                        break;
                }
            }
        });
    }

    private class myAsyntask_tracuu2 extends AsyncTask<Integer, Void, ArrayList<Entry>> {

        @Override
        protected ArrayList<Entry> doInBackground(Integer... params) {
            MyWordDatabase db = new MyWordDatabase(activity);
            arrayList = db.getEntryFlashCard(params[0]);
            arrayList_1.clear();
            arrayList_2.clear();
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).getRemember() == 1) {
                    arrayList_2.add(arrayList.get(i));
                } else {
                    arrayList_1.add(arrayList.get(i));
                }
            }
            return arrayList;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(ArrayList<Entry> entries) {
            super.onPostExecute(entries);

            if (arrayList_1.size() > 0) {
                tv_flashcard_null.setVisibility(View.GONE);
            } else {
                tv_flashcard_null.setVisibility(View.VISIBLE);
            }

//            Collections.shuffle(arrayList);
            Collections.shuffle(arrayList_1);
            Collections.shuffle(arrayList_2);

            mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
            mPagerAdapter1 = new ScreenSlidePagerAdapter1(getSupportFragmentManager());
            mPagerAdapter2 = new ScreenSlidePagerAdapter2(getSupportFragmentManager());

            mPager.setAdapter(mPagerAdapter);
            mPagerAdapter.notifyDataSetChanged();
            mPager1.setAdapter(mPagerAdapter1);
            mPagerAdapter1.notifyDataSetChanged();
            mPager2.setAdapter(mPagerAdapter2);
            mPagerAdapter2.notifyDataSetChanged();
        }
    }

    private boolean back;

    @Override
    public void onBackPressed() {
        back = true;
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        stopService(new Intent(this, FastSearchService.class));
    }

    @Override
    protected void onStop() {
        if (!back && MenuActivity.sharedPreferences.getBoolean("fast_search", false)) {
            if (Build.VERSION.SDK_INT < 23) {
                startService(new Intent(this, FastSearchService.class));
            } else if (Build.VERSION.SDK_INT >= 23 && Settings.canDrawOverlays(this)) {
                startService(new Intent(this, FastSearchService.class));
            }
//            Toast.makeText(this, "start service", Toast.LENGTH_SHORT).show();
        }
        super.onStop();
    }
}



