package eup.mobi.jedictionary.json;

import java.util.List;

/**
 * Created by nguye on 8/27/2015.
 */
public class JsonMean {
    private String word;
    private String phonetic;
    private List<JMean> means;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPhonetic() {
        return phonetic;
    }

    public void setPhonetic(String phonetic) {
        this.phonetic = phonetic;
    }

    public List<JMean> getMeans() {
        return means;
    }

    public void setMeans(List<JMean> means) {
        this.means = means;
    }
}
