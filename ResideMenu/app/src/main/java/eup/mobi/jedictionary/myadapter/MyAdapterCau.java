package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Html;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Example;
import eup.mobi.jedictionary.database.MergeObj;
import eup.mobi.jedictionary.furiganaview.FuriganaView;
import eup.mobi.jedictionary.module.Language;
import eup.mobi.jedictionary.module.MergeKanjiAndHiragana;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;

import java.util.ArrayList;

/**
 * Created by nguye on 8/21/2015.
 */
public class MyAdapterCau extends
        ArrayAdapter<Example> {
    Activity context = null;
    ArrayList<Example> myArray = null;
    int layoutId;
    LayoutInflater inflater;

    public MyAdapterCau(Activity context,
                        int layoutId,
                        ArrayList<Example> arr) {
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
//            holder.tv_position = (TextView) convertView.findViewById(R.id.tv_tab4_position);
            holder.tv_struct_vi = (TextView) convertView.findViewById(R.id.tv_tab4_struct_vi);
            holder.fv_tracuu_cau = (FuriganaView) convertView.findViewById(R.id.fv_tab4);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Example gr = myArray.get(position);
        String mean = "", text = "";

        mean = gr.getmMean();
        text = gr.getmTrans();

        // todo set tv_structvi
        if (mean != null) {
            String upperMean = mean.substring(0, 1).toUpperCase() + mean.substring(1);
            holder.tv_struct_vi.setText(Html.fromHtml(upperMean).toString());
        }
        //todo set fv_tab4
        TextPaint tp = new TextPaint();
        tp.setColor(Color.parseColor("#D32F2F"));
        tp.setTextSize(getContext().getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
        tp.setAntiAlias(true);
        tp.setDither(true);
        tp.setStyle(Paint.Style.FILL);
        tp.setStrokeJoin(Paint.Join.ROUND);

        if (!text.equals("")) {
            holder.fv_tracuu_cau.text_set(tp, text, -1, -1);
            holder.fv_tracuu_cau.setPadding(0, 8, 0, 8);
        }
        return convertView;
    }

    class ViewHolder {
        TextView tv_struct_vi;
        FuriganaView fv_tracuu_cau;
    }
}