package eup.mobi.jedictionary.docbao;

/**
 * Created by nguye on 9/15/2015.
 */
public class SubContent {
    public String image;
    public String video;
    public String audio;
    public String textbody;
    public String textmore;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getTextbody() {
        return textbody;
    }

    public void setTextbody(String textbody) {
        this.textbody = textbody;
    }

    public String getTextmore() {
        return textmore;
    }

    public void setTextmore(String textmore) {
        this.textmore = textmore;
    }

    public SubContent(String image, String video, String audio, String textbody, String textmore) {
        this.image = image;
        this.video = video;
        this.audio = audio;
        this.textbody = textbody;
        this.textmore = textmore;
    }
}
