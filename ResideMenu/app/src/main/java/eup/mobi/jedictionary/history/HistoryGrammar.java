package eup.mobi.jedictionary.history;

/**
 * Created by nguye on 10/30/2015.
 */
public class HistoryGrammar {
    String hGrammar;
    long hDate;

    public String gethWords() {
        return hGrammar;
    }

    public void sethWords(String hGrammar) {
        this.hGrammar = hGrammar;
    }

    public long gethDate() {
        return hDate;
    }

    public void sethDate(long hDate) {
        this.hDate = hDate;
    }

    public HistoryGrammar(String hGrammar, long hDate) {
        this.hGrammar = hGrammar;
        this.hDate = hDate;
    }
}
