package eup.mobi.jedictionary.residemenuactivity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.fast.search.FastSearchService;
import eup.mobi.jedictionary.google.AdsmodUtil;
import eup.mobi.jedictionary.google.NetWork;
import eup.mobi.jedictionary.google.Track;
import eup.mobi.jedictionary.residemenuitems.ChuyenNganhFragment;
import eup.mobi.jedictionary.residemenuitems.DocBaoFragment;
import eup.mobi.jedictionary.residemenuitems.JLPTFragment;
import eup.mobi.jedictionary.residemenuitems.SoTayFragment;
import eup.mobi.jedictionary.residemenuitems.TapVietFragment;
import eup.mobi.jedictionary.residemenuitems.TraCuuFragment;
import eup.mobi.jedictionary.residemenuitems.TuyChinhFragment;
import eup.mobi.jedictionary.residemenuitems.UpdateFragment;
import eup.mobi.jedictionary.residemenuitems.YeuThichFragment;
import eup.mobi.jedictionary.service.WidgetProvider;
import eup.mobi.jedictionary.util.IabHelper;
import eup.mobi.jedictionary.util.IabResult;
import eup.mobi.jedictionary.util.Inventory;
import eup.mobi.jedictionary.util.Purchase;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by LinhNT on 4/4/2016.
 */
public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private Dialog splashScreen;
    private MenuActivity menuActivity;
    String idBanner, idInter, idNative;
    float probBanner, probInter;
    long probAdpress = 14400000, probIntervalAds, timeLoop = 60000;
    public static SharedPreferences sharedPreferences;
    boolean isLoop, isPremium;
    public static boolean isClicked = false;
    private static boolean showIconFastSearch = false;
    // quang cao minder
    //---------inapp purchase
    private String price = "";
    // Debug tag, for logging
    static final String TAG = "TrivialDrive";
    // Does the user have the premium upgrade?
    boolean mIsPremium = false;
    // SKUs for our products: the premium upgrade (non-consumable) and gas (consumable)
    static final String SKU_PREMIUM = "eup.mobi.jedictionary.premium";
    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;
    // The helper object
    IabHelper mHelper;
    // Progress Dialog
    private ProgressDialog pDialog;
    // package name
    public static String PACKAGE_NAME;
    // File url to download
    static final String file_url = "http://eup.mobi/apps/maziien/jaen2.db.zip";
    static final String ZIP = "jaen2.db.zip";
    String version, request_name, request_file;
    private TextView tv_load;

    public static boolean isActivityOther;
    public static AdsmodUtil adsmodUtil = new AdsmodUtil();

    // drawer layout
    public static DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showSplashScreen();

        setContentView(R.layout.activity_main);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        showIconFastSearch = getIntent().getBooleanExtra("showIcon", false);

        menuActivity = this;

        Track.createTracker(menuActivity);
        sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        TraCuuTuActivity.setMenuActivity(menuActivity);
        //check version
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        version = pInfo.versionName;
        if (sharedPreferences.getString("version", null) == null) {
            SharedPreferences.Editor editor1 = sharedPreferences.edit();
            editor1.putString("version", version);
            editor1.putBoolean("RanBefore", false);
            editor1.commit();
        } else if (!sharedPreferences.getString("version", null).equals(version)) {
            SharedPreferences.Editor editor1 = sharedPreferences.edit();
            editor1.putString("version", version);
            editor1.putBoolean("RanBefore", false);
            editor1.commit();
        }

        if (isFirstTime()) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("idBanner", idBanner);
            editor.putString("idInter", idInter);
            editor.putString("request_name", request_name);
            editor.putString("request_file", request_file);
            editor.putLong("probAds", probAdpress);
            editor.putLong("probInterval", probIntervalAds);
            editor.putLong("lastTimeShowAdsInter", 0);
            editor.putFloat("probBanner", probBanner);
            editor.putFloat("probInter", probInter);
//            editor.putString("ga", gaAndroid);
            editor.putBoolean("cb1", true);
            editor.putBoolean("jlpt", true);
//            editor.putBoolean("isGone", false);
            editor.putBoolean("isDownload", false);
//            editor.putBoolean("isPremium", false);
            editor.putLong("time", 0);
            editor.commit();
        }

        if (!isDatabaseNull()) {
            tryToDownload();
        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if ((splashScreen != null) && splashScreen.isShowing())
                            splashScreen.dismiss();
                    } catch (final IllegalArgumentException e) {
                        // Handle or log or ignore
                    } catch (final Exception e) {
                        // Handle or log or ignore
                    } finally {
                        splashScreen = null;
                    }
                }
            }, 2000);
            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            boolean ranBefore = preferences.getBoolean("RanBefore", false);
            if (!ranBefore) {
                // first time
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("RanBefore", true);
                editor.commit();
            }
        }
        if (savedInstanceState == null) {
            Track.sendTrackerScreen("search");
            TraCuuFragment traCuuFragment = new TraCuuFragment();
            traCuuFragment.setMenuActivity(MenuActivity.this);
            changeFragment(traCuuFragment);
        }


        //-----------in app purchase
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApMw+kJTmd4ZoLTFL3OgxFpGiF8hOBW/6l5Or8rFzCj/Wcl65qFuIqOKATjvD9WLJKgo5e94dBSu8HCJpRbhvTpOjSYF6qkEphkVGpkLVZtY/QDa1UdqJK8X90lPHu32gjr+zrR+8s7np2h+0oAL491zf5v5O/ljVGmuh1Hj6wssfQbs4/l0PPmD7WVq/l/pKG/ZJQkH22trGHjXdwvnF0TFPAhMJ65lnskR5KvNFb7mUqmT+nAtJQ251NVjAe9SYqJ7e+21jGmubJ261pdfQzDwP+kL67i2Cgtr7/srTsAjq4ZuZ4AX4PdfDloDTJUr8t3ceqwZzia2rIcxnTVB5uQIDAQAB";
        if (base64EncodedPublicKey.contains("CONSTRUCT_YOUR"))

        {
            throw new RuntimeException("Please put your app's public key in MainActivity.java. See README.");
        }

        if (getPackageName().startsWith("com.example"))

        {
            throw new RuntimeException("Please change the sample's package name! See README.");
        }

        // Create the helper, passing it our context and the public key to verify signatures with
        mHelper = new
                IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener()

                           {
                               public void onIabSetupFinished(IabResult result) {
                                   if (!result.isSuccess()) {
                                       // Oh noes, there was a problem.
                                       complain("Problem setting up in-app billing: " + result);
                                       return;
                                   }

                                   // Have we been disposed of in the meantime? If so, quit.
                                   if (mHelper == null) return;

                                   if (NetWork.isNetWork(menuActivity)) {
                                       // IAB is fully set up. Now, let's get an inventory of stuff we own.
                                       ArrayList<String> skuList = new ArrayList<String>();
                                       skuList.add(SKU_PREMIUM);
                                       mHelper.queryInventoryAsync(true, skuList, mGotInventoryListener);
                                   }
                               }
                           }
        );

        if (NetWork.isNetWork(this)) {
//            getAds();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new readAdmobSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new readAdmobSyncTask().execute();
            }
        }
    }

    private boolean isDatabaseNull() {
        try {
            MyDatabase myDatabase = new MyDatabase(menuActivity);
            return myDatabase.checkDataNull();
        } catch (Exception e) {
            return false;
        }
    }

    private void downloadDatabase() {
        if (NetWork.isNetWork(this)) {
            if (NetWork.isNetWorkMobile(this)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(false)
                        .setMessage(R.string.internet_moblie)
                        .setPositiveButton(R.string.contine, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                new DownloadFileFromURL().execute(file_url);
                            }

                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                downloadDatabase();
                            }
                        });

                builder.create().show();
            } else {
                new DownloadFileFromURL().execute(file_url);
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setCancelable(false)
                    .setMessage(R.string.no_internet)
                    .setPositiveButton(R.string.try_to, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            downloadDatabase();
                        }

                    });
            builder.create().show();
        }
    }

    //press back again to exit
    boolean doubleBackToExitPressedOnce = false;

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    private void showSplashScreen() {
//        splashScreen = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        splashScreen = new Dialog(this, android.R.style.Theme_DeviceDefault_NoActionBar);
        splashScreen.setContentView(R.layout.splashscreen);
//        ProgressView progressBar = (ProgressView) splashScreen.findViewById(R.id.pbHeaderProgress);
        tv_load = (TextView) splashScreen.findViewById(R.id.load);
        ImageView imageBG = (ImageView) splashScreen.findViewById(R.id.imageBG);

//        int width = this.getResources().getDisplayMetrics().widthPixels;
//        int height = this.getResources().getDisplayMetrics().heightPixels;
//
//        Bitmap bitmap = decodeSampledBitmapFromResource(getResources(), R.drawable.drawable_port_screen, width, height);
//        imageBG.setImageBitmap(bitmap);
        Picasso.with(this).load(R.drawable.drawable_port_screen).placeholder(R.drawable.drawable_port_screen).fit().into(imageBG);
        splashScreen.show();
        splashScreen.setCancelable(false);
    }

    private class getMyIndexSyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showSplashScreen();
        }

        @Override
        protected Void doInBackground(Void... params) {
            MyDatabase db = new MyDatabase(getApplication());
            db.getIndex();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            sharedPreferences.edit().putBoolean("isDownload", true).commit();
            SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            boolean ranBefore = preferences.getBoolean("RanBefore", false);
            if (!ranBefore) {
                // first time
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("RanBefore", true);
                editor.commit();
            }
            try {
                if ((splashScreen != null) && splashScreen.isShowing())
                    splashScreen.dismiss();
            } catch (final IllegalArgumentException e) {
                // Handle or log or ignore
            } catch (final Exception e) {
                // Handle or log or ignore
            } finally {
                splashScreen = null;
            }

        }
    }

    private boolean isFirstTime() {
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        boolean ranBefore = preferences.getBoolean("RanBefore", false);
//        if (!ranBefore) {
//            // first time
//            SharedPreferences.Editor editor = preferences.edit();
//            editor.putBoolean("RanBefore", true);
//            editor.commit();
//        }
        return !ranBefore;
    }

    private void getAds() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("idBanner", idBanner);
        editor.putString("idInter", idInter);
        editor.putString("idNative", idNative);
        editor.putString("request_name", request_name);
        editor.putString("request_file", request_file);
        editor.putLong("probAds", probAdpress);
        editor.putLong("probInterval", probIntervalAds);
        editor.putFloat("probBanner", probBanner);
        editor.putFloat("probInter", probInter);

        editor.commit();
    }

    private class readAdmobSyncTask extends AsyncTask<Void, JSONObject, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... params) {
            return readJsonFromUrl(getString(R.string.main_url_adsmod));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            if (jsonObject == null) {
                return;
            }
            if (jsonObject.has("adsProb")) {
                try {
                    JSONObject adsProb = jsonObject.getJSONObject("adsProb");
                    if (adsProb.has("banner")) {
                        probBanner = Float.parseFloat(adsProb.getString("banner"));
                    }
                    if (adsProb.has("interstitial")) {
                        probInter = Float.parseFloat(adsProb.getString("interstitial"));
                    }
                    if (adsProb.has("adpress")) {
                        probAdpress = adsProb.getLong("adpress");
                    }
                    if (adsProb.has("intervalAdsInter")) {
                        probIntervalAds = adsProb.getLong("intervalAdsInter");
                    }
                    if (adsProb.has("loop")) {
                        isLoop = adsProb.getBoolean("loop");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (jsonObject.has("adsid")) {

                try {
                    JSONObject adsID = jsonObject.getJSONObject("adsid");
                    if (adsID.has("android")) {
                        JSONObject androidID = adsID.getJSONObject("android");
                        if (androidID.has("banner")) {
                            idBanner = androidID.getString("banner");
                        }
                        if (androidID.has("interstitial")) {
                            idInter = androidID.getString("interstitial");
                        }
                        if (androidID.has("native_medium")) {
                            idNative = androidID.getString("native_medium");
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (jsonObject.has("audio")) {
                try {
                    JSONObject audioObj = jsonObject.getJSONObject("audio");
                    if (audioObj.has("request_name")) {
                        request_name = audioObj.getString("request_name");
                    }
                    if (audioObj.has("request_file")) {
                        request_file = audioObj.getString("request_file");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            getAds();

            // create ads
            adsmodUtil.createFullAds(menuActivity);
            if (isLoop) {
                //neu do mang cham ma goi den onStart truoc thi can remove ham dc goi o onStart
                if (isAdsOnStart && handler != null) {
                    handler.removeCallbacks(runAdsInter);
                }
                isAdsOnCreate = true;
                loopShowAdsFull();
            }
        }
    }

    private boolean isAdsOnCreate, isAdsOnStart;

    private void loopShowAdsFull() {
        if (handler == null) {
            handler = new Handler();
        }
        handler.postDelayed(runAdsInter, timeLoop);
    }

    private Handler handler;
    private Runnable runAdsInter = new Runnable() {
        @Override
        public void run() {
            adsmodUtil.showIntervalAds();
            handler.postDelayed(this, timeLoop);
        }
    };


    public static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    /**
     * Hàm trả về JSONObject
     *
     * @param url - Truyền link URL có định dạng JSON
     * @return - Trả về JSONOBject
     * @throws IOException
     * @throws JSONException
     */
    public static JSONObject readJsonFromUrl(String url) {
        InputStream is = null;
        JSONObject json = null;
        try {
            is = new URL(url).openStream();
            try {
                //đọc nội dung với Unicode:
                BufferedReader rd = new BufferedReader
                        (new InputStreamReader(is, Charset.forName("UTF-8")));
                String jsonText = readAll(rd);
                json = new JSONObject(jsonText);

            } catch (JSONException e) {
//                Log.d("exception", e.toString());
            } finally {
                is.close();
            }
        } catch (IOException e) {
//            Log.d("exception", e.toString());
        }
        return json;
    }

    //Download DATABASE from url
    class DownloadFileFromURL extends AsyncTask<String, String, Boolean> {
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showSplashScreen();
            PACKAGE_NAME = menuActivity.getPackageName();
            showDialog();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected Boolean doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream("/data/data/" + PACKAGE_NAME + "/databases/" + ZIP);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return true;

            } catch (Exception e) {
            }

            return false;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(Boolean bolean) {
            // dismiss the dialog after the file was downloaded
            try {
                if ((pDialog != null) && pDialog.isShowing())
                    pDialog.dismiss();
            } catch (final IllegalArgumentException e) {
                // Handle or log or ignore
            } catch (final Exception e) {
                // Handle or log or ignore
            } finally {
                pDialog = null;
            }
//            unpackZip("/data/data/"+PACKAGE_NAME+"/", "javn2.db.zip");
            if (bolean) {
                new unzipSynctask().execute();
            } else {
                String msg;
                if (NetWork.isNetWork(MenuActivity.this)) {
                    msg = getString(R.string.error_download);
                } else {
                    msg = getString(R.string.error_download_no_internet);
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(menuActivity);
                builder.setMessage(msg)
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.try_to), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //do things
                                dialog.dismiss();
                                downloadDatabase();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }

    }

    private class unzipSynctask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tv_load.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            unpackZip("/data/data/" + PACKAGE_NAME + "/databases/", ZIP);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            deleteZip();
            new getMyIndexSyncTask().execute();
        }
    }

    private boolean unpackZip(String path, String zipname) {
        InputStream is;
        ZipInputStream zis;
        try {
            String filename;
            is = new FileInputStream(path + zipname);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null) {
                // zapis do souboru
                filename = ze.getName();

                // Need to create directories if not exists, or
                // it will generate an Exception...
                if (ze.isDirectory()) {
                    File fmd = new File(path + filename);
                    fmd.mkdirs();
                    continue;
                }

                FileOutputStream fout = new FileOutputStream(path + filename);

                // cteni zipu a zapis
                while ((count = zis.read(buffer)) != -1) {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void deleteZip() {
        File file = new File("/data/data/" + PACKAGE_NAME + "/databases/" + ZIP);
        if (file.exists()) {
            file.delete();
        }
    }

    /**
     * Showing Dialog
     */
    private ProgressDialog showDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage(getString(R.string.main_dialog_download_database));
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setProgressNumberFormat(null);
        pDialog.setCancelable(false);
        pDialog.show();
        return pDialog;
    }

    void complain(String message) {
//        alert("Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
            try {
                Purchase premiumPurchase = inventory.getPurchase(SKU_PREMIUM);
                price = inventory.getSkuDetails(SKU_PREMIUM).getPrice();
                mIsPremium = (premiumPurchase != null && verifyDeveloperPayload(premiumPurchase));
                if (mIsPremium)
                    MenuActivity.sharedPreferences.edit().putBoolean("isPremium", mIsPremium).commit();
            } catch (Exception e) {
            }
        }
    };

    /**
     * Verifies the developer payload of a purchase.
     */
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

    // User clicked the "Upgrade to Premium" button.
    public void onUpgradeAppButtonClicked() {
        //   setWaitScreen(true);

        /* TODO: for security, generate your payload here for verification. See the comments on
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
        String payload = "";

        mHelper.launchPurchaseFlow(this, SKU_PREMIUM, RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                // setWaitScreen(false);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                //  setWaitScreen(false);
                return;
            }


            if (purchase.getSku().equals(SKU_PREMIUM)) {
                // bought the premium upgrade!
                alert("Thank you for upgrading to premium!");
                mIsPremium = true;
                if (mIsPremium)
                    MenuActivity.sharedPreferences.edit().putBoolean("isPremium", mIsPremium).commit();
                //   updateUi();
                //  setWaitScreen(false);
            }

        }
    };

    @Override
    protected void onDestroy() {

        super.onDestroy();
        // very important:
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
        }
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener1 = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }
            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
            price = inventory.getSkuDetails(SKU_PREMIUM).getPrice();
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof TraCuuFragment) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getString(R.string.press_back_again), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("search");
                        TraCuuFragment traCuuFragment = new TraCuuFragment();
                        traCuuFragment.setMenuActivity(menuActivity);
                        changeFragment(traCuuFragment);
                    }
                }, 250);
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tracuu) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof TraCuuFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("search");
                        TraCuuFragment traCuuFragment = new TraCuuFragment();
                        traCuuFragment.setMenuActivity(menuActivity);
                        changeFragment(traCuuFragment);
                    }
                }, 250);
            }
        } else if (id == R.id.nav_jlpt) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof JLPTFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("jlpt");
                        JLPTFragment jlptFragment = new JLPTFragment();
                        jlptFragment.setMenuActivity(menuActivity);
                        changeFragment(jlptFragment);
                    }
                }, 250);

            }
        } else if (id == R.id.nav_docbao) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof DocBaoFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("news");
                        DocBaoFragment docBaoFragment = new DocBaoFragment();
                        docBaoFragment.setMenuActivity(menuActivity);
                        changeFragment(docBaoFragment);
                    }
                }, 250);

            }
        } else if (id == R.id.nav_sotay) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof SoTayFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("note");
                        SoTayFragment sotayFragment = new SoTayFragment();
                        sotayFragment.setMenuActivity(menuActivity);
                        changeFragment(sotayFragment);
                    }
                }, 250);

            }
        } else if (id == R.id.nav_chuyennganh) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof ChuyenNganhFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("list-special-term");
                        ChuyenNganhFragment chuyenNganhFragment = new ChuyenNganhFragment();
                        chuyenNganhFragment.setMenuActivity(menuActivity);
                        changeFragment(chuyenNganhFragment);
                    }
                }, 250);

            }
        } else if (id == R.id.nav_tapviet) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof TapVietFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("write");
                        TapVietFragment tapVietFragment = new TapVietFragment();
                        tapVietFragment.setMenuActivity(menuActivity);
                        changeFragment(tapVietFragment);
                    }
                }, 250);

            }
        } else if (id == R.id.nav_nangcap) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof UpdateFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("premium");
                        UpdateFragment updateFragment = new UpdateFragment(menuActivity, price);
                        updateFragment.setMenuActivity(menuActivity);
                        changeFragment(updateFragment);
                    }
                }, 250);

            }
        } else if (id == R.id.nav_yeuthich) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof YeuThichFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("favorite");
                        YeuThichFragment yeuThichFragment = new YeuThichFragment();
                        yeuThichFragment.setMenuActivity(menuActivity);
                        changeFragment(yeuThichFragment);
                    }
                }, 250);
            }
        } else if (id == R.id.nav_tuychinh) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (fragment instanceof TuyChinhFragment) {
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Track.sendTrackerScreen("settings");
                        TuyChinhFragment tuyChinhFragment = new TuyChinhFragment();
                        tuyChinhFragment.setMenuActivity(menuActivity);
                        changeFragment(tuyChinhFragment);
                    }
                }, 250);

            }
        } else if (id == R.id.nav_rate) {
            actionRate();
        } else if (id == R.id.nav_feedback) {
            actionFeedBack();
        } else if (id == R.id.nav_share) {
            actionShare();
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.commit();
    }

    private void actionRate() {
        Uri uri = Uri.parse("market://details?id="
                + menuActivity.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            menuActivity.startActivity(goToMarket);
        } catch (android.content.ActivityNotFoundException e) {
            menuActivity.startActivity((new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="
                            + menuActivity.getPackageName()))));
        }

    }

    private void actionFeedBack() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_EMAIL,
                new String[]{"support@mazii.net"});
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.gopy));
        intent.setType("message/rfc822");
        try {
            menuActivity.startActivity(Intent.createChooser(intent,
                    menuActivity.getString(R.string.feed_back)));
        } catch (android.content.ActivityNotFoundException e) {
            Toast.makeText(menuActivity,
                    getResources().getString(R.string.error),
                    Toast.LENGTH_SHORT).show();
        }

    }

    private void actionShare() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = getString(R.string.action_share);
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.chiase));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        menuActivity.startActivity(Intent.createChooser(sharingIntent, getString(R.string.chiase)));
    }

    public void hideSoftKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // todo check dialog download
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
            hideSoftKeyboard(this);
        }
    }

    public static void startMyActivity(Intent myIntent, Activity activity) {
        if (activity != null && myIntent != null) {
            MenuActivity.isActivityOther = true;
            activity.startActivity(myIntent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        stopService(new Intent(this, FastSearchService.class));
        isActivityOther = false;
        if (isLoop && !isAdsOnCreate) {
            isAdsOnStart = true;
            loopShowAdsFull();
        }
    }

    @Override
    protected void onStop() {
//        if (!isFirstTime()) {
        if (sharedPreferences.getBoolean("fast_search", false) && !isActivityOther) {
            if (Build.VERSION.SDK_INT < 23) {
                startService(new Intent(this, FastSearchService.class));
            } else if (Build.VERSION.SDK_INT >= 23 && Settings.canDrawOverlays(this)) {
                startService(new Intent(this, FastSearchService.class));
            }
        }
//        }
        if (handler != null) {
            isAdsOnCreate = false;
            handler.removeCallbacks(runAdsInter);
        }
        super.onStop();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                break;
        }
        return true;
    }

    private boolean isAvaliableToDownload() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = (long) stat.getBlockSize() * (long) stat.getBlockCount();
        long megAvailable = bytesAvailable / 1048576;
        if (megAvailable >= 200) {
            return true;
        }
        return false;
    }

    private void tryToDownload() {
        if (isAvaliableToDownload()) {
            downloadDatabase();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(menuActivity);
            builder.setMessage(getString(R.string.unzip_not_avaliable))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.try_to), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            dialog.dismiss();
                            tryToDownload();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // notify data set changed widget
        final AppWidgetManager mgr = AppWidgetManager.getInstance(this);
        final ComponentName cn = new ComponentName(this,
                WidgetProvider.class);
        mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn),
                R.id.page_flipper);
    }
}
