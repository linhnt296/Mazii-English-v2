package eup.mobi.jedictionary.module;

/**
 * Created by nguye on 12/2/2015.
 */
public class KanjiExampleObj {
    private String w;
    private String p;
    private String m;

    public String getW() {
        return w;
    }

    public void setW(String w) {
        this.w = w;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public KanjiExampleObj(String w, String p, String m) {
        this.w = w;
        this.p = p;
        this.m = m;
    }
}
