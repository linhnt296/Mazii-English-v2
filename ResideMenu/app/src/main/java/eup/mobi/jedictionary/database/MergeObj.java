package eup.mobi.jedictionary.database;

/**
 * Created by nguye on 10/15/2015.
 */
public class MergeObj {
    String hiragana;
    String kanji;

    public String getHiragana() {
        return hiragana;
    }

    public void setHiragana(String hiragana) {
        this.hiragana = hiragana;
    }

    public String getKanji() {
        return kanji;
    }

    public void setKanji(String kanji) {
        this.kanji = kanji;
    }

    public MergeObj(String kanji, String hiragana) {
        this.kanji = kanji;
        this.hiragana = hiragana;
    }
}
