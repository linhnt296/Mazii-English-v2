package eup.mobi.jedictionary.residemenuitems;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import eup.mobi.jedictionary.R;

import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.database.NewsOffline;
import eup.mobi.jedictionary.docbao.JResult;
import eup.mobi.jedictionary.docbao.JValue;
import eup.mobi.jedictionary.google.AdNativeExpress;
import eup.mobi.jedictionary.google.NetWork;
import eup.mobi.jedictionary.myadapter.MyAdapterNews;
import eup.mobi.jedictionary.myadapter.MyAdapterNewsOffline;
import eup.mobi.jedictionary.residemenuactivity.FastTransActivity;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.residemenuactivity.TraCuuTuActivity;

import com.rey.material.widget.CheckBox;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User: special
 * Date: 13-12-22
 * Time: 下午3:26
 * Mail: specialcyci@gmail.com
 */
public class DocBaoFragment extends Fragment implements MediaPlayer.OnPreparedListener {

    private View parentView;
    private ListView listView, lv_NewsOff;
    private Dialog dialog;
    private CheckBox cb_news;
    private MenuActivity menuActivity;
    private ArrayList<Jaen> javi = new ArrayList<>();
    private String wMean, wTitle, wPhonetic;
    int wFavorite, wID, mSeq;
    public static boolean isSearch = false;
    private String docbao_url_item = "http://mazii.net/api/news/";
    private String docbao_url_news = "http://mazii.net/api/news/5/10";

    static Boolean isCbNews;
    String orig = "", trans = "", translit = "";
    FloatingActionButton fab;
//    SearchView searchView;

    MyAdapterNews adapterNews;
    int status;
    String message, _id, _key, vId, vTitle, vDesc, textSelected = "";
    String newsTitle, newsDesc, newsPubdate, newsTxtBody, newsTxtMore, newsImg, newsVideo, newsAudio, newsTop, newsBot;
    WebView web_Title, web_Bot, wv_NewsOff;
    //    VideoView vv_Video, vv_Audio;
    ImageView iv_Img, iv_Boder, iv_Play, iv_boderOff;
    ScrollView scrollView, sv_NewsOff;

    MediaPlayer mp = new MediaPlayer();
    Button play, pause;
    ImageButton bt_tuychinh;
    private SeekBar mSeekBarPlayer;
    private TextView mMediaTime;
    private int current = 0;
    private boolean running = true;
    private int duration = 0;
    private ProgressBar progressBar;

    ArrayList<JResult> listJResult = new ArrayList<>();
    JValue jValue;
    JResult mJResult;
    boolean isPremium;

    static final String CSS = "<style id=\"furigana-config\">\n" + "</style>" +
            "\n" +
            "<style>" +
            "body {\n" +
            "   font-size: 18px;\n" +
            "   font-family: Roboto, serif\n" +
            "}\n" +
            "\n" +
            ".title {\n" +
            "   font-size: 20px;\n" +
            "   font-weight: 600;\n" +
            "}\n" +
            "\n" +
            ".description {\n" +
            "   font-size: 14px;\n" +
            "}\n" +
            "\n" +
            ".description p {\n" +
            "font-size: 14px;\n" +
            "}\n" +
            "\n" +
            ".date-time {\n" +
            "  font-size: 12px;\n" +
            "  color: grey;\n" +
            "  font-style: italic;\n" +
            "}\n" +
            "\n" +
            ".nhk {\n" +
            "  font-size: 12px;\n" +
            "  color: blue;\n" +
            "  font-style: italic;\n" +
            "  text-align: right;" +
            "}\n" +
            "\n" +
            ".main-content {\n" +
            "  font-size: 18px;\n" +
            "}\n" +
            "\n" +
            ".main-content p {\n" +
            "  line-height: 36px;\n" +
            "}\n" +
            "\n" +
            "</style>";
    static String SCRIPT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.docbao, container, false);
        ImageButton bt_menu = (ImageButton) parentView.findViewById(R.id.title_bar_left_menu);
        progressBar = (ProgressBar) parentView.findViewById(R.id.pbHeaderProgress);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.holo_blue), PorterDuff.Mode.SRC_IN);
        MenuActivity.isClicked = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MenuActivity.isClicked = false;
            }
        }, 2000);
        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                        MenuActivity.drawer.openDrawer(GravityCompat.START);
                } catch (Exception e) {
                }
            }
        });

        bt_tuychinh = (ImageButton) parentView.findViewById(R.id.bt_tuychon);
        bt_tuychinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdialogSetting();
            }
        });

        fab = (FloatingActionButton) parentView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!textSelected.equals("") && !textSelected.equals(" ")) {
//                    showdialogFastTrans(textSelected);
                    if (!isSearch)
                        new myAsyntask_tracuu1_ja().execute(textSelected);
                } else {
                    Toast.makeText(getContext(), getString(R.string.docbao_fast_null), Toast.LENGTH_SHORT).show();
                }
            }
        });
        SCRIPT = readJSfromAsset();
        web_Title = (WebView) parentView.findViewById(R.id.news_title);
        web_Bot = (WebView) parentView.findViewById(R.id.newsTextBot);
        wv_NewsOff = (WebView) parentView.findViewById(R.id.wv_Offline);

        // fast news trans
        WebSettings ws_title = web_Title.getSettings();
        ws_title.setJavaScriptEnabled(true);
        WebSettings ws_bot = web_Bot.getSettings();
        ws_bot.setJavaScriptEnabled(true);
        WebSettings ws_off = wv_NewsOff.getSettings();
        ws_off.setJavaScriptEnabled(true);

        web_Title.addJavascriptInterface(new JavaScriptInterface(getActivity()), "JSInterface");
        web_Bot.addJavascriptInterface(new JavaScriptInterface(getActivity()), "JSInterface");
        wv_NewsOff.addJavascriptInterface(new JavaScriptInterface(getActivity()), "JSInterface");

        play = (Button) parentView.findViewById(R.id.play);
        pause = (Button) parentView.findViewById(R.id.pause);
        mMediaTime = (TextView) parentView.findViewById(R.id.mediaTime);
        mSeekBarPlayer = (SeekBar) parentView.findViewById(R.id.progress_bar);

        iv_Img = (ImageView) parentView.findViewById(R.id.news_img);
        iv_Boder = (ImageView) parentView.findViewById(R.id.news_boder);
        iv_boderOff = (ImageView) parentView.findViewById(R.id.boder_off);
        iv_Play = (ImageView) parentView.findViewById(R.id.news_play_bt);
//        iv_full = (ImageView) parentView.findViewById(R.id.news_full_screen);
        listView = (ListView) parentView.findViewById(R.id.lv_NewsItems);
        lv_NewsOff = (ListView) parentView.findViewById(R.id.lv_offline);
        scrollView = (ScrollView) parentView.findViewById(R.id.sv_scrollView);
        sv_NewsOff = (ScrollView) parentView.findViewById(R.id.sv_NewsOff);

        scrollView.scrollTo(0, 0);
        isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        String banner = MenuActivity.sharedPreferences.getString("idNative", getString(R.string.ad_unit_id_medium));
        float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
        try {
            AdNativeExpress.smartNative(parentView, banner, probBanner, isPremium, false);
        } catch (Exception e) {

        }
        new createNewsReadedIfNotExit().execute();
        if (NetWork.isNetWork(getActivity())) {
            initView();
        } else {
            // todo load news offline
//            Toast.makeText(getContext(), "Không thể kết nối mạng!", Toast.LENGTH_SHORT).show();
            fab.setVisibility(View.GONE);
            new loadNewsOfflineSynctask().execute();
        }
        return parentView;
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new loadJValueSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, docbao_url_news);
        } else {
            new loadJValueSyncTask().execute(docbao_url_news);
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        // TODO Auto-generated method stub
        duration = mp.getDuration();
        mSeekBarPlayer.setMax(duration);
        mSeekBarPlayer.postDelayed(onEverySecond, 1000);
    }

    //LOAD LIST NEWS
    private class loadJValueSyncTask extends AsyncTask<String, JSONObject, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jsonObject = null;
            try {
                jsonObject = readJsonFromUrl(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            super.onPostExecute(object);
            if (object != null) {
                JSONObject jsonObject = object;
                if (jsonObject.has("status")) {
                    try {
                        status = jsonObject.getInt("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (jsonObject.has("message")) {
                    try {
                        message = jsonObject.getString("message");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (jsonObject.has("results")) {

                    try {
                        JSONArray arrResult = jsonObject.getJSONArray("results");
                        listJResult = new ArrayList<JResult>();
                        for (int i = 0; i < arrResult.length(); i++) {
                            JSONObject jResult = arrResult.getJSONObject(i);
                            if (jResult.has("id")) {
                                _id = jResult.getString("id");
                                if (i == 0)
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                            new loadSubResultSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, docbao_url_item + _id);
                                        } else {
                                            new loadSubResultSyncTask().execute(docbao_url_item + _id);
                                        }
                                    } catch (Exception e) {
                                    }
//                            new loadSubResultSyncTask().execute("http://mazii.net/api/news/8c03149ebb34458739ea63ccf6121643");
//                            searchView.clearFocus();
                            }
                            if (jResult.has("key")) {
                                _key = jResult.getString("key");
                            }
                            if (jResult.has("value")) {
                                JSONObject mValue = jResult.getJSONObject("value");
                                if (mValue.has("id")) {
                                    vId = mValue.getString("id");
                                }
                                if (mValue.has("title")) {
                                    vTitle = mValue.getString("title");
                                }
                                if (mValue.has("desc")) {
                                    vDesc = mValue.getString("desc");
                                }
                                jValue = new JValue(vId, vTitle, vDesc);


                            }
                            mJResult = new JResult(_id, _key, jValue);
                            listJResult.add(mJResult);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (listJResult != null) {
                    getListJValue();
                }
                for (int i = 0; i < listJResult.size(); i++) {
                    new saveNewsOffSyncTask().execute(listJResult.get(i).getId(), listJResult.get(i).getKey());
                }
            } else {
                fab.setVisibility(View.GONE);
                new loadNewsOfflineSynctask().execute();
            }
        }
    }

    private void getListJValue() {
        if (listJResult == null)
            return;
        try {
            adapterNews = new MyAdapterNews(getActivity(), R.layout.docbao_items, listJResult);
            listView.setAdapter(adapterNews);
            setListViewHeightBasedOnChildren(listView);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    progressBar.setVisibility(View.VISIBLE);
                    String itemsID = listJResult.get(position).getId();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new loadSubResultSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, docbao_url_item + itemsID);
                    } else {
                        new loadSubResultSyncTask().execute(docbao_url_item + itemsID);
                    }
                    scrollView.scrollTo(0, 0);
                    listJResult.get(position).getValue().setIsRead(true);
                    adapterNews.notifyDataSetChanged();
                    new setNewsReaded().execute(listJResult.get(position).getValue().getId());
                    mp.stop();
                    mp.reset();
                    play.setVisibility(View.VISIBLE);
                    pause.setVisibility(View.GONE);
                    MenuActivity.adsmodUtil.showIntervalAds();
                }
            });
            scrollView.scrollTo(0, 0);
        } catch (Exception e) {
        }
    }

    public static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    /**
     * Hàm trả về JSONObject
     *
     * @param url - Truyền link URL có định dạng JSON
     * @return - Trả về JSONOBject
     * @throws IOException
     * @throws JSONException
     */
    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            //đọc nội dung với Unicode:
            BufferedReader rd = new BufferedReader
                    (new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    //set listview full items
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 100;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    //LOAD SUBRESULT
    public class loadSubResultSyncTask extends AsyncTask<String, JSONObject, JSONObject> implements MediaPlayer.OnPreparedListener {

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jsObject = null;
            try {
                jsObject = readJsonFromUrl(params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            publishProgress(jsObject);
            return jsObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            JSONObject jsObject = jsonObject;
            if (jsObject != null) {
                if (jsonObject.has("status")) {
                    try {
                        status = jsonObject.getInt("status");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (jsObject.has("result")) {

                    try {
                        String date = "";
                        JSONObject jsResult = jsObject.getJSONObject("result");
                        if (jsResult.has("title")) {
                            newsTitle = jsResult.getString("title");
                        }
                        if (jsResult.has("pubDate")) {
                            newsPubdate = jsResult.getString("pubDate");
//                        11月12日17時20分
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date newDate = null;
                            try {
                                newDate = format.parse(newsPubdate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            format = new SimpleDateFormat("MM月dd日HH時mm分");
                            date = format.format(newDate);
                        }
                        if (jsResult.has("description")) {
                            newsDesc = jsResult.getString("description");
                            newsTop = "<div class=\"title\">" + newsTitle + "</div>" +
//                                    "<div class=\"description\">" + newsDesc + "</div>" +
                                    "<div class=\"date-time\">" + date + "</div>";

//                            web_Title.loadDataWithBaseURL(null, SCRIPT + newsTop, "text/html", "utf-8", null);
                            web_Title.loadDataWithBaseURL(null, CSS + newsTop + SCRIPT, "text/html", "utf-8", null);
                            if (MenuActivity.sharedPreferences.getBoolean("cb1", true)) {
                                web_Title.loadUrl("javascript:enableFurigana()");
                            } else {
                                web_Title.loadUrl("javascript:disableFurigana()");
                            }
                            web_Title.setVisibility(View.VISIBLE);
                        }
                        if (jsResult.has("content")) {
                            JSONObject jsContent = jsResult.getJSONObject("content");
                            if (jsContent.has("image")) {
                                newsImg = jsContent.getString("image");
                                if (newsImg != "null") {
                                    Picasso.with(getContext()).load(newsImg).into(iv_Img);
//                                if (vv_Video.isPlaying()) {
//                                    iv_Img.setVisibility(View.GONE);
//                                    iv_Play.setVisibility(View.GONE);
//                                } else {
//                                    iv_Img.setVisibility(View.VISIBLE);
//                                    iv_Play.setVisibility(View.VISIBLE);
//                                    iv_Play.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            vv_Video.setVisibility(View.VISIBLE);
//                                            vv_Video.start();
//                                            iv_Play.setVisibility(View.GONE);
//                                            iv_Img.setVisibility(View.GONE);
//                                        }
//                                    });
//                                }

//                                iv_Img.setVisibility(View.VISIBLE);
//                                iv_Play.setVisibility(View.VISIBLE);


                                } else return;

                            }
                            if (jsContent.has("video")) {
                                newsVideo = jsContent.getString("video");
                                if (newsVideo != "null") {
//                                vv_Video.setVisibility(View.VISIBLE);
//                                final MediaController mc = new MediaController(getContext());
//                                mc.setAnchorView(vv_Video);
//                                mc.setMediaPlayer(vv_Video);
//                                Uri vidUri = Uri.parse("https://nhkmovs-i.akamaihd.net/i/news/" + newsVideo + "/master.m3u8");
//                                vv_Video.setVideoURI(vidUri);
//                                vv_Video.setMediaController(mc);

//                                scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//
//                                    @Override
//                                    public void onScrollChanged() {
//                                        mc.hide();
//                                    }
//                                });

                                    iv_Img.setVisibility(View.VISIBLE);
                                    iv_Play.setVisibility(View.VISIBLE);
                                    iv_Play.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            try {
                                                String videoUrl = "https://nhkmovs-i.akamaihd.net/i/news/" + newsVideo + "/master.m3u8";
                                                Intent i = new Intent(Intent.ACTION_VIEW);
                                                i.setDataAndType(Uri.parse(videoUrl), "video/*");
                                                startActivity(i);
                                            } catch (Exception e) {
                                                Toast.makeText(getActivity(), getString(R.string.cannot_play_video), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                                }

                            }
                            if (jsContent.has("audio")) {
                                newsAudio = jsContent.getString("audio");
                                if (newsAudio != "null") {
                                    if (mp.isPlaying()) {
                                        mp.stop();
                                        mp.reset();
                                        mp.setDataSource("http://www3.nhk.or.jp/news/easy/" + newsAudio.replace(".mp3", "") + "/" + newsAudio);
                                        mp.prepare();
                                        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
//                                    mp.start();
                                    } else {
                                        mp.reset();
                                        mp.setDataSource("http://www3.nhk.or.jp/news/easy/" + newsAudio.replace(".mp3", "") + "/" + newsAudio);
                                        mp.prepare();
                                        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
//                                    mp.start();

                                    }
                                    mp.setOnPreparedListener(this);
                                    play.setVisibility(View.VISIBLE);
                                    mSeekBarPlayer.setVisibility(View.VISIBLE);
                                    mMediaTime.setVisibility(View.VISIBLE);
//                                updateTime();
                                    mSeekBarPlayer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                        @Override
                                        public void onStopTrackingTouch(SeekBar seekBar) {
                                        }

                                        @Override
                                        public void onStartTrackingTouch(SeekBar seekBar) {
                                        }

                                        @Override
                                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                            if (fromUser) {
                                                try {
                                                    mp.seekTo(progress);
                                                    updateTime();
                                                } catch (Exception e) {
                                                }
                                            }
                                        }
                                    });

                                    play.setOnClickListener(new View.OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            // TODO Auto-generated method stub
                                            try {
                                                mp.prepare();
                                            } catch (IllegalStateException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            } catch (IOException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                            mp.start();
                                            pause.setVisibility(View.VISIBLE);
                                            play.setVisibility(View.GONE);
                                            mSeekBarPlayer.postDelayed(onEverySecond, 1000);
                                        }
                                    });


                                    pause.setOnClickListener(new View.OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            // TODO Auto-generated method stub
                                            mp.pause();
                                            play.setVisibility(View.VISIBLE);
                                            pause.setVisibility(View.GONE);
                                        }
                                    });
                                }

                            }

                            if (jsContent.has("textbody")) {
                                newsTxtBody = jsContent.getString("textbody");
                                iv_Boder.setVisibility(View.VISIBLE);
                            }
                            if (jsContent.has("textmore")) {
                                newsTxtMore = jsContent.getString("textmore");

                            }
                            newsBot = "<div class=\"main-content\">" + newsTxtBody + "</div>" +
                                    "<div class=\"main-content\">" + newsTxtMore + "</div>" +
                                    "<div class=\"nhk\">ソース：NHK　ニュース</div>";
                            web_Bot.loadDataWithBaseURL(null, CSS + newsBot + SCRIPT, "text/html", "utf-8", null);
                            if (MenuActivity.sharedPreferences.getBoolean("cb1", true)) {
                                web_Bot.loadUrl("javascript:enableFurigana()");
                            } else {
                                web_Bot.loadUrl("javascript:disableFurigana()");
                            }
                            web_Bot.setVisibility(View.VISIBLE);
                            listView.setVisibility(View.VISIBLE);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                    scrollView.setVisibility(View.VISIBLE);
                                }
                            }, 500);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), getString(R.string.docbao_load_faile), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onPrepared(MediaPlayer mp) {
            // TODO Auto-generated method stub
            duration = mp.getDuration();
            mSeekBarPlayer.setMax(duration);
            mSeekBarPlayer.postDelayed(onEverySecond, 1000);
        }
    }

    private Runnable onEverySecond = new Runnable() {
        @Override
        public void run() {
            if (true == running) {
                if (mSeekBarPlayer != null) {
                    mSeekBarPlayer.setProgress(mp.getCurrentPosition());
                }

                if (mp.isPlaying()) {
                    mSeekBarPlayer.postDelayed(onEverySecond, 1000);
                    updateTime();
                }
            }
        }
    };

    private void updateTime() {
        do {
            current = mp.getCurrentPosition();
            System.out.println("duration - " + duration + " current- "
                    + current);
            int dSeconds = (int) (duration / 1000) % 60;
            int dMinutes = (int) ((duration / (1000 * 60)) % 60);
            int dHours = (int) ((duration / (1000 * 60 * 60)) % 24);

            int cSeconds = (int) (current / 1000) % 60;
            int cMinutes = (int) ((current / (1000 * 60)) % 60);
            int cHours = (int) ((current / (1000 * 60 * 60)) % 24);

            if (dHours == 0) {
                mMediaTime.setText(String.format("%02d:%02d / %02d:%02d", cMinutes, cSeconds, dMinutes, dSeconds));
            } else {
                mMediaTime.setText(String.format("%02d:%02d:%02d / %02d:%02d:%02d", cHours, cMinutes, cSeconds, dHours, dMinutes, dSeconds));
            }

            try {
                if (mSeekBarPlayer.getProgress() >= 100) {
                    break;
                }
            } catch (Exception e) {
            }
        } while (mSeekBarPlayer.getProgress() <= 100 && mSeekBarPlayer.getProgress() != 0);
    }


    //todo offline news
    //save to database
    public class saveNewsOffSyncTask extends AsyncTask<String, JSONObject, JSONObject> {
        String id, pubDate;

        @Override
        protected JSONObject doInBackground(String... params) {
            id = (params[0]);
            pubDate = params[1];
            JSONObject jsObject = null;
            try {
                jsObject = readJsonFromUrl(docbao_url_item + params[0]);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            publishProgress(jsObject);
            return jsObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            MyDatabase db = new MyDatabase(getContext());
            // insert JSON
            if (!db.checkIdNewsOffline(id) && db != null) {
                if (id != null && jsonObject != null && pubDate != null) {
                    try {
                        db.insertNewsOffline(id, jsonObject.toString(), pubDate);
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    //load news offline from database
    private class loadNewsOfflineSynctask extends AsyncTask<Void, Void, ArrayList<NewsOffline>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<NewsOffline> doInBackground(Void... params) {
            MyDatabase db = new MyDatabase(getContext());
            ArrayList<NewsOffline> arrNews = db.loadNewsOffFromDB();
            return arrNews;
        }

        @Override
        protected void onPostExecute(final ArrayList<NewsOffline> newsOfflines) {
            super.onPostExecute(newsOfflines);
            getNewsOffView(newsOfflines);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    sv_NewsOff.setVisibility(View.VISIBLE);
                    if (newsOfflines.size() > 0) {
                        iv_boderOff.setVisibility(View.VISIBLE);
                    }
                    progressBar.setVisibility(View.GONE);
                }
            }, 500);
        }
    }

    private void getNewsOffView(final ArrayList<NewsOffline> listNews) {
        if (listNews != null && listNews.size() > 0) {
            wv_NewsOff.loadDataWithBaseURL(null, CSS + listNews.get(0).getHtml() + SCRIPT, "text/html", "utf-8", null);
            if (MenuActivity.sharedPreferences.getBoolean("cb1", true)) {
                wv_NewsOff.loadUrl("javascript:enableFurigana()");
            } else {
                wv_NewsOff.loadUrl("javascript:disableFurigana()");
            }
            final MyAdapterNewsOffline adapterNewsOffline = new MyAdapterNewsOffline(getActivity(), R.layout.docbao_items, listNews);
            lv_NewsOff.setAdapter(adapterNewsOffline);
            setListViewHeightBasedOnChildren(lv_NewsOff);
            lv_NewsOff.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    wv_NewsOff.loadDataWithBaseURL(null, CSS + listNews.get(position).getHtml() + SCRIPT, "text/html", "utf-8", null);
                    if (MenuActivity.sharedPreferences.getBoolean("cb1", true)) {
                        wv_NewsOff.loadUrl("javascript:enableFurigana()");
                    } else {
                        wv_NewsOff.loadUrl("javascript:disableFurigana()");
                    }
                    sv_NewsOff.scrollTo(0, 0);
                    listNews.get(position).setIsRead(true);
                    adapterNewsOffline.notifyDataSetChanged();
                    new setNewsReaded().execute(listNews.get(position).getTitle());
                }
            });
        } else {
            String noData = "<h1 style=\"padding: 200px 0 0 0\" align=\"center\"><font color=\"blue\">Không có dữ liệu Offline</font></h1>";
            wv_NewsOff.loadDataWithBaseURL(null, noData, "text/html", "utf-8", null);
        }
    }

    // tuy chinh
    private void showdialogSetting() {
        // custom dialog
        dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.news_dialog);
        isCbNews = MenuActivity.sharedPreferences.getBoolean("cb1", true);
        cb_news = (com.rey.material.widget.CheckBox) dialog.findViewById(R.id.cb_news);
        cb_news.setChecked(isCbNews);

        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                MenuActivity.sharedPreferences.edit().putBoolean("cb1", cb_news.isChecked()).commit();
                if (cb_news.isChecked()) {
                    if (NetWork.isNetWork(getContext())) {
                        if (web_Title != null) {
                            web_Title.loadUrl("javascript:enableFurigana()");
                        }
                        if (web_Bot != null) {
                            web_Bot.loadUrl("javascript:enableFurigana()");
                        }
                    } else {
                        if (wv_NewsOff != null) {
                            wv_NewsOff.loadUrl("javascript:enableFurigana()");
                        }
                    }
                } else {
                    if (NetWork.isNetWork(getContext())) {
                        if (web_Title != null) {
                            web_Title.loadUrl("javascript:disableFurigana()");
                        }
                        if (web_Bot != null) {
                            web_Bot.loadUrl("javascript:disableFurigana()");
                        }
                    } else {
                        if (wv_NewsOff != null) {
                            wv_NewsOff.loadUrl("javascript:disableFurigana()");
                        }
                    }
                }
            }
        });

    }

    private void showdialogFastTrans(String text) {
        // custom dialog
        if (NetWork.isNetWork(getContext())) {
            getTrans(text);
        }
    }

    private void showdialogSearchNews(String text) {
        if (!isSearch) {
            // query search news
            new myAsyntask_tracuu1_ja().execute(text);
        }
    }

    public class JavaScriptInterface {
        private Activity activity;

        public JavaScriptInterface(Activity activiy) {
            this.activity = activiy;
        }

        @JavascriptInterface
        public void onSelectedChanged(String text) {
            textSelected = text;
        }

        @JavascriptInterface
        public void onClickText(final String text) {
//            Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showdialogSearchNews(text);
                }
            });
        }
    }

    private void getTrans(String text) {
        String google_translate_url = "https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&dt=bd&dj=1&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=at";
        String from = "ja";
        String to = MenuActivity.sharedPreferences.getString("to_language", Locale.getDefault().getLanguage());
        try {
            text = URLEncoder.encode(text, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url = google_translate_url + "&sl=" + from + "&tl=" + to + "&q=" + text;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new transSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
        } else {
            new transSyncTask().execute(url);
        }
    }

    public String getJSON(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
//            c.setRequestProperty("Content-length", "0");
            c.setRequestProperty("charset", "UTF-8");
            c.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36");

            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
//            BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
//            StringBuilder sb = new StringBuilder();
//            String line;
//            while ((line = br.readLine()) != null) {
//                sb.append(line + "\n");
//            }
//            br.close();
//            return sb.toString();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    private JSONObject getMyJSON(String data) throws JSONException {
        JSONObject object = new JSONObject(data);
        return object;
    }

    private class transSyncTask extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jsonObject = null;
            try {
                String data = getJSON(params[0], 500);
                if (data != null)
                    jsonObject = getMyJSON(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject object) {
            super.onPostExecute(object);
            JSONObject jsonObject = object;
            if (jsonObject == null) {
                return;
            }
            if (jsonObject.has("sentences")) {
                try {
                    JSONArray sentences = jsonObject.getJSONArray("sentences");
                    for (int i = 0; i < sentences.length(); i++) {
                        JSONObject jTrans = sentences.getJSONObject(i);
                        if (jTrans.has("trans")) {
                            trans += jTrans.getString("trans");
                        }
                        if (jTrans.has("orig")) {
                            orig += jTrans.getString("orig");
                        }
                        if (jTrans.has("src_translit")) {
                            translit += jTrans.getString("src_translit");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if (orig == null) {
                orig = getActivity().getString(R.string.docbao_trans_null);
            }
            if (translit == null) {
                translit = "";
            }
            if (trans == null) {
                trans = getActivity().getString(R.string.docbao_trans_null1);
            }

            Intent myIntent = new Intent(getActivity(), FastTransActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("orig", orig);
            bundle.putString("translit", translit);
            bundle.putString("trans", trans);

            myIntent.putExtra("FastTrans", bundle);
            MenuActivity.startMyActivity(myIntent, getMenuActivity());
            isSearch = true;
        }
    }

    // tra cuu tu doc bao
    private class myAsyntask_tracuu1_ja extends AsyncTask<String, Void, ArrayList<Jaen>> {

        @Override
        protected ArrayList<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            if (!params[0].equals("") && !params[0].equals(" ")) {
                javi = db.getJaen(params[0]);
            }
            return javi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<Jaen> javis) {
            super.onPostExecute(javis);
            getListJavi();
        }
    }

    private void getListJavi() {
        if (javi == null || javi.size() == 0) {
            showdialogFastTrans(textSelected);
        } else {

            wMean = javi.get(0).getmMean();
            wTitle = javi.get(0).getmWord();
            wPhonetic = javi.get(0).getmPhonetic();
            wFavorite = javi.get(0).getmFavorite();
            wID = javi.get(0).getmId();

            Intent myIntent = new Intent(getActivity(), TraCuuTuActivity.class);
            //Khai báo Bundle
            Bundle bundle = new Bundle();
            //đưa dữ liệu riêng lẻ vào Bundle
            bundle.putString("vMean", wMean);
            bundle.putString("vTitle", wTitle);
            bundle.putString("verbPhonetic", wPhonetic);

            if (wPhonetic != null) {
                bundle.putString("vPhonetic", "「" + wPhonetic + "」");
            }
            bundle.putInt("mFavorite", wFavorite);
            bundle.putInt("wID", wID);

            //Đưa Bundle vào Intent
            myIntent.putExtra("TraCuuFragment", bundle);
            //Mở Activity ResultActivity
            MenuActivity.startMyActivity(myIntent, getMenuActivity());
            isSearch = true;
        }

    }


    public MenuActivity getMenuActivity() {
        return menuActivity;
    }

    public void setMenuActivity(MenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }

    // fix audio play on background

    //    @Override
//    public void onStop() {
//        super.onStop();
//        mp.pause();
//        play.setVisibility(View.VISIBLE);
//        pause.setVisibility(View.GONE);
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mp.stop();
//    }
    private class setNewsReaded extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getActivity());
            db.setNewsReaded(params[0]);
            return null;
        }
    }

    private class createNewsReadedIfNotExit extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            MyDatabase db = new MyDatabase(getActivity());
            db.createNewsReaded();
            return null;
        }
    }

    private String readJSfromAsset() {
        try {
            StringBuilder buf = new StringBuilder();
            InputStream json = null;
            String str;
            json = getActivity().getAssets().open("javascript/news.js");
            BufferedReader in =
                    new BufferedReader(new InputStreamReader(json, "UTF-8"));
            while ((str = in.readLine()) != null) {
                buf.append(str + "\n");
            }
            in.close();
            return buf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
