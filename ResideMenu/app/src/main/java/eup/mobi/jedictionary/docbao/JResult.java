package eup.mobi.jedictionary.docbao;

/**
 * Created by nguye on 9/15/2015.
 */
public class JResult {
    public String id ;
    public String key ;
    public JValue value ;
    public String img;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public JValue getValue() {
        return value;
    }

    public void setValue(JValue value) {
        this.value = value;
    }

    public JResult(String id, String key, JValue value) {
        this.id = id;
        this.key = key;
        this.value = value;
    }

    public JResult(String id, String key, JValue value, String img) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.img = img;
    }
}
