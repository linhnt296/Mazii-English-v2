package eup.mobi.jedictionary.docbao;

/**
 * Created by nguye on 9/15/2015.
 */
public class JValue {
    public String id;
    public String title;
    public String desc;
    public boolean isRead;

    public boolean isRead() {
        return isRead;
    }

    public void setIsRead(boolean isRead) {
        this.isRead = isRead;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public JValue(String id, String title, String desc) {
        this.id = id;
        this.title = title;
        this.desc = desc;
    }

    public JValue(String id, String title, String desc, boolean isRead) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.isRead = isRead;
    }
}
