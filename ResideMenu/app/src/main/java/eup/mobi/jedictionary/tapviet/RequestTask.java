package eup.mobi.jedictionary.tapviet;

import android.os.AsyncTask;

import eup.mobi.jedictionary.residemenuitems.TapVietFragment;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by thanh on 10/12/2015.
 */
public class RequestTask extends AsyncTask<String, String, String> {
    private TapVietFragment tapVietFragment;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s != null) {
            try {
                JSONArray array = new JSONArray(s);
                JSONArray array1 = array.getJSONArray(1);
                JSONArray array2 = array1.getJSONArray(0);
                JSONArray array3 = array2.getJSONArray(1);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public TapVietFragment getTapVietFragment() {
        return tapVietFragment;
    }

    public void setTapVietFragment(TapVietFragment tapVietFragment) {
        this.tapVietFragment = tapVietFragment;
    }

    @Override
    protected String doInBackground(String... params) {
        String json = params[0];
        String result = sendRequest(json);
        if (result != null) {
            return result;
        }
        return null;
    }

    public String sendRequest(String json) {
        String address = "https://inputtools.google.com/request?itc=ja-t-i0-handwrit&app=translate";
        StringBuilder sb = new StringBuilder();
        OutputStreamWriter printlnOut;
        DataInputStream input;
        try {
            URL url = new URL(address);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            printlnOut = new OutputStreamWriter(urlConnection.getOutputStream());
            //Json
            /*StringWriter stringWriter = new StringWriter();
            JsonWriter jsonWriter = new JsonWriter(stringWriter);

            jsonWriter.beginObject(); // {
            jsonWriter.name("api_level").value("537.36");
            jsonWriter.name("app_version").value(0.4);
            jsonWriter.name("input_type").value(0);
            jsonWriter.name("device").value("5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
            jsonWriter.name("options").value("enable_pre_space");
            jsonWriter.name("requests");
            jsonWriter.beginArray(); // [
            jsonWriter.beginObject(); // {
            jsonWriter.name("writing_guide");
            jsonWriter.beginObject(); // {
            jsonWriter.name("writing_area_width").value(425);
            jsonWriter.name("writing_area_height").value(194);
            jsonWriter.endObject(); // }
            jsonWriter.name("pre_context").value("");
            jsonWriter.name("max_num_results").value(10);
            jsonWriter.name("max_completions").value(0);
            jsonWriter.name("ink");
            jsonWriter.beginArray(); // [
            jsonWriter.beginArray(); // [
            jsonWriter.beginArray(); // [
            jsonWriter.value(92);
            jsonWriter.value(91);
            jsonWriter.value(91);
            jsonWriter.value(91);
            jsonWriter.value(91);
            jsonWriter.value(90);
            jsonWriter.value(90);
            jsonWriter.value(90);
            jsonWriter.value(89);
            jsonWriter.value(89);
            jsonWriter.value(88);
            jsonWriter.value(87);
            jsonWriter.value(86);
            jsonWriter.value(85);
            jsonWriter.value(85);
            jsonWriter.value(84);
            jsonWriter.value(83);
            jsonWriter.value(82);
            jsonWriter.value(81);
            jsonWriter.value(80);
            jsonWriter.value(79);
            jsonWriter.value(78);
            jsonWriter.value(77);
            jsonWriter.value(77);
            jsonWriter.value(76);
            jsonWriter.value(75);
            jsonWriter.value(75);
            jsonWriter.value(74);
            jsonWriter.value(72);
            jsonWriter.value(72);
            jsonWriter.value(71);
            jsonWriter.value(70);
            jsonWriter.value(69);
            jsonWriter.value(67);
            jsonWriter.value(66);
            jsonWriter.value(64);
            jsonWriter.value(61);
            jsonWriter.value(59);
            jsonWriter.value(56);
            jsonWriter.value(55);
            jsonWriter.value(53);
            jsonWriter.value(51);
            jsonWriter.value(49);
            jsonWriter.value(46);
            jsonWriter.value(44);
            jsonWriter.value(41);
            jsonWriter.value(39);
            jsonWriter.value(36);
            jsonWriter.value(34);
            jsonWriter.value(32);
            jsonWriter.value(31);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.value(30);
            jsonWriter.endArray(); // ]
            jsonWriter.beginArray(); // [
            jsonWriter.value(55);
            jsonWriter.value(56);
            jsonWriter.value(58);
            jsonWriter.value(61);
            jsonWriter.value(64);
            jsonWriter.value(70);
            jsonWriter.value(73);
            jsonWriter.value(77);
            jsonWriter.value(80);
            jsonWriter.value(84);
            jsonWriter.value(90);
            jsonWriter.value(94);
            jsonWriter.value(98);
            jsonWriter.value(101);
            jsonWriter.value(104);
            jsonWriter.value(106);
            jsonWriter.value(108);
            jsonWriter.value(113);
            jsonWriter.value(116);
            jsonWriter.value(119);
            jsonWriter.value(122);
            jsonWriter.value(126);
            jsonWriter.value(128);
            jsonWriter.value(129);
            jsonWriter.value(131);
            jsonWriter.value(133);
            jsonWriter.value(134);
            jsonWriter.value(137);
            jsonWriter.value(139);
            jsonWriter.value(140);
            jsonWriter.value(142);
            jsonWriter.value(142);
            jsonWriter.value(144);
            jsonWriter.value(145);
            jsonWriter.value(146);
            jsonWriter.value(147);
            jsonWriter.value(147);
            jsonWriter.value(148);
            jsonWriter.value(149);
            jsonWriter.value(149);
            jsonWriter.value(149);
            jsonWriter.value(149);
            jsonWriter.value(149);
            jsonWriter.value(149);
            jsonWriter.value(148);
            jsonWriter.value(147);
            jsonWriter.value(144);
            jsonWriter.value(141);
            jsonWriter.value(137);
            jsonWriter.value(134);
            jsonWriter.value(131);
            jsonWriter.value(127);
            jsonWriter.value(123);
            jsonWriter.value(118);
            jsonWriter.value(109);
            jsonWriter.value(104);
            jsonWriter.value(101);
            jsonWriter.value(99);
            jsonWriter.value(98);
            jsonWriter.value(97);
            jsonWriter.value(96);
            jsonWriter.value(95);
            jsonWriter.endArray(); // ]
            jsonWriter.beginArray(); // [
            jsonWriter.value(0);
            jsonWriter.value(187);
            jsonWriter.value(203);
            jsonWriter.value(203);
            jsonWriter.value(219);
            jsonWriter.value( 234);
            jsonWriter.value( 250);
            jsonWriter.value( 250);
            jsonWriter.value( 265);
            jsonWriter.value(265);
            jsonWriter.value( 281);
            jsonWriter.value(297);
            jsonWriter.value( 297);
            jsonWriter.value(  312);
            jsonWriter.value( 328);
            jsonWriter.value( 343);
            jsonWriter.value( 343);
            jsonWriter.value( 359);
            jsonWriter.value( 375);
            jsonWriter.value( 375);
            jsonWriter.value( 390);
            jsonWriter.value(406);
            jsonWriter.value( 406);
            jsonWriter.value( 421);
            jsonWriter.value( 437);
            jsonWriter.value(  437);
            jsonWriter.value( 453);
            jsonWriter.value( 468);
            jsonWriter.value( 484);
            jsonWriter.value( 484);
            jsonWriter.value( 499);
            jsonWriter.value( 515);
            jsonWriter.value( 515);
            jsonWriter.value( 531);
            jsonWriter.value( 531);
            jsonWriter.value(   546);
            jsonWriter.value(   562);
            jsonWriter.value(   562);
            jsonWriter.value( 577);
            jsonWriter.value( 593);
            jsonWriter.value(609);
            jsonWriter.value( 609);
            jsonWriter.value(  624);
            jsonWriter.value(   640);
            jsonWriter.value(  640);
            jsonWriter.value(  655);
            jsonWriter.value(  671);
            jsonWriter.value(671);
            jsonWriter.value(  687);
            jsonWriter.value( 702);
            jsonWriter.value(702);
            jsonWriter.value( 718);
            jsonWriter.value( 733);
            jsonWriter.value( 749);
            jsonWriter.value(  765);
            jsonWriter.value( 765);
            jsonWriter.value( 780);
            jsonWriter.value(796);
            jsonWriter.value(796);
            jsonWriter.value(811);
            jsonWriter.value(843);
            jsonWriter.value(858);
            jsonWriter.endArray(); // ]
            jsonWriter.endArray(); // ]
            jsonWriter.endArray(); // ]
            //jsonWriter.name("language").value("ja");
            jsonWriter.endObject(); // }
           *//* jsonWriter.beginObject();
            jsonWriter.name("feedback").value("∅[deleted]");
            jsonWriter.name("select_type").value("deleted");
            jsonWriter.name("ink_hash").value("18d06cfd82f0175f");
            jsonWriter.endObject();*//*
            jsonWriter.endArray(); // ]
            jsonWriter.endObject(); // }

            jsonWriter.flush();
            jsonWriter.close();
            String json1 = stringWriter.toString();
            Log.d("Json", json);*/
            //==========
            printlnOut.write(json.toString());
            printlnOut.flush();
            printlnOut.close();
            int httpResult = urlConnection.getResponseCode();

            if (httpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                return sb.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
