package eup.mobi.jedictionary.residemenuitems;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Example;
import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.Kanji;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.google.NetWork;
import eup.mobi.jedictionary.history.HistoryCau;
import eup.mobi.jedictionary.history.HistoryGrammar;
import eup.mobi.jedictionary.history.HistoryKanji;
import eup.mobi.jedictionary.history.HistoryWord;
import eup.mobi.jedictionary.kanjirecognize.OfflineRecogDialog;
import eup.mobi.jedictionary.module.Language;
import eup.mobi.jedictionary.module.MyGridView;
import eup.mobi.jedictionary.module.MyHashMap;
import eup.mobi.jedictionary.module.VerbTable;
import eup.mobi.jedictionary.module.WanaKanaJava;
import eup.mobi.jedictionary.myadapter.MyAdapterCau;
import eup.mobi.jedictionary.myadapter.MyAdapterHanTu;
import eup.mobi.jedictionary.myadapter.MyAdapterHanTu2;
import eup.mobi.jedictionary.myadapter.MyAdapterHistoryCau;
import eup.mobi.jedictionary.myadapter.MyAdapterHistoryKanji;
import eup.mobi.jedictionary.myadapter.MyAdapterHistoryWord;
import eup.mobi.jedictionary.myadapter.MyAdapterTuJa;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.tapviet.HandWriteDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.sfsu.cs.orange.ocr.CaptureActivity;

/**
 * User: special
 * Date: 13-12-223
 * Time: 下午1:33
 * Mail: specialcyci@gmail.com
 */
public class TraCuuFragment extends Fragment {
    public OfflineRecogDialog offlineRecogDialog;
    public HandWriteDialog handWriteDialog;
    private View parentView;
    ArrayList<Jaen> javi, sJavi;
    ArrayList<Kanji> kanji, sKanji;
    ArrayList<Example> cau;
    TabHost tab;
    public static SearchView searchView;
    //    TextView tv_mMean;
    ListView lv_suggest, lv_tab4;
    MyGridView lv_2;
    CardView cardView;
    MyAdapterTuJa adapter_suggest_ja;
    MyAdapterHanTu adapter_suggest_kanji;
    MyAdapterHanTu2 adapter_2;
    MyAdapterCau adapter_suggest_cau;
    String vMean, vTitle, mKanji, mMean, vPhonetic;
    int mFavorite, wID, mSeq;
    private String mKun, mOn, mStroke_count, mLevel, mCompDetail, mDetail, mExamples = null;
    Boolean isVija = false;
    long startTime;
    public static long time1;
    int count = 0;
    String svg_kanji;
    boolean isSearch, isTranslate, isSubK;
    public static boolean isSubmit;
    ImageButton write;
    public static FloatingActionMenu fab_menu;
    FloatingActionButton fab_micro, fab_camera;
    Handler handler, handler_2;
    TextView tv_japanese, tv_vietnamese, tv_romanji, tv_dich;
    String trans, orig, translit, url;
    String mQuery, query_WKG = "";
    public static final int RESULT_SPEECH = 1;
    SwipeMenuListView lv_historyWord, lv_historyKanji, lv_historyCau;
    ArrayList<HistoryWord> arrWord = new ArrayList<>();
    ArrayList<HistoryKanji> arrKanji = new ArrayList<>();
    ArrayList<HistoryCau> arrCau = new ArrayList<>();
    MyAdapterHistoryWord adapterHistoryWord = null;
    MyAdapterHistoryKanji adapterHistoryKanji = null;
    MyAdapterHistoryCau adapterHistoryCau = null;
    private String oldQueryWord = "", oldQueryKanji = "", oldQueryCau = "";
    FragmentManager fragmentManager1, fragmentManager2;
    FrameLayout frameLayout1, frameLayout2;
    FragTab1 tab1_Fagment;
    FragTab2 tab2_Fagment;
    MenuActivity menuActivity;
    private ScrollView scrollView;
    private int oldPos;
    public static boolean isShowFab = true;

    public MenuActivity getMenuActivity() {
        return menuActivity;
    }

    public void setMenuActivity(MenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.tracuu, container, false);
        handWriteDialog = new HandWriteDialog(getActivity());
        offlineRecogDialog = new OfflineRecogDialog(getActivity());
        ImageButton bt_menu = (ImageButton) parentView.findViewById(R.id.title_bar_left_menu);
        lv_suggest = (ListView) parentView.findViewById(R.id.lv_suggest);
        lv_historyWord = (SwipeMenuListView) parentView.findViewById(R.id.lv_historyWord);
        lv_historyKanji = (SwipeMenuListView) parentView.findViewById(R.id.lv_historyKanji);
        lv_historyCau = (SwipeMenuListView) parentView.findViewById(R.id.lv_historyCau);
        tv_japanese = (TextView) parentView.findViewById(R.id.tv_japanese);
        tv_romanji = (TextView) parentView.findViewById(R.id.tv_romanji);
        tv_vietnamese = (TextView) parentView.findViewById(R.id.tv_vietnamese);
        tv_dich = (TextView) parentView.findViewById(R.id.tv_dich);
        tab = (TabHost) parentView.findViewById(android.R.id.tabhost);
        fab_menu = (FloatingActionMenu) parentView.findViewById(R.id.fab_menu);
        fab_menu.setClosedOnTouchOutside(true);
        searchView = (SearchView) parentView.findViewById(R.id.searchView);
        scrollView = (ScrollView) parentView.findViewById(R.id.scrollView2);
        // set color text hint
        EditText editTextSearch = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        editTextSearch.setHintTextColor(getResources().getColor(R.color.search_hint));
        editTextSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (tab.getCurrentTab()) {
                    case 0:
                        if ((adapter_suggest_ja != null && !adapter_suggest_ja.isEmpty())  && !searchView.getQuery().toString().equals(""))
                            lv_suggest.setVisibility(View.VISIBLE);
                    case 1:
                        if (adapter_suggest_kanji != null && !adapter_suggest_kanji.isEmpty() && !searchView.getQuery().toString().equals(""))
                            lv_suggest.setVisibility(View.VISIBLE);
                }
            }
        });
        searchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                searchView.requestFocusFromTouch();
                return true;
            }
        });

        fragmentManager1 = getChildFragmentManager();
        fragmentManager2 = getChildFragmentManager();
        frameLayout1 = (FrameLayout) parentView.findViewById(R.id.frame_tab1);
        frameLayout2 = (FrameLayout) parentView.findViewById(R.id.frame_tab2);
        lv_2 = (MyGridView) parentView.findViewById(R.id.gridView_k);
        lv_tab4 = (ListView) parentView.findViewById(R.id.lv_tab4);
        cardView = (CardView) parentView.findViewById(R.id.cardview1);

        MyHashMap.init();
        VerbTable.init();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MenuActivity.isClicked = false;
            }
        }, 2000);

//      SearchView searchView = new SearchView(getContext());
        SearchView.SearchAutoComplete theTextArea = (SearchView.SearchAutoComplete) searchView.findViewById(R.id.search_src_text);
        theTextArea.setTextColor(Color.WHITE);//or any color that you want
        searchView.post(new Runnable() {
            @Override
            public void run() {
                try {
                    searchView.requestFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(getContext().INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(searchView,
                            InputMethodManager.SHOW_IMPLICIT);
                }catch (Exception e){

                }
            }
        });
        // list ví dụ
        lv_tab4.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == 0) {
                    // show floating action button
                    if (!isShowFab) {
                        fab_menu.showMenu(true);
                        isShowFab = true;
                    }
                } else {
                    // hide
                    fab_menu.hideMenu(true);
                    isShowFab = false;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
        // hide floating action button
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY(); //for verticalScrollView
                //DO SOMETHING WITH THE SCROLL COORDINATES
                if (scrollY - oldPos <= -20) {
                    // show floating action button
                    if (!isShowFab) {
                        fab_menu.showMenu(true);
                        isShowFab = true;
                    }
                } else if (scrollY - oldPos >= 20) {
                    // hide
                    fab_menu.hideMenu(true);
                    isShowFab = false;
                }
                oldPos = scrollY;
            }
        });

        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(getActivity());
                if (offlineRecogDialog.isShowing()) {
                    offlineRecogDialog.dismiss();
                }
                if (handWriteDialog.isShowing()) {
                    handWriteDialog.dismiss();
                }
                try {
                    if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                        MenuActivity.drawer.openDrawer(GravityCompat.START);
                } catch (Exception e) {
                }
               /* Animation animation = AnimationUtils.loadAnimation(menuActivity, R.anim.menu_fade_anim);
                menuActivity.getMenuList().startAnimation(animation);*/
                hideSoftKeyboard(menuActivity);
            }
        });

        handler = new Handler();
        handler_2 = new Handler();
        //swipe to delete
        swipeListView();
        // click write icon
        clickWrite();
        // click speak icon
        clickSpeak();
        // click camera icon
        clickCamera();

        if (checkHistory()) {
            new HistoryWordSynctask().execute();
            new HistoryKanjiSynctask().execute();
            new HistoryCauSynctask().execute();
        } else {
            arrWord = new ArrayList<HistoryWord>();
            arrKanji = new ArrayList<HistoryKanji>();
            arrCau = new ArrayList<HistoryCau>();
            getListHistoryWord();
            getListHistoryKanji();
            getListHistoryCau();
        }

        startTime = System.currentTimeMillis();
        loadTabs();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(final String query) {
                countToLoadAds();
                if (Language.isVietnamese(query)) {
                    isVija = true;
                } else {
                    isVija = false;
                }
                switch (tab.getCurrentTab()) {
                    case 0:
                        isSubmit = true;
                        time1 = System.currentTimeMillis();
                        if ((!query.equals("")) && (!query.equals(" "))) {
                            if (!oldQueryWord.equals(query)) {
                                final long time = System.currentTimeMillis();
                                new addHWordSynctask().execute(query, String.valueOf(time));
                                oldQueryWord = query;
                            }
                            //todo Submit tab1-ja
                            new myAsyntask_tracuu1_ja().execute(query);

                            tv_japanese.setVisibility(View.GONE);
                            tv_romanji.setVisibility(View.GONE);
                            tv_vietnamese.setVisibility(View.GONE);
                            tv_dich.setVisibility(View.GONE);
                            lv_historyWord.setVisibility(View.GONE);
                            query_WKG = query;
                        }

                        break;
                    case 1:
                        isSubK = true;
                        if ((!query.equals("")) && (!query.equals(" "))) {
                            if (!oldQueryKanji.equals(query)) {
                                final long time = System.currentTimeMillis();
                                new addHKanjiSynctask().execute(query, String.valueOf(time));
                                oldQueryKanji = query;
                            }
                            //todo Submit tab2
                            new myAsyntask_tracuu2().execute(query);

                            lv_historyKanji.setVisibility(View.GONE);
                            lv_suggest.setVisibility(View.GONE);
                            tab.setVisibility(View.VISIBLE);
                            frameLayout2.setVisibility(View.VISIBLE);
//                            rl_tab2.setVisibility(View.VISIBLE);
//                            lv_2.setVisibility(View.VISIBLE);
                            query_WKG = query;
                        }
//
                        break;
                    case 2:
//                        Toast.makeText(getContext(), "Text submit", Toast.LENGTH_SHORT).show();
                        if ((!query.equals("")) && (!query.equals(" "))) {
                            if (!oldQueryCau.equals(query)) {
                                boolean isAdd = true;
                                long time = System.currentTimeMillis();
                                new addHCauSynctask().execute(query, String.valueOf(time));
                                if (arrCau != null)
                                    for (HistoryCau cau : arrCau) {
                                        if (query.equals(cau.gethWords())) {
                                            isAdd = false;
                                            break;
                                        }
                                    }
                                if (isAdd) {
                                    arrCau.add(0, new HistoryCau(query, time));
                                    if (adapterHistoryCau != null)
                                        adapterHistoryCau.notifyDataSetChanged();
                                }
                                oldQueryCau = query;
                            }
                            //todo Submit tab4
                            new myAsyntask_tracuu4().execute(query);

                            lv_historyCau.setVisibility(View.GONE);
                            lv_suggest.setVisibility(View.GONE);
                            tab.setVisibility(View.VISIBLE);
                            lv_tab4.setVisibility(View.VISIBLE);
                            query_WKG = query;
                        }
                        break;
                }
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
//
                switch (tab.getCurrentTab()) {
                    case 0:
                        if (newText.equals("")) {
                            lv_historyWord.setVisibility(View.VISIBLE);
                            frameLayout1.setVisibility(View.GONE);
                        } else {
                            lv_historyWord.setVisibility(View.GONE);
                        }
                        tv_japanese.setVisibility(View.GONE);
                        tv_romanji.setVisibility(View.GONE);
                        tv_vietnamese.setVisibility(View.GONE);
                        tv_dich.setVisibility(View.GONE);

                        if (newText.equals("") || newText.equals(" ")) {
                            if (adapter_suggest_ja != null) {
                                adapter_suggest_ja.clear();
                                adapter_suggest_ja.notifyDataSetChanged();
                                lv_suggest.setVisibility(View.GONE);
                                tab.setVisibility(View.VISIBLE);
                            }
                            FragmentTransaction fragmentTransaction2 = fragmentManager1.beginTransaction();
                            if (tab1_Fagment != null)
                                fragmentTransaction2.remove(tab1_Fagment).commitAllowingStateLoss();
                        }
                        if (!isSubmit) {
                            //todo some suggest
                            if (!newText.equals("") && !newText.equals(" ")) {
                                if (isSearch) {
                                    handler.removeCallbacksAndMessages(null);
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            getSuggest(newText);
                                            isSearch = true;
                                        }
                                    }, 500);
                                } else {
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            getSuggest(newText);
                                        }
                                    }, 500);
                                }
                                isSearch = true;
                            }
                        }

                        break;
                    case 1:
                        if (newText.equals("")) {
                            lv_historyKanji.setVisibility(View.VISIBLE);
                            frameLayout2.setVisibility(View.GONE);
                            lv_2.setVisibility(View.GONE);
                            cardView.setVisibility(View.GONE);
                        } else {
                            lv_historyKanji.setVisibility(View.GONE);
                            frameLayout2.setVisibility(View.VISIBLE);
//                            rl_tab2.setVisibility(View.VISIBLE);
//                            lv_2.setVisibility(View.VISIBLE);
                        }
                        if (newText.equals("") || newText.equals(" ")) {
                            if (adapter_2 != null) {
                                adapter_2.clear();
                                adapter_2.notifyDataSetChanged();
                            }
                        }
                        if (newText.equals("") || newText.equals(" ")) {
                            if (adapter_suggest_ja != null) {
                                adapter_suggest_ja.clear();
                                adapter_suggest_ja.notifyDataSetChanged();
                                lv_suggest.setVisibility(View.GONE);
                                tab.setVisibility(View.VISIBLE);
                            } else if (adapter_suggest_kanji != null) {
                                adapter_suggest_kanji.clear();
                                adapter_suggest_kanji.notifyDataSetChanged();
                                lv_suggest.setVisibility(View.GONE);
                                tab.setVisibility(View.VISIBLE);
                            }
                        }
                        if (!isSubK) {
                            if (!newText.equals("") && !newText.equals(" ")) {
                                if (isSearch) {
                                    handler.removeCallbacksAndMessages(null);
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            suggestKanji(newText);
                                            isSearch = true;
                                        }
                                    }, 500);
                                } else {
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            suggestKanji(newText);
                                        }
                                    }, 500);
                                }

                                isSearch = true;
                            }
                        }
                        break;
                    case 2:
                        if (newText.equals("") || newText.equals(" ")) {
                            lv_historyCau.setVisibility(View.VISIBLE);
                            lv_tab4.setVisibility(View.GONE);
                        } else {
                            lv_historyCau.setVisibility(View.GONE);
                            lv_tab4.setVisibility(View.VISIBLE);
                        }
                }
                if (searchView != null)
                    searchView.requestFocusFromTouch();
                return true;
            }
        });

        tab.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                String tabQuery = searchView.getQuery().toString();
                if (tabId.equals("tracuu_tab1")) {
                    if (!tabQuery.equals("") && !tabQuery.equals(" ")) {
                        isSubmit = true;
                        searchView.setQuery(tabQuery, true);
                    } else {
                        frameLayout1.setVisibility(View.GONE);
                        //Translate
                        tv_japanese.setVisibility(View.GONE);
                        tv_romanji.setVisibility(View.GONE);
                        tv_vietnamese.setVisibility(View.GONE);
                        tv_dich.setVisibility(View.GONE);
                        lv_historyWord.setVisibility(View.VISIBLE);
                    }
                }
                if (tabId.equals("tracuu_tab2")) {
                    FragmentTransaction fragmentTransaction2 = fragmentManager1.beginTransaction();
                    if (tab1_Fagment != null)
                        fragmentTransaction2.remove(tab1_Fagment).commitAllowingStateLoss();
                    if (!tabQuery.equals("") && !tabQuery.equals(" ")) {
                        isSubK = true;
                        searchView.setQuery(tabQuery, true);
                    } else {
                        frameLayout2.setVisibility(View.GONE);
                        lv_2.setVisibility(View.GONE);
                        cardView.setVisibility(View.GONE);
                        lv_historyKanji.setVisibility(View.VISIBLE);
                    }
                }
                if (tabId.equals("tracuu_tab4")) {
                    FragmentTransaction fragmentTransaction2 = fragmentManager1.beginTransaction();
                    if (tab1_Fagment != null)
                        fragmentTransaction2.remove(tab1_Fagment).commitAllowingStateLoss();

                    if (!tabQuery.equals("") && !tabQuery.equals(" ")) {
                        searchView.setQuery(tabQuery, true);
                    } else {
                        lv_tab4.setVisibility(View.GONE);
                        lv_historyCau.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        return parentView;
    }

    //Jaen
    private class myAsyntask_tracuu1_ja extends AsyncTask<String, Void, ArrayList<Jaen>> {
        long time_2;

        @Override
        protected ArrayList<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            if (!params[0].equals("") && !params[0].equals(" ")) {
                mQuery = params[0];
                javi = db.getJaen(params[0]);
            }
            return javi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tv_japanese.setVisibility(View.GONE);
            tv_romanji.setVisibility(View.GONE);
            tv_vietnamese.setVisibility(View.GONE);
            tv_dich.setVisibility(View.GONE);
            lv_suggest.setVisibility(View.GONE);
            tab.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(ArrayList<Jaen> javis) {
            super.onPostExecute(javis);
            getListJavi();
            time_2 = System.currentTimeMillis() - time1;
            isSubmit = false;
            if (javis.isEmpty()) toastNoResult();
        }
    }

    private void toastNoResult() {
        Toast.makeText(getActivity(), getString(R.string.no_result), Toast.LENGTH_SHORT).show();
    }

    private void getListJavi() {
        if (javi == null) {
            return;
        }
        if (javi.size() == 0) {
            //Translate
            if (NetWork.isNetWork(getContext())) {
                String google_translate_url = "https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&dt=bd&dj=1&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=at";
                String from = "ja";
                String to = MenuActivity.sharedPreferences.getString("to_language", Locale.getDefault().getLanguage());;
                tv_japanese.setVisibility(View.VISIBLE);
                tv_romanji.setVisibility(View.VISIBLE);
                tv_vietnamese.setVisibility(View.VISIBLE);
                tv_dich.setVisibility(View.VISIBLE);
                try {
                    if (!Language.isVietnamese(mQuery)) {
                        EditText editText = null;
                        WanaKanaJava wanaKanaJava = new WanaKanaJava(editText, false);
                        mQuery = wanaKanaJava.toKana(mQuery);
                    }
                    mQuery = URLEncoder.encode(mQuery, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                url = google_translate_url + "&sl=" + from + "&tl=" + to + "&q=" + mQuery;

                if (isTranslate) {
                    handler_2.removeCallbacksAndMessages(null);
                    handler_2.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new transSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
                            } else {
                                new transSyncTask().execute(url);
                            }
                            isTranslate = true;


                        }
                    }, 500);
                } else {
                    handler_2.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new transSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
                            } else {
                                new transSyncTask().execute(url);
                            }
                        }
                    }, 500);
                }

                isTranslate = true;

            } else {
                return;
            }
            frameLayout1.setVisibility(View.GONE);
        } else {
            frameLayout1.setVisibility(View.VISIBLE);
            tv_japanese.setVisibility(View.GONE);
            tv_romanji.setVisibility(View.GONE);
            tv_vietnamese.setVisibility(View.GONE);
            tv_dich.setVisibility(View.GONE);
            lv_suggest.setVisibility(View.GONE);
            tab.setVisibility(View.VISIBLE);

            vMean = javi.get(0).getmMean();
            vTitle = javi.get(0).getmWord();

            vPhonetic = javi.get(0).getmPhonetic();
            mFavorite = javi.get(0).getmFavorite();
            wID = javi.get(0).getmId();

            tab1_Fagment = new FragTab1();
            Bundle bundle = new Bundle();
            bundle.putString("vMean", vMean);
            bundle.putString("vTitle", vTitle);
            bundle.putString("verbPhonetic", vPhonetic);
            if (vPhonetic != null) {
                    bundle.putString("vPhonetic", "「" + vPhonetic + "」");
            }
            bundle.putInt("mFavorite", mFavorite);
            bundle.putInt("wID", wID);
            bundle.putInt("seq", mSeq);
            tab1_Fagment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = fragmentManager1.beginTransaction();
            fragmentTransaction.replace(R.id.frame_tab1, tab1_Fagment);
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    private class myAsyntask_tracuu2 extends AsyncTask<String, Void, ArrayList<Kanji>> {
        long time_1, time_2;

        @Override
        protected ArrayList<Kanji> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            kanji = db.getKanji(params[0]);
            return kanji;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            time_1 = System.currentTimeMillis();
        }

        @Override
        protected void onPostExecute(ArrayList<Kanji> kanjis) {
            super.onPostExecute(kanjis);
            if (kanji.size() != 0 && menuActivity != null) {
                getListKanji();
            }
            time_2 = System.currentTimeMillis() - time_1;
            isSubK = false;
            if (kanjis.isEmpty()) {
                toastNoResult();
            }
        }
    }

    private void getListSuggestKanji() {
        if (menuActivity != null && sKanji != null && sKanji.size() > 0) {
            adapter_suggest_kanji = new MyAdapterHanTu(menuActivity, R.layout.tracuu_tab2_items, sKanji);

            if (adapter_suggest_kanji != null) {
                lv_suggest.setAdapter(adapter_suggest_kanji);
                lv_suggest.setVisibility(View.VISIBLE);
                tab.setVisibility(View.GONE);
                frameLayout2.setVisibility(View.GONE);
                lv_suggest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            countToLoadAds();
                            query_WKG = sKanji.get(position).getmKanji().toString();
                            String[] arrquery = query_WKG.split(" ");
                            query_WKG = arrquery[0];

                            //todo Suggest selected 2
                            isSubK = true;
                            searchView.setQuery(query_WKG, true);
                            lv_suggest.setVisibility(View.GONE);
                            tab.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                        }
                    }
                });
            }
        } else {
            lv_suggest.setVisibility(View.GONE);
            tab.setVisibility(View.VISIBLE);
        }
    }

    private void getListKanji() {
        if (kanji == null) {
            return;
        }
        if (kanji.size() > 0) {
            tab2_Fagment = new FragTab2();
            Bundle bundle = new Bundle();
            mKanji = kanji.get(0).getmKanji();
            if (kanji.get(0).getmMean() != null)
                mMean = kanji.get(0).getmMean();
            else mMean = null;
            if (kanji.get(0).getmKun() != null) {
                mKun = kanji.get(0).getmKun();
            } else mKun = "";
            if (kanji.get(0).getmImg() != null) {
                svg_kanji = kanji.get(0).getmImg();
            } else {
                svg_kanji = "";
            }
            if (kanji.get(0).getmOn() != null) {
                mOn = kanji.get(0).getmOn();
            } else mOn = "";
            if (String.valueOf(kanji.get(0).getmStroke_count()) != null) {
                mStroke_count = String.valueOf(kanji.get(0).getmStroke_count());
            } else mStroke_count = "";
            if (String.valueOf(kanji.get(0).getmLevel()) != null) {
                mLevel = String.valueOf(kanji.get(0).getmLevel());
            } else mLevel = "";
            if (kanji.get(0).getmCompdetail() != null) {
                mCompDetail = kanji.get(0).getmCompdetail();
            } else mCompDetail = "";
            if (kanji.get(0).getmDetail() != null) {
                mDetail = kanji.get(0).getmDetail();
            } else mDetail = "";
            if (kanji.get(0).getmExamples() != null) {
                mExamples = kanji.get(0).getmExamples();
            } else mExamples = "";
            mFavorite = kanji.get(0).getmFavorite();
            wID = kanji.get(0).getmId();

            //đưa dữ liệu riêng lẻ vào Bundle
            bundle.putString("mKanji", mKanji);
            bundle.putString("mMean", mMean);
            bundle.putString("mKun", mKun);
            bundle.putString("svg_kanji", svg_kanji);
            bundle.putString("mOn", mOn);
            bundle.putString("mStroke_count", mStroke_count);
            bundle.putString("mLevel", mLevel);
            bundle.putString("mCompDetail", mCompDetail);
            bundle.putString("mDetail", mDetail);
            bundle.putString("mExamples", mExamples);
            bundle.putInt("mFavorite", mFavorite);
            bundle.putInt("wID", wID);

            tab2_Fagment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = fragmentManager2.beginTransaction();
            fragmentTransaction.replace(R.id.frame_tab2, tab2_Fagment);
            fragmentTransaction.commitAllowingStateLoss();
        }
        if (kanji.size() > 1) {
            if (kanji.size() > 18) {
                ArrayList<Kanji> arrayList = new ArrayList<>();
                for (int k = 0; k < 18; k++) {
                    arrayList.add(kanji.get(k));
                }
                adapter_2 = new MyAdapterHanTu2(menuActivity, R.layout.gridview_ht, arrayList);
                lv_2.setAdapter(adapter_2);
                lv_2.setVisibility(View.VISIBLE);
                cardView.setVisibility(View.VISIBLE);
                adapter_2.notifyDataSetChanged();
                setGridViewHeightBasedOnChildren2(lv_2, 6);
            } else {
                adapter_2 = new MyAdapterHanTu2(menuActivity, R.layout.gridview_ht, kanji);
                lv_2.setAdapter(adapter_2);
                lv_2.setVisibility(View.VISIBLE);
                cardView.setVisibility(View.VISIBLE);
                adapter_2.notifyDataSetChanged();
                setGridViewHeightBasedOnChildren(lv_2, 6);
            }

            lv_2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    tab2_Fagment = new FragTab2();
                    Bundle bundle = new Bundle();
                    mKanji = kanji.get(position).getmKanji();
                    if (kanji.get(position).getmMean() != null)
                        mMean = kanji.get(position).getmMean();
                    else mMean = null;
                    if (kanji.get(position).getmKun() != null) {
                        mKun = kanji.get(position).getmKun();
                    } else mKun = "";
                    if (kanji.get(position).getmImg() != null) {
                        svg_kanji = kanji.get(position).getmImg();
                    } else {
                        svg_kanji = "";
                    }
                    if (kanji.get(position).getmOn() != null) {
                        mOn = kanji.get(position).getmOn();
                    } else mOn = "";
                    if (String.valueOf(kanji.get(position).getmStroke_count()) != null) {
                        mStroke_count = String.valueOf(kanji.get(position).getmStroke_count());
                    } else mStroke_count = "";
                    if (String.valueOf(kanji.get(position).getmLevel()) != null) {
                        mLevel = String.valueOf(kanji.get(position).getmLevel());
                    } else mLevel = "";
                    if (kanji.get(position).getmCompdetail() != null) {
                        mCompDetail = kanji.get(position).getmCompdetail();
                    } else mCompDetail = "";
                    if (kanji.get(position).getmDetail() != null) {
                        mDetail = kanji.get(position).getmDetail();
                    } else mDetail = "";
                    if (kanji.get(position).getmExamples() != null) {
                        mExamples = kanji.get(position).getmExamples();
                    } else mExamples = "";
                    mFavorite = kanji.get(position).getmFavorite();
                    wID = kanji.get(position).getmId();

                    //đưa dữ liệu riêng lẻ vào Bundle
                    bundle.putString("mKanji", mKanji);
                    bundle.putString("mMean", mMean);
                    bundle.putString("mKun", mKun);
                    bundle.putString("svg_kanji", svg_kanji);
                    bundle.putString("mOn", mOn);
                    bundle.putString("mStroke_count", mStroke_count);
                    bundle.putString("mLevel", mLevel);
                    bundle.putString("mCompDetail", mCompDetail);
                    bundle.putString("mDetail", mDetail);
                    bundle.putString("mExamples", mExamples);
                    bundle.putInt("mFavorite", mFavorite);
                    bundle.putInt("wID", wID);

                    tab2_Fagment.setArguments(bundle);
                    FragmentTransaction fragmentTransaction = fragmentManager2.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_tab2, tab2_Fagment);
                    fragmentTransaction.commitAllowingStateLoss();
                }
            });
        } else {
            lv_2.setVisibility(View.GONE);
            cardView.setVisibility(View.GONE);
        }
    }

    // My Asynctask_tracuu tab4
    private class myAsyntask_tracuu4 extends AsyncTask<String, Void, ArrayList<Example>> {
        @Override
        protected ArrayList<Example> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            cau = db.getTracuuCau(params[0]);
            return cau;
        }

        @Override
        protected void onPostExecute(ArrayList<Example> examples) {
            super.onPostExecute(examples);
            if (menuActivity != null)
                getListCau();
            if (examples.isEmpty()) toastNoResult();
        }
    }

    // Cấu hình tab
    private void loadTabs() {
        // Lấy Tabhost _id ra trước (cái này của built - in android

        // gọi lệnh setup
        tab.setup();
        TabHost.TabSpec spec;
        // Tạo tab1
        spec = tab.newTabSpec("tracuu_tab1");
        spec.setContent(R.id.tracuu_tab1);
        spec.setIndicator(getString(R.string.tracuu_tab1));
        tab.addTab(spec);
        // Tạo tab2
        spec = tab.newTabSpec("tracuu_tab2");
        spec.setContent(R.id.tracuu_tab2);
        spec.setIndicator(getString(R.string.tracuu_tab2));
        tab.addTab(spec);
        // Tạo tab4
        spec = tab.newTabSpec("tracuu_tab4");
        spec.setContent(R.id.tracuu_tab4);
        spec.setIndicator(getString(R.string.tracuu_tab4));
        tab.addTab(spec);
        // Thiết lập tab mặc định được chọn ban đầu là tab 0
        tab.setCurrentTab(0);
    }

    //event click speak to text
    void clickSpeak() {
        fab_micro = (FloatingActionButton) parentView.findViewById(R.id.fab_micro);
        fab_micro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide fab button
                fab_menu.close(true);
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "ja-JP");
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "ja-JP");
                try {
                    startActivityForResult(intent, RESULT_SPEECH);
                } catch (ActivityNotFoundException a) {
                }
            }
        });
    }

    private void clickCamera() {
        fab_camera = (FloatingActionButton) parentView.findViewById(R.id.fab_camera);
        fab_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fab_menu.close(true);
                startActivity(new Intent(getActivity(), CaptureActivity.class));
            }
        });
    }

    private boolean isShow;

    //event click write
    void clickWrite() {
        write = (ImageButton) parentView.findViewById(R.id.btn_write);
        write.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(getActivity());
                if (NetWork.isNetWork(getActivity())) {
                    if (!isShow) {
                        try {
                            isShow = true;
                            handWriteDialog.setTraCuuFragment(getTraCuuFragment());
                            handWriteDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
                            handWriteDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
                            handWriteDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM, WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                            handWriteDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                            WindowManager.LayoutParams layoutParams = handWriteDialog.getWindow().getAttributes();
                            layoutParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
                            layoutParams.x = 0;
                            layoutParams.y = 0;
                            handWriteDialog.show();
                            Window window = handWriteDialog.getWindow();
                            View rootView = getActivity().getWindow().getDecorView();
                            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, rootView.getHeight() * 2 / 3);
                        } catch (Exception e) {
                            Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        isShow = false;
                        handWriteDialog.dismiss();
                    }
                } else {
                    if (!isShow) {
                        isShow = true;
                        offlineRecogDialog.setTraCuuFragment(getTraCuuFragment());
                        offlineRecogDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
                        offlineRecogDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
                        offlineRecogDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM, WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                        offlineRecogDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                        WindowManager.LayoutParams layoutParams = offlineRecogDialog.getWindow().getAttributes();
                        layoutParams.gravity = Gravity.BOTTOM | Gravity.LEFT;
                        layoutParams.x = 0;
                        layoutParams.y = 0;
                        offlineRecogDialog.show();
                        Window window = offlineRecogDialog.getWindow();
                        View rootView = getActivity().getWindow().getDecorView();
                        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, rootView.getHeight() * 2 / 3);
                    } else {
                        isShow = false;
                        offlineRecogDialog.dismiss();
                    }
                }
            }
        });
    }


    private void countToLoadAds() {
        count += 1;
        if (count == 30) {
            MenuActivity.adsmodUtil.showIntervalAds();
            count = 0;
        }
    }

    public void setGridViewHeightBasedOnChildren2(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if (items > columns) {
            x = items / columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight + 50;
        gridView.setLayoutParams(params);

    }

    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if (items > columns) {
            x = items / columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight + 10;
        gridView.setLayoutParams(params);

    }

    public String getJSON(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
//            c.setRequestProperty("Content-length", "0");
            c.setRequestProperty("charset", "UTF-8");
            c.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36");

            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();

            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    private JSONObject getMyJSON(String data) {
        try {
            return new JSONObject(data);
        } catch (Exception e) {
            return null;
        }
    }

    private class transSyncTask extends AsyncTask<String, JSONObject, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            JSONObject jsonObject = null;
            String data = getJSON(params[0], 500);
            jsonObject = getMyJSON(data);

            publishProgress(jsonObject);
            return null;
        }

        @Override
        protected void onProgressUpdate(JSONObject... values) {
            super.onProgressUpdate(values);
            JSONObject jsonObject = values[0];
            if (jsonObject == null) {
                return;
            }
            if (jsonObject.has("sentences")) {
                try {
                    JSONArray sentences = jsonObject.getJSONArray("sentences");
                    JSONObject jTrans = sentences.getJSONObject(0);
                    JSONObject jTranslit = sentences.getJSONObject(1);
                    // read trans
                    if (jTrans.has("trans")) {
                        trans = jTrans.getString("trans");
                    }
                    if (jTrans.has("orig")) {
                        orig = jTrans.getString("orig");
                    }
                    if (jTranslit.has("src_translit")) {
                        translit = jTranslit.getString("src_translit");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            super.onPostExecute(strings);
            if (orig != null)
                tv_japanese.setText(orig);
            if (translit != null)
                tv_romanji.setText(translit);
            if (trans != null)
                tv_vietnamese.setText(trans);

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH:
                if (resultCode == getActivity().RESULT_OK && data != null) {
                    ArrayList<String> speechToText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    for (int i = 0; i < speechToText.size(); i++) {
                    }

                    searchView.setQuery(speechToText.get(0), false);
                }
                break;
        }
    }

    public void hideSoftKeyboard(Activity activity) {
        try {
            if (activity.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }

    public TraCuuFragment getTraCuuFragment() {
        return this;
    }

    private void getSuggest(final String newText) {
        if (!newText.equals("") && !newText.equals(" ")) {
            new sJaSynctask().execute(newText);
        }

    }

    // HistorySynctask
    private class HistoryWordSynctask extends AsyncTask<String, Void, ArrayList<HistoryWord>> {
        @Override
        protected ArrayList<HistoryWord> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            arrWord = db.getHistoryWord();
            return arrWord;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<HistoryWord> words) {
            super.onPostExecute(words);
            if (arrWord != null && arrWord.size() > 0)
                getListHistoryWord();
        }
    }

    private void getListHistoryWord() {
        if (menuActivity != null) {
            adapterHistoryWord = new MyAdapterHistoryWord(menuActivity, R.layout.tracuu_history_items, arrWord);
            if (adapterHistoryWord != null) {
                lv_historyWord.setAdapter(adapterHistoryWord);
            }
//        setListViewHeightBasedOnChildren(lv_historyWord);

            //Swipe items click listener
            lv_historyWord.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    switch (index) {
                        case 0:
                            // open
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle(getString(R.string.history_title))
                                    .setMessage(getString(R.string.history_message))
                                    .setCancelable(true)
                                    .setNegativeButton(getString(R.string.history_delete), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                            Toast.makeText(getContext(), getString(R.string.history_deleted), Toast.LENGTH_SHORT).show();
                                            new removeAllHWord().execute();
                                        }
                                    })
                                    .setPositiveButton(getString(R.string.history_cancel), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                            break;
                        case 1:
                            // delete
                            HistoryWord word = arrWord.get(position);
                            Toast.makeText(getContext(), getString(R.string.history_daxoa) + word.gethWords() + getString(R.string.history_khoilichsu), Toast.LENGTH_SHORT).show();
                            new removeHWord().execute(word);
                            break;
                    }
                    // false : close the menu; true : not close the menu
                    return false;
                }
            });
            //item click listener
            lv_historyWord.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    HistoryWord word = arrWord.get(position);
                    isSubmit = true;
                    searchView.setQuery(word.gethWords(), true);
                }
            });
        }
    }

    private class HistoryKanjiSynctask extends AsyncTask<String, Void, ArrayList<HistoryKanji>> {
        @Override
        protected ArrayList<HistoryKanji> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            arrKanji = db.getHistoryKanji();
            return arrKanji;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<HistoryKanji> kanjis) {
            super.onPostExecute(kanjis);
            if (arrKanji != null && menuActivity != null)
                getListHistoryKanji();
        }
    }

    private void getListHistoryKanji() {
        adapterHistoryKanji = new MyAdapterHistoryKanji(menuActivity, R.layout.tracuu_history_items, arrKanji);
        if (adapterHistoryKanji != null) {
            lv_historyKanji.setAdapter(adapterHistoryKanji);
            adapterHistoryKanji.notifyDataSetChanged();
        }
        //swipe
        lv_historyKanji.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // open
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(getString(R.string.history_title))
                                .setMessage(getString(R.string.history_message))
                                .setCancelable(true)
                                .setNegativeButton(getString(R.string.history_delete), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        Toast.makeText(getContext(), getString(R.string.history_deleted), Toast.LENGTH_SHORT).show();
                                        new removeAllHKanji().execute();
                                    }
                                })
                                .setPositiveButton(getString(R.string.history_cancel), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                        break;
                    case 1:
                        // delete
                        HistoryKanji kanji = arrKanji.get(position);
                        Toast.makeText(getContext(), getString(R.string.history_daxoa) + kanji.gethWords() + getString(R.string.history_khoilichsu), Toast.LENGTH_SHORT).show();
                        new removeHKanji().execute(kanji);
                        break;
                }
                return false;
            }
        });
        lv_historyKanji.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HistoryKanji kanji = arrKanji.get(position);
                isSubK = true;
                searchView.setQuery(kanji.gethWords(), true);
            }
        });

    }

    private class HistoryCauSynctask extends AsyncTask<String, Void, ArrayList<HistoryCau>> {
        @Override
        protected ArrayList<HistoryCau> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            arrCau = db.getHistoryCau();
            return arrCau;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<HistoryCau> caus) {
            super.onPostExecute(caus);
            if (arrCau != null && menuActivity != null)
                getListHistoryCau();
        }
    }

    private void getListHistoryCau() {
        adapterHistoryCau = new MyAdapterHistoryCau(menuActivity, R.layout.tracuu_history_items, arrCau);
        if (adapterHistoryCau != null) {
            lv_historyCau.setAdapter(adapterHistoryCau);
            adapterHistoryCau.notifyDataSetChanged();
        }
        lv_historyCau.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // open
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(getString(R.string.history_title))
                                .setMessage(getString(R.string.history_message))
                                .setCancelable(true)
                                .setNegativeButton(getString(R.string.history_delete), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        Toast.makeText(getContext(), getString(R.string.history_deleted), Toast.LENGTH_SHORT).show();
                                        new removeAllHCau().execute();
                                    }
                                })
                                .setPositiveButton(getString(R.string.history_cancel), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                        break;
                    case 1:
                        // delete
                        HistoryCau cau = arrCau.get(position);
                        Toast.makeText(getContext(), getString(R.string.history_daxoa) + getString(R.string.cau) + getString(R.string.history_khoilichsu), Toast.LENGTH_SHORT).show();
                        new removeHCau().execute(cau);

                        break;
                }
                return false;
            }
        });

        lv_historyCau.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HistoryCau hCau = arrCau.get(position);
                searchView.setQuery(hCau.gethWords(), true);
            }
        });
    }

    // add history
    private class addHWordSynctask extends AsyncTask<String, Void, Void> {
        boolean isAdd;
        String param0, param1;

        @Override
        protected Void doInBackground(String... params) {
            if (arrWord != null) {
                isAdd = checkAddHWord(arrWord, params[0]);
            } else {
                isAdd = true;
            }

            if (isAdd) {
                MyDatabase db = new MyDatabase(getContext());
                db.AddHistoryWord(params[0], Long.parseLong(params[1]));
                param0 = params[0];
                param1 = params[1];
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isAdd) {
                HistoryWord word = new HistoryWord(param0, Long.parseLong(param1));
                if (arrWord != null) {
                    arrWord.add(0, word);
                } else {
                    arrWord = new ArrayList<HistoryWord>();
                    arrWord.add(0, word);
                }
                if (adapterHistoryWord != null) adapterHistoryWord.notifyDataSetChanged();
                else {
                    if (menuActivity != null && arrWord != null) {
                        adapterHistoryWord = new MyAdapterHistoryWord(menuActivity, R.layout.tracuu_history_items, arrWord);
                        if (lv_historyWord != null) lv_historyWord.setAdapter(adapterHistoryWord);
                    }
                }
            }
        }
    }

    private class addHKanjiSynctask extends AsyncTask<String, Void, Void> {
        boolean isAdd;
        String param0, param1;

        @Override
        protected Void doInBackground(String... params) {
            if (arrKanji != null) {
                isAdd = checkAddHKanji(arrKanji, params[0]);
            } else {
                isAdd = true;
            }
            if (isAdd) {
                MyDatabase db = new MyDatabase(getContext());
                db.AddHistoryKanji(params[0], Long.parseLong(params[1]));
                param0 = params[0];
                param1 = params[1];
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (isAdd) {
                HistoryKanji word = new HistoryKanji(param0, Long.parseLong(param1));
                if (arrKanji != null) {
                    arrKanji.add(0, word);
                } else {
                    arrKanji = new ArrayList<HistoryKanji>();
                    arrKanji.add(0, word);
                }
                if (adapterHistoryKanji != null)
                    adapterHistoryKanji.notifyDataSetChanged();
            }
        }
    }

    private class addHCauSynctask extends AsyncTask<String, Void, Void> {
        boolean isAdd;
        String param0, param1;

        @Override
        protected Void doInBackground(String... params) {
            if (arrCau != null) {
                isAdd = checkAddHCau(arrCau, params[0]);
            } else {
                isAdd = true;
            }
            if (isAdd) {
                MyDatabase db = new MyDatabase(getContext());
                db.AddHistoryCau(params[0], Long.parseLong(params[1]));
                param0 = params[0];
                param1 = params[1];
            }
            return null;
        }
    }

    // check xem tu da co trong lich su tim kiem chua
    private boolean checkAddHWord(ArrayList<HistoryWord> arr, String s) {
        boolean isAdd = true;
        if (arr != null)
            for (int i = 0; i < arr.size(); i++) {
                if (arr.get(i).gethWords().equals(s)) {
                    isAdd = false;
                    break;
                }
            }
        return isAdd;
    }

    private boolean checkAddHKanji(ArrayList<HistoryKanji> arr, String s) {
        boolean isAdd = true;
        if (arr != null)
            for (int i = 0; i < arr.size(); i++) {
                if (arr.get(i).gethWords().equals(s)) {
                    isAdd = false;
                    break;
                }
            }
        return isAdd;
    }

    private boolean checkAddHGrammar(ArrayList<HistoryGrammar> arr, String s) {
        boolean isAdd = true;
        if (arr != null)
            for (int i = 0; i < arr.size(); i++) {
                if (arr.get(i).gethWords().equals(s)) {
                    isAdd = false;
                    break;
                }
            }
        return isAdd;
    }

    private boolean checkAddHCau(ArrayList<HistoryCau> arr, String s) {
        boolean isAdd = true;
        if (arr != null)
            for (int i = 0; i < arr.size(); i++) {
                if (arr.get(i).gethWords().equals(s)) {
                    isAdd = false;
                    break;
                }
            }
        return isAdd;
    }

    //Delete history
    private class removeHWord extends AsyncTask<HistoryWord, Void, Void> {
        HistoryWord word;

        @Override
        protected Void doInBackground(HistoryWord... params) {
            MyDatabase db = new MyDatabase(getContext());
            word = params[0];
            db.RemoveHistoryWord(word.gethDate());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrWord != null && menuActivity != null && arrWord.size() > 0) {
                arrWord.remove(word);
                if (adapterHistoryWord != null)
                    adapterHistoryWord.notifyDataSetChanged();
            }
        }
    }

    private class removeHKanji extends AsyncTask<HistoryKanji, Void, Void> {
        HistoryKanji kanji;

        @Override
        protected Void doInBackground(HistoryKanji... params) {
            MyDatabase db = new MyDatabase(getContext());
            kanji = params[0];
            db.RemoveHistoryKanji(kanji.gethDate());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrKanji != null) {
                arrKanji.remove(kanji);
                if (adapterHistoryKanji != null)
                    adapterHistoryKanji.notifyDataSetChanged();
            }
        }
    }

    private class removeHCau extends AsyncTask<HistoryCau, Void, Void> {
        HistoryCau word;

        @Override
        protected Void doInBackground(HistoryCau... params) {
            MyDatabase db = new MyDatabase(getContext());
            word = params[0];
            db.RemoveHistoryCau(word.gethDate());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrCau != null && menuActivity != null) {
                arrCau.remove(word);
                if (adapterHistoryCau != null)
                    adapterHistoryCau.notifyDataSetChanged();
            }
        }
    }

    private class removeAllHWord extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            MyDatabase db = new MyDatabase(getContext());
            db.RemoveAllHWord();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrWord != null && menuActivity != null) {
                arrWord.clear();
                if (adapterHistoryWord != null)
                    adapterHistoryWord.notifyDataSetChanged();
            }
        }
    }

    private class removeAllHKanji extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            MyDatabase db = new MyDatabase(getContext());
            db.RemoveAllHKanji();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrKanji != null && menuActivity != null) {
                arrKanji.clear();
                if (adapterHistoryKanji != null)
                    adapterHistoryKanji.notifyDataSetChanged();
            }
//            setListViewHeightBasedOnChildren(lv_historyKanji);
        }
    }

    private class removeAllHCau extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            MyDatabase db = new MyDatabase(getContext());
            db.RemoveAllHCau();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrCau != null && menuActivity != null) {
                arrCau.clear();
                if (adapterHistoryCau != null)
                    adapterHistoryCau.notifyDataSetChanged();
            }
        }
    }

    private void swipeListView() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.parseColor("#EFE4B0")));
                // set item width
                openItem.setWidth(90);
                ;
                //set icon
                openItem.setIcon(android.R.drawable.ic_menu_delete);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.parseColor("#F33F3F")));
                // set item width
                deleteItem.setWidth(90);
                // set a icon
                deleteItem.setIcon(android.R.drawable.ic_input_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        lv_historyWord.setMenuCreator(creator);
        lv_historyKanji.setMenuCreator(creator);
        lv_historyCau.setMenuCreator(creator);

        lv_historyWord.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        lv_historyKanji.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        lv_historyCau.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

    }

    private class sJaSynctask extends AsyncTask<String, Void, List<Jaen>> {

        @Override
        protected List<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            if (!params[0].equals("") && !params[0].equals(" ")) {
                sJavi = db.getJaenSuggest(params[0]);
            }
            return sJavi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<Jaen> vijas) {
            super.onPostExecute(vijas);
            if (getActivity() != null)
                getListSuggestJa();

        }
    }

    private void getListSuggestJa() {
        if (sJavi != null && sJavi.size() > 0) {
            adapter_suggest_ja = new MyAdapterTuJa(getActivity(), R.layout.tracuu_tab1_ja_items, sJavi);

            if (adapter_suggest_ja != null) {
                lv_suggest.setAdapter(adapter_suggest_ja);
//                setListViewHeightBasedOnChildren(lv_suggest);
                lv_suggest.setVisibility(View.VISIBLE);
                tab.setVisibility(View.GONE);
                frameLayout1.setVisibility(View.GONE);
                lv_suggest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            countToLoadAds();
                            query_WKG = sJavi.get(position).getmWord().toString();

                            //todo Suggest selected 1-ja
                            isSubK = true;
                            isSubmit = true;
                            searchView.setQuery(query_WKG, true);
                            lv_suggest.setVisibility(View.GONE);
                            tab.setVisibility(View.VISIBLE);
                        } catch (Exception e) {
                        }
                    }
                });
            }
        } else {
            lv_suggest.setVisibility(View.GONE);
            tab.setVisibility(View.VISIBLE);
        }
    }

    private void suggestKanji(String newText) {
        if (!newText.equals("") && !newText.equals(" ")) {
            new suggestKanjiSynctask().execute(newText);
        }
    }

    private class suggestKanjiSynctask extends AsyncTask<String, Void, ArrayList<Kanji>> {
        long time_1, time_2;
        String k_query;

        @Override
        protected ArrayList<Kanji> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            k_query = params[0];
            sKanji = db.getKanji(k_query);
            return sKanji;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            time_1 = System.currentTimeMillis();
        }

        @Override
        protected void onPostExecute(ArrayList<Kanji> kanjis) {
            super.onPostExecute(kanjis);
            if (sKanji.size() != 0) {
                getListSuggestKanji();
            } else {
                new sJaSynctask().execute(k_query);
            }
            time_2 = System.currentTimeMillis() - time_1;

        }
    }

    private void getListCau() {
        if (cau != null && cau.size() > 0) {
            adapter_suggest_cau = new MyAdapterCau(menuActivity, R.layout.tracuu_tab4_items, cau);
            if (adapter_suggest_cau != null) {
                lv_tab4.setAdapter(adapter_suggest_cau);
//                setListViewHeightBasedOnChildren(lv_tab4);
                lv_tab4.setVisibility(View.VISIBLE);
                lv_historyCau.setVisibility(View.GONE);
            }
        } else {
            lv_tab4.setVisibility(View.GONE);
        }
    }

    private boolean checkHistory() {
        MyDatabase db = new MyDatabase(getActivity());
        return db.checkHistory();
    }
}