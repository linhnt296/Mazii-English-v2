package eup.mobi.jedictionary.google;

import android.app.Activity;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.NativeExpressAdView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;

import java.util.Random;

/**
 * Created by LinhNT on 5/30/2016.
 */
public class AdNativeExpress {
    public static NativeExpressAdView smartNative(Activity activity, String bannerAd, float prob, boolean isPremium) {
        Random ran = new Random();
        final RelativeLayout layout = (RelativeLayout) activity
                .findViewById(R.id.adView);
        if (prob < ran.nextFloat()) {
            if (isHideBanner())
                layout.setVisibility(View.GONE);
            return null;
        }

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);

//		layout.setVisibility(View.VISIBLE);

        final NativeExpressAdView mAdView = new NativeExpressAdView(activity);
        mAdView.setAdSize(new AdSize(328, 150));
        mAdView.setAdUnitId(bannerAd);
        mAdView.setLayoutParams(params);
        try {
            // Create an ad request.
            AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

            // Optionally populate the ad request builder.
            adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            adRequestBuilder.addTestDevice(activity.getString(R.string.test_device));
            // Add the AdView to the view hierarchy.
            layout.addView(mAdView);

            // Start loading the ad.
            mAdView.loadAd(adRequestBuilder.build());
            mAdView.resume();
        } catch (Exception e) {

        }
        if (isHideBanner() || isPremium) {
            layout.setVisibility(View.GONE);
        }
        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                MenuActivity.sharedPreferences.edit().putLong("time", System.currentTimeMillis()).commit();
                layout.setVisibility(View.GONE);
                Track.sendTrackerAction("Click Ads", "true");
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        return mAdView;
    }

    public static NativeExpressAdView smartNative(View fragment, String bannerAd, float prob, boolean isPremium, boolean isSmall) {
        Random ran = new Random();
        final RelativeLayout layout = (RelativeLayout) fragment
                .findViewById(R.id.adView);
        if (prob < ran.nextFloat()) {
            if (isHideBanner())
                layout.setVisibility(View.GONE);
            return null;
        }

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);

//		layout.setVisibility(View.VISIBLE);

        final NativeExpressAdView mAdView = new NativeExpressAdView(fragment.getContext());
        if (isSmall) mAdView.setAdSize(new AdSize(328, 150));
        else mAdView.setAdSize(new AdSize(350, 150));
        mAdView.setAdUnitId(bannerAd);
        mAdView.setLayoutParams(params);
        try {
            // Create an ad request.
            AdRequest.Builder adRequestBuilder = new AdRequest.Builder();

            // Optionally populate the ad request builder.
            adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            adRequestBuilder.addTestDevice(fragment.getContext().getString(R.string.test_device));
            // Add the AdView to the view hierarchy.
            layout.addView(mAdView);

            // Start loading the ad.
            mAdView.loadAd(adRequestBuilder.build());
            mAdView.resume();
        } catch (Exception e) {

        }
        if (isHideBanner() || isPremium) {
            layout.setVisibility(View.GONE);
        }

        mAdView.setAdListener(new AdListener() {

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                MenuActivity.sharedPreferences.edit().putLong("time", System.currentTimeMillis()).commit();
                layout.setVisibility(View.GONE);
                Track.sendTrackerAction("Click Ads", "true");
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                mAdView.setVisibility(View.GONE);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mAdView.setVisibility(View.VISIBLE);
            }
        });
        return mAdView;
    }

    private static boolean isHideBanner() {
        // check ads clicked
        long lastTimeClickedAds = MenuActivity.sharedPreferences.getLong("time", 0);
        long probAds = MenuActivity.sharedPreferences.getLong("probAds", 0);
        long currentTime = System.currentTimeMillis();
        if (currentTime < (lastTimeClickedAds + probAds)) {
            return true;
        }
        return false;
    }
}
