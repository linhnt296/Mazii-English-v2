package eup.mobi.jedictionary.module;

/**
 * Created by dovantiep on 12/01/2016.
 */
public class Category {
    private int id;
    private String name, date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
