package eup.mobi.jedictionary.kanjirecognize;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.horizontallistview.HorizontalListView;
import eup.mobi.jedictionary.residemenuitems.TraCuuFragment;
import eup.mobi.jedictionary.tapviet.ResultGridAdapter;

import java.util.ArrayList;

/**
 * Created by thanh on 10/27/2015.
 */
public class OfflineRecogDialog extends Dialog implements AddingStrokeListener, DataLoadedListener, AdapterView.OnItemClickListener {
    private Button btnClear, btnUndo;
    public OfflineCanvasView canvasView;
    private HorizontalListView gridView;
    public ArrayList<String> resultList;
    public ResultGridAdapter resultGridAdapter;
    private Context context;
    public TraCuuFragment traCuuFragment;
    public ProgressBar progressBar;

    public TraCuuFragment getTraCuuFragment() {
        return traCuuFragment;
    }

    public void setTraCuuFragment(TraCuuFragment traCuuFragment) {
        this.traCuuFragment = traCuuFragment;
    }

    public OfflineRecogDialog(Context context) {
        super(context);
        this.context = context;
    }
    public static boolean isShow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.offline_canvas_dialog);
        btnClear = (Button)findViewById(R.id.button_clear_dialog_zinnia);
        btnUndo = (Button)findViewById(R.id.button_undo_dialog_zinnia);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        canvasView = (OfflineCanvasView)findViewById(R.id.dialog_canvas_zinnia);
        gridView = (HorizontalListView)findViewById(R.id.grid_dialog_zinnia);
        progressBar.setVisibility(View.VISIBLE);

        btnUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvasView.undoCanvas();
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvasView.clearCanvas();
            }
        });
        resultList = new ArrayList<String>();
        resultGridAdapter = new ResultGridAdapter(resultList, context);
        gridView.setAdapter(resultGridAdapter);
        gridView.setOnItemClickListener(this);
        canvasView.mDataLoadedListener = this;
        canvasView.mStrokeAddListener = this;

    }

    @Override
    public void onStrokeAddListener() {
        resultList.clear();
        resultList.addAll(canvasView.getKanjiResultList());
        resultGridAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDataLoaded() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String text = traCuuFragment.searchView.getQuery().toString();
        text += resultList.get(position);
        resultList.clear();
        resultGridAdapter.notifyDataSetChanged();
        canvasView.clearCanvas();
        traCuuFragment.searchView.setQuery(text, false);
    }
}
