package eup.mobi.jedictionary.database;

import android.text.TextUtils;

/**
 * Created by nguye on 8/21/2015.
 */
public class Example {
    private int mId;
    private String mContent;
    private String mMean;
    private String mTrans;

    public Example(int mId, String mContent, String mMean) {
        this.mId = mId;
        this.mMean = mMean;
        setmContent(mContent);
    }

    public void setmMean(String mMean) {
        this.mMean = mMean;
    }

    public void setmContent(String mContent) {
        mTrans = convertToFurigana(mContent);
        this.mContent = mContent;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }


    public String getmContent() {
        return mContent;
    }

    public int getmId() {
        return mId;
    }

    public String getmMean() {
        return mMean;
    }

    public String getmTrans() {
        return mTrans;
    }

    private String convertToFurigana(String content) {
        String trans = "";
        String[] _ref = content.split("\t");
        for (int i = 0; i < _ref.length; i++) {
            String j = _ref[i];
            String[] ref1 = j.split(" ");
            String writing = ref1[0], pos = null, reading = null;
            if (ref1.length > 1) pos = ref1[1];
            if (ref1.length > 2) reading = ref1[2];
            String word = writing;
            if (!TextUtils.isEmpty(pos) && !TextUtils.isEmpty(reading)) {
//                    word = '<ruby><rb>' + writing + '</rb><rt>' + reading + '</rt></ruby>';
                word = String.format("{%s;%s}", writing, reading);
            }
            if (!TextUtils.isEmpty(word)) trans = trans.concat(word);
        }
        return trans.equals("") ? null : trans;
    }
}
