package eup.mobi.jedictionary.flashcard;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Kanji;
import eup.mobi.jedictionary.residemenuactivity.FlashCardActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import me.grantland.widget.AutofitTextView;

/**
 * Created by chenupt@gmail.com on 2015/1/31.
 * Description TODO
 */
public class FlashCardFragment2 extends Fragment {
    TextView tvKanji, tvMean, tvOn;
    AutofitTextView tvKun;
    int mPostion;
    String mKanji = "", mMean = "", mKun = "", mOn = "", mExamples;
    Kanji kanji;
    String jW, jP, jM;
    ImageView iv_boder;
    LinearLayout row1, row2, row3;
    TextView list_item_w, list_item_p, list_item_m, list_item_w2, list_item_p2, list_item_m2, list_item_w3, list_item_p3, list_item_m3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup parentView = (ViewGroup) inflater.inflate(
                R.layout.flashcard_fragment2, container, false);
        tvKanji = (TextView) parentView.findViewById(R.id.fc_kanji);
        tvMean = (TextView) parentView.findViewById(R.id.fc_mean);
        tvKun = (AutofitTextView) parentView.findViewById(R.id.fc_kun);
        tvOn = (TextView) parentView.findViewById(R.id.fc_on);
        iv_boder = (ImageView) parentView.findViewById(R.id.fc_border);

        row1 = (LinearLayout) parentView.findViewById(R.id.row_1);
        row2 = (LinearLayout) parentView.findViewById(R.id.row_2);
        row3 = (LinearLayout) parentView.findViewById(R.id.row_3);

        list_item_w = (TextView) parentView.findViewById(R.id.list_item_w);
        list_item_w2 = (TextView) parentView.findViewById(R.id.list_item_w2);
        list_item_w3 = (TextView) parentView.findViewById(R.id.list_item_w3);
        list_item_p = (TextView) parentView.findViewById(R.id.list_item_p);
        list_item_p2 = (TextView) parentView.findViewById(R.id.list_item_p2);
        list_item_p3 = (TextView) parentView.findViewById(R.id.list_item_p3);
        list_item_m = (TextView) parentView.findViewById(R.id.list_item_m);
        list_item_m2 = (TextView) parentView.findViewById(R.id.list_item_m2);
        list_item_m3 = (TextView) parentView.findViewById(R.id.list_item_m3);
        if (FlashCardActivity.STATUS_FC == 2) {
            Bundle bundle2 = getArguments();
            mPostion = bundle2.getInt("mposition");
            kanji = FlashCardActivity.arrayList.get(mPostion);
        } else if (FlashCardActivity.STATUS_FC == 0) {
            if (FlashCardActivity.arrayList_1.size() > 0) {
                Bundle bundle2 = getArguments();
                mPostion = bundle2.getInt("position");
                kanji = FlashCardActivity.arrayList_1.get(mPostion - 1);
            }
        } else if (FlashCardActivity.STATUS_FC == 1) {
            if (FlashCardActivity.arrayList_2.size() > 0) {
                Bundle bundle2 = getArguments();
                mPostion = bundle2.getInt("position");
                kanji = FlashCardActivity.arrayList_2.get(mPostion - 1);
            }
        }
        if (kanji!=null) {
            mKanji = kanji.getmKanji();
            mMean = kanji.getmMean();
            mKun = kanji.getmKun();
            mOn = kanji.getmOn();
            mExamples = kanji.getmExamples();

            Boolean isJSONExample = false;
            for (int j = 0; j < mExamples.length(); j++) {
                if (mExamples.charAt(j) == '{') {
                    isJSONExample = true;
                }
            }
            if (isJSONExample) {
                new JsonExampleAsyntask().execute(mExamples);
            }

            tvKanji.setText(mKanji);
            tvMean.setText(mMean);
            tvKun.setText(mKun);
            tvOn.setText(mOn);
        }

        return parentView;
    }

    //     JSON asyntask
    private class JsonExampleAsyntask extends AsyncTask<String, JSONArray, JSONArray> {

        @Override
        protected JSONArray doInBackground(String... params) {
            JSONArray jExampleArr = null;
            try {
                jExampleArr = new JSONArray(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return jExampleArr;
        }


        @Override
        protected void onPostExecute(JSONArray values) {
            super.onPostExecute(values);
            JSONArray jExampleArr = values;
            try {
                for (int i = 0; i < jExampleArr.length(); i++) {
                    if (i == 0) {
                        JSONObject jobj = jExampleArr.getJSONObject(i);
                        if (jobj.has("w"))
                            jW = jobj.getString("w");
                        if (jobj.has("p"))
                            jP = jobj.getString("p");
                        if (jobj.has("m")) {
                            jM = jobj.getString("m");
                            jM = jM.substring(0, 1).toUpperCase() + jM.substring(1);
                        }
                        list_item_w.setText(jW);
                        list_item_p.setText(jP);
                        list_item_m.setText(jM);
                        row1.setVisibility(View.VISIBLE);
                    } else if (i == 1) {
                        JSONObject jobj = jExampleArr.getJSONObject(i);
                        if (jobj.has("w"))
                            jW = jobj.getString("w");
                        if (jobj.has("p"))
                            jP = jobj.getString("p");
                        if (jobj.has("m")) {
                            jM = jobj.getString("m");
                            jM = jM.substring(0, 1).toUpperCase() + jM.substring(1);
                        }
                        list_item_w2.setText(jW);
                        list_item_p2.setText(jP);
                        list_item_m2.setText(jM);
                        row2.setVisibility(View.VISIBLE);
                    } else if (i == 2) {
                        JSONObject jobj = jExampleArr.getJSONObject(i);
                        if (jobj.has("w"))
                            jW = jobj.getString("w");
                        if (jobj.has("p"))
                            jP = jobj.getString("p");
                        if (jobj.has("m")) {
                            jM = jobj.getString("m");
                            jM = jM.substring(0, 1).toUpperCase() + jM.substring(1);
                        }
                        list_item_w3.setText(jW);
                        list_item_p3.setText(jP);
                        list_item_m3.setText(jM);
                        row3.setVisibility(View.VISIBLE);
                    } else {
                        break;
                    }
                }

            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }
}