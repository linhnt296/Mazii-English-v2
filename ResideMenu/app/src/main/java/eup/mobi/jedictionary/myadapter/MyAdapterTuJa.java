package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Jaen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by nguye on 8/21/2015.
 */
public class MyAdapterTuJa extends
        ArrayAdapter<Jaen> {
    Activity context = null;
    ArrayList<Jaen> myArray = null;
    int layoutId;
    String jMean = null;
    LayoutInflater inflater;

    public MyAdapterTuJa(Activity context,
                         int layoutId,
                         ArrayList<Jaen> arr) {
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_word = (TextView) convertView.findViewById(R.id.tv_tracuutab1_ja);
            holder.tv_mean = (TextView) convertView.findViewById(R.id.tv_tracuutab1_mean_ja);
            holder.tv_phonetic = (TextView) convertView.findViewById(R.id.tv_tracuutab1_phonetic_ja);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Jaen ja = myArray.get(position);
        if (ja.getmWord() != null) {
            holder.tv_word.setText(ja.getmWord());
            if (ja.getmPhonetic() != null && !ja.getmPhonetic().equals("")) {
                String mPhonetic = ja.getmPhonetic();
                mPhonetic = mPhonetic.trim();
                mPhonetic = mPhonetic.replace(" ", ", ");
                holder.tv_phonetic.setText(" 「" + mPhonetic + "」");
            } else holder.tv_phonetic.setText("");
        } else {
            if (ja.getmPhonetic() != null)
                holder.tv_word.setText(ja.getmPhonetic());
        }
        if (ja.getmMean() != null) {

            try {
                JSONArray jsonArray = new JSONArray(ja.getmMean());
                JSONObject jsonObject = jsonArray.getJSONObject(0);
                if (jsonObject.has("mean")) {
                    jMean = jsonObject.getString("mean");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jMean != null)
                holder.tv_mean.setText(jMean);

        }
        return convertView;
    }

    class ViewHolder {
        TextView tv_word, tv_phonetic, tv_mean;
    }
}
