package eup.mobi.jedictionary.docbao;

/**
 * Created by nguye on 9/15/2015.
 */
public class SubRootObj {
    public int status ;
    public SubResult result ;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public SubResult getResult() {
        return result;
    }

    public void setResult(SubResult result) {
        this.result = result;
    }

    public SubRootObj(int status, SubResult result) {
        this.status = status;
        this.result = result;
    }
}
