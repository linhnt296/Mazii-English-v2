package eup.mobi.jedictionary.residemenuactivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.CardView;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.MergeObj;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.fast.search.FastSearchService;
import eup.mobi.jedictionary.furiganaview.FuriganaView;
import eup.mobi.jedictionary.google.AdNativeExpress;
import eup.mobi.jedictionary.module.DynamicView;
import eup.mobi.jedictionary.module.KanjiExampleObj;
import eup.mobi.jedictionary.module.Language;
import eup.mobi.jedictionary.module.MergeKanjiAndHiragana;
import eup.mobi.jedictionary.svgwriter.SVGCanvasView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class TraCuuHanTuActivity extends Activity {
    private String mKanji;
    private String mMean, mKun, mOn, mStroke_count, mLevel, mCompDetail, mDetail, mExamples, detail_less, detail_more;
    private int mFavorite, wID, count = 0;
    String jW, jP, jM;
    String txt_comDetail, txt_Temp, jCc, jCh, svg_kanji;
    SVGCanvasView svgView;
    TextView tvKanji, tvMean, tvKun, tvOn, tvStrokeCount, tvLevel, tvComDetail, tvDetail, tvDetailMore;
    CardView cardView1, cardView2, cardView3, cardExam;
    ArrayList<KanjiExampleObj> arrExample = new ArrayList<>();
    Button bt_replay, bt_add_word;
    Activity mContext;
    ExpandableRelativeLayout mExpandLayout;
    ArrayList<Jaen> arrExam = new ArrayList<>();
    private LinearLayout layout_exam;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        //set fullscreen
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tracuuhantu_activity);
        mContext = this;
        cardView1 = (CardView) findViewById(R.id.frag2_card1);
        cardView2 = (CardView) findViewById(R.id.frag2_card2);
        cardView3 = (CardView) findViewById(R.id.frag2_card3);
        tvKanji = (TextView) findViewById(R.id.frag2_kanji);
        tvMean = (TextView) findViewById(R.id.frag2_mean);
        tvKun = (TextView) findViewById(R.id.frag2_kun);
        tvOn = (TextView) findViewById(R.id.frag2_on);
        tvStrokeCount = (TextView) findViewById(R.id.frag2_strokeCount);
        tvLevel = (TextView) findViewById(R.id.frag2_level);
        tvComDetail = (TextView) findViewById(R.id.frag2_comDetail);
        tvDetail = (TextView) findViewById(R.id.frag2_detail);
        tvDetailMore = (TextView) findViewById(R.id.frag2_detail_more);
        bt_replay = (Button) findViewById(R.id.button_replay);
        bt_add_word = (Button) findViewById(R.id.bt_add_word);
        mExpandLayout = (ExpandableRelativeLayout) findViewById(R.id.expandableLayout);
        layout_exam = (LinearLayout) findViewById(R.id.layout_exam);
        cardExam = (CardView) findViewById(R.id.frag1_card_exam);
        // Expand layout
        final ImageView mExpand_more = (ImageView) findViewById(R.id.expand_more);
        final ImageView mExpand_less = (ImageView) findViewById(R.id.expand_less);

        if (MenuActivity.sharedPreferences != null) {
            boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
            String banner = MenuActivity.sharedPreferences.getString("idNative", getString(R.string.ad_unit_id_medium));
            float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
            try {
                AdNativeExpress.smartNative(mContext, banner, probBanner, isPremium);
            }catch (Exception e){

            }
        }

        //get bundle
        Intent callerIntent = getIntent();
        //có intent rồi thì lấy Bundle dựa vào MyPackage
        Bundle packageFromCaller =
                callerIntent.getBundleExtra("TraCuuFragment_2");
        mKanji = packageFromCaller.getString("mKanji");
        mMean = packageFromCaller.getString("mMean");
        mKun = packageFromCaller.getString("mKun");
        svg_kanji = readSvg(mKanji.charAt(0));
        mOn = packageFromCaller.getString("mOn");
        mStroke_count = packageFromCaller.getString("mStroke_count");
        mLevel = packageFromCaller.getString("mLevel");
        mCompDetail = packageFromCaller.getString("mCompDetail");
        mDetail = packageFromCaller.getString("mDetail");
        int m = 0;
        if (!mDetail.equals("")) {
            for (int k = 0; k < mDetail.length(); k++) {
                if (mDetail.charAt(k) == '#' && mDetail.charAt(k + 1) == '#') {
                    count++;
                    if (count == 2) {
                        m = k;
                    }
                }
            }
            if (count > 2) {
                detail_less = mDetail.substring(0, m);
                detail_more = mDetail.substring(m + 2, mDetail.length() - 1);
            } else if (count <= 2) {
                detail_less = mDetail;
//                rl_expand.setVisibility(View.GONE);
                mExpand_more.setVisibility(View.GONE);
            }
            if (detail_less != null)
                detail_less = detail_less.replace("##", "\n");
            if (detail_more != null)
                detail_more = detail_more.replace("##", "\n");
        } else {
//            rl_expand.setVisibility(View.GONE);
            mExpand_more.setVisibility(View.GONE);
        }
        if (packageFromCaller.getString("mExamples") != "") {
            mExamples = packageFromCaller.getString("mExamples");
        } else mExamples = "";
        mFavorite = packageFromCaller.getInt("mFavorite");
        wID = packageFromCaller.getInt("wID");
        if (mCompDetail != "") {
            new JsoncomDetailAsyntask().execute(mCompDetail);
        }

        Boolean isJSONExample = false;
        for (int j = 0; j < mExamples.length(); j++) {
            if (mExamples.charAt(j) == '{') {
                isJSONExample = true;
            }
        }
        if (isJSONExample) {
            new JsonExampleAsyntask().execute(mExamples);
        }
        new loadExamSynctask().execute(mKanji);
        bt_add_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, PlusActitity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString("word", mKanji);
                bundle1.putString("mean", mDetail.replace("##", "\n"));
                bundle1.putString("phonetic", mKun + " - " + mOn);
                bundle1.putString("type", "kanji");
                bundle1.putInt("id", wID);
                bundle1.putInt("favorite", mFavorite);

                intent.putExtra("PlusActivity", bundle1);
                //Mở Activity ResultActivity
                back = true;
                startActivity(intent);
            }
        });
        mExpand_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpand_more.setVisibility(View.GONE);
                mExpand_less.setVisibility(View.VISIBLE);
                // todo expand more
                mExpandLayout.toggle();

            }
        });
        mExpand_less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpand_less.setVisibility(View.GONE);
                mExpand_more.setVisibility(View.VISIBLE);
                //todo expand less
                mExpandLayout.move(0);
            }
        });

        bt_replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "Replay", Toast.LENGTH_SHORT).show();
                svgView.rePaint();
            }
        });
        // webView Han tu setup

        tvKanji.setText(mKanji);
        if (mMean != null) {
            tvMean.setText(mMean);
        }
        tvKun.setText(mKun);
        tvOn.setText(mOn);
        tvStrokeCount.setText(mStroke_count);
        tvLevel.setText(mLevel);
        tvDetail.setText(detail_less);
        tvDetailMore.setText(detail_more);
        cardView1.setVisibility(View.VISIBLE);
        bt_add_word.setVisibility(View.VISIBLE);

        if (!svg_kanji.equals("") && svg_kanji != null) {
            svgView = (SVGCanvasView) findViewById(R.id.svg_kanji);
            svgView.init(svg_kanji);
            cardView2.setVisibility(View.VISIBLE);
        } else {
            cardView2.setVisibility(View.GONE);
        }
    }

    //     JSON asyntask
    class JsonExampleAsyntask extends AsyncTask<String, JSONArray, Void> {

        @Override
        protected Void doInBackground(String... params) {
            JSONArray jExampleArr = null;
            try {
                jExampleArr = new JSONArray(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            JSONObject subJMean = jb.getJSONObject("response");
            publishProgress(jExampleArr);
            return null;
        }

        @Override
        protected void onProgressUpdate(JSONArray... values) {
            super.onProgressUpdate(values);
            JSONArray jExampleArr = values[0];
            try {
                for (int i = 0; i < jExampleArr.length(); i++) {
                    JSONObject jobj = jExampleArr.getJSONObject(i);
                    if (jobj.has("w"))
                        jW = jobj.getString("w");
                    if (jobj.has("p"))
                        jP = jobj.getString("p");
                    if (jobj.has("m")) {
                        jM = jobj.getString("m");
                        jM = jM.substring(0, 1).toUpperCase() + jM.substring(1);
                    }
                    KanjiExampleObj obj = new KanjiExampleObj(jW, jP, jM);
                    arrExample.add(obj);
                }
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrExample.size() > 0) {
                try {
                    DynamicView.viduRecycler(mContext, arrExample);
                    cardView3.setVisibility(View.VISIBLE);
                } catch (Exception e) {
                }
            } else {
                cardView3.setVisibility(View.GONE);
            }
        }
    }

    //     JSON asyntask
    class JsoncomDetailAsyntask extends AsyncTask<String, JSONArray, String> {

        @Override
        protected String doInBackground(String... params) {
            JSONArray jcomDetailArr = null;
            try {
                jcomDetailArr = new JSONArray(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jcomDetailArr != null) {
                try {
                    txt_comDetail = "";
                    for (int i = 0; i < jcomDetailArr.length(); i++) {
                        JSONObject jobj = jcomDetailArr.getJSONObject(i);

                        txt_Temp = "";
                        if (jobj.has("w"))
                            jCc = jobj.getString("w");
                        if (jobj.has("h"))
                            jCh = jobj.getString("h");

                        txt_Temp += jCc + "(" + jCh + ")";
                        txt_comDetail += txt_Temp + "  ";
                    }

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            } else txt_comDetail = "";

            return txt_comDetail;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tvComDetail.setText(s);
        }
    }

    private boolean back;

    @Override
    public void onBackPressed() {
        back = true;
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        back = false;
        stopService(new Intent(this, FastSearchService.class));
    }

    @Override
    protected void onStop() {
        if (!back && MenuActivity.sharedPreferences.getBoolean("fast_search", false)) {
            if (Build.VERSION.SDK_INT < 23) {
                startService(new Intent(this, FastSearchService.class));
            } else if (Build.VERSION.SDK_INT >= 23 && Settings.canDrawOverlays(this)) {
                startService(new Intent(this, FastSearchService.class));
            }
//            Toast.makeText(this, "start service", Toast.LENGTH_SHORT).show();
        }
        super.onStop();
    }
    private class loadExamSynctask extends AsyncTask<String, Void, ArrayList<Jaen>> {
        @Override
        protected ArrayList<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(mContext);
            if (params[0] != null && !params[0].equals(""))
                arrExam = db.qExampleKanji(params[0]);
            return arrExam;
        }

        @Override
        protected void onPostExecute(ArrayList<Jaen> examples) {
            super.onPostExecute(examples);
            if (examples != null && examples.size() > 0) {

                for (Jaen ex : examples) {

                    View v = LayoutInflater.from(mContext).inflate(R.layout.item_example_kanji, layout_exam, false);
                    TextView tvMean = (TextView) v.findViewById(R.id.tvMean);
                    FuriganaView furi = (FuriganaView) v.findViewById(R.id.furi);

                    if (ex.getmMean() != null) {
                        try {
                            JSONArray array = new JSONArray(ex.getmMean());
                            JSONObject object = array.getJSONObject(0);
                            tvMean.setText(object.optString("mean"));
                        } catch (JSONException e) {
                        }
                    } else {
                        tvMean.setVisibility(View.INVISIBLE);
                    }

                    //todo set fv_tab4
                    TextPaint tp = new TextPaint();
                    tp.setColor(Color.parseColor("#808080"));
                    tp.setTextSize(mContext.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                    tp.setAntiAlias(true);
                    tp.setDither(true);
                    tp.setStyle(Paint.Style.FILL);
                    tp.setStrokeJoin(Paint.Join.ROUND);

                    String text = mergeKH(ex.getmWord(), ex.getmPhonetic());
                    if (text != null && !text.equals("")) {
                        furi.text_set(tp, text, -1, -1);
                        furi.setPadding(0, 28, 0, 16);
                    }

                    layout_exam.addView(v);
                }
                cardExam.setVisibility(View.VISIBLE);
            }
        }
    }

    private static String mergeKH(String kanji, String hiragana) {
        String myruby = null;
        if (hiragana == null) {
            myruby = kanji;
        } else if (hiragana.equals("xxx。") || hiragana.equals("xxx") || Language.isVietnamese(hiragana)) {
            myruby = kanji;
        } else {
            ArrayList<MergeObj> mergeObjs = MergeKanjiAndHiragana.mergeKanjiAndHiragana(kanji, hiragana);
            if (mergeObjs != null) {

                for (int i = 0; i < mergeObjs.size(); i++) {
                    MergeObj myMg = mergeObjs.get(i);
                    String ruby = "{" + myMg.getKanji() + ";" + myMg.getHiragana() + "}";
                    if (myruby == null) {
                        myruby = ruby;
                    } else {
                        myruby += ruby;
                    }
                }
            } else {
                myruby = "";
            }
        }
        return myruby;
    }

    private String readSvg(char file_name) {
        String hex = Integer.toHexString(file_name);
        if (hex.length() < 5) {
            for (int i = 0; i < (5 - hex.length()); i++)
                hex = "0" + hex;
        }
        StringBuilder result = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(mContext.getAssets().open("stroke/" + hex + ".svg"), "UTF-8"));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                result.append(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return result.toString();
    }
}
