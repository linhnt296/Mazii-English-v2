package eup.mobi.jedictionary.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.text.TextUtils;

import eup.mobi.jedictionary.database.sqliteAsset.SQLiteAssetHelper;
import eup.mobi.jedictionary.module.Category;
import eup.mobi.jedictionary.module.Entry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dovantiep on 12/01/2016.
 */
public class MyWordDatabase extends SQLiteAssetHelper {
    public MyWordDatabase(Context context) {
        super(context, "myWord.db", null, 1);
    }

    public boolean insertEntry(Entry entry) {
        SQLiteDatabase db = getWritableDatabase();
        String qCheck = "select * from entry where id_category = " + entry.getIdCategory()
                + " and id_entry = " + entry.getIdEntry();
        Cursor cursor = db.rawQuery(qCheck, null);
        int i = 0;
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            i++;
            cursor.moveToNext();
        }
        if (i == 0) {
            ContentValues cv = new ContentValues();
            cv.put("id_category", entry.getIdCategory());
            cv.put("id_entry", entry.getIdEntry());
            cv.put("type", TextUtils.isEmpty(entry.getType()) ? "" : entry.getType());
            cv.put("word", TextUtils.isEmpty(entry.getWord()) ? "" : entry.getWord());
            cv.put("phonetic", TextUtils.isEmpty(entry.getPhonetic()) ? "" : entry.getPhonetic());
            cv.put("mean", TextUtils.isEmpty(entry.getMean()) ? "" : entry.getMean());
            long rowId = db.insert("entry", null, cv);
            return rowId >= 0;
        } else {
            return false;
        }
    }

    public long insertCategory(Category category) {
        SQLiteDatabase db = getWritableDatabase();
        String qCheck = String.format("select * from category where name = '%s'", category.getName());
        Cursor cursor = db.rawQuery(qCheck, null);
        cursor.moveToFirst();
        int i = 0;
        while (!cursor.isAfterLast()) {
            i++;
            cursor.moveToNext();
        }
        if (i != 0) {
            return -1;
        } else {
            ContentValues cv = new ContentValues();
            cv.put("name", category.getName());
            return db.insert("category", null, cv);
        }
    }

    public List<Category> qCategory() {
        List<Category> categoryList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String q = "select * from category order by date desc";

        Cursor cursor = db.rawQuery(q, null);
        cursor.moveToFirst();

        Category category;
        while (!cursor.isAfterLast()) {
            category = new Category();

            category.setId(cursor.getInt(0));
            category.setName(cursor.getString(1));
            category.setDate(cursor.getString(2));

            categoryList.add(category);

            cursor.moveToNext();
        }
        cursor.close();
        return categoryList;
    }

    public List<Entry> qEntry(int categoryId) {
        List<Entry> entryList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String q;
        if (categoryId == -1) {
            q = "select * from entry order by date desc";
        } else q = "select * from entry where id_category = " + categoryId + " order by date desc";
        Cursor cursor = db.rawQuery(q, null);
        cursor.moveToFirst();

        Entry entry;
        while (!cursor.isAfterLast()) {

            entry = new Entry();

            entry.setId(cursor.getInt(0));
            entry.setIdCategory(cursor.getInt(1));
            entry.setIdEntry(cursor.getInt(2));
            entry.setType(cursor.getString(3));
            entry.setWord(cursor.getString(4));
            entry.setPhonetic(cursor.getString(5));
            entry.setMean(cursor.getString(6));
            entry.setDate(cursor.getString(7));
//            entry.setRemember(cursor.getInt(8));

            entryList.add(entry);

            cursor.moveToNext();
        }

        cursor.close();
        return entryList;
    }

    public void deleleEntry(final int id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteDatabase db = getWritableDatabase();
                db.delete("entry", "id=?", new String[]{String.valueOf(id)});
            }
        }).start();
    }

    public void deleleCategory(final int id) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                SQLiteDatabase db = getWritableDatabase();
                String q = "DELETE FROM entry WHERE id_category = " + id;
                db.execSQL(q);//xoa category trong entry
                db.delete("category", "id=?", new String[]{String.valueOf(id)});
                return null;
            }
        }.execute();
    }

    public void deleleEntryCategory(final int categoryId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SQLiteDatabase db = getWritableDatabase();
                String q = "DELETE FROM entry WHERE id_category = " + categoryId;
                db.execSQL(q);
            }
        }).start();
    }

    public boolean renameCategory(String newName, int idCategory) {
        if (TextUtils.isEmpty(newName)) return false;
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("name", newName);
        try {
            return db.update("category", cv, "id=?", new String[]{String.valueOf(idCategory)}) > -1;
        } catch (Exception e) {
            return false;
        }
    }

    public ArrayList<Entry> getEntryFlashCard(int categoryId) {
        ArrayList<Entry> entryList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String q = "select * from entry where id_category = " + categoryId + " order by date desc";
        Cursor cursor = db.rawQuery(q, null);
        cursor.moveToFirst();

        Entry entry;
        while (!cursor.isAfterLast()) {

            entry = new Entry();

            entry.setId(cursor.getInt(0));
            entry.setIdCategory(cursor.getInt(1));
            entry.setIdEntry(cursor.getInt(2));
            entry.setType(cursor.getString(3));
            entry.setWord(cursor.getString(4));
            entry.setPhonetic(cursor.getString(5));
            entry.setMean(cursor.getString(6));
            entry.setDate(cursor.getString(7));
            entry.setRemember(cursor.getInt(8));

            entryList.add(entry);

            cursor.moveToNext();
        }

        cursor.close();
        return entryList;
    }

    public void setRememberOn(int id) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
        }
        if (db != null) {
            String sql_Query1 = "UPDATE entry SET remember= 1 WHERE id= " + id;
            db.execSQL(sql_Query1);
        }
    }

    public void setRememberOff(int id) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
        }
        if (db != null) {
            String sql_Query1 = "UPDATE entry SET remember= 0 WHERE id= " + id;
            db.execSQL(sql_Query1);
        }
    }
}
