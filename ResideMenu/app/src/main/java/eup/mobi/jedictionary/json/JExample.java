package eup.mobi.jedictionary.json;

/**
 * Created by nguye on 9/3/2015.
 */
public class JExample {
    private String _w;
    private String _p;
    private String _m;

    public String get_p() {
        return _p;
    }

    public void set_p(String _p) {
        this._p = _p;
    }

    public String get_w() {
        return _w;
    }

    public void set_w(String _w) {
        this._w = _w;
    }

    public String get_m() {
        return _m;
    }

    public void set_m(String _m) {
        this._m = _m;
    }

    public JExample(String _w, String _p, String _m) {
        this._w = _w;
        this._p = _p;
        this._m = _m;
    }
}
