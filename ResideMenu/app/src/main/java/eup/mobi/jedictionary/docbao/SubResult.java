package eup.mobi.jedictionary.docbao;

/**
 * Created by nguye on 9/15/2015.
 */
public class SubResult {
    public String _id ;
    public String _rev ;
    public String title ;
    public String link ;
    public String pubDate ;
    public String description ;
    public SubContent content ;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_rev() {
        return _rev;
    }

    public void set_rev(String _rev) {
        this._rev = _rev;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SubContent getContent() {
        return content;
    }

    public void setContent(SubContent content) {
        this.content = content;
    }

    public SubResult(String _id, String _rev, String title, String link, String pubDate, String description, SubContent content) {
        this._id = _id;
        this._rev = _rev;
        this.title = title;
        this.link = link;
        this.pubDate = pubDate;
        this.description = description;
        this.content = content;
    }
}
