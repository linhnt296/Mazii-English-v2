package eup.mobi.jedictionary.residemenuitems;


import android.app.Dialog;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.MyWordDatabase;
import eup.mobi.jedictionary.google.Track;
import eup.mobi.jedictionary.module.Category;
import eup.mobi.jedictionary.myadapter.MyGroupWordAdapter;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.residemenuactivity.MyWordActivity;
import eup.mobi.jedictionary.service.WidgetProvider;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SoTayFragment extends Fragment {
    private View parentView;
    private MenuActivity menuActivity;
    private ImageButton bt_plus;
    List<Category> categories;
    private SwipeMenuListView swipeListView;
    MyWordDatabase db;
    MyGroupWordAdapter adapter;
    EditText et_add, et_edit;
    Dialog sub, dialogEdit;
    Button btn_tao, btn_huy, btn_edit, btn_huy_edit;
    int edit_position;
    String edit_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.sotay, container, false);
        ImageButton bt_menu = (ImageButton) parentView.findViewById(R.id.title_bar_left_menu);
        bt_plus = (ImageButton) parentView.findViewById(R.id.bt_plus);
        swipeListView = (SwipeMenuListView) parentView.findViewById(R.id.swipe_listview);
        FloatingActionButton fab = (FloatingActionButton) parentView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Track.sendTrackerScreen("Search");
                // changeFragment(new TraCuuFragment());
                TraCuuFragment traCuuFragment = new TraCuuFragment();
                traCuuFragment.setMenuActivity(menuActivity);
                changeFragment(traCuuFragment);
            }
        });
        // setup swipe menulistview
        swipeListView();

        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                        MenuActivity.drawer.openDrawer(GravityCompat.START);
                } catch (Exception e) {
                }
            }
        });

        boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
        float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);

//        Adsmod.fragmentBanner(parentView, banner, probBanner, isPremium);


        bt_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert();
            }
        });

        //load categories
        new loadCategories().execute();

        return parentView;
    }

    public void setMenuActivity(MenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }

    //change Fragment
    private void changeFragment(Fragment targetFragment) {
        //resideMenu.clearIgnoredViewList();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void showAlert() {
        // custom dialog
        sub = new Dialog(getActivity());
        sub.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sub.setContentView(R.layout.add_group_dialog);
        // map edit text, button
        et_add = (EditText) sub.findViewById(R.id.et_add);
        btn_tao = (Button) sub.findViewById(R.id.btn_tao);
        btn_huy = (Button) sub.findViewById(R.id.btn_huy);

        btn_tao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedTao();
            }
        });
        btn_huy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sub.dismiss();
            }
        });

        sub.show();
        Window window = sub.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        sub.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });
    }

    private class loadCategories extends AsyncTask<Void, Void, List<Category>> {
        @Override
        protected List<Category> doInBackground(Void... params) {
            db = new MyWordDatabase(getContext());
            categories = db.qCategory();
            return categories;
        }

        @Override
        protected void onPostExecute(final List<Category> categories) {
            super.onPostExecute(categories);
            getListCategories();
        }
    }

    private void clickedTao() {
        if (et_add.getText() == null) {
            return;
        }
        final String text = et_add.getText().toString().trim();
        if (TextUtils.isEmpty(text) || text.length() == 0) {
            return;
        }
        new AsyncTask<Void, Void, Boolean>() {
            Category category, category_temp;

            @Override
            protected Boolean doInBackground(Void... params) {
                category = new Category();
                category_temp = new Category();
                category.setName(text);

                long date_temp = System.currentTimeMillis();
                String dateString = new SimpleDateFormat("yyyy-MM-dd").format(new Date(date_temp));
                category_temp.setName(text);
                category_temp.setDate(dateString);

                long idCategory = db.insertCategory(category);
                if (idCategory != -1) {
                    SoTayFragment.this.categories.add(0, category_temp);
                    return true;
                } else {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                sub.dismiss();
                if (aBoolean) {
                    adapter.notifyDataSetChanged();
                    Toast.makeText(sub.getContext(), getString(R.string.add_group_success, category.getName()), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(sub.getContext(), R.string.add_group_fail, Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();

    }

    private void swipeListView() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.parseColor("#EFE4B0")));
                // set item width
                openItem.setWidth(90);
//                // set item title
//                openItem.setTitle("Open");
//                // set item title fontsize
//                openItem.setTitleSize(18);
//                // set item title font color
//                openItem.setTitleColor(Color.WHITE);
                //set icon
                openItem.setIcon(android.R.drawable.ic_menu_edit);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.parseColor("#F33F3F")));
                // set item width
                deleteItem.setWidth(90);
                // set a icon
                deleteItem.setIcon(android.R.drawable.ic_menu_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        swipeListView.setMenuCreator(creator);
        swipeListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
    }

    public void onDelRow(int pos) {
        db.deleleCategory(categories.get(pos).getId());
        categories.remove(pos);
        adapter = new MyGroupWordAdapter(getActivity(), R.layout.sotay_item_recyclerview, categories);
        swipeListView.setAdapter(adapter);
    }

    private void getListCategories() {
        adapter = new MyGroupWordAdapter(getActivity(), R.layout.sotay_item_recyclerview, categories);
        if (adapter != null) {
            swipeListView.setAdapter(adapter);
        }
        //Swipe items click listener
        swipeListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // edit
//                        Toast.makeText(getContext(), "Edit", Toast.LENGTH_SHORT).show();
                        showDialogEdit(position);
                        break;
                    case 1:
                        // delete
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(getString(R.string.sotay_alert_title))
                                .setMessage(getString(R.string.sotay_alert_message))
                                .setCancelable(true)
                                .setNegativeButton(getString(R.string.history_delete), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        Toast.makeText(getContext(), getString(R.string.sotay_deleted), Toast.LENGTH_SHORT).show();
                                        onDelRow(position);
                                        notifyWidget();
                                    }
                                })
                                .setPositiveButton(getString(R.string.history_cancel), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
        //item click listener
        swipeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Track.sendTrackerScreen("note-detail");
                Intent intent = new Intent(getActivity(), MyWordActivity.class);
                intent.putExtra("id", categories.get(position).getId());
                intent.putExtra("name", categories.get(position).getName());
                MenuActivity.startMyActivity(intent, getActivity());
            }
        });
    }

    private void showDialogEdit(final int pos) {
        edit_position = pos;
        dialogEdit = new Dialog(getActivity());
        dialogEdit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEdit.setContentView(R.layout.edit_group_dialog);
        // map edit text, button
        et_edit = (EditText) dialogEdit.findViewById(R.id.et_add);
        btn_edit = (Button) dialogEdit.findViewById(R.id.btn_tao);
        btn_huy_edit = (Button) dialogEdit.findViewById(R.id.btn_huy);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedEdit(pos);
            }
        });
        btn_huy_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickedHuyEdit();
            }
        });

        dialogEdit.show();
        Window window = dialogEdit.getWindow();
        window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
        dialogEdit.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
            }
        });
    }

    private void clickedHuyEdit() {
        dialogEdit.dismiss();
    }

    private void clickedEdit(int pos) {
        if (et_edit.getText() == null) {
            return;
        }
        final String text = et_edit.getText().toString().trim();
        if (TextUtils.isEmpty(text) || text.length() == 0) {
            return;
        }
        new editCategories().execute(text, String.valueOf(categories.get(pos).getId()));
    }

    private class editCategories extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            MyWordDatabase db = new MyWordDatabase(getContext());
            edit_name = params[0];
            return db.renameCategory(params[0], Integer.parseInt(params[1]));
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dialogEdit.dismiss();
            if (aBoolean) {
                //todo change category name
                categories.get(edit_position).setName(edit_name);
                adapter = new MyGroupWordAdapter(getActivity(), R.layout.sotay_item_recyclerview, categories);
                swipeListView.setAdapter(adapter);
                Toast.makeText(dialogEdit.getContext(), getString(R.string.edit_group_success), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(dialogEdit.getContext(), getString(R.string.edit_group_fail), Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void notifyWidget(){
        // notify data set changed widget
        final AppWidgetManager mgr = AppWidgetManager.getInstance(getActivity());
        final ComponentName cn = new ComponentName(getActivity(),
                WidgetProvider.class);
        mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn),
                R.id.page_flipper);
    }
}
