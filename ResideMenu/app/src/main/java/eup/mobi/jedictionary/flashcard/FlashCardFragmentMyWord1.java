package eup.mobi.jedictionary.flashcard;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.MyWordDatabase;
import eup.mobi.jedictionary.module.Entry;
import eup.mobi.jedictionary.residemenuactivity.FlashCardMyWordActivity;

import me.grantland.widget.AutofitTextView;

/**
 * Created by chenupt@gmail.com on 2015/1/31.
 * Description TODO
 */
public class FlashCardFragmentMyWord1 extends Fragment {
    private TextView tv_posNum;
    AutofitTextView tv_flashcard;
    int num, pos, mRemember;
    String word;
    Entry entry;
    ImageView mBekyou, mFinished;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.flashcard_fragment_mw1, container, false);

        tv_flashcard = (AutofitTextView) rootView.findViewById(R.id.tv_flashcard);
        tv_posNum = (TextView) rootView.findViewById(R.id.tv_posnum);
        //check on/off favorite
        mBekyou = (ImageView) rootView.findViewById(R.id.bt_fc_bekyou);
        mFinished = (ImageView) rootView.findViewById(R.id.bt_fc_finished);

        switch (FlashCardMyWordActivity.STATUS_FC) {
            case 2:
                Bundle bundle2 = getArguments();
                pos = bundle2.getInt("position");
                num = bundle2.getInt("numpage");

                entry = FlashCardMyWordActivity.arrayList.get(pos - 1);
                break;

            case 0:
                if (FlashCardMyWordActivity.arrayList_1.size() > 0) {
                    Bundle bundle = getArguments();
                    pos = bundle.getInt("position");
                    num = bundle.getInt("numpage");
                    entry = FlashCardMyWordActivity.arrayList_1.get(pos - 1);
                } else {
                    Toast.makeText(getContext(), getString(R.string.flashcard_chuathuoc_null), Toast.LENGTH_SHORT).show();
                }
                break;
            case 1:
                if (FlashCardMyWordActivity.arrayList_2.size() > 0) {
                    Bundle bundle1 = getArguments();
                    pos = bundle1.getInt("position");
                    num = bundle1.getInt("numpage");
                    entry = FlashCardMyWordActivity.arrayList_2.get(pos - 1);
                } else {
                    Toast.makeText(getContext(), getString(R.string.flashcard_dathuoc_null), Toast.LENGTH_SHORT).show();
                }
                break;
        }
        if (entry != null) {
            word = entry.getWord();
            mRemember = entry.getRemember();
            tv_flashcard.setText(word);
            tv_posNum.setText(pos + "/" + num);
        }
        switch (FlashCardMyWordActivity.STATUS_FC) {
            case 2:
                if (mRemember == 1) {
                    mBekyou.setVisibility(View.GONE);
                    mFinished.setVisibility(View.VISIBLE);
                } else {
                    mFinished.setVisibility(View.GONE);
                    mBekyou.setVisibility(View.VISIBLE);
                }
                break;
            case 0:
                mFinished.setVisibility(View.GONE);
                mBekyou.setVisibility(View.VISIBLE);
                break;
            case 1:
                mBekyou.setVisibility(View.GONE);
                mFinished.setVisibility(View.VISIBLE);
        }
        //set image on if isFavorite

        mBekyou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBekyou.setVisibility(View.GONE);
                mFinished.setVisibility(View.VISIBLE);
                // do query
                new UPDATESyntask().execute(entry.getId());
                Toast.makeText(getContext(), getString(R.string.flashcard_add) + word + getString(R.string.flashcard_chamthan), Toast.LENGTH_SHORT).show();
            }
        });
        mFinished.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinished.setVisibility(View.GONE);
                mBekyou.setVisibility(View.VISIBLE);
                // do query
                new REMOVESyntask().execute(entry.getId());
                Toast.makeText(getContext(), getString(R.string.flashcard_remove) + word + getString(R.string.flashcard_chamthan), Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }

    //update favorite tu
    private class UPDATESyntask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            MyWordDatabase db = new MyWordDatabase(getContext());
            db.setRememberOn(params[0]);
            return null;
        }
    }

    //remove favorite tu
    private class REMOVESyntask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            MyWordDatabase db = new MyWordDatabase(getContext());
            db.setRememberOff(params[0]);
            return null;
        }
    }
}