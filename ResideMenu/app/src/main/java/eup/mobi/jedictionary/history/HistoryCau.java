package eup.mobi.jedictionary.history;

/**
 * Created by nguye on 10/30/2015.
 */
public class HistoryCau {
    String hCau;
    long hDate;

    public String gethWords() {
        return hCau;
    }

    public void sethWords(String hCau) {
        this.hCau = hCau;
    }

    public long gethDate() {
        return hDate;
    }

    public void sethDate(long hDate) {
        this.hDate = hDate;
    }

    public HistoryCau(String hCau, long hDate) {
        this.hCau = hCau;
        this.hDate = hDate;
    }
}
