package eup.mobi.jedictionary.service;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

import eup.mobi.jedictionary.R;


public class WidgetProvider extends AppWidgetProvider {
    public static final String REFRESH_ACTION = "REFRESH";
    public static final String NEXT_ACTION = "NEXT";

    @Override
    public void onUpdate(final Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        for (int id : appWidgetIds) {

            RemoteViews rv = new RemoteViews(context.getPackageName(),
                    R.layout.layout_widget);
            // Specify the service to provide data for the collection widget.
            // Note that we need to
            // embed the appWidgetId via the data otherwise it will be ignored.
            final Intent intent = new Intent(context, WidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));
            rv.setRemoteAdapter(R.id.page_flipper, intent);

            // Bind the click intent for the next button on the widget
            final Intent nextIntent = new Intent(context,
                    WidgetProvider.class);
            nextIntent.setAction(WidgetProvider.NEXT_ACTION);
            nextIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
            final PendingIntent nextPendingIntent = PendingIntent
                    .getBroadcast(context, 0, nextIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setOnClickPendingIntent(R.id.next, nextPendingIntent);

            // Bind the click intent for the next button on the widget
            final Intent prevIntent = new Intent(context,
                    WidgetProvider.class);
            prevIntent.setAction(WidgetProvider.REFRESH_ACTION);
            prevIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, id);
            final PendingIntent prevPendingIntent = PendingIntent
                    .getBroadcast(context, 0, prevIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            rv.setOnClickPendingIntent(R.id.refresh, prevPendingIntent);
            appWidgetManager.updateAppWidget(id, rv);
        }

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final String action = intent.getAction();

        if (action.equals(REFRESH_ACTION)) {
            RemoteViews rv = new RemoteViews(context.getPackageName(),
                    R.layout.layout_widget);

            rv.showNext(R.id.page_flipper);
            AppWidgetManager.getInstance(context).partiallyUpdateAppWidget(
                    intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                            AppWidgetManager.INVALID_APPWIDGET_ID), rv);
        }
        if (action.equals(NEXT_ACTION)) {
            next(context, intent);
        }
        super.onReceive(context, intent);
    }

    private void next(Context context, Intent intent) {
        RemoteViews rv = new RemoteViews(context.getPackageName(),
                R.layout.layout_widget);

        rv.showPrevious(R.id.page_flipper);
        AppWidgetManager.getInstance(context).partiallyUpdateAppWidget(
                intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                        AppWidgetManager.INVALID_APPWIDGET_ID), rv);
    }
}