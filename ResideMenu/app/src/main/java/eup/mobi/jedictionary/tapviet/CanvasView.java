package eup.mobi.jedictionary.tapviet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.util.JsonWriter;
import android.view.MotionEvent;
import android.view.View;

import eup.mobi.jedictionary.residemenuitems.TapVietFragment;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by thanh on 10/9/2015.
 */
public class CanvasView extends View {
    public int width, height;
    private Bitmap bitmap;
    private Canvas mCanvas;
    private Path path;
    Context context;
    private Paint paint;
    private float mX, mY;
    private static final float TOLERANCE = 1;
    private ArrayList<Float> currentLineSig1;
    private ArrayList<Float> currentLineSig2;
    private ArrayList<Float> currentLineSig3;
    private ArrayList<Path> pathArrayList;
    private ArrayList<Path> undoPath;
    private ArrayList<Integer> undoInk;
    private int startLength = 0, currentLength = 0;
    private TapVietFragment tapVietFragment;
    private long startDrawTime = 0, currentDrawTime = 0;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch();
                invalidate();
                break;
        }
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (Path p : pathArrayList) {
            canvas.drawPath(p, paint);
        }
        canvas.drawPath(path, paint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(bitmap);
    }

    public CanvasView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.context = context;
        path = new Path();
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(10f);
        currentLineSig1 = new ArrayList<Float>();
        currentLineSig2 = new ArrayList<Float>();
        currentLineSig3 = new ArrayList<Float>();
        pathArrayList = new ArrayList<Path>();
        undoPath = new ArrayList<Path>();
        undoInk = new ArrayList<Integer>();
    }

    private void startTouch(float x, float y) {
        path.moveTo(x, y);
        mX = x;
        mY = y;
        currentLineSig1.add(x);
        currentLineSig2.add(y);
        currentLength = 1;
        if (startDrawTime == 0) {
            startDrawTime = new Date().getTime();
            currentLineSig3.add(0f);
        } else {
            currentDrawTime = new Date().getTime();
            currentLineSig3.add((float) currentDrawTime - startDrawTime);
        }
        undoPath.clear();
    }

    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOLERANCE || dy >= TOLERANCE) {
            path.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
        currentDrawTime = new Date().getTime();
        currentLineSig1.add(x);
        currentLineSig2.add(y);
        currentLength++;
        currentLineSig3.add((float) currentDrawTime - startDrawTime);
    }

    public void clearCanvas() {
        path.reset();
        pathArrayList.clear();
        undoInk.clear();
        invalidate();
        currentLineSig1.clear();
        currentLineSig2.clear();
        currentLineSig3.clear();

    }

    public TapVietFragment getTapVietFragment() {
        return tapVietFragment;
    }

    public void setTapVietFragment(TapVietFragment tapVietFragment) {
        this.tapVietFragment = tapVietFragment;
    }

    private void upTouch() {
        undoInk.add(currentLength);
        path.lineTo(mX, mY);
        pathArrayList.add(path);
        path = new Path();
        RequestTask requestTask = new RequestTask();
        requestTask.setTapVietFragment(tapVietFragment);
        String json = jsonCreat();
        if (json != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                requestTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
            } else {
                requestTask.execute(json);
            }

        }
    }

    public String jsonCreat() {
        StringWriter stringWriter = new StringWriter();
        JsonWriter jsonWriter = new JsonWriter(stringWriter);

        try {
            jsonWriter.beginObject(); // {
            jsonWriter.name("api_level").value("537.36");
            jsonWriter.name("app_version").value(0.4);
            jsonWriter.name("input_type").value(0);
            jsonWriter.name("device").value("5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36");
            jsonWriter.name("options").value("enable_pre_space");
            jsonWriter.name("requests");
            jsonWriter.beginArray(); // [
            jsonWriter.beginObject(); // {
            jsonWriter.name("writing_guide");
            jsonWriter.beginObject(); // {
            jsonWriter.name("writing_area_width").value(425);
            jsonWriter.name("writing_area_height").value(194);
            jsonWriter.endObject(); // }
            jsonWriter.name("pre_context").value("");
            jsonWriter.name("max_num_results").value(10);
            jsonWriter.name("max_completions").value(0);
            jsonWriter.name("ink");
            jsonWriter.beginArray(); // [
            jsonWriter.beginArray(); // [
            jsonWriter.beginArray(); // [
            for (int i = 0; i < currentLineSig1.size(); i++) {
                jsonWriter.value(currentLineSig1.get(i));
            }
            jsonWriter.endArray(); // ]
            jsonWriter.beginArray(); // [
            for (int i = 0; i < currentLineSig2.size(); i++) {
                jsonWriter.value(currentLineSig2.get(i));
            }
            jsonWriter.endArray(); // ]
            jsonWriter.beginArray(); // [
            for (int i = 0; i < currentLineSig3.size(); i++) {
                jsonWriter.value(currentLineSig3.get(i));
            }
            jsonWriter.endArray(); // ]
            jsonWriter.endArray(); // ]
            jsonWriter.endArray(); // ]
            jsonWriter.endObject(); // }
            jsonWriter.endArray(); // ]
            jsonWriter.endObject(); // }

            jsonWriter.flush();
            jsonWriter.close();
            String json = stringWriter.toString();
            return json;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void undo() {
        if (currentLineSig1.size() > 0 && undoInk.size() > 0) {
            removeInk(undoInk.get(undoInk.size() - 1));
            undoInk.remove(undoInk.size() - 1);
        } else {

        }
        String json = jsonCreat();
        RequestTask task = new RequestTask();
        task.setTapVietFragment(tapVietFragment);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } else {
            task.execute(json);
        }

        if (pathArrayList.size() > 0) {
            undoPath.add(pathArrayList.remove(pathArrayList.size() - 1));
            invalidate();
        }


    }

    public void removeInk(int number) {
        for (int i = 0; i < number; i++) {
            currentLineSig1.remove(currentLineSig1.size() - 1);
            currentLineSig2.remove(currentLineSig2.size() - 1);
            currentLineSig3.remove(currentLineSig3.size() - 1);

        }
    }
}
