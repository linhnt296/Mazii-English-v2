package eup.mobi.jedictionary.residemenuitems;

/**
 * Created by nguye on 11/9/2015.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.MergeObj;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.furiganaview.FuriganaView;
import eup.mobi.jedictionary.google.AdNativeExpress;
import eup.mobi.jedictionary.google.NetWork;
import eup.mobi.jedictionary.module.DynamicView;
import eup.mobi.jedictionary.module.KanjiExampleObj;
import eup.mobi.jedictionary.module.Language;
import eup.mobi.jedictionary.module.MergeKanjiAndHiragana;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.residemenuactivity.PlusActitity;
import eup.mobi.jedictionary.svgwriter.SVGCanvasView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class FragTab2 extends Fragment {
    private View parentView;
    private String mKanji;
    private String mMean, mKun, mOn, mStroke_count, mLevel, mCompDetail, mDetail, mExamples, detail_less, detail_more;
    private int mFavorite, wID, count = 0;
    String jW, jP, jM;
    String txt_comDetail, txt_Temp, jCc, jCh, svg_kanji;
    SVGCanvasView svgView;
    TextView tvKanji, tvMean, tvKun, tvOn, tvStrokeCount, tvLevel, tvComDetail, tvDetail, tvDetailMore;
    CardView cardView1, cardView2, cardView3, cardExam;
    ArrayList<KanjiExampleObj> arrExample = new ArrayList<>();
    Button bt_replay, bt_add_word;
    ExpandableRelativeLayout mExpandLayout;
    Activity mContext;
    ArrayList<Jaen> arrExam = new ArrayList<>();
    private LinearLayout layout_exam;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.frag_tab2, container, false);
        mContext = this.getActivity();
        cardView1 = (CardView) parentView.findViewById(R.id.frag2_card1);
        cardView2 = (CardView) parentView.findViewById(R.id.frag2_card2);
        cardView3 = (CardView) parentView.findViewById(R.id.frag2_card3);
        tvKanji = (TextView) parentView.findViewById(R.id.frag2_kanji);
        tvMean = (TextView) parentView.findViewById(R.id.frag2_mean);
        tvKun = (TextView) parentView.findViewById(R.id.frag2_kun);
        tvOn = (TextView) parentView.findViewById(R.id.frag2_on);
        tvStrokeCount = (TextView) parentView.findViewById(R.id.frag2_strokeCount);
        tvLevel = (TextView) parentView.findViewById(R.id.frag2_level);
        tvComDetail = (TextView) parentView.findViewById(R.id.frag2_comDetail);
        tvDetail = (TextView) parentView.findViewById(R.id.frag2_detail);
        tvDetailMore = (TextView) parentView.findViewById(R.id.frag2_detail_more);
        mExpandLayout = (ExpandableRelativeLayout) parentView.findViewById(R.id.expandableLayout);
        bt_replay = (Button) parentView.findViewById(R.id.button_replay);
        bt_add_word = (Button) parentView.findViewById(R.id.bt_add_word);
        cardExam = (CardView) parentView.findViewById(R.id.frag1_card_exam);
        layout_exam = (LinearLayout) parentView.findViewById(R.id.layout_exam);


        // Expand layout
        final ImageView mExpand_more = (ImageView) parentView.findViewById(R.id.expand_more);
        final ImageView mExpand_less = (ImageView) parentView.findViewById(R.id.expand_less);

        Bundle bundle = getArguments();
        mKanji = bundle.getString("mKanji");
        mMean = bundle.getString("mMean");
        mKun = bundle.getString("mKun");
        svg_kanji = readSvg(mKanji.charAt(0));
        mOn = bundle.getString("mOn");
        mStroke_count = bundle.getString("mStroke_count");
        mLevel = bundle.getString("mLevel");
        mCompDetail = bundle.getString("mCompDetail");
        mDetail = bundle.getString("mDetail");
        int m = 0;
        if (!mDetail.equals("")) {
            for (int k = 0; k < mDetail.length(); k++) {
                if (mDetail.charAt(k) == '#' && mDetail.charAt(k + 1) == '#') {
                    count++;
                    if (count == 2) {
                        m = k;
                    }
                }
            }
            if (count > 2) {
                detail_less = mDetail.substring(0, m);
                detail_more = mDetail.substring(m + 2, mDetail.length() - 1);
            } else if (count <= 2) {
                detail_less = mDetail;
//                rl_expand.setVisibility(View.GONE);
                mExpand_more.setVisibility(View.GONE);
            }
            if (detail_less != null)
                detail_less = detail_less.replace("##", "\n");
            if (detail_more != null)
                detail_more = detail_more.replace("##", "\n");
        } else {
//            rl_expand.setVisibility(View.GONE);
            mExpand_more.setVisibility(View.GONE);
        }
        if (bundle.getString("mExamples") != "") {
            mExamples = bundle.getString("mExamples");
        } else mExamples = "";
        mFavorite = bundle.getInt("mFavorite");
        wID = bundle.getInt("wID");
        if (mCompDetail != "") {
            new JsoncomDetailAsyntask().execute(mCompDetail);
        }

        Boolean isJSONExample = false;
        for (int j = 0; j < mExamples.length(); j++) {
            if (mExamples.charAt(j) == '{') {
                isJSONExample = true;
            }
        }
        if (isJSONExample) {
            new JsonExampleAsyntask().execute(mExamples);
        }
        new loadExamSynctask().execute(mKanji);

        bt_add_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PlusActitity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString("word", mKanji);
                bundle1.putString("mean", mDetail.replace("##", "\n"));
                bundle1.putString("phonetic", mKun + " - " + mOn);
                bundle1.putString("type", "kanji");
                bundle1.putInt("id", wID);
                bundle1.putInt("favorite", mFavorite);

                intent.putExtra("PlusActivity", bundle1);
                //Mở Activity ResultActivity
                MenuActivity.startMyActivity(intent, getActivity());
            }
        });


        mExpand_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpand_more.setVisibility(View.GONE);
                mExpand_less.setVisibility(View.VISIBLE);
                // todo expand more
                mExpandLayout.toggle();

            }
        });
        mExpand_less.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpand_less.setVisibility(View.GONE);
                mExpand_more.setVisibility(View.VISIBLE);
                //todo expand less
                mExpandLayout.move(0);
            }
        });

        bt_replay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(getContext(), "Replay", Toast.LENGTH_SHORT).show();
                svgView.rePaint();
            }
        });

        tvKanji.setText(mKanji);
        if (mMean != null) {
            tvMean.setText(mMean);
        }
        tvKun.setText(mKun);
        tvOn.setText(mOn);
        tvStrokeCount.setText(mStroke_count);
        tvLevel.setText(mLevel);
        tvDetail.setText(detail_less);
        tvDetailMore.setText(detail_more);
        cardView1.setVisibility(View.VISIBLE);
        bt_add_word.setVisibility(View.VISIBLE);

        if (!svg_kanji.equals("") && svg_kanji != null) {
            svgView = (SVGCanvasView) parentView.findViewById(R.id.svg_kanji);
            svgView.init(svg_kanji);
            cardView2.setVisibility(View.VISIBLE);
        } else {
            cardView2.setVisibility(View.GONE);
        }

        // native ad
        if (NetWork.isNetWork(getActivity())) {
            boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
            float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
            String banner = MenuActivity.sharedPreferences.getString("idNative", getString(R.string.ad_unit_id_medium));
            try {
                AdNativeExpress.smartNative(parentView, banner, probBanner, isPremium, true);
            } catch (Exception e) {

            }
        }

        return parentView;
    }

    //     JSON asyntask
    private class JsonExampleAsyntask extends AsyncTask<String, JSONArray, Void> {

        @Override
        protected Void doInBackground(String... params) {
            JSONArray jExampleArr = null;
            try {
                jExampleArr = new JSONArray(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            JSONObject subJMean = jb.getJSONObject("response");
            publishProgress(jExampleArr);
            return null;
        }

        @Override
        protected void onProgressUpdate(JSONArray... values) {
            super.onProgressUpdate(values);
            JSONArray jExampleArr = values[0];
            try {
                for (int i = 0; i < jExampleArr.length(); i++) {
                    JSONObject jobj = jExampleArr.getJSONObject(i);
                    if (jobj.has("w"))
                        jW = jobj.getString("w");
                    if (jobj.has("p"))
                        jP = jobj.getString("p");
                    if (jobj.has("m")) {
                        jM = jobj.getString("m");
                        jM = jM.substring(0, 1).toUpperCase() + jM.substring(1);
                    }
                    KanjiExampleObj obj = new KanjiExampleObj(jW, jP, jM);
                    arrExample.add(obj);
                }

            } catch (JSONException e1) {
                e1.printStackTrace();
            }

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (arrExample.size() > 0) {
                try {
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            DynamicView.viduRecycler(mContext, arrExample);
                            cardView3.setVisibility(View.VISIBLE);
                        }
                    });
                } catch (Exception e) {

                }
            } else {
                cardView3.setVisibility(View.GONE);
            }
        }
    }

    //     JSON asyntask
    private class JsoncomDetailAsyntask extends AsyncTask<String, JSONArray, String> {

        @Override
        protected String doInBackground(String... params) {
            JSONArray jcomDetailArr = null;
            try {
                jcomDetailArr = new JSONArray(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (jcomDetailArr != null) {
                try {
                    txt_comDetail = "";
                    for (int i = 0; i < jcomDetailArr.length(); i++) {
                        JSONObject jobj = jcomDetailArr.getJSONObject(i);

                        txt_Temp = "";
                        if (jobj.has("w"))
                            jCc = jobj.getString("w");
                        if (jobj.has("h"))
                            jCh = jobj.getString("h");
                        txt_Temp += jCc + "(" + jCh + ")";
                        txt_comDetail += txt_Temp + "  ";
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            } else txt_comDetail = "";

            return txt_comDetail;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tvComDetail.setText(s);
        }
    }

    private class loadExamSynctask extends AsyncTask<String, Void, ArrayList<Jaen>> {
        @Override
        protected ArrayList<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(mContext);
            if (params[0] != null && !params[0].equals(""))
                arrExam = db.qExampleKanji(params[0]);
            return arrExam;
        }

        @Override
        protected void onPostExecute(ArrayList<Jaen> examples) {
            super.onPostExecute(examples);
            if (examples != null && examples.size() > 0) {

                for (Jaen ex : examples) {

                    View v = LayoutInflater.from(mContext).inflate(R.layout.item_example_kanji, layout_exam, false);
                    TextView tvMean = (TextView) v.findViewById(R.id.tvMean);
                    FuriganaView furi = (FuriganaView) v.findViewById(R.id.furi);

                    if (ex.getmMean() != null) {
                        try {
                            JSONArray array = new JSONArray(ex.getmMean());
                            JSONObject object = array.getJSONObject(0);
                            tvMean.setText(object.optString("mean"));
                        } catch (JSONException e) {
                        }
                    } else {
                        tvMean.setVisibility(View.INVISIBLE);
                    }

                    //todo set fv_tab4
                    TextPaint tp = new TextPaint();
                    tp.setColor(Color.parseColor("#808080"));
                    tp.setTextSize(mContext.getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                    tp.setAntiAlias(true);
                    tp.setDither(true);
                    tp.setStyle(Paint.Style.FILL);
                    tp.setStrokeJoin(Paint.Join.ROUND);

                    String text = mergeKH(ex.getmWord(), ex.getmPhonetic());
                    if (text != null && !text.equals("")) {
                        furi.text_set(tp, text, -1, -1);
                        furi.setPadding(0, 28, 0, 16);
                    }

                    layout_exam.addView(v);
                }
                cardExam.setVisibility(View.VISIBLE);
            }
        }
    }

    private static String mergeKH(String kanji, String hiragana) {
        String myruby = null;
        if (hiragana == null) {
            myruby = kanji;
        } else if (hiragana.equals("xxx。") || hiragana.equals("xxx") || Language.isVietnamese(hiragana)) {
            myruby = kanji;
        } else {
            ArrayList<MergeObj> mergeObjs = MergeKanjiAndHiragana.mergeKanjiAndHiragana(kanji, hiragana);
            if (mergeObjs != null) {

                for (int i = 0; i < mergeObjs.size(); i++) {
                    MergeObj myMg = mergeObjs.get(i);
                    String ruby = "{" + myMg.getKanji() + ";" + myMg.getHiragana() + "}";
                    if (myruby == null) {
                        myruby = ruby;
                    } else {
                        myruby += ruby;
                    }
                }
            } else {
                myruby = "";
            }
        }
        return myruby;
    }

    private String readSvg(char file_name) {
        String hex = Integer.toHexString(file_name);
        if (hex.length() < 5) {
            for (int i = 0; i < (5 - hex.length()); i++)
                hex = "0" + hex;
        }
        StringBuilder result = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(mContext.getAssets().open("stroke/" + hex + ".svg"), "UTF-8"));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                result.append(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return result.toString();
    }
}

