package eup.mobi.jedictionary.residemenuitems;

/**
 * Created by nguye on 11/9/2015.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Example;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.database.VerbObj;
import eup.mobi.jedictionary.furiganaview.FuriganaView;
import eup.mobi.jedictionary.google.AdNativeExpress;
import eup.mobi.jedictionary.google.NetWork;
import eup.mobi.jedictionary.module.DynamicView;
import eup.mobi.jedictionary.module.JMeanObj;
import eup.mobi.jedictionary.module.Language;
import eup.mobi.jedictionary.module.MyHashMap;
import eup.mobi.jedictionary.module.VerbTable;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.residemenuactivity.PlusActitity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;


public class FragTab1 extends Fragment {
    private View parentView;
    TextToSpeech t1;
    private String vMean, entryMean, requestNameUrl, requestFileUrl;
    private String vTitle, vPhonetic, verbPhonetic;
    private Button bt_txt_to_speech, bt_add_word;
    private int mFavorite, wID;
    String pos, synonym, url, t_query;
    static Activity mContext;
    RelativeLayout rl_tab1;
    CardView cardVerb, cardSynset, cardExam;
    ArrayList<JMeanObj> arrMean = new ArrayList<JMeanObj>();
    ArrayList<String[]> arrEntry = null;
    ArrayList<VerbObj> verbArrList = null;
    TextView tv_Word, tv_Phonetic;
    MediaPlayer mp = new MediaPlayer();
    static String text_to_speech = "", result = "";
    private ScrollView scrollView;
    private int oldPos = 0;
    LinearLayout layout_exam;
    private ArrayList<Example> arrExam;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.frag_tab1, container, false);
        mContext = this.getActivity();
        rl_tab1 = (RelativeLayout) parentView.findViewById(R.id.rl_tab1);
        cardVerb = (CardView) parentView.findViewById(R.id.frag1_card2);
        cardSynset = (CardView) parentView.findViewById(R.id.frag1_card3);
        bt_txt_to_speech = (Button) parentView.findViewById(R.id.bt_txt_to_speech);
        bt_add_word = (Button) parentView.findViewById(R.id.bt_add_word);
        tv_Word = (TextView) parentView.findViewById(R.id.frag1_word);
        tv_Phonetic = (TextView) parentView.findViewById(R.id.frag1_phonetic);
        scrollView = (ScrollView) parentView.findViewById(R.id.scrollView1);
        layout_exam = (LinearLayout) parentView.findViewById(R.id.layout_exam);
        cardExam = (CardView) parentView.findViewById(R.id.frag1_card_exam);

        //lấy intent gọi Activity này
        final Bundle bundle = getArguments();

        //Có Bundle rồi thì lấy các thông số dựa vào soa, sob
        vMean = bundle.getString("vMean");
        vTitle = bundle.getString("vTitle");
        vPhonetic = bundle.getString("vPhonetic");
        verbPhonetic = bundle.getString("verbPhonetic");
        mFavorite = bundle.getInt("mFavorite");
        wID = bundle.getInt("wID");

        //tiến hành xử lý

        // todo word , phonetic
        if (Language.isVietnamese(vTitle)) {
            vTitle = vTitle.substring(0, 1).toUpperCase() + vTitle.substring(1);
        }
        if (!Language.isJapanese(vTitle)) {
            bt_txt_to_speech.setVisibility(View.GONE);
        }
        //TXT to speech
        if (NetWork.isNetWork(getActivity()))
            onTTSClick();
        else textToSpeech();

        // hide floating action button
        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY(); //for verticalScrollView
                //DO SOMETHING WITH THE SCROLL COORDINATES
                if (scrollY - oldPos <= -20) {
                    // show floating action button
                    if (!TraCuuFragment.isShowFab) {
                        TraCuuFragment.fab_menu.showMenu(true);
                        TraCuuFragment.isShowFab = true;
                    }
                } else if (scrollY - oldPos >= 20) {
                    // hide
                    TraCuuFragment.fab_menu.hideMenu(true);
                    TraCuuFragment.isShowFab = false;
                }
                oldPos = scrollY;
            }
        });

        // TODO: 1/11/2016
        bt_add_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PlusActitity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putString("word", vTitle);
                bundle1.putString("mean", entryMean);
                bundle1.putString("type", "jaen");
                bundle1.putString("phonetic", vPhonetic);
                bundle1.putInt("id", wID);
                bundle1.putInt("favorite", mFavorite);

                intent.putExtra("PlusActivity", bundle1);
                //Mở Activity ResultActivity
                MenuActivity.startMyActivity(intent, getActivity());
            }
        });


        new JsonAsyntask().execute(vMean);
        try {
            new loadExamSynctask().execute(vTitle, verbPhonetic);
        } catch (Exception e) {

        }
        if (Language.isJapanese(vTitle)) {
            if (NetWork.isNetWork(mContext)) {
                String google_translate_url = "https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&dt=bd&dj=1&dt=ex&dt=ld&dt=md&dt=qca&dt=rw&dt=rm&dt=ss&dt=at";
                String from = "ja";
                String to = MenuActivity.sharedPreferences.getString("to_language", Locale.getDefault().getLanguage());
                try {
                    t_query = URLEncoder.encode(vTitle, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                url = google_translate_url + "&sl=" + from + "&tl=" + to + "&q=" + t_query;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new transSyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
                } else {
                    new transSyncTask().execute(url);
                }
            }
        }
        // native ad
        if (NetWork.isNetWork(getActivity())) {
            boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
            float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
            String banner = MenuActivity.sharedPreferences.getString("idNative", getString(R.string.ad_unit_id_medium));
            try {
                AdNativeExpress.smartNative(parentView, banner, probBanner, isPremium, true);
            } catch (Exception e) {

            }
        }
        return parentView;
    }

    private void onTTSClick() {
        requestNameUrl = MenuActivity.sharedPreferences.getString("request_name", "http://dws2.voicetext.jp/tomcat/servlet/vt");
        requestFileUrl = MenuActivity.sharedPreferences.getString("request_file", "http://dws2.voicetext.jp/ASLCLCLVVS/JMEJSYGDCHMSMHSRKPJL/");
        bt_txt_to_speech.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!text_to_speech.equals(vTitle)) {
                    text_to_speech = vTitle;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new postRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, text_to_speech);
                    } else {
                        new postRequest().execute(text_to_speech);
                    }
                } else {
                    playAudio();
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (t1 != null) {
            if (t1.isSpeaking()) {
                t1.stop();
            }
            t1.shutdown();
            t1 = null;
        }
    }

    // JSON asyntask
    private class JsonAsyntask extends AsyncTask<String, JSONArray, Void> {

        @Override
        protected Void doInBackground(String... params) {
            vMean = params[0];
            JSONArray jMeanArr = null;
            try {
                jMeanArr = new JSONArray(vMean);
            } catch (JSONException e) {
                e.printStackTrace();
            }
//            JSONObject subJMean = jb.getJSONObject("response");
            publishProgress(jMeanArr);
            return null;
        }

        @Override
        protected void onProgressUpdate(JSONArray... values) {
            super.onProgressUpdate(values);
            JSONArray jMeanArr = values[0];
//            try {
////kiểm tra xem có tồn tại thuộc tính id hay không
//                if (jMeanObj.has("word"))
//                    tv_jWord.setText(jMeanObj.getString("word"));
//                if (jMeanObj.has("phonetic"))
//                    tv_jPhonetic.setText(jMeanObj.getString("phonetic"));
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
            try {
//                JSONArray jMeanArr = jMeanObj.getJSONArray("means");
                String[] type = new String[0];
                boolean isVerb = false;

                for (int i = 0; i < jMeanArr.length(); i++) {
                    JSONObject jobj = jMeanArr.getJSONObject(i);
                    String jKind = "", jMean = "", jExample = "";
                    ArrayList<Example> arrExample = new ArrayList<Example>();
                    boolean isMoreOnekind = false;
                    if (jobj.has("kind")) {
                        if (!isVerb) {
                            type = jobj.getString("kind").split(", ");
                            try {
                                for (int j = 0; j < type.length; j++) {
                                    String[] rule = VerbTable.tableConjugationConvert(type[j]);
                                    if (rule == null) {
                                        isVerb = false;
                                    } else {
                                        isVerb = true;
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }
                        jKind = jobj.getString("kind");
                        for (int j = 0; j < jKind.length(); j++) {
                            if (jKind.charAt(j) == ',') {
                                isMoreOnekind = true;
                            }
                        }
                        if (isMoreOnekind) {
                            String[] newJkind = jKind.split(", ");
                            jKind = "";
                            for (int j = 0; j < newJkind.length; j++) {
                                newJkind[j] = MyHashMap.getResult(newJkind[j]);
                                if (j != newJkind.length - 1) {
                                    jKind += newJkind[j] + ", ";
                                } else jKind += newJkind[j];
                            }
                        } else jKind = MyHashMap.getResult(jKind);
                        //VerbTable
                    }

                    if (jobj.has("mean")) {
                        if (i == 0) {
                            entryMean = jobj.getString("mean");
                        }
                        jMean = jobj.getString("mean");
                    } else jMean = "";

                    JMeanObj obj = new JMeanObj(jKind, jMean, arrExample);
                    arrMean.add(obj);
                }

                try {
                    for (int j = 0; j < type.length; j++) {
                        if (verbArrList == null) {
                            verbArrList = VerbTable.getConjugationTableOfVerb(vTitle, verbPhonetic, type[j], getActivity());
                            break;
                        }
                    }
                } catch (Exception e) {
                }

            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (!vTitle.equals("")) {
                tv_Word.setText(vTitle);
                if (vPhonetic != null) {
                    if (!vPhonetic.equals("「null」") && !vPhonetic.equals("「」")) {
//                        vPhonetic = vPhonetic.replace(" ", "/");
                        tv_Phonetic.setText(vPhonetic);
                    } else {
                        tv_Phonetic.setVisibility(View.GONE);
                    }
                } else {
                    tv_Phonetic.setVisibility(View.GONE);
                }
            } else {
                if (vPhonetic != null) {
                    tv_Word.setText(verbPhonetic);
                }
            }
            if (parentView != null && arrMean != null && arrMean.size() > 0) {
                DynamicView.mLayoutJMean(parentView, arrMean);
            }
            rl_tab1.setVisibility(View.VISIBLE);

            if (verbArrList != null && verbArrList.size() > 0) {
                DynamicView.verbTable(getActivity(), verbArrList);
                cardVerb.setVisibility(View.VISIBLE);
            }

        }
    }


    public String getJSON(String url, int timeout) {
        HttpURLConnection c = null;
        try {
            URL u = new URL(url);
            c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
//            c.setRequestProperty("Content-length", "0");
            c.setRequestProperty("charset", "UTF-8");
            c.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36");

            c.setUseCaches(false);
            c.setAllowUserInteraction(false);
            c.setConnectTimeout(timeout);
            c.setReadTimeout(timeout);
            c.connect();
            int status = c.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    return sb.toString();
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.disconnect();
                } catch (Exception ex) {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return null;
    }

    private JSONObject getMyJSON(String data) throws JSONException {
        if (data == null) {
            return null;
        }
        JSONObject object = new JSONObject(data);
        return object;
    }

    private class transSyncTask extends AsyncTask<String, JSONObject, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            JSONObject jsonObject = null;
            try {
                String data = getJSON(params[0], 1500);
                jsonObject = getMyJSON(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            publishProgress(jsonObject);
            return null;
        }

        @Override
        protected void onProgressUpdate(JSONObject... values) {
            super.onProgressUpdate(values);
            JSONObject jsonObject = values[0];
            if (jsonObject == null) {
                return;
            }
//            if (jsonObject.has("sentences")) {
//                try {
//                    JSONArray sentences = jsonObject.getJSONArray("sentences");
//                    JSONObject jTrans = sentences.getJSONObject(0);
//                    JSONObject jTranslit = sentences.getJSONObject(1);
//                    // read trans
//                    if (jTrans.has("trans")) {
//                        trans = jTrans.getString("trans");
//                    }
//                    if (jTrans.has("orig")) {
//                        orig = jTrans.getString("orig");
//                    }
//                    if (jTranslit.has("src_translit")) {
//                        translit = jTranslit.getString("src_translit");
//                    }
//                    Log.d("Translate", "Trans: "+trans + " ,Translit: " + translit + " ,Orig: " + orig);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
            if (jsonObject.has("synsets")) {
                try {
                    JSONArray jSynsets = jsonObject.getJSONArray("synsets");
                    for (int i = 0; i < jSynsets.length(); i++) {
                        JSONObject subSyn = jSynsets.getJSONObject(i);
                        arrEntry = new ArrayList<String[]>();
                        if (subSyn.has("pos")) {
                            pos = subSyn.getString("pos");
                        }
                        if (subSyn.has("entry")) {
                            JSONArray jEntry = subSyn.getJSONArray("entry");
                            for (int j = 0; j < jEntry.length(); j++) {
                                JSONObject jSynonym = jEntry.getJSONObject(j);
                                if (jSynonym.has("synonym")) {
                                    synonym = jSynonym.getString("synonym");
                                    synonym = synonym.replace("[\"", "");
                                    synonym = synonym.replace("\"]", "");
                                    boolean isMore = false;
                                    for (int m = 0; m < synonym.length(); m++) {
                                        if (synonym.charAt(m) == ',') {
                                            isMore = true;
                                            break;
                                        }
                                    }
                                    if (isMore) {
                                        String[] arrSynon = synonym.split("\",\"");
                                        arrEntry.add(arrSynon);
                                    } else {
                                        String[] arrSynon = new String[]{synonym};
                                        arrEntry.add(arrSynon);
                                    }
                                }

                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            super.onPostExecute(strings);
            //todo get Veiw Trans
            if (parentView != null && arrEntry != null) {
                DynamicView.synsetLayout(parentView, vTitle, pos, arrEntry);
                cardSynset.setVisibility(View.VISIBLE);
            }
        }
    }

    public static void onSynonymClick(String synonym) {
        Toast.makeText(mContext, synonym, Toast.LENGTH_SHORT).show();
        TraCuuFragment.isSubmit = true;
        TraCuuFragment.searchView.setQuery(synonym, true);
    }

    private class postRequest extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            boolean isSuccess = false;

            HttpURLConnection connection = null;
            try {
                URL website = new URL(requestNameUrl);
                connection = (HttpURLConnection) website.openConnection();

                String query = "text=" + URLEncoder.encode(params[0], "UTF-8") + "&talkID=308&volume=100&speed=100&pitch=100&dict=3";
                String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/53.2.123 Chrome/47.2.2526.123 Safari/537.36";

                connection.setRequestMethod("POST");

                connection.setRequestProperty("Content-length", String.valueOf(query.length()));
                connection.setRequestProperty("User-Agent", USER_AGENT);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                connection.setRequestProperty("Connection", "keep-alive");
                connection.setRequestProperty("Accept", "text/plain, */*; q=0.01");
                connection.setRequestProperty("Accept-Encoding", "gzip, deflate");
                connection.setRequestProperty("Accept-Language", "vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2");
                connection.setRequestProperty("Host", "dws2.voicetext.jp");
                connection.setRequestProperty("Origin", "http://dws2.voicetext.jp");
                connection.setRequestProperty("Referer", "http://dws2.voicetext.jp/tomcat/demonstration/top.html");
                connection.setRequestProperty("X-Requested-With", "XMLHttpRequest");
                connection.setDoInput(true);
                connection.setDoOutput(true);

                DataOutputStream output = new DataOutputStream(connection.getOutputStream());

                output.writeBytes(query);

                output.close();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                connection.getInputStream()));

                StringBuilder response = new StringBuilder();
                String inputLine;

                while ((inputLine = in.readLine()) != null)
                    response.append(inputLine);
                in.close();

                result = requestFileUrl
                        .concat(response.toString().substring(response.toString().indexOf('=') + 1));
                isSuccess = true;

            } catch (IOException e) {
                return null;
            } finally {
                try {
                    if (connection != null)
                        connection.disconnect();
                } catch (Exception e) {

                }
            }
            return isSuccess;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            try {
                if (aBoolean) {
                    if (result != null && !result.equals(""))
                        playAudio();
                } else {
                    textToSpeech();
                }
            } catch (Exception e) {
                textToSpeech();
            }
        }

    }

    private void playAudio() {
        try {
            if (mp.isPlaying()) {
                mp.stop();
                mp.reset();
                mp.setDataSource(result);
                mp.prepare();
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.start();
            } else {
                mp.reset();
                mp.setDataSource(result);
                mp.prepare();
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.start();
            }
        } catch (IOException e) {
            textToSpeech();
        }
    }

    //TXT to speech
    private void textToSpeech() {
        try {
            t1 = new TextToSpeech(mContext, new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.ERROR) {
                        if (Language.isJapanese(vTitle)) {
                            if (t1 != null)
                                t1.setLanguage(Locale.JAPAN);
                        }
                        bt_txt_to_speech.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (t1 != null)
                                    // Toast.makeText(getContext(), vTitle, Toast.LENGTH_SHORT).show();
                                    t1.speak(vTitle, TextToSpeech.QUEUE_FLUSH, null);
                            }
                        });
                    } else {
                        Toast.makeText(mContext, getString(R.string.text_to_speech_notsuport), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            Toast.makeText(mContext, getString(R.string.text_to_speech_notsuport), Toast.LENGTH_SHORT).show();
        }
    }

    private class loadExamSynctask extends AsyncTask<String, Void, ArrayList<Example>> {
        @Override
        protected ArrayList<Example> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getActivity());
            if (params[0] != null && !params[0].equals(""))
                arrExam = db.qExampleWord(params[0], params[1]);
            return arrExam;
        }

        @Override
        protected void onPostExecute(ArrayList<Example> examples) {
            super.onPostExecute(examples);
            if (examples != null && examples.size() > 0) {

                for (Example ex : examples) {
                    if (ex.getmTrans() == null || ex.getmTrans().equals("")) continue;
                    View v = LayoutInflater.from(mContext).inflate(R.layout.tracuu_tab4_items, layout_exam, false);
                    TextView tvTitle = (TextView) v.findViewById(R.id.tv_tab4_struct_vi);
                    FuriganaView furi = (FuriganaView) v.findViewById(R.id.fv_tab4);
                    tvTitle.setText(ex.getmMean());
                    //todo set fv_tab4
                    TextPaint tp = new TextPaint();
                    tp.setColor(Color.parseColor("#D32F2F"));
                    tp.setTextSize(getContext().getResources().getDimensionPixelSize(R.dimen.textSizeFrag_lv3));
                    tp.setAntiAlias(true);
                    tp.setDither(true);
                    tp.setStyle(Paint.Style.FILL);
                    tp.setStrokeJoin(Paint.Join.ROUND);

                    furi.text_set(tp, ex.getmTrans(), -1, -1);
                    furi.setPadding(0, 28, 0, 16);

                    layout_exam.addView(v);
                }
                cardExam.setVisibility(View.VISIBLE);
            }
        }
    }
}