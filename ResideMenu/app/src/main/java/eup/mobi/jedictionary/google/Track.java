package eup.mobi.jedictionary.google;

import android.app.Activity;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class Track {
	private static Tracker t;

	public static void createTracker(Activity ac) {
		t = ((AnalyticsApp) ac.getApplication())
				.getTracker(AnalyticsApp.TrackerName.APP_TRACKER);
	}

	public static void sendTrackerScreen(String screenName) {
//		 Set screen name.
		 t.setScreenName(screenName);
		
		 // Send a screen view.
		 t.send(new HitBuilders.ScreenViewBuilder().build());
	}

	public static void sendTrackerAction(String Category, String Action) {
		t.send(new HitBuilders.EventBuilder()
				.setCategory(Category)
				.setAction(Action)
				.build());
	}
}
