package eup.mobi.jedictionary.tapviet;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.horizontallistview.HorizontalListView;
import eup.mobi.jedictionary.residemenuitems.TraCuuFragment;

import java.util.ArrayList;

/**
 * Created by thanh on 10/16/2015.
 */
public class HandWriteDialog extends Dialog {
    public TraCuuFragment getTraCuuFragment() {
        return traCuuFragment;
    }

    public void setTraCuuFragment(TraCuuFragment traCuuFragment) {
        this.traCuuFragment = traCuuFragment;
    }

    public Context context;
    public Dialog dialog;
    public Button btnUndo, btnClear;
    public CanvasViewDialog canvasView;
    public HorizontalListView gridView;
    public ResultGridAdapter resultAdapter;
    public ArrayList<String> resultList;
    public TraCuuFragment traCuuFragment;
    private CanvasViewDialog canvasViewDialog;
    public static boolean isShow;

    public HandWriteDialog(Context context) {
        super(context);
        this.context = context;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.handwrite_dialog);
        resultList = new ArrayList<String>();
        resultAdapter = new ResultGridAdapter(resultList, getContext());
        canvasViewDialog = (CanvasViewDialog)findViewById(R.id.dialog_canvas);
        gridView = (HorizontalListView) findViewById(R.id.grid_dialog);
        gridView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_MOVE) {
                    return true;
                }
                return false;
            }
        });
        gridView.setAdapter(resultAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = traCuuFragment.searchView.getQuery().toString();
                text += resultList.get(position);
                resultList.clear();
                resultAdapter.notifyDataSetChanged();
                canvasViewDialog.clearCanvas();
                traCuuFragment.searchView.setQuery(text, false);
            }
        });
        btnUndo = (Button) findViewById(R.id.button_undo_dialog);
        btnClear = (Button) findViewById(R.id.button_clear_dialog);
        canvasView = (CanvasViewDialog) findViewById(R.id.dialog_canvas);
        canvasView.setHandWriteDialog(this);
        btnUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvasView.undo();
            }
        });
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvasView.clearCanvas();
            }
        });
    }

    @Override
    public void setOnDismissListener(OnDismissListener listener) {
        super.setOnDismissListener(listener);
    }
}
