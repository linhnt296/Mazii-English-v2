package eup.mobi.jedictionary.residemenuactivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.InterstitialAd;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.fast.search.FastSearchService;
import eup.mobi.jedictionary.google.AdsmodUtil;
import eup.mobi.jedictionary.google.NetWork;
import eup.mobi.jedictionary.myadapter.MyAdapterTuJa;

import java.util.ArrayList;

public class TuLoaiAcitivity extends Activity {
    ProgressBar progressBar;
    private String sqlTL, title;
    ArrayList<Jaen> javi;
    ListView lv_tl;
    MyAdapterTuJa adapter_ja;
    String vMean, vTitle, vPhonetic;
    int mFavorite, wID, mSeq;
    TextView tv_null, tv_title;
    Button bt_back;

    private Button btn_prev2;
    private Button btn_next2;
    private int pageCount;

    private int increment = 0;

    public int TOTAL_LIST_ITEMS = 0;
    public int NUM_ITEMS_PAGE = 100;
    private int noOfBtns;
    private Button[] btns;
    LinearLayout ll;
    LinearLayout.LayoutParams lp;
    Activity activity;
    int count = 0;
    boolean isPremium;
    InterstitialAd mInterstitialAd;
    AdsmodUtil adsmodUtil = new AdsmodUtil();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        //set fullscreen
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tu_loai_acitivity);
        activity = this;
        progressBar = (ProgressBar) findViewById(R.id.pbHeaderProgress);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.holo_blue), PorterDuff.Mode.SRC_IN);
        tv_null = (TextView) findViewById(R.id.tv_null);
        tv_title = (TextView) findViewById(R.id.tv_title);
        bt_back = (Button) findViewById(R.id.button_back);

        lv_tl = (ListView) findViewById(R.id.lv_tl);
        btn_prev2 = (Button) findViewById(R.id.bt_prev2);
        btn_next2 = (Button) findViewById(R.id.bt_next2);
        ll = (LinearLayout) findViewById(R.id.btnLay);
        lp = new LinearLayout.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT);

        //lấy intent gọi Activity này
        Intent callerIntent = getIntent();
        //có intent rồi thì lấy Bundle dựa vào MyPackage
        Bundle packageFromCaller =
                callerIntent.getBundleExtra("ChuyenNganhFragment");
        //Có Bundle rồi thì lấy các thông số dựa vào soa, sob
        sqlTL = packageFromCaller.getString("sqlTL");
        title = packageFromCaller.getString("title");
        tv_title.setText(title);
        bt_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
//        Toast.makeText(getApplication(), sqlTL, Toast.LENGTH_SHORT).show();


        new myAsyntask_Tuloai_ja().execute(sqlTL);
        if (MenuActivity.sharedPreferences != null) {
            isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
            String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
            float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
//            Adsmod.banner(activity, banner, probBanner, isPremium);
        }

        adsmodUtil.createFullAds(activity);
    }

    private class myAsyntask_Tuloai_ja extends AsyncTask<String, Void, ArrayList<Jaen>> {
        @Override
        protected ArrayList<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(TuLoaiAcitivity.this);
            javi = db.getTuLoaiJaen(params[0]);
            return javi;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(ArrayList<Jaen> javis) {
            super.onPostExecute(javis);
            progressBar.setVisibility(View.GONE);

            if (javi == null) {
                tv_null.setVisibility(View.VISIBLE);
                return;
            }
            pagingListView();
//            adapter_ja = new MyAdapterTuJa(TuLoaiAcitivity.this, R.layout.tracuu_tab1_ja_items, javi);
//            lv.setAdapter(adapter_ja);
////                                    tv_mMean.setText("");
////                                    lv.setVisibility(View.VISIBLE);
//            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
////                                            lv.setVisibility(View.GONE);
////                                            String vMean = vija.get(position).getmMean().toString();
////                                            tv_mMean.setText(vMean);
////                                Toast.makeText(getContext(), vMean, Toast.LENGTH_SHORT).show();
//                    Intent myIntent = new Intent(TuLoaiAcitivity.this, TraCuuTuActivity.class);
//                    //Khai báo Bundle
//                    Bundle bundle = new Bundle();
//                    vMean = javi.get(position).getmMean();
//                    vTitle = javi.get(position).getmWord();
//                    vPhonetic = javi.get(position).getmPhonetic();
//                    mFavorite = javi.get(position).getmFavorite();
//                    wID = javi.get(position).getmId();
//
//                    //đưa dữ liệu riêng lẻ vào Bundle
//                    bundle.putString("vMean", vMean);
//                    bundle.putString("vTitle", vTitle);
//                    bundle.putString("vPhonetic", vPhonetic);
//                    bundle.putString("mTable", "javi");
//                    bundle.putInt("mFavorite", mFavorite);
//                    bundle.putInt("wID", wID);
//
//                    //Đưa Bundle vào Intent
//                    myIntent.putExtra("TraCuuFragment", bundle);
//                    //Mở Activity ResultActivity
//                    startActivity(myIntent);
//
//                }
//            });
        }
    }

    private void pagingListView() {
//        data = new ArrayList<String>();
//        if (kanji == null) {
//            return;
//        }
        TOTAL_LIST_ITEMS = javi.size();
        /**
         * this block is for checking the number of pages
         * ====================================================
         */

        int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
        val = val == 0 ? 0 : 1;
        pageCount = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
        /**
         * =====================================================
         */

        /**
         * The ArrayList data contains all the list items
         */
//        for (int i = 0; i < TOTAL_LIST_ITEMS; i++) {
//            data.add("This is Item " + (i + 1));
//        }

        loadList(0);
//        CheckEnable_HT();
//        sp1.setVisibility(View.VISIBLE);
//        sp2.setVisibility(View.VISIBLE);
        lv_tl.setVisibility(View.VISIBLE);
        btn_prev2.setEnabled(false);
        btn_prev2.setVisibility(View.VISIBLE);
        btn_next2.setVisibility(View.VISIBLE);
        ll.setVisibility(View.VISIBLE);
        Btnfooter();
        CheckBtnBackGroud(0);
        btn_next2.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                increment++;
                CheckEnable();
                loadList(increment);
                CheckBtnBackGroud(increment);
            }
        });
        btn_prev2.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                increment--;
                CheckEnable();
                loadList(increment);
                CheckBtnBackGroud(increment);
            }
        });

        CheckEnable();
    }

    private void CheckEnable() {
        if (increment + 1 == pageCount && increment != 0) {
            btn_next2.setEnabled(false);
            btn_prev2.setEnabled(true);
        } else if (increment == 0 && pageCount != 1) {
            btn_prev2.setEnabled(false);
            btn_next2.setEnabled(true);
        } else if (increment == 0 && pageCount == 1) {
            btn_prev2.setEnabled(false);
            btn_next2.setEnabled(false);
        } else {
            btn_prev2.setEnabled(true);
            btn_next2.setEnabled(true);
        }
    }

    private void Btnfooter() {
        int val = TOTAL_LIST_ITEMS % NUM_ITEMS_PAGE;
        val = val == 0 ? 0 : 1;
        noOfBtns = TOTAL_LIST_ITEMS / NUM_ITEMS_PAGE + val;
        btns = new Button[noOfBtns];
        ll.removeAllViews();
        for (int i = 0; i < noOfBtns; i++) {
            btns[i] = new Button(getApplicationContext());
            btns[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
            btns[i].setText("" + (i + 1));

//            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(AbsListView.LayoutParams.WRAP_CONTENT, AbsListView.LayoutParams.WRAP_CONTENT);
            ll.addView(btns[i], lp);

            final int j = i;
            btns[j].setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    loadList(j);
                    CheckBtnBackGroud(j);
                }
            });
        }

    }

    private void CheckBtnBackGroud(int index) {
        increment = index;
        for (int i = 0; i < noOfBtns; i++) {
            if (i == index) {

                btns[index].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns[i].setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
                btns[i].setTextSize(24);
            } else {

                btns[i].setBackgroundColor(getResources().getColor(android.R.color.transparent));
                btns[i].setTextColor(getResources().getColor(android.R.color.black));
                btns[i].setTextSize(12);
            }
        }

    }

    private void loadList(int number) {

        ArrayList<Jaen> sort = new ArrayList<Jaen>();
        int start = number * NUM_ITEMS_PAGE;
        for (int i = start; i < (start) + NUM_ITEMS_PAGE; i++) {
            if (i < javi.size()) {
                sort.add(javi.get(i));
            } else {
                break;
            }
        }
        if (sort != null) {
            adapter_ja = new MyAdapterTuJa(TuLoaiAcitivity.this, R.layout.tracuu_tab1_ja_items, sort);
            lv_tl.setAdapter(adapter_ja);
            setListViewHeightBasedOnChildren(lv_tl);
            lv_tl.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (NetWork.isNetWork(activity)) {
                        count += 1;
                        if (count == 30) {
                            adsmodUtil.showIntervalAds();
                            count = 0;
                        }
                    }

                    int mPosition = position + increment * NUM_ITEMS_PAGE;
                    Intent myIntent = new Intent(TuLoaiAcitivity.this, TraCuuTuActivity.class);
                    //Khai báo Bundle
                    Bundle bundle = new Bundle();
                    vMean = javi.get(mPosition).getmMean();
                    vTitle = javi.get(mPosition).getmWord();

                    vPhonetic = javi.get(mPosition).getmPhonetic();
                    mFavorite = javi.get(mPosition).getmFavorite();
                    wID = javi.get(mPosition).getmId();

                    //đưa dữ liệu riêng lẻ vào Bundle
                    bundle.putString("vMean", vMean);
                    bundle.putString("vTitle", vTitle);
                    bundle.putString("verbPhonetic", vPhonetic);
                    if (vPhonetic != null) {
                            bundle.putString("vPhonetic", "「" + vPhonetic + "」");
                    }
                    bundle.putInt("mFavorite", mFavorite);
                    bundle.putInt("wID", wID);

                    //Đưa Bundle vào Intent
                    myIntent.putExtra("TraCuuFragment", bundle);
                    //Mở Activity ResultActivity
                    back = true;
                    startActivity(myIntent);

                }
            });
        }
    }

    //set listview full items
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 10;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MenuActivity.isActivityOther = true;
    }

    private boolean back;

    @Override
    public void onBackPressed() {
        back = true;
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        back = false;
        stopService(new Intent(this, FastSearchService.class));
    }

    @Override
    protected void onStop() {
        if (!back && MenuActivity.sharedPreferences.getBoolean("fast_search", false)) {
            if (Build.VERSION.SDK_INT < 23) {
                startService(new Intent(this, FastSearchService.class));
            } else if (Build.VERSION.SDK_INT >= 23 && Settings.canDrawOverlays(this)) {
                startService(new Intent(this, FastSearchService.class));
            }
//            Toast.makeText(this, "start service", Toast.LENGTH_SHORT).show();
        }
        super.onStop();
    }
}
