package eup.mobi.jedictionary.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


/**
 * Created by nguye on 8/21/2015.
 */

public class OldDatabase extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "javn.db";

    public OldDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
//            db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
//            db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    //yeu thich
    public ArrayList<String> backUpVija() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<String> listVija = new ArrayList<String>();
        int mID;

        String query_vi = "SELECT * FROM vija where favorite =1";
        Cursor cursor_vi = db.rawQuery(query_vi, null);

        cursor_vi.moveToFirst();

        while (!cursor_vi.isAfterLast()) {

            mID = cursor_vi.getInt(cursor_vi.getColumnIndex("id"));

            listVija.add(String.valueOf(mID));
            cursor_vi.moveToNext();
        }

        cursor_vi.close();
        return listVija;
    }

    public ArrayList<String> backUpJavi() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<String> listVija = new ArrayList<String>();
        int mID;

        String query_vi = "SELECT * FROM javi where favorite =1";
        Cursor cursor_vi = db.rawQuery(query_vi, null);

        cursor_vi.moveToFirst();

        while (!cursor_vi.isAfterLast()) {

            mID = cursor_vi.getInt(cursor_vi.getColumnIndex("id"));

            listVija.add(String.valueOf(mID));
            cursor_vi.moveToNext();
        }

        cursor_vi.close();
        return listVija;
    }

    // yeu thich kanji
    public ArrayList<String> backUpKanji() {

        SQLiteDatabase db = getReadableDatabase();
//        ArrayList<Example>
        ArrayList<String> listKanji = new ArrayList<String>();
        int mId;
        //        int mLevel;
        String query = "SELECT * FROM kanji where favorite=1";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            mId = cursor.getInt(cursor.getColumnIndex("id"));
            listKanji.add(String.valueOf(mId));
            cursor.moveToNext();
        }

        cursor.close();
        return listKanji;
    }

    //get list tab 3 Grammar
    public ArrayList<String> backUpGrammar() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<String> listGrammar = new ArrayList<String>();
        int mID;
        String query = "select * from grammar where favorite=1";
        Cursor cursor = db.rawQuery(query, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            mID = cursor.getInt(cursor.getColumnIndex("id"));

            listGrammar.add(String.valueOf(mID));
            cursor.moveToNext();
        }

        cursor.close();
        return listGrammar;
    }
}