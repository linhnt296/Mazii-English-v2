package eup.mobi.jedictionary.residemenuitems;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.Kanji;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.module.VerbTable;
import eup.mobi.jedictionary.myadapter.MyAdapterHanTu;
import eup.mobi.jedictionary.myadapter.MyAdapterYeuThich;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.residemenuactivity.TraCuuHanTuActivity;
import eup.mobi.jedictionary.residemenuactivity.TraCuuTuActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nguye on 8/18/2015.
 */
public class YeuThichFragment extends Fragment {
    private View parentView;
    private TabHost tab;
    private SwipeMenuListView lv_1, lv_2;
    private MenuActivity menuActivity;
    MyAdapterYeuThich adapter_vi;
    MyAdapterHanTu adapter_2;

    private String vMean, vTitle, mPhonetic;
    private int mFavorite, wID, mPosition;
    private String mKun, mOn, mStroke_count, mLevel, mCompDetail, mDetail, mExamples = null, mMean, mKanji, svg_kanji;

//    SearchView searchView;

    ArrayList<Jaen> vija;
    ArrayList<Kanji> kanji;
    ProgressBar progressBar;
    TextView tv_null_1, tv_null_2, tv_null_3, tv_null_32, tv_null_22, tv_null_12;
    ImageView iv_favorite_1, iv_favorite_2, iv_favorite_3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.yeuthich, container, false);
        ImageButton bt_menu = (ImageButton) parentView.findViewById(R.id.title_bar_left_menu);
        FloatingActionButton fab = (FloatingActionButton) parentView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //changeFragment(new TraCuuFragment());
                TraCuuFragment traCuuFragment = new TraCuuFragment();
                traCuuFragment.setMenuActivity(menuActivity);
                changeFragment(traCuuFragment);
            }
        });
//        searchView = (SearchView) parentView.findViewById(R.id.searchView);
        lv_1 = (SwipeMenuListView) parentView.findViewById(R.id.lv_yeuthich1);
        lv_2 = (SwipeMenuListView) parentView.findViewById(R.id.lv_yeuthich2);

        tv_null_1 = (TextView) parentView.findViewById(R.id.tv_null_1);
        tv_null_2 = (TextView) parentView.findViewById(R.id.tv_null_2);
        tv_null_22 = (TextView) parentView.findViewById(R.id.tv_null_22);
        tv_null_12 = (TextView) parentView.findViewById(R.id.tv_null_12);

        iv_favorite_2 = (ImageView) parentView.findViewById(R.id.iv_fa_2);
        iv_favorite_1 = (ImageView) parentView.findViewById(R.id.iv_fa_1);

        progressBar = (ProgressBar) parentView.findViewById(R.id.pbHeaderProgress);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.holo_blue), PorterDuff.Mode.SRC_IN);


        MenuActivity.isClicked = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MenuActivity.isClicked = false;
            }
        }, 2000);
        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
                try {
                    if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                        MenuActivity.drawer.openDrawer(GravityCompat.START);
                } catch (Exception e) {
                }
            }
        });
       /* resideMenu.addIgnoredView(lv_1);
        resideMenu.addIgnoredView(lv_2);
        resideMenu.addIgnoredView(lv_3);*/
        loadTabs();
        swipeListView();
        VerbTable.init();

        boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
        float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
//        Adsmod.fragmentBanner(parentView, banner, probBanner, isPremium);

        return parentView;
    }


    // Cấu hình tab
    private void loadTabs() {
        // Lấy Tabhost _id ra trước (cái này của built - in android
        tab = (TabHost) parentView.findViewById(android.R.id.tabhost);

        // gọi lệnh setup
        tab.setup();
        TabHost.TabSpec spec;
        // Tạo tab1
        spec = tab.newTabSpec("yeuhich_tab1");
        spec.setContent(R.id.yeuthich_tab1);
        spec.setIndicator(getString(R.string.tracuu_tab1));
        tab.addTab(spec);
        // Tạo tab2
        spec = tab.newTabSpec("yeuthich_tab2");
        spec.setContent(R.id.yeuthich_tab2);
        spec.setIndicator(getString(R.string.tracuu_tab2));
        tab.addTab(spec);
        tab.addTab(spec);

        tab.setCurrentTab(0);

    }

    //my Asyntask tu
    private class myAsyntask_yeuthich1_vi extends AsyncTask<Void, Void, List<Jaen>> {

        @Override
        protected List<Jaen> doInBackground(Void... params) {
            MyDatabase db = new MyDatabase(getContext());
            vija = db.getJaenYeuThich();
            return vija;
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List<Jaen> vijas) {
            super.onPostExecute(vijas);
            progressBar.setVisibility(View.GONE);
            if (vija == null) {
                return;
            }
            if (vija.size() == 0) {
                tv_null_1.setVisibility(View.VISIBLE);
                tv_null_12.setVisibility(View.VISIBLE);
                iv_favorite_1.setVisibility(View.VISIBLE);
            }
            if (vija != null && getActivity() != null) {
                adapter_vi = new MyAdapterYeuThich(getActivity(), R.layout.yeuthich_tab1_vi_items, vija);
                lv_1.setAdapter(adapter_vi);
                setListViewHeightBasedOnChildren(lv_1);
//                                    tv_mMean.setText("");
//                                    lv.setVisibility(View.VISIBLE);
                lv_1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                                            lv.setVisibility(View.GONE);
//                                            String vMean = vija.get(position).getmMean().toString();
//                                            tv_mMean.setText(vMean);
//                                Toast.makeText(getContext(), vMean, Toast.LENGTH_SHORT).show();
                        Intent myIntent = new Intent(getActivity(), TraCuuTuActivity.class);
                        //Khai báo Bundle
                        Bundle bundle = new Bundle();
                        wID = vija.get(position).getmId();
                        vMean = vija.get(position).getmMean();
                        vTitle = vija.get(position).getmWord();
                        mFavorite = vija.get(position).getmFavorite();
                        mPhonetic = vija.get(position).getmPhonetic();

                        //đưa dữ liệu riêng lẻ vào Bundle
                        bundle.putString("vMean", vMean);
                        bundle.putString("vTitle", vTitle);
                        bundle.putInt("mFavorite", mFavorite);
                        bundle.putString("verbPhonetic", mPhonetic);
                        if (mPhonetic != null) {
                                bundle.putString("vPhonetic", "「" + mPhonetic + "」");
                        }
                        bundle.putInt("wID", wID);

                        //Đưa Bundle vào Intent
                        myIntent.putExtra("TraCuuFragment", bundle);
                        //Mở Activity ResultActivity
                        MenuActivity.startMyActivity(myIntent, getMenuActivity());

                    }
                });

                //Swipe items click listener
                lv_1.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                        mPosition = position;
                        wID = vija.get(position).getmId();
                        vMean = vija.get(position).getmMean();
                        vTitle = vija.get(position).getmWord();
                        mFavorite = vija.get(position).getmFavorite();
                        mPhonetic = vija.get(position).getmPhonetic();

                        switch (index) {
                            case 0:
                                // open
                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                builder.setTitle(getString(R.string.remove_favorite_title))
                                        .setMessage(getString(R.string.remove_favorite_message))
                                        .setCancelable(true)
                                        .setNegativeButton(getString(R.string.history_delete), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //do things
                                                vija.clear();
                                                new REMOVEAllSyntask_tu().execute();
                                                Toast.makeText(getContext(), getString(R.string.remove_favorite_toast), Toast.LENGTH_SHORT).show();
                                                adapter_vi = new MyAdapterYeuThich(getActivity(), R.layout.yeuthich_tab1_vi_items, vija);
                                                lv_1.setAdapter(adapter_vi);
                                            }
                                        })
                                        .setPositiveButton(getString(R.string.history_cancel), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                //do things
                                                dialog.dismiss();
                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();

                                break;
                            case 1:
                                // delete
                                new REMOVESyntask_tu().execute(String.valueOf(wID));
                                vija.remove(mPosition);
                                if (vTitle == null) {
                                    Toast.makeText(getContext(), getString(R.string.history_daxoa) + mPhonetic + getString(R.string.add_to_favorite_khoiyeuthich), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getContext(), getString(R.string.history_daxoa) + vTitle + getString(R.string.add_to_favorite_khoiyeuthich), Toast.LENGTH_SHORT).show();
                                }
                                adapter_vi = new MyAdapterYeuThich(getActivity(), R.layout.yeuthich_tab1_vi_items, vija);
                                lv_1.setAdapter(adapter_vi);
                                break;
                        }
                        // false : close the menu; true : not close the menu
                        return false;
                    }
                });
            }

        }
    }

    private class myAsyntask_yeuthich2 extends AsyncTask<String, Void, ArrayList<Kanji>> {

        @Override
        protected ArrayList<Kanji> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            kanji = db.getYeuThichKanji();
            return kanji;
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(ArrayList<Kanji> kanjis) {
            super.onPostExecute(kanjis);
            progressBar.setVisibility(View.GONE);
            if (kanji == null) {
                return;
            }
            if (kanji.size() == 0) {
                tv_null_2.setVisibility(View.VISIBLE);
                tv_null_22.setVisibility(View.VISIBLE);
                iv_favorite_2.setVisibility(View.VISIBLE);
            }
            if (kanji != null && kanji.size()>0) {
                adapter_2 = new MyAdapterHanTu(getActivity(), R.layout.yeuthich_tab2_items, kanji);
                lv_2.setAdapter(adapter_2);
                setListViewHeightBasedOnChildren(lv_2);
            }

            lv_2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent myIntent = new Intent(getActivity(), TraCuuHanTuActivity.class);
                    //Khai báo Bundle
                    Bundle bundle = new Bundle();
                    mKanji = kanji.get(position).getmKanji();
                    if (kanji.get(position).getmMean() != null)
                        mMean = kanji.get(position).getmMean();
                    else mMean = "";
                    if (kanji.get(position).getmKun() != null) {
                        mKun = kanji.get(position).getmKun();
                    } else mKun = "";
                    if (kanji.get(position).getmImg() != null) {
                        svg_kanji = kanji.get(position).getmImg();
                    } else {
                        svg_kanji = "";
                    }
                    if (kanji.get(position).getmOn() != null) {
                        mOn = kanji.get(position).getmOn();
                    } else mOn = "";
                    if (String.valueOf(kanji.get(position).getmStroke_count()) != null) {
                        mStroke_count = String.valueOf(kanji.get(position).getmStroke_count());
                    } else mStroke_count = "";
                    if (String.valueOf(kanji.get(position).getmLevel()) != null) {
                        mLevel = String.valueOf(kanji.get(position).getmLevel());
                    } else mLevel = "";
                    if (kanji.get(position).getmCompdetail() != null) {
                        mCompDetail = kanji.get(position).getmCompdetail();
                    } else mCompDetail = "";
                    if (kanji.get(position).getmDetail() != null) {
                        mDetail = kanji.get(position).getmDetail();
                    } else mDetail = "";
                    if (kanji.get(position).getmExamples() != null) {
                        mExamples = kanji.get(position).getmExamples();
                    } else mExamples = "";
                    mFavorite = kanji.get(position).getmFavorite();
                    wID = kanji.get(position).getmId();

                    //đưa dữ liệu riêng lẻ vào Bundle
                    bundle.putString("mKanji", mKanji);
                    bundle.putString("mMean", mMean);
                    bundle.putString("mKun", mKun);
                    bundle.putString("svg_kanji", svg_kanji);
                    bundle.putString("mOn", mOn);
                    bundle.putString("mStroke_count", mStroke_count);
                    bundle.putString("mLevel", mLevel);
                    bundle.putString("mCompDetail", mCompDetail);
                    bundle.putString("mDetail", mDetail);
                    bundle.putString("mExamples", mExamples);
                    bundle.putInt("mFavorite", mFavorite);
                    bundle.putInt("wID", wID);
                    //Đưa Bundle vào Intent
                    myIntent.putExtra("TraCuuFragment_2", bundle);
                    //Mở Activity ResultActivity
                    MenuActivity.startMyActivity(myIntent, getMenuActivity());

                }
            });


            //Swipe items click listener
            lv_2.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                    wID = kanji.get(position).getmId();
                    mKanji = kanji.get(position).getmKanji();
                    mPosition = position;
                    switch (index) {
                        case 0:
                            // open
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle(getString(R.string.remove_favorite_title))
                                    .setMessage(getString(R.string.remove_favorite_message))
                                    .setCancelable(true)
                                    .setNegativeButton(getString(R.string.history_delete), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                            kanji.clear();
                                            new REMOVEAllSyntask_hantu().execute(String.valueOf(wID));
                                            Toast.makeText(getContext(), getString(R.string.remove_favorite_toast2), Toast.LENGTH_SHORT).show();
                                            adapter_2 = new MyAdapterHanTu(getActivity(), R.layout.yeuthich_tab2_items, kanji);
                                            lv_2.setAdapter(adapter_2);
                                        }
                                    })
                                    .setPositiveButton(getString(R.string.history_cancel), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //do things
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                            break;
                        case 1:
                            // delete
                            new REMOVESyntask_hantu().execute(String.valueOf(wID));
                            kanji.remove(mPosition);
                            Toast.makeText(getContext(), getString(R.string.history_daxoa) + mKanji + getString(R.string.add_to_favorite_khoiyeuthich), Toast.LENGTH_SHORT).show();
                            adapter_2 = new MyAdapterHanTu(getActivity(), R.layout.yeuthich_tab2_items, kanji);
                            lv_2.setAdapter(adapter_2);
                            break;
                    }
                    // false : close the menu; true : not close the menu
                    return false;
                }
            });

        }
    }

    //remove favorite tu
    private class REMOVESyntask_tu extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());

            db.setFavoriteOff_tu(params[0]);
            return null;
        }
    }

    //remove favorite tu
    private class REMOVEAllSyntask_tu extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());

            db.setAllFavoriteOff_tu();
            return null;
        }
    }

    //remove favorite tu
    private class REMOVESyntask_hantu extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());

            db.setFavoriteOff_hantu(params[0]);
            return null;
        }
    }

    //remove favorite tu
    private class REMOVEAllSyntask_hantu extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());

            db.setAllFavoriteOff_hantu();
            return null;
        }
    }

    //remove favorite tu
    private class REMOVESyntask_nguphap extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());

            db.setFavoriteOff_nguphap(params[0]);
            return null;
        }
    }

    //remove favorite tu
    private class REMOVEAllSyntask_nguphap extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            db.setAllFavoriteOff_nguphap();
            return null;
        }
    }

    private void swipeListView() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.parseColor("#EFE4B0")));
                // set item width
                openItem.setWidth(90);
//                // set item title
//                openItem.setTitle("Open");
//                // set item title fontsize
//                openItem.setTitleSize(18);
//                // set item title font color
//                openItem.setTitleColor(Color.WHITE);
                //set icon
                openItem.setIcon(android.R.drawable.ic_menu_delete);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.parseColor("#F33F3F")));
                // set item width
                deleteItem.setWidth(90);
                // set a icon
                deleteItem.setIcon(android.R.drawable.ic_input_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        lv_1.setMenuCreator(creator);
        lv_2.setMenuCreator(creator);

        lv_1.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        lv_2.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 10;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    //change Fragment
    private void changeFragment(Fragment targetFragment) {
        // resideMenu.clearIgnoredViewList();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void setMenuActivity(MenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }

    public MenuActivity getMenuActivity() {
        return menuActivity;
    }

    @Override
    public void onResume() {
        super.onResume();
        // set listview tu_vi
        new myAsyntask_yeuthich1_vi().execute();
        // set listvew hantu
        new myAsyntask_yeuthich2().execute();
    }
}
