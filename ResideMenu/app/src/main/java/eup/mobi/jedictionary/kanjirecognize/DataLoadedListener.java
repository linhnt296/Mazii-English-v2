package eup.mobi.jedictionary.kanjirecognize;

public interface DataLoadedListener {
        void onDataLoaded();
    }