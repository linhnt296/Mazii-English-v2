package eup.mobi.jedictionary.tapviet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by thanh on 10/16/2015.
 */
public class RequestDialog extends AsyncTask<String , String, String > {
    private HandWriteDialog handWriteDialog;

    public HandWriteDialog getHandWriteDialog() {
        return handWriteDialog;
    }

    public void setHandWriteDialog(HandWriteDialog handWriteDialog) {
        this.handWriteDialog = handWriteDialog;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s != null){
            try {
                JSONArray array = new JSONArray(s);
                JSONArray array1 = array.getJSONArray(1);
                JSONArray array2 = array1.getJSONArray(0);
                JSONArray array3 = array2.getJSONArray(1);
                handWriteDialog.resultList.clear();
                for(int i = 0; i < array3.length(); i++) {
                    handWriteDialog.resultList.add(array3.get(i).toString());
                }
                handWriteDialog.resultAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            handWriteDialog.resultList.clear();
            handWriteDialog.resultAdapter.notifyDataSetChanged();
        }
    }
    @Override
    protected String doInBackground(String... params) {
        String json = params[0];
        String result =  sendRequest(json);
        if(result != null){
            return result;
        }
        return null;
    }
    public String sendRequest(String json){
        String address = "https://inputtools.google.com/request?itc=ja-t-i0-handwrit&app=translate";
        StringBuilder sb = new StringBuilder();
        OutputStreamWriter printlnOut ;
        DataInputStream input;
        try {
            URL url = new URL(address);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.connect();
            printlnOut = new OutputStreamWriter(urlConnection.getOutputStream());
            printlnOut.write(json.toString());
//            Log.d("Json encoded", URLEncoder.encode(json, "UTF-8"));
            printlnOut.flush();
            printlnOut.close();
            int httpResult = urlConnection.getResponseCode();

            if(httpResult == HttpURLConnection.HTTP_OK){
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
                String line = null;
                while((line = br.readLine()) != null){
                    sb.append(line+"\n");
                }
                br.close();
                return sb.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)handWriteDialog.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
