package eup.mobi.jedictionary.residemenuitems;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.module.VerbTable;
import eup.mobi.jedictionary.myadapter.MyAdapterChuyenNganh;
import eup.mobi.jedictionary.residemenuactivity.ChuyenNganhActivity;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.residemenuactivity.TuLoaiAcitivity;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * User: special
 * Date: 13-12-22
 * Time: 下午1:31
 * Mail: specialcyci@gmail.com
 */
public class ChuyenNganhFragment extends Fragment {
    private View parentView;
    ListView lv_1, lv_2;
    boolean isPremium;
    TabHost tab;
    private MenuActivity menuActivity;

    public MenuActivity getMenuActivity() {
        return menuActivity;
    }

    public void setMenuActivity(MenuActivity menuActivity) {
        this.menuActivity = menuActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.chuyennganh, container, false);
        ImageButton bt_menu = (ImageButton) parentView.findViewById(R.id.title_bar_left_menu);
        FloatingActionButton fab = (FloatingActionButton) parentView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Track.sendTrackerScreen("Search");
                TraCuuFragment traCuuFragment = new TraCuuFragment();
                traCuuFragment.setMenuActivity(menuActivity);
                changeFragment(traCuuFragment);
            }
        });
        lv_1 = (ListView) parentView.findViewById(R.id.lv_chuyennganh1);
        lv_2 = (ListView) parentView.findViewById(R.id.lv_chuyennganh2);
        tab = (TabHost) parentView.findViewById(android.R.id.tabhost);
        MenuActivity.isClicked = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MenuActivity.isClicked = false;
            }
        }, 2000);
        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                    MenuActivity.drawer.openDrawer(GravityCompat.START);
            }
        });
        loadTabs();
        init();
        VerbTable.init();

        isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
        float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);

//        Adsmod.fragmentBanner(parentView, banner, probBanner, isPremium);
        return parentView;
    }

    private void init() {
        MyAdapterChuyenNganh adapter_1 = new MyAdapterChuyenNganh(getActivity(), R.layout.chuyennganh_items, getList1());
        MyAdapterChuyenNganh adapter_2 = new MyAdapterChuyenNganh(getActivity(), R.layout.chuyennganh_items, getList2());

        lv_1.setAdapter(adapter_1);
        lv_2.setAdapter(adapter_2);
        setListViewHeightBasedOnChildren(lv_1);
        setListViewHeightBasedOnChildren(lv_2);

        lv_1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MenuActivity.adsmodUtil.showIntervalAds();

                String key = getList1().get(position);
                String sqlTL = getResultTL(key);
//                Toast.makeText(getContext(), sqlTL, Toast.LENGTH_SHORT).show();


                Intent myIntent = new Intent(getActivity(), TuLoaiAcitivity.class);
                //Khai báo Bundle
                Bundle bundle = new Bundle();

                //đưa dữ liệu riêng lẻ vào Bundle
                bundle.putString("sqlTL", sqlTL);
                bundle.putString("title", key);

                //Đưa Bundle vào Intent
                myIntent.putExtra("ChuyenNganhFragment", bundle);
                //Mở Activity ResultActivity
                MenuActivity.startMyActivity(myIntent, getMenuActivity());

            }
        });

        lv_2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MenuActivity.adsmodUtil.showIntervalAds();

                String key = getList2().get(position);
                String sqlCN = getResultCN(key);
//                Toast.makeText(getContext(), sqlCN, Toast.LENGTH_SHORT).show();

                Intent myIntent = new Intent(getActivity(), ChuyenNganhActivity.class);
                //Khai báo Bundle
                Bundle bundle = new Bundle();

                //đưa dữ liệu riêng lẻ vào Bundle
                bundle.putString("sqlCN", sqlCN);
                bundle.putString("title", key);


                //Đưa Bundle vào Intent
                myIntent.putExtra("ChuyenNganhFragment", bundle);
                //Mở Activity ResultActivity
                MenuActivity.startMyActivity(myIntent, getMenuActivity());
            }
        });
    }

    private ArrayList<String> getList1() {
        ArrayList<String> listTuLoai = new ArrayList<String>();
        listTuLoai.add(getString(R.string.chuyennganh_tuloai1));
        listTuLoai.add(getString(R.string.chuyennganh_tuloai2));
        listTuLoai.add(getString(R.string.chuyennganh_tuloai3));
        return listTuLoai;
    }

    private ArrayList<String> getList2() {
        ArrayList<String> listChuyenNganh = new ArrayList<String>();
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh1));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh2));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh3));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh4));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh5));

        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh6));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh7));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh8));

        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh9));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh10));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh11));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh12));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh13));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh14));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh15));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh16));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh17));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh18));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh19));

        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh20));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh21));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh22));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh23));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh24));
        listChuyenNganh.add(getString(R.string.chuyennganh_chuyennganh25));
        return listChuyenNganh;
    }


    //set listview full items
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 10;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, AbsListView.LayoutParams.WRAP_CONTENT));
            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private String getResultCN(String key) {
        String sqliteCN = null;
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put(getString(R.string.chuyennganh_chuyennganh1), "engr");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh2), "comp");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh3), "bus");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh4), "econ");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh5), "finc");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh6), "med");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh7), "anat");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh8), "food");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh9), "chem");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh10), "geom");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh11), "math");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh12), "physics");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh13), "astron");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh14), "biol");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh15), "bot");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh16), "geol");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh17), "archit");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh18), "zool");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh19), "law");

        hashMap.put(getString(R.string.chuyennganh_chuyennganh20), "Buddh");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh21), "mil");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh22), "baseb");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh23), "music");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh24), "sports");
        hashMap.put(getString(R.string.chuyennganh_chuyennganh25), "sumo");

        sqliteCN = hashMap.get(key);
        return sqliteCN;
    }

    private String getResultTL(String key) {
        String sqliteTL = null;
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put(getString(R.string.chuyennganh_tuloai1), "vi");
        hashMap.put(getString(R.string.chuyennganh_tuloai2), "vt");
        hashMap.put(getString(R.string.chuyennganh_tuloai3), "prt");

        sqliteTL = hashMap.get(key);
        return sqliteTL;
    }

    // Cấu hình tab
    private void loadTabs() {
        // Lấy Tabhost _id ra trước (cái này của built - in android

        // gọi lệnh setup
        tab.setup();
        TabHost.TabSpec spec;
        // Tạo tab1
        spec = tab.newTabSpec("chuyennganh_tab1");
        spec.setContent(R.id.chuyennganh_tab1);
        spec.setIndicator(getString(R.string.chuyennganh_tuloai));
        tab.addTab(spec);
        // Tạo tab2
        spec = tab.newTabSpec("chuyennganh_tab2");
        spec.setContent(R.id.chuyennganh_tab2);
        spec.setIndicator(getString(R.string.chuyennganh_chuyennganh));
        tab.addTab(spec);

//        TabWidget widget = tab.getTabWidget();
//        for(int i = 0; i < widget.getChildCount(); i++) {
//            View v = widget.getChildAt(i);
//
//            // Look for the title view to ensure this is an indicator and not a divider.
//            TextView tv = (TextView) tab.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
//            tv.setTextColor(Color.parseColor("#FFFFFF"));
//            if(tv == null) {
//                continue;
//            }
//            v.setBackgroundResource(R.drawable.tab_indicator);
//        }

        // Thiết lập tab mặc định được chọn ban đầu là tab 0
        tab.setCurrentTab(0);
    }

    //change Fragment
    private void changeFragment(Fragment targetFragment) {
        //  resideMenu.clearIgnoredViewList();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}