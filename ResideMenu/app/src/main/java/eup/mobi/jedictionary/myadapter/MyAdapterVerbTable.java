package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.database.VerbObj;

import java.util.ArrayList;


/**
 * Created by nguye on 9/15/2015.
 */
public class MyAdapterVerbTable extends
        ArrayAdapter<VerbObj> {
    Activity context = null;
    ArrayList<VerbObj> myArray = null;
    int layoutId;
    LayoutInflater inflater;


    public MyAdapterVerbTable(Activity context,
                              int layoutId,
                              ArrayList<VerbObj> arr) {

        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_title = (TextView) convertView.findViewById(R.id.list_item_n);
            holder.tv_description = (TextView) convertView.findViewById(R.id.list_item_w);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final VerbObj jResult = myArray.get(position);

        holder.tv_title.setText(jResult.getName());
        holder.tv_description.setText(jResult.getWord());
        return convertView;
    }

    class ViewHolder {
        TextView tv_title, tv_description;
    }
}