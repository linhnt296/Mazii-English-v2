package eup.mobi.jedictionary.module;

import eup.mobi.jedictionary.database.Example;

import java.util.ArrayList;

/**
 * Created by nguye on 12/1/2015.
 */
public class JMeanObj {
    private String kind;
    private String mean;
    private String example;
    private ArrayList<Example> examples;

    public JMeanObj(String jKind, String jMean, String jExample) {
        this.kind = jKind;
        this.mean = jMean;
        this.example = jExample;
    }

    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getMean() {
        return mean;
    }

    public void setMean(String mean) {
        this.mean = mean;
    }

    public ArrayList<Example> getExamples() {
        return examples;
    }

    public void setExamples(ArrayList<Example> examples) {
        this.examples = examples;
    }

    public JMeanObj(String kind, String mean, ArrayList<Example> examples) {
        this.kind = kind;
        this.mean = mean;
        this.examples = examples;
    }
}
