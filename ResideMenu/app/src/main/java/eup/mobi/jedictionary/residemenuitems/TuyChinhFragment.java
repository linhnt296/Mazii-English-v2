package eup.mobi.jedictionary.residemenuitems;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import eup.mobi.jedictionary.database.MyWordDatabase;
import eup.mobi.jedictionary.fast.search.FastSearchService;

import eup.mobi.jedictionary.R;

import eup.mobi.jedictionary.google.Track;
import eup.mobi.jedictionary.module.Category;
import eup.mobi.jedictionary.myadapter.MyAdapterLanguage;
import eup.mobi.jedictionary.myadapter.MyGroupWordAdapter;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;
import eup.mobi.jedictionary.service.WidgetProvider;

import com.rey.material.widget.CheckBox;

import java.util.List;
import java.util.Locale;

// ...

public class TuyChinhFragment extends Fragment {
    private View parentView;
    private CheckBox cb1, cbFastSearch;
    static Boolean isCb1;
    private boolean isFastSearch;
    private MenuActivity menuActivity;
    SharedPreferences sharedPreferences;
    private final String LOCK_SCREEN = "LOCK_MINDER";
    TextView tv_select_category, tv_select_language;
    List<Category> categoryList;
    private int id;
    private String language;
    private String[][] allLanguages = new String[][]{
            {"af", "Afrikaans", ""},
            {"ar", "Arabic", "ar-SA"},
            {"az", "Azerbaijani", ""},
            {"be", "Belarusian", ""},
            {"bg", "Bulgarian", ""},
            {"bn", "Bengali", ""},
            {"bs", "Bosnian", ""},
            {"ca", "Catalan", ""},
            {"ceb", "Cebuano", ""},
            {"cs", "Czech", "cs-CZ"},
            {"cy", "Welsh", ""},
            {"da", "Danish", "da-DK"},
            {"de", "German", "de-DE"},
            {"el", "Greek", "el-GR"},
            {"en", "English", "en-US"},
            {"eo", "Esperanto", ""},
            {"es", "Spanish", "es-ES"},
            {"et", "Estonian", ""},
            {"eu", "Basque", ""},
            {"fa", "Persian", ""},
            {"fi", "Finnish", "fi-FI"},
            {"fr", "French", "fr-CA"},
            {"ga", "Irish", ""},
            {"gl", "Galician", ""},
            {"gu", "Gujarati", ""},
            {"ha", "Hausa", ""},
            {"hi", "Hindi", "hi-IN"},
            {"hmn", "Hmong", ""},
            {"hr", "Croatian", ""},
            {"ht", "Haitian Creole", ""},
            {"hu", "Hungarian", "hu-HU"},
            {"hy", "Armenian", ""},
            {"id", "Indonesian", "id-ID"},
            {"ig", "Igbo", ""},
            {"is", "Icelandic", ""},
            {"it", "Italian", "it-IT"},
            {"iw", "Hebrew", "he-IL"},
            {"ja", "Japanese", "ja-JP"},
            {"jw", "Javanese", ""},
            {"ka", "Georgian", ""},
            {"kk", "Kazakh", ""},
            {"km", "Khmer", ""},
            {"kn", "Kannada", ""},
            {"ko", "Korean", "ko-KR"},
            {"la", "Latin", ""},
            {"lo", "Lao", ""},
            {"lt", "Lithuanian", ""},
            {"lv", "Latvian", ""},
            {"ma", "Punjabi", ""},
            {"mg", "Malagasy", ""},
            {"mi", "Maori", ""},
            {"mk", "Macedonian", ""},
            {"ml", "Malayalam", ""},
            {"mn", "Mongolian", ""},
            {"mr", "Marathi", ""},
            {"ms", "Malay", ""},
            {"mt", "Maltese", ""},
            {"my", "Myanmar (Burmese)", ""},
            {"ne", "Nepali", ""},
            {"nl", "Dutch", ""},
            {"no", "Norwegian", "no-NO"},
            {"ny", "Chichewa", ""},
            {"pl", "Polish", "pl-PL"},
            {"pt", "Portuguese", "pt-BR"},
            {"ro", "Romanian", "ro-RO"},
            {"ru", "Russian", "ru-RU"},
            {"si", "Sinhala",},
            {"sk", "Slovak", "sk-SK"},
            {"sl", "Slovenian", ""},
            {"so", "Somali", ""},
            {"sq", "Albanian", ""},
            {"sr", "Serbian", ""},
            {"st", "Sesotho", ""},
            {"su", "Sudanese", ""},
            {"sv", "Swedish", "sv-SE"},
            {"sw", "Swahili", ""},
            {"ta", "Tamil", ""},
            {"te", "Telugu", ""},
            {"tg", "Tajik", ""},
            {"th", "Thai", "th-TH"},
            {"tl", "Filipino", ""},
            {"tr", "Turkish", "tr-TR"},
            {"uk", "Ukrainian", ""},
            {"ur", "Urdu", ""},
            {"uz", "Uzbek", ""},
            {"vi", "Vietnamese", ""},
            {"yi", "Yiddish", ""},
            {"yo", "Yoruba", ""},
            {"zh-CN", "Chinese Simplified", "zh-CN"},
            {"zh-TW", "Chinese Traditional", "zh-CN"},
            {"zu", "Zulu", ""}
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        isCb1 = MenuActivity.sharedPreferences.getBoolean("cb1", true);
        isFastSearch = MenuActivity.sharedPreferences.getBoolean("fast_search", false);
        parentView = inflater.inflate(R.layout.tuychinh, container, false);

        // get list Category
        new getListMyWord().execute();

        sharedPreferences = getMenuActivity().getSharedPreferences(LOCK_SCREEN, Context.MODE_PRIVATE);
        id = sharedPreferences.getInt("id_widget", -1);
        language = MenuActivity.sharedPreferences.getString("to_language", Locale.getDefault().getLanguage());
        ImageButton bt_menu = (ImageButton) parentView.findViewById(R.id.title_bar_left_menu);
        FloatingActionButton fab = (FloatingActionButton) parentView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Track.sendTrackerScreen("Search");
                // changeFragment(new TraCuuFragment());
                TraCuuFragment traCuuFragment = new TraCuuFragment();
                traCuuFragment.setMenuActivity(menuActivity);
                changeFragment(traCuuFragment);
            }
        });
        cb1 = (CheckBox) parentView.findViewById(R.id.cb_1);
        cbFastSearch = (CheckBox) parentView.findViewById(R.id.cbFastSearch);
        tv_select_category = (TextView) parentView.findViewById(R.id.tv_select_category);
        tv_select_language = (TextView) parentView.findViewById(R.id.tv_select_language);
        cb1.setChecked(isCb1);
        cbFastSearch.setChecked(isFastSearch);
        cbFastSearch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    getMenuActivity().stopService(new Intent(getMenuActivity(), FastSearchService.class));
                }
            }
        });
        // set text select language
        for (String[] language : allLanguages) {
            if (this.language.equals(language[0])) {
                tv_select_language.setText(language[1]);
                break;
            }
        }
        bt_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                        MenuActivity.drawer.openDrawer(GravityCompat.START);
                } catch (Exception e) {
                }
            }
        });
        tv_select_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogLanguage();
            }
        });
        tv_select_category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryList != null && categoryList.size() > 0)
                    showDialogWidget();
                else
                    Toast.makeText(getActivity(), getString(R.string.no_data_widget), Toast.LENGTH_SHORT).show();
            }
        });

        boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
        float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);

//        Adsmod.fragmentBanner(parentView, banner, probBanner, isPremium);
        return parentView;
    }

    private class getListMyWord extends AsyncTask<Void, Void, List<Category>> {

        @Override
        protected List<Category> doInBackground(Void... params) {
            MyWordDatabase db = new MyWordDatabase(getActivity());
            return db.qCategory();
        }

        @Override
        protected void onPostExecute(List<Category> categories) {
            super.onPostExecute(categories);
            if (categories != null && categories.size() > 0) {
                categoryList = categories;
                if (id != -1) {
                    for (Category category : categories) {
                        if (category.getId() == id) {
                            tv_select_category.setText(category.getName());
                            break;
                        }
                    }
                } else {
                    tv_select_category.setText(categories.get(0).getName());
                }
            } else tv_select_category.setText(getString(R.string.no_data_widget));
        }
    }

    private void showDialogLanguage() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle(R.string.select_my_language);

        final MyAdapterLanguage adapter = new MyAdapterLanguage(getActivity(), R.layout.item_select_language, allLanguages);

        builderSingle.setNegativeButton(
                R.string.add_group_dialog_button2,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                adapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tv_select_language.setText(allLanguages[which][1]);
                        MenuActivity.sharedPreferences.edit().putString("to_language", allLanguages[which][0]).commit();
                    }
                });
        builderSingle.show();
    }

    private void showDialogWidget() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
        builderSingle.setTitle(R.string.select_my_word);

        final MyGroupWordAdapter adapter = new MyGroupWordAdapter(getActivity(), R.layout.sotay_item_recyclerview, categoryList);

        builderSingle.setNegativeButton(
                R.string.add_group_dialog_button2,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                adapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tv_select_category.setText(adapter.getItem(which).getName());
                        sharedPreferences.edit().putInt("id_widget", categoryList.get(which).getId()).commit();
                        // notify data set changed widget
                        final AppWidgetManager mgr = AppWidgetManager.getInstance(getActivity());
                        final ComponentName cn = new ComponentName(getActivity(),
                                WidgetProvider.class);
                        mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn),
                                R.id.page_flipper);
                    }
                });
        builderSingle.show();
    }

    @Override
    public void onPause() {
        super.onPause();
        MenuActivity.sharedPreferences.edit().putBoolean("cb1", cb1.isChecked()).commit();
        MenuActivity.sharedPreferences.edit().putBoolean("fast_search", cbFastSearch.isChecked()).commit();
    }

    public MenuActivity getMenuActivity() {
        return menuActivity;
    }

    public void setMenuActivity(MenuActivity menuActivity) {
        TuyChinhFragment.this.menuActivity = menuActivity;
    }

    //change Fragment
    private void changeFragment(Fragment targetFragment) {
        //resideMenu.clearIgnoredViewList();
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}