package eup.mobi.jedictionary.purchasing;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import eup.mobi.jedictionary.util.IabHelper;
import eup.mobi.jedictionary.util.IabResult;

/**
 * Created by thanh on 11/9/2015.
 */
public class InAppPurchasing {
    public InAppPurchasing(Context context) {
        this.context = context;
    }

    private Context context;
    // Debug tag, for logging
    static final String TAG = "TrivialDrive";
    // Does the user have the premium upgrade?
    boolean mIsPremium = false;
    // SKUs for our products: the premium upgrade (non-consumable) and gas (consumable)
    static final String SKU_PREMIUM = "premium";
    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;

    // The helper object
    IabHelper mHelper;
    public void init(){
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1skJ3qAZARhxw204txdBJGWjwyRDPgkAuohH3HeKDE6kmDbDpWPUdii+G/DSe41lQSxG9VY2hsaZD+UNO0/xmsQ8zYHKwu1cBLaBg9ZXXLrSzAut5spc86/gxVi3bMKk+6xGed1KUTA/6I8N5pm3T6lfhpPCWYs2/WUr3t4sUvXSH+SwXSs6EBWmNfJ8sxux57B1db04vgRLDG164gwFW1sWPQoaulzYIGOv7WQGVv0Dz6n8BGYemDAk5SVmQJ3iCsZLXrsjCOESpp8P1qowfvQ7ZlEtMYMw9OqWaftrxlRjanKgPGoZHXQYJ0oPpCe6RHohVxWMqa+XsxPgkLPTQIDAQAB";
        if (base64EncodedPublicKey.contains("CONSTRUCT_YOUR")) {
            throw new RuntimeException("Please put your app's public key in MainActivity.java. See README.");
        }
        Log.d("Packet Name", context.getPackageName());
        if (context.getPackageName().startsWith("com.example")) {
            throw new RuntimeException("Please change the sample's package name! See README.");
        }

        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper(this.context, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
             //   mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }
    void complain(String message) {
        Log.e(TAG, "**** TrivialDrive Error: " + message);
        alert("Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this.context);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Log.d(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

}
