package eup.mobi.jedictionary.database;

/**
 * Created by LinhNT on 1/12/2016.
 */
public class JlptWord {
    private int id;
    private String word;
    private String phonetic;
    private String mean;
    private int level;
    private int remember;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getPhonetic() {
        return phonetic;
    }

    public void setPhonetic(String phonetic) {
        this.phonetic = phonetic;
    }

    public String getMean() {
        return mean;
    }

    public void setMean(String mean) {
        this.mean = mean;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getRemember() {
        return remember;
    }

    public void setRemember(int remember) {
        this.remember = remember;
    }

    public JlptWord(int id, String word, String phonetic, String mean, int level, int remember) {
        this.id = id;
        this.word = word;
        this.phonetic = phonetic;
        this.mean = mean;
        this.level = level;
        this.remember = remember;
    }
}
