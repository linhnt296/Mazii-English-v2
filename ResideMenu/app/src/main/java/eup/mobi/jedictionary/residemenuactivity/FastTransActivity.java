package eup.mobi.jedictionary.residemenuactivity;

import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.fast.search.FastSearchService;
import eup.mobi.jedictionary.residemenuitems.DocBaoFragment;

public class FastTransActivity extends AppCompatActivity {
    TextView tvJapanese, tvVietnamese, tvRomanji;
    String orig = "", translit = "", trans = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fast_trans_activity);
        tvJapanese = (TextView) findViewById(R.id.tv_japanese);
        tvRomanji = (TextView) findViewById(R.id.tv_romanji);
        tvVietnamese = (TextView) findViewById(R.id.tv_vietnamese);

        //Admob
        if (MenuActivity.sharedPreferences != null) {
            boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
            String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
            float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);
//            Adsmod.banner(this, banner, probBanner, isPremium);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("FastTrans");
        orig = bundle.getString("orig");
        translit = bundle.getString("translit");
        trans = bundle.getString("trans");

        tvJapanese.setText(orig);
        tvRomanji.setText(translit);
        tvVietnamese.setText(trans);
    }


    @Override
    protected void onResume() {
        super.onResume();
        MenuActivity.isActivityOther = true;
    }

    private boolean back;

    @Override
    public void onBackPressed() {
        back = true;
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        back = false;
        stopService(new Intent(this, FastSearchService.class));
    }

    @Override
    protected void onStop() {
        if (!back && MenuActivity.sharedPreferences.getBoolean("fast_search", false)) {
            if (Build.VERSION.SDK_INT < 23) {
                startService(new Intent(this, FastSearchService.class));
            } else if (Build.VERSION.SDK_INT >= 23 && Settings.canDrawOverlays(this)) {
                startService(new Intent(this, FastSearchService.class));
            }
//            Toast.makeText(this, "start service", Toast.LENGTH_SHORT).show();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DocBaoFragment.isSearch = false;
    }
}
