package eup.mobi.jedictionary.svgwriter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.support.v4.widget.ExploreByTouchHelper;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.larvalabs.svgandroid.SVGParser;
import eup.mobi.jedictionary.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by thanh on 11/2/2015.
 */
public class TapvietCanvasView extends View {
    private String svgData;
    private ArrayList<String> listPath;
    private ArrayList<Path> pathArrayList;
    private ArrayList<TextPoint> textPointArrayList;
    private boolean isRunning = false;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    Context context;
    private Paint mPaint, pencilPaint, textPaint;
    long startTime;
    int numStroke = 0;
    private boolean stop;
    private PathMeasure measure;
    private float distance, speed;
    private float pos[], tan[];
    private int scaleNumber;
    private int currentWidth, currentHeight;
    private ArrayList<String> colorPath;
    private ArrayList<Integer> ramdomNumber;
    private float scaleUnit;
    private Matrix scaleMatrix;
    private boolean isScalePath;
    private Path myPath;
    private float mX, mY;
    private static final float TOLERANCE = 5;
    private boolean isHumanWriting, isHideSample, isErase;

    public TapvietCanvasView(Context c, AttributeSet attrs) {
        super(c, attrs);
        stop = false;
        context = c;
        startTime = System.currentTimeMillis();
        this.postInvalidate();
        // we set a new Path
        mPath = new Path();
        pathArrayList = new ArrayList<Path>();
        // and we set a new Paint with the desired attributes
        //todo panit
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.parseColor("#3367D6"));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(8f);
        measure = new PathMeasure();
        distance = 0;
        speed = 0;
        pos = new float[2];
        tan = new float[2];
        scaleMatrix = new Matrix();
        //todo pencil
        pencilPaint = new Paint();
        pencilPaint.setAntiAlias(true);
        pencilPaint.setDither(true);
        pencilPaint.setColor(Color.parseColor("#BDBDBD"));
        pencilPaint.setStyle(Paint.Style.STROKE);
        pencilPaint.setStrokeJoin(Paint.Join.ROUND);
        pencilPaint.setStrokeWidth(5f);
        // todo number
        textPaint = new TextPaint();
        textPaint.setAntiAlias(true);
        textPaint.setDither(true);
        textPaint.setColor(Color.parseColor("#BDBDBD"));
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setStrokeJoin(Paint.Join.ROUND);
        textPaint.setStrokeWidth(1f);
        textPaint.setTextSize(24f);
        listPath = new ArrayList<String>();
        pathArrayList = new ArrayList<Path>();
        textPointArrayList = new ArrayList<TextPoint>();
        isScalePath = false;
        scaleUnit = -1;
        myPath = new Path();
        isHumanWriting = false;
        isErase = false;
        isHideSample = false;
    }

    // override onSizeChanged
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // your Canvas will draw onto the defined Bitmap
        if (w > 0 && h > 0) {
            mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            currentWidth = w;
            currentHeight = h;
            scaleNumber = w / 110;
        }
//        Log.d("Scale number onsize change", Integer.toString(scaleNumber));
    }

    // override onDraw
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
       /* for (Path path : pathArrayList) {
            canvas.drawPath(path, pencilPaint);
        }*/
        if (!isScalePath) {
            for (Path path : pathArrayList) {
                Matrix scaleMatrix1 = new Matrix();
                scaleMatrix1.setScale(scaleUnit, scaleUnit);
                //  Log.d("Scale Number", Integer.toString(numberScale/110));
                path.transform(scaleMatrix1);
            }
            for (TextPoint textPoint : textPointArrayList) {
                textPoint.setX(textPoint.getX() * scaleUnit);
                textPoint.setY(textPoint.getY() * scaleUnit);
            }
            textPaint.setTextSize(scaleUnit * 6);
            textPaint.setTypeface(Typeface.SANS_SERIF);
            mPaint.setStrokeWidth(scaleUnit * 2);
            pencilPaint.setStrokeWidth(scaleUnit * 2);
            isScalePath = true;

        }
        canvas.drawBitmap(mBitmap, 0f, 0f, new Paint(4));
        if (!isHumanWriting) {
            if (isRunning && !stop) {

                if (numStroke >= pathArrayList.size()) {
                    mPath.reset();
                    numStroke = 0;
                    stop = true;
                    isHumanWriting = true;
                }
                measure.setPath(pathArrayList.get(numStroke), false);
                if (distance < measure.getLength()) {
                    measure.getPosTan(distance, pos, tan);
                    speed = measure.getLength() / 10;
                    if (distance == 0) {
                        mCanvas.drawText(Integer.toString(numStroke + 1), textPointArrayList.get(numStroke).getX(), textPointArrayList.get(numStroke).getY(), textPaint);
                        canvas.drawText(Integer.toString(numStroke + 1), textPointArrayList.get(numStroke).getX(), textPointArrayList.get(numStroke).getY(), textPaint);
                        mPath.moveTo(pos[0], pos[1]);
                    }
                    // todo speed
                    distance += 12f;

                    mPath.lineTo(pos[0], pos[1]);
                    canvas.drawPath(mPath, pencilPaint);

                    for (int i = 0; i <= numStroke; i++) {
                        //  canvas.drawText(Integer.toString(i + 1), textPointArrayList.get(i).getX(), textPointArrayList.get(i).getY(), textPaint);
                    }
                } else {
                    for (int i = 0; i <= numStroke; i++) {
                    /*canvas.drawText(Integer.toString(i + 1), textPointArrayList.get(i).getX(), textPointArrayList.get(i).getY(), textPaint);
                    canvas.drawPath(mPath, mPaint);*/
                    }
                    mCanvas.drawText(Integer.toString(numStroke + 1), textPointArrayList.get(numStroke).getX(), textPointArrayList.get(numStroke).getY(), textPaint);
                    mCanvas.drawPath(mPath, pencilPaint);
                    //  mCanvas.drawText(Integer.toString(numStroke + 1), textPointArrayList.get(numStroke).getX(), textPointArrayList.get(numStroke).getY(), textPaint);
                    distance = 0;
                    numStroke++;
                    mPath.reset();
                    // mPaint.setColor(Color.parseColor(colorPath.get(ramdomNumber.get(numStroke - 1))));
                    //   textPaint.setColor(Color.parseColor(colorPath.get(ramdomNumber.get(numStroke - 1))));
                }
                invalidate();
            }
        } else {
            canvas.drawPath(myPath, mPaint);
            invalidate();
        }
    }

    public ArrayList<String> getListPath() {
        return listPath;
    }

    public void setListPath(ArrayList<String> listPath, ArrayList<TextPoint> textPoints) {
        isRunning = true;
        this.listPath = listPath;
        this.textPointArrayList = textPoints;
        SvgPathParser pathParser = new SvgPathParser();
        for (int i = 0; i < listPath.size(); i++) {
            try {
                Path path = pathParser.parsePath(listPath.get(i));
                Matrix scaleMatrix = new Matrix();
                // todo matrix
                scaleMatrix.setScale(10f, 10f);
                path.transform(scaleMatrix);
                pathArrayList.add(path);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void nodeTravel(Node node) {
        if (node.hasChildNodes()) {
            NodeList nodeList = node.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                nodeTravel(nodeList.item(i));
            }
        } else {
            Element element = (Element) node;
            listPath.add(element.getAttribute("d"));
        }

    }

    public void textTravel(Node node) {
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Element element = (Element) nodeList.item(i);
            textPointArrayList.add(stringToTextPoint(element.getAttribute("transform")));
        }
    }

    public TextPoint stringToTextPoint(String string) {
        String sub1 = string.replace("matrix(1 0 0 1 ", "");
        String sub2 = sub1.replace(")", "");
        String sub3 = sub2.substring(0, sub2.lastIndexOf(" "));
        String sub4 = sub2.substring(sub2.lastIndexOf(" ") + 1, sub2.length());
        //todo number scale
        return new TextPoint(Float.parseFloat(sub3), Float.parseFloat(sub4));
    }

    public void init(String data) {
        isRunning = true;
        invalidate();
        InputStream stream = null;
        try {
            stream = new ByteArrayInputStream(data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;

        try {
            builder = fac.newDocumentBuilder();
            Document doc = builder.parse(stream);
            Element root = doc.getDocumentElement();
            NodeList list = root.getChildNodes();
            Node node = list.item(0);
            Node textNode = list.item(1);
            textTravel(textNode);
            nodeTravel(node);

            SVGParser pathParser = new SVGParser();
            for (int i = 0; i < listPath.size(); i++) {
                Path path = pathParser.parsePath(listPath.get(i));
                /*Matrix scaleMatrix = new Matrix();
                scaleMatrix.setScale(4f, 4f);
                Log.d("Scale Number", Integer.toString(scaleNumber));
                path.transform(scaleMatrix);*/
                pathArrayList.add(path);
            }


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        resolveColor();
    }

    public void init(String data, int numberScale) {
        isRunning = true;
        invalidate();
        InputStream stream = null;
        try {
            stream = new ByteArrayInputStream(data.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;

        try {
            builder = fac.newDocumentBuilder();
            Document doc = builder.parse(stream);
            Element root = doc.getDocumentElement();
            NodeList list = root.getChildNodes();
            Node node = list.item(0);
            Node textNode = list.item(1);
            textTravel(textNode);
            nodeTravel(node);

            SVGParser pathParser = new SVGParser();
            for (int i = 0; i < listPath.size(); i++) {
                Path path = pathParser.parsePath(listPath.get(i));
                Matrix scaleMatrix = new Matrix();
                scaleMatrix.setScale(4f, 4f);
                path.transform(scaleMatrix);
                pathArrayList.add(path);
            }


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        resolveColor();
    }

    public void resolveColor() {
        colorPath = new ArrayList<String>();
        colorPath.add(context.getResources().getString(R.string.color_1));
        colorPath.add(context.getResources().getString(R.string.color_2));
        colorPath.add(context.getResources().getString(R.string.color_3));
        colorPath.add(context.getResources().getString(R.string.color_4));
        colorPath.add(context.getResources().getString(R.string.color_5));
        colorPath.add(context.getResources().getString(R.string.color_6));
        colorPath.add(context.getResources().getString(R.string.color_7));
        colorPath.add(context.getResources().getString(R.string.color_8));
        colorPath.add(context.getResources().getString(R.string.color_9));
        colorPath.add(context.getResources().getString(R.string.color_10));
        colorPath.add(context.getResources().getString(R.string.color_11));
        colorPath.add(context.getResources().getString(R.string.color_12));
        colorPath.add(context.getResources().getString(R.string.color_13));
        colorPath.add(context.getResources().getString(R.string.color_14));
        colorPath.add(context.getResources().getString(R.string.color_15));
        colorPath.add(context.getResources().getString(R.string.color_16));
        colorPath.add(context.getResources().getString(R.string.color_17));
        colorPath.add(context.getResources().getString(R.string.color_18));
        colorPath.add(context.getResources().getString(R.string.color_19));
        colorPath.add(context.getResources().getString(R.string.color_20));
        Random r = new Random();
        ramdomNumber = new ArrayList<Integer>();
        for (int i = 0; i < pathArrayList.size(); i++) {
            if (i <= (colorPath.size() - 1)) {
                ramdomNumber.add(i);

            } else {
                ramdomNumber.add(i % colorPath.size());
            }
        }
        //  mPaint.setColor(Color.parseColor(colorPath.get(ramdomNumber.get(0))));
        //  textPaint.setColor(Color.parseColor(colorPath.get(ramdomNumber.get(0))));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int heightSpec;
        if (getLayoutParams().height == -2) {
            heightSpec = MeasureSpec.makeMeasureSpec(536870911, ExploreByTouchHelper.INVALID_ID);
        } else {
            heightSpec = heightMeasureSpec;
        }
        super.onMeasure(widthMeasureSpec, heightSpec);
        float f = this.context.getResources().getDisplayMetrics().density;

        int i = getMeasuredWidth();
        setMeasuredDimension(i, getMeasuredHeight());
        if (this.scaleUnit < 0.0f) {
            this.scaleUnit = ((float) i) / 109.0f;
            this.scaleMatrix.setScale(this.scaleUnit, this.scaleUnit);
        }

    }

    public void rePaint() {
        mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
        distance = 0;
        numStroke = 0;
        mPath.reset();
        stop = false;
        myPath.reset();
        invalidate();
        isHumanWriting = false;
    }

    public void startTouch(float x, float y) {
        myPath.moveTo(x, y);
        mX = x;
        mY = y;
    }

    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOLERANCE || dy >= TOLERANCE) {
            myPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    public void clearCanvas() {
        myPath.reset();
        invalidate();
    }

    // when ACTION_UP stop touch
    private void upTouch() {
        myPath.lineTo(mX, mY);
    }

    //override the onTouchEvent
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        if (isHumanWriting) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startTouch(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    moveTouch(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    upTouch();
                    invalidate();
                    break;
            }
        }
        return true;
    }

    public void hideSample() {
        if (isHumanWriting) {
            if (!isHideSample) {
                mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
                isHideSample = true;
            } else {
                for (int i = 0; i < pathArrayList.size(); i++) {
                    mCanvas.drawText(Integer.toString(i + 1), textPointArrayList.get(i).getX(), textPointArrayList.get(i).getY(), textPaint);
                    mCanvas.drawPath(pathArrayList.get(i), pencilPaint);
                    isHideSample = false;
                    invalidate();
                }
            }
        }
    }

    public void setErase() {
        if (!isErase) {
            isErase = true;
            invalidate();
            // mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            mPaint.setColor(Color.TRANSPARENT);
        } else {
            invalidate();
            mPaint.setColor(Color.parseColor("#3367D6"));
            isErase = false;
        }
    }
}
