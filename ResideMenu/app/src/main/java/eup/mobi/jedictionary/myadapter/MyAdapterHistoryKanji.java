package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.history.HistoryKanji;
import eup.mobi.jedictionary.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by nguye on 8/21/2015.
 */
public class MyAdapterHistoryKanji extends
        ArrayAdapter<HistoryKanji> {
    Activity context = null;
    ArrayList<HistoryKanji> myArray = null;
    int layoutId;
    LayoutInflater inflater;

    public MyAdapterHistoryKanji(Activity context,
                                 int layoutId,
                                 ArrayList<HistoryKanji> arr) {
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_date = (TextView) convertView.findViewById(R.id.tv_hdate);
            holder.tv_word = (TextView) convertView.findViewById(R.id.tv_history);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final HistoryKanji historyWord = myArray.get(position);
        Date hDate = new Date(historyWord.gethDate());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, HH:mm");
        String stringDate = simpleDateFormat.format(hDate);
        holder.tv_date.setText(stringDate);
        holder.tv_word.setText(historyWord.gethWords());
        return convertView;
    }

    class ViewHolder {
        TextView tv_word, tv_date;
    }
}
