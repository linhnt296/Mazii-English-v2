package eup.mobi.jedictionary.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import eup.mobi.jedictionary.google.Track;
import eup.mobi.jedictionary.history.HistoryCau;
import eup.mobi.jedictionary.history.HistoryGrammar;
import eup.mobi.jedictionary.history.HistoryKanji;
import eup.mobi.jedictionary.history.HistoryWord;
import eup.mobi.jedictionary.module.Language;
import eup.mobi.jedictionary.module.WanaKanaJava;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by nguye on 8/21/2015.
 */

public class MyDatabase extends SQLiteOpenHelper {
    Context context;
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "jaen.db";

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
//        addRememberIfNotExists();
    }

//    private void addRememberIfNotExists() {
//        try {
//            SQLiteDatabase db = getWritableDatabase();
//            db.execSQL("alter table grammar add column remember integer");
//        } catch (SQLException e) {
//
//        }
//    }

    public void onCreate(SQLiteDatabase db) {
//            db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
//            db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    //get jaen
    public ArrayList<Jaen> getJaen(String mSearch) {
        ArrayList<Jaen> listJaen = new ArrayList<>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Jaen mJaen;
                String mWord;
                String mMean;
                String mPhonetic;
                int mFavorite, mID;
                EditText editText = null;
                WanaKanaJava wanaKanaJava = new WanaKanaJava(editText, false);
                mSearch = mSearch.replace("\'", "");
                String convert_query = wanaKanaJava.toKana(mSearch);
                String query = "select * from jaen_fts where jaen_fts MATCH '" + convert_query + "*' ORDER BY LENGTH(word) LIMIT 50";
                Cursor cursor = db.rawQuery(query, null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    mID = cursor.getInt(cursor.getColumnIndex("id"));
                    mWord = cursor.getString(cursor.getColumnIndex("word"));
                    if (mWord == null) {
                        mWord = "";
                    }
                    mPhonetic = cursor.getString(cursor.getColumnIndex("phonetic"));
                    if (mPhonetic == null) {
                        mPhonetic = "";
                    }
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));

                    mJaen = new Jaen(mID, mPhonetic, mWord, mFavorite, mMean);
                    if (mPhonetic.equals(convert_query) || mWord.equals(convert_query)) {
                        listJaen.add(0, mJaen);
                        Log.d("Suggest jaen", mSearch + " " + mJaen.getmPhonetic() + "-" + mJaen.getmWord());
                    } else
                        listJaen.add(mJaen);

                    cursor.moveToNext();
                }
                cursor.close();

            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }

        return listJaen;
    }

    //get jaen
    public ArrayList<Jaen> getJaenSuggest(String mSearch) {
        ArrayList<Jaen> listJaen = new ArrayList<Jaen>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Jaen mJaen;
                String mWord;
                String mMean;
                String mPhonetic;
                int mFavorite, mID;
                EditText editText = null;
                WanaKanaJava wanaKanaJava = new WanaKanaJava(editText, false);
                mSearch = mSearch.replace("\'", "");
                String query = "select * from jaen_fts where jaen_fts MATCH '" + wanaKanaJava.toKana(mSearch) + "*' ORDER BY LENGTH(word) LIMIT 50";
                Cursor cursor = db.rawQuery(query, null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    mID = cursor.getInt(cursor.getColumnIndex("id"));
                    mWord = cursor.getString(cursor.getColumnIndex("word"));
                    if (mWord == null) {
                        mWord = "";
                    }
                    mPhonetic = cursor.getString(cursor.getColumnIndex("phonetic"));
                    if (mPhonetic == null) {
                        mPhonetic = "";
                    }
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));

                    mJaen = new Jaen(mID, mPhonetic, mWord, mFavorite, mMean);

                    if (mPhonetic.equals(wanaKanaJava.toKana(mSearch)) || mWord.equals(wanaKanaJava.toKana(mSearch))) {
                        listJaen.add(0, mJaen);
                        Log.d("Suggest jaen", wanaKanaJava.toKana(mSearch) + " " + mJaen.getmPhonetic() + "-" + mJaen.getmWord());
                    } else
                        listJaen.add(mJaen);
                    cursor.moveToNext();
                }
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }

        }
        return listJaen;
    }

    public Example getExample(String mID) {
        SQLiteDatabase db = null;
        Example mExample = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
//        int mId;
            try {
                String mContent;
                String mMean;
                String mTrans;
                int id;
//        int mLevel;

                String query = "SELECT * FROM exam_fts where id =" + mID + "";
                Cursor cursor = db.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    id = cursor.getInt(cursor.getColumnIndex("id"));
                    mContent = cursor.getString(cursor.getColumnIndex("jap"));
                    mMean = cursor.getString(cursor.getColumnIndex("eng"));
                    mExample = new Example(id, mContent, mMean);
                    cursor.moveToNext();
                }

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }

        }
        return mExample;
    }

    //lay vi du khi tra cuu tu
    public ArrayList<Example> qExampleWord(String word, String phonetic) {
        String query = "select * from exam_fts where exam_fts match '" + word + "* OR " + phonetic + "*' ORDER BY RANDOM() limit 5";
        return qExample(query);
    }

    public ArrayList<Example> getTracuuCau(String mQuery) {
        EditText editText = null;
        WanaKanaJava wanaKanaJava = new WanaKanaJava(editText, false);
//        String query = "select * from exam_fts where exam_fts match '*" + mQuery + "* OR *" + wanaKanaJava.toKana(mQuery) + "*' ORDER BY RANDOM() limit 50";
        String query = "select * from exam_fts where eng like '%" + mQuery + "%' OR jap like '%" + wanaKanaJava.toKana(mQuery) + "%' ORDER BY RANDOM() limit 50";
        return qExample(query);
    }

    private ArrayList<Example> qExample(String query) {
        SQLiteDatabase db = null;
        ArrayList<Example> arrayList = new ArrayList<>();
        Example mExample = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
//        int mId;
            try {
                String mContent;
                String mMean;
                String mTrans;
//                if (Language.isJapanese(mQuery)) {
//                    query = "SELECT * FROM exam_fts WHERE content LIKE '%" + mQuery + "%' OR mean LIKE '%" + mQuery + "%' OR trans LIKE '%" + mQuery + "%' LIMIT 50";
//                } else {
////                query = "SELECT * FROM example WHERE example MATCH '*" + wanaKanaJava.toKana(mQuery) + "*' LIMIT 50";
//                    query = "SELECT * FROM example WHERE content LIKE '%" + wanaKanaJava.toKana(mQuery) + "%' OR mean LIKE '%" + wanaKanaJava.toKana(mQuery) + "%' OR trans LIKE '%" + wanaKanaJava.toKana(mQuery) + "%' LIMIT 50";
//                }
                Cursor cursor = db.rawQuery(query, null);
                cursor.moveToFirst();
                int id;
                while (!cursor.isAfterLast()) {
                    id = cursor.getInt(cursor.getColumnIndex("id"));
                    mMean = cursor.getString(cursor.getColumnIndex("eng"));
                    mContent = cursor.getString(cursor.getColumnIndex("jap"));
                    mExample = new Example(id, mContent, mMean);
                    arrayList.add(mExample);
                    cursor.moveToNext();
                }

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return arrayList;
    }

    public ArrayList<Kanji> getKanji(String mQurey) {
        ArrayList<Kanji> listKanji = new ArrayList<Kanji>();
        ArrayList<Kanji> finalList = new ArrayList<Kanji>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Kanji Kanji = null;
                int mId = -1;
                String mKanji;
                String mMean;
                int mLevel;
                String mOn;
                String mKun;
                String mImg;
                String mDetail;
                String mShort;
                int mFreq;
                String mComp;
                int mStroke_count;
                String mCompdetail;
                String mExamples;
                int mFavorite;
                int mRemember;
                ArrayList<String> mSort = new ArrayList<>();
//        int mLevel;
                mQurey = mQurey.replace("\'", "");
                if (Language.isJapanese(mQurey)) {
                    String[] sortArr = mQurey.split("");
                    for (int s = 0; s < sortArr.length - 1; s++) {
                        if (Language.isKanji(sortArr[s + 1])) {
                            mSort.add(sortArr[s + 1]);
                        }
                    }
                }
                String query = searchMultiKanji(mQurey);
//        String query = "SELECT * FROM kanji where kanji like '" + mQurey + "%' or mean like '%" + mQurey.toUpperCase() + "%' ORDER BY LENGTH(kanji)";
                Cursor cursor = db.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    mId = cursor.getInt(cursor.getColumnIndex("id"));
                    mKanji = cursor.getString(cursor.getColumnIndex("kanji"));
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mLevel = cursor.getInt(cursor.getColumnIndex("jlpt"));
                    mOn = cursor.getString(cursor.getColumnIndex("on"));
                    mKun = cursor.getString(cursor.getColumnIndex("kun"));
                    mFreq = cursor.getInt(cursor.getColumnIndex("seq"));
                    mStroke_count = cursor.getInt(cursor.getColumnIndex("stroke"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));
                    mRemember = cursor.getInt(cursor.getColumnIndex("remember"));

                    Kanji = new Kanji(mId, mKanji, mMean, mLevel, mOn, mKun, mFreq, mStroke_count, mFavorite, mRemember);
                    listKanji.add(Kanji);
                    cursor.moveToNext();
                }
                cursor.close();

                if (mSort != null && !mSort.isEmpty() && listKanji != null) {
                    for (String q : mSort) {
                        for (Kanji kanji : listKanji) {
                            if (q.equals(kanji.getmKanji())) {
                                finalList.add(kanji);
                            }
                        }
                    }
                } else finalList = listKanji;
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }

        return finalList;
    }

    //get list tab 3 Grammar
    public ArrayList<Grammar> getGrammar(String mSearch) {
        ArrayList<Grammar> listGrammar = new ArrayList<Grammar>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Grammar mGrammar;
                String mStruct, mStruct_vi, mDetail, mLevel;
                int mFavorite, mID;
                EditText editText = null;
                String query = "";
                WanaKanaJava wanaKanaJava = new WanaKanaJava(editText, false);
                mSearch = mSearch.replace("\'", "");
                if (Language.isVietnamese(mSearch)) {
                    query = "select * from grammar where struct like '%" + mSearch + "%' or struct_vi like '%" + mSearch + "%' ORDER BY LENGTH(struct) LIMIT 20";
                } else {
                    query = "select * from grammar where struct like '%" + wanaKanaJava.toKana(mSearch) + "%' or struct_vi like '%" + mSearch + "%' ORDER BY LENGTH(struct) LIMIT 20";
                }
                Cursor cursor = db.rawQuery(query, null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    mID = cursor.getInt(cursor.getColumnIndex("id"));
                    mStruct = cursor.getString(cursor.getColumnIndex("struct"));
                    mDetail = cursor.getString(cursor.getColumnIndex("detail"));
                    mLevel = cursor.getString(cursor.getColumnIndex("level"));
                    mStruct_vi = cursor.getString(cursor.getColumnIndex("struct_vi"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));


                    mGrammar = new Grammar(mID, mStruct, mDetail, mLevel, mStruct_vi, mFavorite);

                    listGrammar.add(mGrammar);
                    cursor.moveToNext();
                }

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listGrammar;
    }

    //yeu thich
    public ArrayList<Jaen> getJaenYeuThich() {
        ArrayList<Jaen> listJaen = new ArrayList<Jaen>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Jaen mJaen;
                String mWord;
                String mMean, mPhonetic;
                int mFavorite, mID;

                String query_ja = "SELECT * FROM jaen_fts where favorite =1";
                Cursor cursor_ja = db.rawQuery(query_ja, null);

                cursor_ja.moveToFirst();

                while (!cursor_ja.isAfterLast()) {

                    mID = cursor_ja.getInt(cursor_ja.getColumnIndex("id"));
                    mWord = cursor_ja.getString(cursor_ja.getColumnIndex("word"));
                    mMean = cursor_ja.getString(cursor_ja.getColumnIndex("mean"));
                    mFavorite = cursor_ja.getInt(cursor_ja.getColumnIndex("favorite"));
                    mPhonetic = cursor_ja.getString(cursor_ja.getColumnIndex("phonetic"));

                    mJaen = new Jaen(mID, mPhonetic, mWord, mFavorite, mMean);

                    listJaen.add(mJaen);
                    cursor_ja.moveToNext();
                }

                cursor_ja.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listJaen;
    }

    // yeu thich kanji
    public ArrayList<Kanji> getYeuThichKanji() {
//        ArrayList<Example>
        ArrayList<Kanji> listKanji = new ArrayList<Kanji>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Kanji Kanji = null;
                int mId;
                String mKanji;
                String mMean;
                int mLevel;
                String mOn;
                String mKun;
                String mImg;
                String mDetail;
                String mShort;
                int mFreq;
                String mComp;
                int mStroke_count;
                String mCompdetail;
                String mExamples;
                int mFavorite, mRemember;
//        int mLevel;
                String query = "SELECT * FROM kanji_fts where favorite=1";
                Cursor cursor = db.rawQuery(query, null);
                cursor.moveToFirst();

                while (!cursor.isAfterLast()) {
                    mId = cursor.getInt(cursor.getColumnIndex("id"));
                    mKanji = cursor.getString(cursor.getColumnIndex("kanji"));
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mLevel = cursor.getInt(cursor.getColumnIndex("jlpt"));
                    mOn = cursor.getString(cursor.getColumnIndex("on"));
                    mKun = cursor.getString(cursor.getColumnIndex("kun"));
                    mFreq = cursor.getInt(cursor.getColumnIndex("seq"));
                    mStroke_count = cursor.getInt(cursor.getColumnIndex("stroke"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));
                    mRemember = cursor.getInt(cursor.getColumnIndex("remember"));

                    Kanji = new Kanji(mId, mKanji, mMean, mLevel, mOn, mKun, mFreq, mStroke_count, mFavorite, mRemember);
                    listKanji.add(Kanji);
                    cursor.moveToNext();
                }
                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listKanji;
    }

    public void setFavoriteOn_tu(String mID) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE jaen_fts SET favorite= 1 WHERE id= " + mID;
            db.execSQL(sql_Query1);
        }
    }

    public void setFavoriteOff_tu(String mID) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE jaen_fts SET favorite= 0 WHERE id= " + mID;
            db.execSQL(sql_Query1);
        }
    }

    public void setAllFavoriteOff_tu() {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query2 = "UPDATE jaen_fts SET favorite= 0 WHERE favorite= 1";
            db.execSQL(sql_Query2);
        }
    }

    public void setFavoriteOn_hantu(String mID) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE kanji_fts SET favorite= 1 WHERE id= " + mID;
            db.execSQL(sql_Query1);
        }
    }

    public void setFavoriteOff_hantu(String mID) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE kanji_fts SET favorite= 0 WHERE id= " + mID;
            db.execSQL(sql_Query1);
        }
    }

    public void setAllFavoriteOff_hantu() {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE kanji_fts SET favorite= 0 WHERE favorite= 1";
            db.execSQL(sql_Query1);
        }
    }

    public void setFavoriteOn_nguphap(String mID) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE grammar SET favorite= 1 WHERE id= " + mID;
            db.execSQL(sql_Query1);
        }
    }

    public void setFavoriteOff_nguphap(String mID) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE grammar SET favorite= 0 WHERE id= " + mID;
            db.execSQL(sql_Query1);
        }
    }

    public void setAllFavoriteOff_nguphap() {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE grammar SET favorite= 0 WHERE favorite= 1";
            db.execSQL(sql_Query1);
        }
    }

    // get tu loai
    public ArrayList<Jaen> getTuLoaiJaen(String mSearch) {
        ArrayList<Jaen> listJaen = new ArrayList<Jaen>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Jaen mJaen;
                String mWord;
                String mMean;
                String mPhonetic;
                int mFavorite, mID;

                mSearch = mSearch.replace("\'", "");
                String query = "SELECT * FROM jaen_fts WHERE mean LIKE '%\"kind\":\"%" + mSearch + "\"%'";

                Cursor cursor = db.rawQuery(query, null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    mID = cursor.getInt(cursor.getColumnIndex("id"));
                    mWord = cursor.getString(cursor.getColumnIndex("word"));
                    mPhonetic = cursor.getString(cursor.getColumnIndex("phonetic"));
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));

                    mJaen = new Jaen(mID, mPhonetic, mWord, mFavorite, mMean);

                    listJaen.add(mJaen);
                    cursor.moveToNext();
                }
                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listJaen;
    }

    // get tu loai
    public ArrayList<Jaen> getChuyenNganhJaen(String mSearch) {
        ArrayList<Jaen> listJaen = new ArrayList<Jaen>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Jaen mJaen;
                String mWord;
                String mMean;
                String mPhonetic;
                int mFavorite, mID;

                mSearch = mSearch.replace("\'", "");
                String query = "SELECT * FROM jaen_fts WHERE mean LIKE '%\"field\":\"" + mSearch + "\"%'";
                Cursor cursor = db.rawQuery(query, null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    mID = cursor.getInt(cursor.getColumnIndex("id"));
                    mWord = cursor.getString(cursor.getColumnIndex("word"));
                    mPhonetic = cursor.getString(cursor.getColumnIndex("phonetic"));
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));

                    mJaen = new Jaen(mID, mPhonetic, mWord, mFavorite, mMean);

                    listJaen.add(mJaen);
                    cursor.moveToNext();
                }
                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listJaen;
    }

    // JLPT
    public ArrayList<Kanji> getKanjiJLPT(String mQurey) {

//        ArrayList<Example>
        ArrayList<Kanji> listKanji = new ArrayList<Kanji>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Kanji Kanji = null;
                int mId;
                String mKanji;
                String mMean;
                int mLevel;
                String mOn;
                String mKun;
                String mImg;
                String mDetail;
                String mShort;
                int mFreq;
                String mComp;
                int mStroke_count;
                String mCompdetail;
                String mExamples;
                int mFavorite, mRemember;
//        int mLevel;
                mQurey = mQurey.replace("N", "");
                String query = "SELECT * FROM kanji_fts where jlpt =" + mQurey;
                Cursor cursor = db.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    mId = cursor.getInt(cursor.getColumnIndex("id"));
                    mKanji = cursor.getString(cursor.getColumnIndex("kanji"));
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mLevel = cursor.getInt(cursor.getColumnIndex("jlpt"));
                    mOn = cursor.getString(cursor.getColumnIndex("on"));
                    mKun = cursor.getString(cursor.getColumnIndex("kun"));
                    mFreq = cursor.getInt(cursor.getColumnIndex("seq"));
                    mStroke_count = cursor.getInt(cursor.getColumnIndex("stroke"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));
                    mRemember = cursor.getInt(cursor.getColumnIndex("remember"));

                    Kanji = new Kanji(mId, mKanji, mMean, mLevel, mOn, mKun, mFreq, mStroke_count, mFavorite, mRemember);
                    listKanji.add(Kanji);
                    cursor.moveToNext();
                }
                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listKanji;
    }

    public Jaen getWordJLPT(String mSearch) {
        Jaen jaen = null;
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {

                String mWord, mMean, mPhonetic;
                int mFavorite, mID;
                String query = "select * from jaen_fts where word MATCH '" + mSearch + "'";
                Cursor cursor = db.rawQuery(query, null);

                cursor.moveToFirst();
                mID = cursor.getInt(cursor.getColumnIndex("id"));
                mWord = cursor.getString(cursor.getColumnIndex("word"));
                mMean = cursor.getString(cursor.getColumnIndex("mean"));
                mPhonetic = cursor.getString(cursor.getColumnIndex("phonetic"));
                mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));

                jaen = new Jaen(mID, mPhonetic, mWord, mFavorite, mMean);

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return jaen;
    }

    public void getIndex() {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {

                String sql_CREATE_h_word = "CREATE TABLE IF NOT EXISTS \"h_word\" (\"date\" DOUBLE PRIMARY KEY  NOT NULL , \"word\" TEXT NOT NULL )";
                String sql_CREATE_h_kanji = "CREATE TABLE IF NOT EXISTS \"h_kanji\" (\"date\" DOUBLE PRIMARY KEY  NOT NULL , \"kanji\" TEXT NOT NULL )";
                String sql_CREATE_h_grammar = "CREATE TABLE IF NOT EXISTS \"h_grammar\" (\"date\" DOUBLE PRIMARY KEY  NOT NULL , \"grammar\" TEXT NOT NULL )";
                String sql_CREATE_h_cau = "CREATE TABLE IF NOT EXISTS \"h_cau\" (\"date\" DOUBLE PRIMARY KEY  NOT NULL , \"cau\" TEXT NOT NULL )";
                String sql_CREATE_news_offline = "CREATE TABLE IF NOT EXISTS \"main\".\"news_offline\" (\"id\" TEXT PRIMARY KEY  NOT NULL , \"json\" TEXT NOT NULL , \"pubdate\" DOUBLE NOT NULL )";
                String sql_CREATE_news_readed = "CREATE  TABLE  IF NOT EXISTS \"main\".\"news_readed\" (\"id\" TEXT PRIMARY KEY  NOT NULL , \"date\" DATETIME DEFAULT CURRENT_DATE)";
//            String sql_CREATE_category = "CREATE TABLE \"category\" (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"name\" TEXT NOT NULL , \"date\" DATETIME DEFAULT CURRENT_DATE )";
//            String sql_CREATE_entry = "CREATE TABLE \"entry\" (\"id\" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , \"id_category\" INTEGER NOT NULL , \"id_entry\" INTEGER NOT NULL , \"type\" TEXT NOT NULL , \"word\" TEXT NOT NULL , \"des\" TEXT NOT NULL , \"date\" DATETIME DEFAULT CURRENT_DATE )";
                db.execSQL(sql_CREATE_h_word);
                db.execSQL(sql_CREATE_h_kanji);
                db.execSQL(sql_CREATE_h_grammar);
                db.execSQL(sql_CREATE_h_cau);
                db.execSQL(sql_CREATE_news_offline);
                db.execSQL(sql_CREATE_news_readed);
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }

        }
    }

    private String searchMultiKanji(String mQurey) {

        String multiSearch = "";
        if (Language.isJapanese(mQurey) && mQurey.length() > 0) {
            multiSearch = multiSearch.concat("kanji match '").concat(String.valueOf(mQurey.charAt(0)));
            for (int i = 1; i < mQurey.length(); i++) {
                multiSearch = multiSearch.concat(" OR ").concat(String.valueOf(mQurey.charAt(i)));
            }
            multiSearch = multiSearch.concat("'");
        } else if (mQurey.length() > 0) {
            multiSearch = "mean match '" + mQurey + "*' LIMIT 20";
        }

        String finalQuery = "SELECT * FROM kanji_fts WHERE ".concat(multiSearch);
//        Log.d("finalQuery", finalQuery);
        return finalQuery;
    }

// get Array List History

    public ArrayList<HistoryWord> getHistoryWord() {
        ArrayList<HistoryWord> listHistory = new ArrayList<HistoryWord>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                HistoryWord historyWord;
                String hWord;
                long hDate;
                String query = "select * from h_word ORDER BY date DESC LIMIT 50";
                Cursor cursor = db.rawQuery(query, null);


                cursor.moveToFirst();
//        int count = 0;
                while (!cursor.isAfterLast()) {

                    hWord = cursor.getString(cursor.getColumnIndex("word"));
                    hDate = cursor.getLong(cursor.getColumnIndex("date"));
                    historyWord = new HistoryWord(hWord, hDate);

                    listHistory.add(historyWord);
                    cursor.moveToNext();
//            count++;
                }
                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listHistory;
    }

    public ArrayList<HistoryKanji> getHistoryKanji() {
        ArrayList<HistoryKanji> listHistory = new ArrayList<HistoryKanji>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                HistoryKanji historyKanji;
                String hKanji;
                long hDate;
                String query = "select * from h_kanji ORDER BY date DESC LIMIT 50";
                Cursor cursor = db.rawQuery(query, null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    hKanji = cursor.getString(cursor.getColumnIndex("kanji"));
                    hDate = cursor.getLong(cursor.getColumnIndex("date"));
                    historyKanji = new HistoryKanji(hKanji, hDate);

                    listHistory.add(historyKanji);
                    cursor.moveToNext();
                }

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listHistory;
    }

    public ArrayList<HistoryGrammar> getHistoryGrammar() {
        ArrayList<HistoryGrammar> listHistory = new ArrayList<HistoryGrammar>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                HistoryGrammar historyGrammar;
                String hWord;
                long hDate;
                String query = "select * from h_grammar ORDER BY date DESC LIMIT 50";
                Cursor cursor = db.rawQuery(query, null);


                cursor.moveToFirst();
//        int count = 0;
                while (!cursor.isAfterLast()) {

                    hWord = cursor.getString(cursor.getColumnIndex("grammar"));
                    hDate = cursor.getLong(cursor.getColumnIndex("date"));
                    historyGrammar = new HistoryGrammar(hWord, hDate);

                    listHistory.add(historyGrammar);
                    cursor.moveToNext();
//            count++;
                }

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listHistory;
    }

    public ArrayList<HistoryCau> getHistoryCau() {
        ArrayList<HistoryCau> listHistory = new ArrayList<HistoryCau>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                HistoryCau historyCau;
                String hWord;
                long hDate;
                String query = "select * from h_cau ORDER BY date DESC LIMIT 50";
                Cursor cursor = db.rawQuery(query, null);


                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    hWord = cursor.getString(cursor.getColumnIndex("cau"));
                    hDate = cursor.getLong(cursor.getColumnIndex("date"));
                    historyCau = new HistoryCau(hWord, hDate);

                    listHistory.add(historyCau);
                    cursor.moveToNext();
                }
                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listHistory;
    }

    //Add history
    public void AddHistoryWord(String hWord, long hDate) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "INSERT INTO h_word (date, word)  \n" +
                    "VALUES (" + hDate + ",'" + hWord + "')";
            db.execSQL(sql_Query1);
            Log.d("insert Word History", "Done!");
        }
    }

    public void AddHistoryKanji(String hKanji, long hDate) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "INSERT INTO h_kanji (date, kanji)  \n" +
                    "VALUES (" + hDate + ",'" + hKanji + "')";
            db.execSQL(sql_Query1);
            Log.d("insert Kanji History", "Done!");
        }
    }

    public void AddHistoryGrammar(String hGrammar, long hDate) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "INSERT INTO h_grammar (date, grammar)  \n" +
                    "VALUES (" + hDate + ",'" + hGrammar + "')";
            db.execSQL(sql_Query1);
            Log.d("insert Kanji History", "Done!");
        }
    }

    //Add history
    public void AddHistoryCau(String hWord, long hDate) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "INSERT INTO h_cau (date, cau)  \n" +
                    "VALUES (" + hDate + ",'" + hWord + "')";
            db.execSQL(sql_Query1);
            Log.d("insert Cau History", "Done!");
        }
    }

    //Remove history
    public void RemoveHistoryWord(long hDate) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "DELETE FROM h_word WHERE date = " + hDate;
            db.execSQL(sql_Query1);
        }
    }

    public void RemoveHistoryKanji(long hDate) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "DELETE FROM h_kanji WHERE date = " + hDate;
            db.execSQL(sql_Query1);
        }
    }

    public void RemoveHistoryGrammar(long hDate) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "DELETE FROM h_grammar WHERE date = " + hDate;
            db.execSQL(sql_Query1);
        }
    }

    public void RemoveHistoryCau(long hDate) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "DELETE FROM h_cau WHERE date = " + hDate;
            db.execSQL(sql_Query1);
        }
    }

    // Delete all history
    public void RemoveAllHWord() {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "DELETE FROM h_word";
            db.execSQL(sql_Query1);
        }
    }

    public void RemoveAllHKanji() {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "DELETE FROM h_kanji";
            db.execSQL(sql_Query1);
        }
    }

    public void RemoveAllHGrammar() {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "DELETE FROM h_grammar";
            db.execSQL(sql_Query1);
        }
    }

    public void RemoveAllHCau() {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "DELETE FROM h_cau";
            db.execSQL(sql_Query1);
        }
    }

    //todo Create table news_offline
    public void insertNewsOffline(String mId, String mJson, String pubDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = null;
        try {
            newDate = format.parse(pubDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long longDate = newDate.getTime();
        SQLiteDatabase database = null;
        try {
            database = this.getWritableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (database != null) {
            ContentValues values = new ContentValues();
            values.put("id", mId);
            values.put("json", mJson);
            values.put("pubdate", longDate);
            database.insert("news_offline", null, values);
//            database.insertWithOnConflict("news_offline", null, values, SQLiteDatabase.CONFLICT_IGNORE);
            database.close();
        }
    }

    public boolean checkIdNewsOffline(String id) {
        boolean isCreated = false;
        String mID = "";
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String query = "SELECT * FROM news_offline where id ='" + id + "'";
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                mID = cursor.getString(cursor.getColumnIndex("id"));
                Log.d("newsOff", mID);
                cursor.moveToNext();
            }
            cursor.close();
            if (!mID.equals("")) {
                isCreated = true;
            }
        }
        return isCreated;
    }

    //load news off from database
    public ArrayList<NewsOffline> loadNewsOffFromDB() {
        ArrayList<NewsOffline> listNews = new ArrayList<NewsOffline>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            NewsOffline mNews;
            String mTitle = null, mDes = null, mHtml = null, mJson, mTextBody = null, mTextMore = null;
            String mPubdate = null, newsPubdate = null;

            String query = "SELECT * FROM news_offline ORDER BY (pubdate) LIMIT 10";

            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {

                mJson = cursor.getString(cursor.getColumnIndex("json"));
                JSONObject jsObject = null;
                try {
                    jsObject = new JSONObject(mJson);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jsObject.has("result")) {

                    try {
                        JSONObject jsResult = jsObject.getJSONObject("result");
                        if (jsResult.has("title")) {
                            mTitle = jsResult.getString("title");
                        }
                        if (jsResult.has("pubDate")) {
                            newsPubdate = jsResult.getString("pubDate");
//                        11月12日17時20分
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            Date newDate = null;
                            try {
                                newDate = format.parse(newsPubdate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            format = new SimpleDateFormat("MM月dd日HH時mm分");
                            mPubdate = format.format(newDate);
                        }
                        if (jsResult.has("description")) {
                            mDes = jsResult.getString("description");
                        }
                        if (jsResult.has("content")) {
                            JSONObject jsContent = jsResult.getJSONObject("content");
                            if (jsContent.has("textbody")) {
                                mTextBody = jsContent.getString("textbody");
                            }
                            if (jsContent.has("textmore")) {
                                mTextMore = jsContent.getString("textmore");
                            }
                            mHtml = "<style>\n" +
                                    "\n" +
                                    "body {\n" +
                                    "   font-size: 18px;\n" +
                                    "   font-family: Roboto, serif\n" +
                                    "}\n" +
                                    "\n" +
                                    ".title {\n" +
                                    "   font-size: 20px;\n" +
                                    "   font-weight: 600;\n" +
                                    "}\n" +
                                    "\n" +
                                    ".description {\n" +
                                    "   font-size: 14px;\n" +
                                    "}\n" +
                                    "\n" +
                                    ".description p {\n" +
                                    "font-size: 14px;\n" +
                                    "}\n" +
                                    "\n" +
                                    ".date-time {\n" +
                                    "  font-size: 12px;\n" +
                                    "  color: grey;\n" +
                                    "  font-style: italic;\n" +
                                    "}\n" +
                                    "\n" +
                                    ".nhk {\n" +
                                    "  font-size: 12px;\n" +
                                    "  color: blue;\n" +
                                    "  font-style: italic;\n" +
                                    "  text-align: right;" +
                                    "}\n" +
                                    "\n" +
                                    ".main-content {\n" +
                                    "  font-size: 18px;\n" +
                                    "}\n" +
                                    "\n" +
                                    ".main-content p {\n" +
                                    "  line-height: 36px;\n" +
                                    "}\n" +
                                    "\n" +
                                    "</style>" +
                                    "<div class=\"title\">" + mTitle + "</div>" +
//                                    "<div class=\"description\">" + mDes + "</div>" +
                                    "<div class=\"date-time\">" + mPubdate + "</div>" +
                                    "<div class=\"main-content\">" + mTextBody + "</div>" +
                                    "<div class=\"main-content\">" + mTextMore + "</div>" +
                                    "<div class=\"nhk\">ソース：NHK　ニュース</div>";
//                            Log.d("Docbao", mHtml);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                mNews = new NewsOffline(mTitle, mDes, newsPubdate, mHtml);
                listNews.add(mNews);
                cursor.moveToNext();
            }

            cursor.close();
        }
        return listNews;
    }

    public void setRememberOn(String mWord) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE kanji_fts SET remember= 1 WHERE kanji= '" + mWord + "'";
            db.execSQL(sql_Query1);
        }
    }

    public void setRememberOff(String mWord) {
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "UPDATE kanji_fts SET remember= 0 WHERE kanji= '" + mWord + "'";
            db.execSQL(sql_Query1);
        }
    }

    public ArrayList<Kanji> getRandomKanji() {
        ArrayList<Kanji> listKanji = new ArrayList<Kanji>();
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                Kanji Kanji = null;
                int mId;
                String mKanji;
                String mMean;
                int mLevel;
                String mOn;
                String mKun;
                String mImg;
                String mDetail;
                String mShort;
                int mFreq;
                String mComp;
                int mStroke_count;
                String mCompdetail;
                String mExamples;
                int mFavorite;
                int mRemember;
//        int mLevel;

                String query = "SELECT * FROM kanji_fts where jlpt = 5 ORDER BY RANDOM() LIMIT 10";
                Cursor cursor = db.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    mId = cursor.getInt(cursor.getColumnIndex("id"));
                    mKanji = cursor.getString(cursor.getColumnIndex("kanji"));
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mLevel = cursor.getInt(cursor.getColumnIndex("jlpt"));
                    mOn = cursor.getString(cursor.getColumnIndex("on"));
                    mKun = cursor.getString(cursor.getColumnIndex("kun"));
                    mFreq = cursor.getInt(cursor.getColumnIndex("seq"));
                    mStroke_count = cursor.getInt(cursor.getColumnIndex("stroke"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));
                    mRemember = cursor.getInt(cursor.getColumnIndex("remember"));

                    Kanji = new Kanji(mId, mKanji, mMean, mLevel, mOn, mKun, mFreq, mStroke_count, mFavorite, mRemember);
                    listKanji.add(Kanji);
                    cursor.moveToNext();
                }

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return listKanji;
    }

    public Jaen getJaenMyWord(String id) {
        Jaen jaen = null;
        SQLiteDatabase db = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                String mWord, mMean, mPhonetic;
                int mFavorite, mID;
                String query = "select * from jaen_fts where id MATCH " + id;
                Cursor cursor = db.rawQuery(query, null);

                cursor.moveToFirst();
                mID = cursor.getInt(cursor.getColumnIndex("id"));
                mWord = cursor.getString(cursor.getColumnIndex("word"));
                mMean = cursor.getString(cursor.getColumnIndex("mean"));
                mPhonetic = cursor.getString(cursor.getColumnIndex("phonetic"));
                mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));

                jaen = new Jaen(mID, mPhonetic, mWord, mFavorite, mMean);

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return jaen;
    }

    public Kanji getKanjiMyWord(String id) {
        SQLiteDatabase db = null;
        Kanji kanji = null;
        try {
            db = getReadableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            try {
                int mId;
                String mKanji;
                String mMean;
                int mLevel;
                String mOn;
                String mKun;
                String mImg = null;
                String mDetail = null;
                String mShort = null;
                int mFreq = 0;
                String mComp = null;
                int mStroke_count;
                String mCompdetail = null;
                String mExamples = null;
                int mFavorite;
                int mRemember;

                String query = "SELECT * FROM kanji_fts where id = " + id;
                Cursor cursor = db.rawQuery(query, null);
                cursor.moveToFirst();
                mId = cursor.getInt(cursor.getColumnIndex("id"));
                mKanji = cursor.getString(cursor.getColumnIndex("kanji"));
                mMean = cursor.getString(cursor.getColumnIndex("mean"));
                mLevel = cursor.getInt(cursor.getColumnIndex("jlpt"));
                mOn = cursor.getString(cursor.getColumnIndex("on"));
                mKun = cursor.getString(cursor.getColumnIndex("kun"));
                mStroke_count = cursor.getInt(cursor.getColumnIndex("stroke"));
                mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));
                mRemember = cursor.getInt(cursor.getColumnIndex("remember"));

                kanji = new Kanji(mId, mKanji, mMean, mLevel, mOn, mKun, mImg, mDetail, mShort, mFreq, mComp, mStroke_count, mCompdetail, mExamples, mFavorite, mRemember);

                cursor.close();
            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }

        }
        return kanji;
    }

    public void updateRememberOff(int param) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "update grammar set remember = 0 where id = " + param;
            db.execSQL(sql_Query1);
            Log.d("Remember_G", "OFF");
        }
    }

    public void updateRememberOn(int param) {
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();

        } catch (Exception e) {
            Log.d("get Database Error", String.valueOf(e));
        }
        if (db != null) {
            String sql_Query1 = "update grammar set remember = 1 where id = " + param;
            db.execSQL(sql_Query1);
            Log.d("Remember_G", "ON");
        }
    }

    public boolean checkDataNull() {
        try {
            SQLiteDatabase db = getReadableDatabase();
            String query = "select * from jaen_fts LIMIT 3";
            Log.d("mQuery", query);
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                cursor.close();
                return true;
            }
            cursor.close();
            return false;
        } catch (Exception e) {
            return false;
        }

    }

    public boolean checkHistory() {
        try {
            SQLiteDatabase db = getReadableDatabase();
            String query = "select * from h_word LIMIT 3";
            Log.d("mQuery", query);
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                cursor.close();
                return true;
            }
            cursor.close();
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public ArrayList<Jaen> qExampleKanji(String word) {
        ArrayList<Jaen> jaenArrayList = new ArrayList<>();
        if (TextUtils.isEmpty(word)) return jaenArrayList;
        SQLiteDatabase db = getReadableDatabase();
        if (db != null) {
            try {
                Jaen mJaen;
                String mWord;
                String mMean;
                String mPhonetic;
                int mFavorite, mID;

                String q = "select * from jaen_fts where word match '" + word + "*' ORDER BY RANDOM() limit 5";
                Cursor cursor = db.rawQuery(q, null);

                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {

                    mID = cursor.getInt(cursor.getColumnIndex("id"));
                    mWord = cursor.getString(cursor.getColumnIndex("word"));
                    if (mWord == null) {
                        mWord = "";
                    }
                    mPhonetic = cursor.getString(cursor.getColumnIndex("phonetic"));
                    if (mPhonetic == null) {
                        mPhonetic = "";
                    }
                    mMean = cursor.getString(cursor.getColumnIndex("mean"));
                    mFavorite = cursor.getInt(cursor.getColumnIndex("favorite"));

                    mJaen = new Jaen(mID, mPhonetic, mWord, mFavorite, mMean);
                    jaenArrayList.add(mJaen);

                    cursor.moveToNext();
                }
                cursor.close();

            } catch (Exception e) {
                Track.sendTrackerAction("Error", String.valueOf(e));
            }
        }
        return jaenArrayList;
    }

    // news_readed
    public void setNewsReaded(String id) {
        try {
            SQLiteDatabase db = getReadableDatabase();
            ContentValues cv = new ContentValues();
            cv.put("id", id);
            db.insert("news_readed", null, cv);
            int count = getCountNewsReaded(db);
            if (count == 1000) {
                deleteNewsReaded();
            }
        } catch (SQLiteException e) {

        }
    }

    public boolean checkNewsReaded(String id) {
        try {
            SQLiteDatabase db = getReadableDatabase();
            String query = "select * from news_readed where id = '" + id + "'";
            Cursor cursor = db.rawQuery(query, null);
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                cursor.close();
                return true;
            }
            cursor.close();
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public int getCountNewsReaded(SQLiteDatabase db) {
        try {
            String countQuery = "SELECT  * FROM news_readed";
            Cursor cursor = db.rawQuery(countQuery, null);
            int count = cursor.getCount();
            cursor.close();
            return count;
        } catch (SQLiteException e) {
            return 0;
        }
    }

    public void deleteNewsReaded() {
        try {
            SQLiteDatabase db = getReadableDatabase();
            String query = "delete from news_readed where id IN \n" +
                    "(SELECT id from news_readed order by date desc limit 10)";
            db.execSQL(query);
        } catch (SQLiteException e) {

        }
    }

    public void createNewsReaded() {
        try {
            SQLiteDatabase db = getReadableDatabase();
            String query = "CREATE  TABLE  IF NOT EXISTS \"main\".\"news_readed\" (\"id\" TEXT PRIMARY KEY  NOT NULL , \"date\" DATETIME DEFAULT CURRENT_DATE)";
            db.execSQL(query);
        } catch (SQLiteException e) {

        }
    }
}