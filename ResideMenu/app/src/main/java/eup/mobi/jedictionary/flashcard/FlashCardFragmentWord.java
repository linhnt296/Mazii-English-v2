package eup.mobi.jedictionary.flashcard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.residemenuactivity.FlashCardWordActivity;

/**
 * Created by chenupt@gmail.com on 2015/1/31.
 * Description TODO
 */
public class FlashCardFragmentWord extends Fragment {
    boolean mShowingBack = false;
    FrameLayout frameLayout;
    int mPostion, pos, num;
    String word;
    FragmentManager fragmentManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.flashcard_fragment_w, container, false);
        fragmentManager = getChildFragmentManager();

        Bundle bundle = getArguments();
        if (FlashCardWordActivity.STATUS_FC == 2) {
            mPostion = bundle.getInt("mposition");
        }
        pos = bundle.getInt("position") + 1;
        num = bundle.getInt("numpage");

        if (savedInstanceState == null) {
            FlashCardFragmentWord1 fragment1 = new FlashCardFragmentWord1();
            Bundle bundle1 = new Bundle();
            if (FlashCardWordActivity.STATUS_FC == 2) {
                bundle1.putInt("mposition", mPostion);
            }
            bundle1.putInt("position", pos);
            bundle1.putInt("numpage", num);

            fragment1.setArguments(bundle1);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_flashcard, fragment1);
            fragmentTransaction.commit();
        }
        frameLayout = (FrameLayout) rootView.findViewById(R.id.frame_flashcard);
        onFrameClick();
//        rootView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                flipCard();
//            }
//        });
        return rootView;
    }

    private void onFrameClick() {
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
            }
        });
    }

    private void flipCard() {
        if (mShowingBack) {
            FlashCardFragmentWord1 fragment1 = new FlashCardFragmentWord1();
            Bundle bundle1 = new Bundle();
            if (FlashCardWordActivity.STATUS_FC == 2) {
                bundle1.putInt("mposition", mPostion);
            }
            bundle1.putInt("position", pos);
            bundle1.putInt("numpage", num);
            fragment1.setArguments(bundle1);

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.from_middle, R.anim.to_middle);
            fragmentTransaction.replace(R.id.frame_flashcard, fragment1);
            fragmentTransaction.commit();
            mShowingBack = false;
        } else {
            FlashCardFragmentWord2 fragment2 = new FlashCardFragmentWord2();
            //bundle
            Bundle bundle2 = new Bundle();
            if (FlashCardWordActivity.STATUS_FC==2) {
                bundle2.putInt("mposition", mPostion);
            }else {
                bundle2.putInt("position", pos);

            }
            fragment2.setArguments(bundle2);

            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.from_middle, R.anim.to_middle);
            fragmentTransaction.replace(R.id.frame_flashcard, fragment2);
            fragmentTransaction.commit();
            mShowingBack = true;
        }
    }
}