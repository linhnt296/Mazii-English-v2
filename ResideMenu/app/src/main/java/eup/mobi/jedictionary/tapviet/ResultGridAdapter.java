package eup.mobi.jedictionary.tapviet;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import eup.mobi.jedictionary.R;

import java.util.ArrayList;
/**
 * Created by thanh on 10/17/2015.
 */
public class ResultGridAdapter extends BaseAdapter {
    ArrayList<String> result;
    Context context;
    LayoutInflater inflater;

    public ResultGridAdapter(ArrayList<String> result, Context context) {
        this.result = result;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        return result;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.result_grid_layout, null);
            holder.textView = (TextView)convertView.findViewById(R.id.result_word);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String string = result.get(position);
        holder.textView.setText(string);
        holder.textView.setTextSize(23f);
        return convertView;
    }
    static class ViewHolder{
        TextView textView;
    }
}
