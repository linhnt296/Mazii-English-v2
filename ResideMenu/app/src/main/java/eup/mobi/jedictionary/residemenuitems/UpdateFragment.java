package eup.mobi.jedictionary.residemenuitems;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.google.NetWork;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by thanh on 11/23/2015.
 */
public class UpdateFragment extends Fragment {
    private MenuActivity activity;
    private Button btnUpdate, btnInsertCode;
    private ImageButton btnResideMenu;
    private String priceText;
    AdView adView = null;

    public String getPriceText() {
        return priceText;
    }

    public void setPriceText(String priceText) {
        this.priceText = priceText;
    }

    @SuppressLint("ValidFragment")
    public UpdateFragment(MenuActivity activity, String priceText) {
        this.activity = activity;
        this.priceText = priceText;
    }

    public UpdateFragment() {
    }

    @Nullable

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.update_fragment, null);
        btnUpdate = (Button) view.findViewById(R.id.buy_button);
        btnResideMenu = (ImageButton) view.findViewById(R.id.title_bar_left_menu);
        btnInsertCode = (Button) view.findViewById(R.id.send_button);
        // resideMenu = ((MenuActivity) getActivity()).getResideMenu();

        if (MenuActivity.sharedPreferences.getBoolean("isPremium", false)) {
            btnUpdate.setEnabled(false);
            btnInsertCode.setEnabled(false);
            btnUpdate.setText(getString(R.string.bt_update_1));
            btnInsertCode.setText(getString(R.string.bt_update_1));
        } else {
            if (priceText == "") {
                btnUpdate.setText(getString(R.string.bt_update_2));
                btnUpdate.setEnabled(false);
            } else {
                btnUpdate.setText(priceText);
            }
        }

        btnResideMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (MenuActivity.drawer != null && !MenuActivity.drawer.isDrawerOpen(GravityCompat.START))
                        MenuActivity.drawer.openDrawer(GravityCompat.START);
                } catch (Exception e) {
                }
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    activity.onUpgradeAppButtonClicked();
                } catch (Exception e) {
                }
            }
        });
        btnInsertCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetWork.isNetWork(getActivity())) {
                    UpgradeDialog upgradeDialog = new UpgradeDialog(getContext());
                    upgradeDialog.show();
                    Window window = upgradeDialog.getWindow();
                    window.setLayout(AbsListView.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.WRAP_CONTENT);
                }
            }
        });
        boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        String banner = MenuActivity.sharedPreferences.getString("idBanner", "ca-app-pub-6030218925242749/8137364314");
        float probBanner = MenuActivity.sharedPreferences.getFloat("probBanner", (float) 0.9);

//        adView = Adsmod.fragmentBanner(view, banner, probBanner, isPremium);
        if (!NetWork.isNetWork(getActivity())) {
            btnUpdate.setEnabled(false);
            btnInsertCode.setEnabled(false);
        }
        return view;
    }

    private class UpgradeDialog extends Dialog {
        public UpgradeDialog(Context context) {
            super(context);
        }

        private int isCorrect = 0;
        private EditText editCode;
        private Button btnKichHoat;
        private String query;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

            setContentView(R.layout.upgrade_dialog);
            editCode = (EditText) findViewById(R.id.edit_code);
            editCode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (editCode.length() == 0) {
                        isCorrect = 0;
                    } else if (editCode.length() > 0 && editCode.length() < 12) {
                        isCorrect = 1;
                    } else if (editCode.length() == 12) {
                        isCorrect = 2;
                        query = s.toString();
                    } else {
                        isCorrect = -1;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            editCode.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        // Perform action on key press
                        if (isCorrect == 0) {
                            Toast.makeText(getContext(), getString(R.string.update_error_1), Toast.LENGTH_SHORT).show();
                        } else if (isCorrect == 1) {
                            Toast.makeText(getContext(), getString(R.string.update_error_2), Toast.LENGTH_SHORT).show();
                        } else if (isCorrect == 2) {
                            submidCode();
                        }

                        return true;
                    }
                    return false;
                }
            });

            btnKichHoat = (Button) findViewById(R.id.kichhoat_button);
            btnKichHoat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isCorrect == 0) {
                        Toast.makeText(getContext(), getString(R.string.update_error_1), Toast.LENGTH_SHORT).show();
                    } else if (isCorrect == 1) {
                        Toast.makeText(getContext(), getString(R.string.update_error_2), Toast.LENGTH_SHORT).show();
                    } else if (isCorrect == 2) {
                        submidCode();
                    }
                }
            });
        }

        public void submidCode() {
            if (query != null) {
                String data = "keyactive=" + query + "&" + "userkey=" + Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                SubmitTask submitTask = new SubmitTask();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    submitTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data.toString());
                } else {
                    submitTask.execute(data.toString());
                }

            }
        }

        public class SubmitTask extends AsyncTask<String, String, String> {
            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (s != null) {
                    if (s.equals("{\"flag\":0}\n")) {
                        MenuActivity.sharedPreferences.edit().putBoolean("isPremium", true).commit();
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(getString(R.string.submit_success_title))
                                .setMessage(getString(R.string.submit_success_message))
                                .setCancelable(true)
                                .setPositiveButton(getString(R.string.main_alert_confirm), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();

                        btnUpdate.setText(getString(R.string.bt_update_1));
                        btnUpdate.setEnabled(false);
                        btnInsertCode.setText(getString(R.string.bt_update_1));
                        btnInsertCode.setEnabled(false);
                        if (adView != null) {
                            adView.setVisibility(View.GONE);
                        }

                        UpgradeDialog.this.dismiss();

                    } else if (s.equals("{\"flag\":-1}\n")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(getString(R.string.submit_faile_title))
                                .setMessage(getString(R.string.submit_faile_message))
                                .setCancelable(true)
                                .setPositiveButton(getString(R.string.main_alert_confirm), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else if (s.equals("{\"flag\":-2}\n")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(getString(R.string.submit_faile_title))
                                .setMessage(getString(R.string.submit_faile_message2))
                                .setCancelable(true)
                                .setPositiveButton(getString(R.string.main_alert_confirm), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(getString(R.string.submit_faile_title))
                                .setMessage(getString(R.string.submit_faile_message3))
                                .setCancelable(true)
                                .setPositiveButton(getString(R.string.main_alert_confirm), new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }
                }
            }

            @Override
            protected String doInBackground(String... params) {
                String json = params[0];
                String result = sendRequest(json);
                if (result != null) {
                    return result;
                } else {
                    return null;
                }
            }

            public String sendRequest(String json) {
                String address = getString(R.string.url_purchase);
                StringBuilder sb = new StringBuilder();
                OutputStreamWriter printlnOut;
                try {
                    URL url = new URL(address);
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
                    urlConnection.setRequestProperty("Accept", "application/json");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.connect();
                    printlnOut = new OutputStreamWriter(urlConnection.getOutputStream());
                    printlnOut.write(json.toString());
                    printlnOut.flush();
                    printlnOut.close();
                    int httpResult = urlConnection.getResponseCode();
                    if (httpResult == HttpURLConnection.HTTP_OK) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();
                        return sb.toString();
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }
    }

    public MenuActivity getMenuActivity() {
        return activity;
    }

    public void setMenuActivity(MenuActivity activity) {
        this.activity = activity;
    }
}
