package eup.mobi.jedictionary.database;

/**
 * Created by nguye on 8/21/2015.
 */
public class Jaen {
    private int mId;
    private String mWord;
    private String mPhonetic;
    private int mFavorite;

    public Jaen(String mPhonetic, String mWord, int mFavorite, String mMean) {
        this.mPhonetic = mPhonetic;
        this.mWord = mWord;
        this.mFavorite = mFavorite;
        this.mMean = mMean;
    }

    public Jaen(int mID, String mPhonetic, String mWord, int mFavorite, String mMean) {
        this.mPhonetic = mPhonetic;
        this.mWord = mWord;
        this.mFavorite = mFavorite;
        this.mMean = mMean;
        this.mId = mID;
    }

    public Jaen(int mID, String mWord, int mFavorite, String mMean) {
        this.mWord = mWord;
        this.mFavorite = mFavorite;
        this.mMean = mMean;
        this.mId = mID;
    }

    public int getmFavorite() {
        return mFavorite;
    }

    public void setmFavorite(int mFavorite) {
        this.mFavorite = mFavorite;
    }

    public Jaen(String mWord, String mPhonetic, String mMean) {
        this.mWord = mWord;
        this.mPhonetic = mPhonetic;
        this.mMean = mMean;
    }

    public String getmPhonetic() {

        return mPhonetic;
    }

    public void setmPhonetic(String mPhonetic) {
        this.mPhonetic = mPhonetic;
    }

    public Jaen(String mWord, String mMean) {
        this.mWord = mWord;
        this.mMean = mMean;
    }

    private String mMean;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmWord() {
        return mWord;
    }

    public void setmWord(String mWord) {
        this.mWord = mWord;
    }

    public String getmMean() {
        return mMean;
    }

    public void setmMean(String mMean) {
        this.mMean = mMean;
    }
}
