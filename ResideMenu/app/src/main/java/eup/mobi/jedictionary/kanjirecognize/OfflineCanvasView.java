package eup.mobi.jedictionary.kanjirecognize;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import eup.mobi.jedictionary.R;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Stack;

/**
 * Created by nguye on 9/26/2015.
 */
public class OfflineCanvasView extends View {

    public int width;
    public int height;
    private Path mPath;
    private Stack<Path> mPathStack, mRedoStack;
    private Stack<InputStroke> mRedoStrokeStack;
    Context context;
    private Paint mPaint;
    private float mX, mY;
    private static final float TOLERANCE = 5;
    private float startX, startY, lastX, lastY;
    public AddingStrokeListener mStrokeAddListener;
    public DataLoadedListener mDataLoadedListener;
    private ArrayList<InputStroke> mStrokeArrayList;
    private ArrayList<ArrayList<ArrayList<Integer>>> mInk;

    private KanjiMatch[] mKanjiMatches;

    private KanjiList mKanjiList = null;

    InputStream databaseStream;
    private boolean isOpened = false;

    public OfflineCanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initCanvas();
        openRecogFile();
        setFocusable(false);
    }

    public void initCanvas(){
        // set a new Path
        mPath = new Path();
        mPathStack = new Stack<>();
        mRedoStack = new Stack<>();
        mRedoStrokeStack = new Stack<>();

        mStrokeArrayList = new ArrayList<>();
        // we set a new Paint with the desired attributes
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.hwCanvasWidth));
    }

    public void openRecogFile(){
        AsyncTask<Void, Void, Boolean> asyncTask = new AsyncTask <Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void[] params) {
                try {
                    databaseStream = context.getAssets().open("strokes-20100823.xml");
                    mKanjiList = new KanjiList(databaseStream);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }
                return true;
            }
            @Override
            protected void onPostExecute (Boolean result){
                super.onPostExecute(result);
                isOpened = result;
                mDataLoadedListener.onDataLoaded();

            }
        };
        asyncTask.execute();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the mPath with the mPaint on the canvas when onDraw
        for (Path p : mPathStack)
            canvas.drawPath(p, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isOpened) {
            return false;
        }
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                startTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                moveTouch(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_UP:
                upTouch(x, y);
                invalidate();
                break;
        }
        return true;
    }


    /********On event listener*********************/
    protected void startTouch(float x, float y) {
        mPath = new Path();
        mPathStack.push(mPath);
        mPath.moveTo(x, y);
        mX = x;
        mY = y;
        setStartCoordinate(x, y);
    }

    private void moveTouch(float x, float y) {
        float dx = Math.abs(x - mX);
        float dy = Math.abs(y - mY);
        if (dx >= TOLERANCE || dy >= TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
            mX = x;
            mY = y;
        }
    }

    private void upTouch(float xEvent, float yEvent) {
        mPath.lineTo(mX, mY);
        setEndCoordinate(xEvent, yEvent);

        InputStroke stroke = new InputStroke(startX, startY, lastX, lastY);
        mStrokeArrayList.add(stroke);
        mRedoStrokeStack.clear();
        mRedoStack.clear();
        mStrokeAddListener.onStrokeAddListener();
    }



    /********Access canvas*********************/

    public void clearCanvas() {
        mPathStack.clear();
        mStrokeArrayList.clear();
        mRedoStrokeStack.clear();
        mRedoStack.clear();
        if (mInk != null) {
            mInk.clear();
        }
        mStrokeAddListener.onStrokeAddListener();
        invalidate();
    }

    public void undoCanvas() {
        if (!mPathStack.empty()) {
            mRedoStack.push(mPathStack.pop());
            if (!mStrokeArrayList.isEmpty()){
                int index = mStrokeArrayList.size() - 1;
                mRedoStrokeStack.push(mStrokeArrayList.get(index));
                mStrokeArrayList.remove(index);
            }
        }
        mStrokeAddListener.onStrokeAddListener();
        invalidate();
    }

    public void redoCanvas() {
        if (!mRedoStack.empty()) {
            mPathStack.push(mRedoStack.pop());
            if(!mRedoStrokeStack.empty()){
                mStrokeArrayList.add(mRedoStrokeStack.pop());
            }
        }
        mStrokeAddListener.onStrokeAddListener();
        invalidate();
    }

    /************Get infor *****************/


    public int getStroke(){
        if(mStrokeArrayList !=null)
            return mStrokeArrayList.size();
        return 0;
    }

    public ArrayList<String> getKanjiResultList() {
        InputStroke[] inputStrokes = new InputStroke[mStrokeArrayList.size()];
        for (int i = 0; i < mStrokeArrayList.size(); i++) {
            inputStrokes[i] = mStrokeArrayList.get(i);
        }
        onRecognize(inputStrokes, KanjiInfo.MatchAlgorithm.STRICT);

        ArrayList<String> mKanjiResultList = new ArrayList<String>(); //new String[mKanjiMatches.length];
        for (int i = 0; i < mKanjiMatches.length; i++) {
            mKanjiResultList.add(mKanjiMatches[i].getKanji().getKanji());
        }
        invalidate();
        return mKanjiResultList;
    }




    /************Models *****************/

    public void setStartCoordinate(float startX, float startY) {
        this.startX = startX;
        this.startY = startY;
    }
    public void setEndCoordinate(float endX, float endY) {
        this.lastX = endX;
        this.lastY = endY;
    }

    public void onRecognize(InputStroke[] strokes, KanjiInfo.MatchAlgorithm algo) {
        KanjiInfo mPotentialKanji = new KanjiInfo("?");
        for (InputStroke stroke : strokes) {
            mPotentialKanji.addStroke(stroke);
        }
        mPotentialKanji.finish();

        mKanjiMatches = mKanjiList.getTopMatches(mPotentialKanji, algo, null);
    }

}