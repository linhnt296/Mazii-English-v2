package eup.mobi.jedictionary.docbao;

import java.util.List;

/**
 * Created by nguye on 9/15/2015.
 */
public class JRootObj {
    public int status ;
    public String message ;
    public List<JResult> JResults;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<JResult> getJResults() {
        return JResults;
    }

    public void setJResults(List<JResult> JResults) {
        this.JResults = JResults;
    }

    public JRootObj(int status, String message, List<JResult> JResults) {
        this.status = status;
        this.message = message;
        this.JResults = JResults;
    }
}
