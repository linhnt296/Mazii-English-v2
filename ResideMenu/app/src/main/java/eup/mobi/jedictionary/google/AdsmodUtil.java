package eup.mobi.jedictionary.google;

import android.app.Activity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import eup.mobi.jedictionary.R;
import eup.mobi.jedictionary.residemenuactivity.MenuActivity;

import java.util.Random;

/**
 * Created by LinhNT on 1/22/2016.
 */
public class AdsmodUtil {
    private InterstitialAd mInterstitialAd;
    private Activity _activity;
    private Float probInter;
    private String idInter;
    private long adpress, intervalAdsInter;

    public void createFullAds(Activity activity) {

        idInter = MenuActivity.sharedPreferences.getString("idInter", "ca-app-pub-6030218925242749/9614097518");
        probInter = MenuActivity.sharedPreferences.getFloat("probInter", (float) 0.9);
        adpress = MenuActivity.sharedPreferences.getLong("probAds", 0);
        intervalAdsInter = MenuActivity.sharedPreferences.getLong("probInterval", 0);

        mInterstitialAd = new InterstitialAd(activity);
        _activity = activity;
        mInterstitialAd.setAdUnitId(idInter);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }

            @Override
            public void onAdLeftApplication() {
                super.onAdLeftApplication();
                MenuActivity.sharedPreferences.edit().putLong("time", System.currentTimeMillis()).commit();
                Track.sendTrackerAction("Click Ads", "true");
            }
        });
        requestNewInterstitial();
    }

    private boolean adShowAble() {

        // check premium
        boolean isPremium = MenuActivity.sharedPreferences.getBoolean("isPremium", false);
        if (isPremium) {
            return false;
        }

        // check ads clicked
        long lastTimeClickedAds = MenuActivity.sharedPreferences.getLong("time", 0);
        long currentTime = System.currentTimeMillis();
        if (currentTime < (lastTimeClickedAds + adpress)) {
            return false;
        }

        return true;
    }

    public void showIntervalAds() {
        if (mInterstitialAd == null)
            return;
        Random ran = new Random();
        if (adShowAble() && probInter >= ran.nextFloat()) {
            long currentTime = System.currentTimeMillis();
            long lastTimeShowAdsInter = MenuActivity.sharedPreferences.getLong("lastTimeShowAdsInter", 0);
            if (currentTime > (lastTimeShowAdsInter + intervalAdsInter) && mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                MenuActivity.sharedPreferences.edit().putLong("lastTimeShowAdsInter", System.currentTimeMillis()).commit();
            }
        }
    }
    public void showTest(){
        mInterstitialAd.show();
    }

    //ads
    private void requestNewInterstitial() {
        if (mInterstitialAd == null)
            return;
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(_activity.getString(R.string.test_device))
                .build();
        mInterstitialAd.loadAd(adRequest);
    }
}
