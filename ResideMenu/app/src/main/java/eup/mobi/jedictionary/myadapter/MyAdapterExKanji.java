package eup.mobi.jedictionary.myadapter;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import eup.mobi.jedictionary.R;

import eup.mobi.jedictionary.database.Jaen;
import eup.mobi.jedictionary.database.MyDatabase;
import eup.mobi.jedictionary.module.KanjiExampleObj;
import eup.mobi.jedictionary.module.Language;
import eup.mobi.jedictionary.residemenuactivity.TraCuuTuActivity;

import java.util.ArrayList;


/**
 * Created by nguye on 9/15/2015.
 */
public class MyAdapterExKanji extends
        ArrayAdapter<KanjiExampleObj> {
    Activity context = null;
    ArrayList<KanjiExampleObj> myArray = null;
    int layoutId;
    LayoutInflater inflater;

    public MyAdapterExKanji(Activity context,
                            int layoutId,
                            ArrayList<KanjiExampleObj> arr) {

        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.myArray = arr;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * hàm dùng để custom layout, ta phải override lại hàm này
     * từ MainActivity truyền vào
     *
     * @param position     : là vị trí của phần tử trong danh sách nhân viên
     * @param convertView: convertView, dùng nó để xử lý Item
     * @param parent       : Danh sách nhân viên truyền từ Main
     * @return View: trả về chính convertView
     */
    public View getView(int position, View convertView,
                        ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(layoutId, parent, false);
            holder = new ViewHolder();
            holder.tv_word = (TextView) convertView.findViewById(R.id.tv_word);
            holder.tv_phonetic = (TextView) convertView.findViewById(R.id.tv_phonetic);
            holder.tv_mean = (TextView) convertView.findViewById(R.id.tv_mean);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final KanjiExampleObj jResult = myArray.get(position);
        if (!jResult.getW().equals(""))
            holder.tv_word.setText(jResult.getW());
        if (!jResult.getP().equals("xxx。") && !jResult.getP().equals("xxx") && !Language.isVietnamese(jResult.getP()))
            holder.tv_phonetic.setText("「" + jResult.getP() + "」");
        if (jResult.getM() != null)
            holder.tv_mean.setText(jResult.getM());

        final String finalKanji = jResult.getW();
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new fvExamKanjiSynctask().execute(finalKanji);
            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView tv_word, tv_phonetic, tv_miniKanji, tv_mean;
    }

    private class fvExamKanjiSynctask extends AsyncTask<String, Void, ArrayList<Jaen>> {
        @Override
        protected ArrayList<Jaen> doInBackground(String... params) {
            MyDatabase db = new MyDatabase(getContext());
            return db.getJaen(params[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<Jaen> javis) {
            super.onPostExecute(javis);
            if (javis != null && javis.size() > 0) {
                String vMean, vTitle, mPhonetic;
                int wID, mFavorite, mSeq;
                Intent myIntent = new Intent(getContext(), TraCuuTuActivity.class);
                //Khai báo Bundle
                Bundle bundle = new Bundle();
                wID = javis.get(0).getmId();
                vMean = javis.get(0).getmMean();
                vTitle = javis.get(0).getmWord();

                mFavorite = javis.get(0).getmFavorite();
                mPhonetic = javis.get(0).getmPhonetic();
                //đưa dữ liệu riêng lẻ vào Bundle
                bundle.putString("vMean", vMean);
                bundle.putString("vTitle", vTitle);
                bundle.putInt("mFavorite", mFavorite);
                bundle.putString("verbPhonetic", mPhonetic);
                if (mPhonetic != null) {
                    bundle.putString("vPhonetic", "「" + mPhonetic + "」");
                }
                bundle.putInt("wID", wID);
                //Đưa Bundle vào Intent
                myIntent.putExtra("TraCuuFragment", bundle);
                //Mở Activity ResultActivity
                getContext().startActivity(myIntent);
            }
        }
    }
}