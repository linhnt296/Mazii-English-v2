package eup.mobi.jedictionary.database;

/**
 * Created by nguye on 10/12/2015.
 */
public class synsets {
    String pos;
    String entry;

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }

    public synsets(String pos, String entry) {
        this.pos = pos;
        this.entry = entry;
    }
}
