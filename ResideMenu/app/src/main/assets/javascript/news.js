<script type="text/javascript">
function enableFurigana() {
	clearStateRuby();
    document.getElementById("furigana-config").innerHTML = "";
}

function disableFurigana() {
	clearStateRuby();
    document.getElementById("furigana-config").innerHTML = "rt { display: none; }";
}

function sendMsgToNative(text) {
    console.log("send text: " + text);
   if (window.JSInterface) {
		window.JSInterface.onSelectedChanged(text);
	} else {
		console.log("Not found js interface");
	}
	sendMsgLate = null;
   }
  
 function clearStateRuby () {
	var rubyTags = document.getElementsByTagName("ruby");
    if (rubyTags == null) {
        return;
    }

    for (var i = 0; i < rubyTags.length; i++) {
		var rtTags = rubyTags[i].getElementsByTagName("rt");
		if (rtTags == null || rtTags.length == 0) {
			break;
		};

		var firstRt = rtTags[0];
		firstRt.style.display = "";
    } 
 } 
  
 function registerRubyEvent() {
    var rubyTags = document.getElementsByTagName("ruby");
    if (rubyTags == null) {
        return;
    }

    for (var i = 0; i < rubyTags.length; i++) {
        rubyTags[i].addEventListener("click", function (event) {
            var rbTag = event.target;
            var rtTags = rbTag.getElementsByTagName("rt");
            if (rtTags == null || rtTags.length == 0) {
                return;
            };

            var firstRt = rtTags[0];
			var curStyle = firstRt.currentStyle ? firstRt.currentStyle.display :
                          getComputedStyle(firstRt, null).display;
            if (curStyle == "block") {
                firstRt.style.display = "none";
            } else {
                firstRt.style.display = "block";
            }
        })
    }
}


var dictWins = document.getElementsByClassName("dicWin");
for (var i = 0; i < dictWins.length; i++) {
	dictWins[i].addEventListener("click", function (e) {
		var unders = e.currentTarget.getElementsByClassName("under");
		var content = "";
		for (var j = 0; j < unders.length; j++) {
			content += unders[j].innerText;
		}
		content = content.trim();
		if (content != '') {
			if (window.JSInterface) {
				window.JSInterface.onClickText(e.target.innerText);
			}
		}
	});
}

var lastSelectedText = "";
var sendMsgLate = null;
var getSelectedText = function () {
	var selectedText = window.getSelection().toString();
	if (selectedText == "") {
		lastSelectedText = "";
		return;
	}
	
	if (lastSelectedText != selectedText) {
		lastSelectedText = selectedText;
		
		if (sendMsgLate != null) {
			clearTimeout(sendMsgLate);
		}
		
		// wait 1 seconds then send event to native
		sendMsgLate = setTimeout(sendMsgToNative, 400, lastSelectedText);
		
	}
};

setInterval(getSelectedText, 1000);
registerRubyEvent();
 
 
</script>